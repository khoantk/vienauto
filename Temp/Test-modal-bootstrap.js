import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';

function ButtonModal(props) {
    return (
        <Fragment>
            <button type="button" className="btn btn-primary" data-toggle="modal" data-target={`#${props.id}`}>
                Launch demo modal
            </button>
        </Fragment>
    );
}

class BodyModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            age: ""
        }
    }

    HandleChange(event) {
        switch (event.target.name) {
            case "name":
                this.state.name = event.target.value;
                break;
            case "age":
                this.state.age = event.target.value;
                break;
        }

        this.setState(this.state);
    }

    ShowInfo() {
        console.log(this.state);
    }

    render() {
        return (
            <Fragment>
                <div className="form-group row">
                    <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Họ và tên</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" id="inputEmail3" name="name" placeholder="Họ và tên" value={this.state.name} onChange={this.HandleChange.bind(this)} />
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="inputEmail4" className="col-sm-2 col-form-label">Tuổi</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" id="inputEmail4" name="age" placeholder="Tuổi" value={this.state.age} onChange={this.HandleChange.bind(this)} />
                    </div>
                </div>
            </Fragment>
        );
    }

}


class Modal extends Component {
    constructor(props) {
        super(props);
    }

    HandleSubmit(event) {
        event.preventDefault();
        this.body.ShowInfo();
    }

    render() {
        return (
            <Fragment>
                <div className="modal fade" id={this.props.id} tabIndex="-1" role="dialog">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <form id="form" onSubmit={this.HandleSubmit.bind(this)}>
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <BodyModal ref={el => this.body = el} />
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" className="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

ReactDOM.render(
    <Fragment>
        <ButtonModal id={'hoang'}/>
        <Modal id={'hoang'} />
    </Fragment>

    , document.getElementById('bootstrap')
)