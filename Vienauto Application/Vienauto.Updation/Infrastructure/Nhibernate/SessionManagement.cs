﻿using NHibernate;
using Common.Core.Data.SqlLog;
using Common.Core.Nhibernate.Base;
using Common.Core.Nhibernate.Context;

namespace Vienauto.Updation.Infrastructure.Nhibernate
{
    public class SessionManagement : Repository
    {
        public SessionManagement()
        {
            Session = OpenDefaultSession();
        }

        protected override ISession Session { get; set; }

        protected override ISession OpenDefaultSession()
        {
            var sessionFactory = NhibernateConfiguration.GetDefaultSessionFactory();
#if DEBUG
            return sessionFactory.OpenSession(new SQLDebugOutput());
#else
            return sessionFactory.OpenSession();
#endif
        }

        protected override ISession OpenSession(string factoryKey = "")
        {
            var sessionFactory = NhibernateConfiguration.GetSessionFactory(factoryKey);
#if DEBUG
            return sessionFactory.OpenSession(new SQLDebugOutput());
#else
            return sessionFactory.OpenSession();
#endif
        }
    }
}
