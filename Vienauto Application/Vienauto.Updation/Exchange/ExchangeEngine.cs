﻿using System;
using Serilog;
using System.Linq;
using System.Threading;
using Common.Core.Extension;
using System.Threading.Tasks;
using Vienauto.Entity.Entities;
using System.Collections.Generic;
using Vienauto.Updation.Exchange.Provider;
using Vienauto.Updation.Infrastructure.Nhibernate;
using static Vienauto.Updation.Utilities.OutputLog;

namespace Vienauto.Updation.Exchange
{
    public class ExchangeEngine : SessionManagement
    {
        Timer schedular;
        private readonly ILogger _logger;

        public ExchangeEngine(ILogger logger)
        {
            _logger = logger.BindLogContext<ExchangeEngine>();
        }

        public void Start()
        {
            try
            {
                _logger.Debug("Exchange Engine".PrintStartLog());
                ScheduleUpdateExchangeRate();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, PrintErrorLog("start Exchange Engine"));
            }
        }

        public void Stop()
        {
            try
            {
                schedular.Dispose();
                _logger.Debug("Exchange Engine".PrintStartLog(true));
            }
            catch (Exception ex)
            {
                _logger.Error(ex, PrintErrorLog("stop Exchange Engine"));
            }
        }

        private void CallBackSchedule(object e)
        {
            _logger.Debug("Callback Schedule..");
            ScheduleUpdateExchangeRate();
        }

        private void ScheduleUpdateExchangeRate()
        {
            try
            {
                _logger.Debug("Scheduling..");
                schedular = new Timer(new TimerCallback(CallBackSchedule));

                var scheduledTime = DateTime.Parse(ConfigExtensions<string>.GetValue("ExchangeScheduleTime"));
                _logger.Information("Schedule time for today: {ScheduleTime}", scheduledTime);

                if (DateTime.Now >= scheduledTime)
                {
                    UpdateExchangeRate();
                    scheduledTime = scheduledTime.AddDays(1);
                    _logger.Information("Schedule time for next day: {ScheduleTimeUpdated}", scheduledTime);
                }

                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);
                schedular.Change(dueTime, Timeout.Infinite);

                _logger.Debug("Completed schedule.");
            }
            catch (Exception ex)
            {
                _logger.Error(ex, PrintErrorLog("schedule update exchange rate"));
            }
        }

        private void UpdateExchangeRate()
        {
            try
            {
                _logger.Debug("Updating Exchange Rate..");
                var exchangeRateCollection = FetchExchangeRateCollectionFromWebs();
                _logger.Information("Fetched Exchange Rate. Number of exchange rate after fetching: {NumberER}",
                                        exchangeRateCollection.Count);

                if (exchangeRateCollection.Count != 3)
                    _logger.Warning("May be error when fetching data. Exchange rate collection should have 3 values!");

                if (exchangeRateCollection != null && exchangeRateCollection.Count > 0)
                {
                    var maxExchangeRate = exchangeRateCollection.Max();
                    using (var session = Session)
                    {
                        _logger.Debug("Updating to database..");
                        //get default Id = 1
                        var setting = session.Get<Setting>(1);
                        if (setting != null)
                        {
                            setting.Exchange = maxExchangeRate;
                            session.Update(setting);
                            session.Flush();
                        }
                        _logger.Debug("Updated to database");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, PrintErrorLog("update exchange rate"));
            }
        }

        private List<float> FetchExchangeRateCollectionFromWebs()
        {
            var exchangeRateStore = new List<float>();
            try
            {
                _logger.Debug("Fetching Exchange Rate from webs..");

                Task.WaitAll(
                    Task.Factory.StartNew(() => FetchExchangeRateFromBIDVBank(exchangeRateStore), TaskCreationOptions.LongRunning),
                    Task.Factory.StartNew(() => FetchExchangeRateFromSacomBank(exchangeRateStore), TaskCreationOptions.LongRunning),
                    Task.Factory.StartNew(() => FetchExchangeRateFromVietcomBank(exchangeRateStore), TaskCreationOptions.LongRunning)
                );
            }
            catch (Exception ex)
            {
                _logger.Error(ex, PrintErrorLog("fetch exchange rate from webs"));
            }
            return exchangeRateStore;
        }

        private void FetchExchangeRateFromSacomBank(List<float> exchangeStore)
        {
            _logger.Debug("Fetching from Sacom Bank web..");
            LogRunningThreadId();

            var Sacombank = new BankProvider(_logger, new SacombankProvider());
            var exchangeRate = Sacombank.GetBankExchangeRate();

            _logger.Information("Exchange Rate for Sacom Bank: {ExchangeRate}", exchangeRate);

            if (exchangeRate > 0)
                exchangeStore.Add(exchangeRate);

            Thread.Sleep(3000);
        }

        private void FetchExchangeRateFromBIDVBank(List<float> exchangeStore)
        {
            _logger.Debug("Fetching from BIDV Bank web..");
            LogRunningThreadId();

            var BIDVBank = new BankProvider(_logger, new BIDVProvider());
            var exchangeRate = BIDVBank.GetBankExchangeRate();

            _logger.Information("Exchange Rate for BIDV Bank: {ExchangeRate}", exchangeRate);

            if (exchangeRate > 0)
                exchangeStore.Add(exchangeRate);

            Thread.Sleep(3000);
        }

        private void FetchExchangeRateFromVietcomBank(List<float> exchangeStore)
        {
            _logger.Debug("Fetching from VietcomBank web..");
            LogRunningThreadId();

            var vietcomBank = new BankProvider(_logger, new VietcomBankProvider());
            var exchangeRate = vietcomBank.GetBankExchangeRate();

            _logger.Information("Exchange Rate for VietcomBank: {ExchangeRate}", exchangeRate);

            if (exchangeRate > 0)
                exchangeStore.Add(exchangeRate);

            Thread.Sleep(3000);
        }

        private void LogRunningThreadId()
        {
            _logger.Information("Running ThreadId: {ThreadId}", Thread.CurrentThread.ManagedThreadId);
        }
    }
}
