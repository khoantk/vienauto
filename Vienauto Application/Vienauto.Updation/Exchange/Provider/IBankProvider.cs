﻿using Vienauto.Updation.Exchange.Enum;

namespace Vienauto.Updation.Exchange.Provider
{
    public interface IBankProvider
    {
        BankNameEnum BankName { get; }
        string BankURL { get; }
        float GetUSDExchangeRate();
    }
}
