﻿using HtmlAgilityPack;
using Vienauto.Updation.Exchange.Enum;

namespace Vienauto.Updation.Exchange.Provider
{
    public class VietcomBankProvider : IBankProvider
    {
        public BankNameEnum BankName { get { return BankNameEnum.Vietcombank; } }

        public string BankURL { get { return "http://www.vietcombank.com.vn/ExchangeRates"; } }

        public float GetUSDExchangeRate()
        {
            float usdExchangeRate = 0;

            HtmlWeb web = new HtmlWeb();
            var htmlDoc = web.Load(BankURL);

            var node = htmlDoc.DocumentNode.SelectNodes("//td[not(contains(@class, 'code'))]")[55].InnerText.Replace(".00", "").Replace(",", "");

            usdExchangeRate = float.Parse(node);

            return usdExchangeRate;
        }
    }
}
