﻿using HtmlAgilityPack;
using Vienauto.Updation.Exchange.Enum;

namespace Vienauto.Updation.Exchange.Provider
{
    public class BIDVProvider : IBankProvider
    {
        public BankNameEnum BankName { get { return BankNameEnum.BIDV; } }

        public string BankURL { get { return "http://bidv.com.vn/Ty-gia-ngoai-te.aspx"; } }

        public float GetUSDExchangeRate()
        {            
            float usdExchangeRate = 0;

            HtmlWeb web = new HtmlWeb();
            var htmlDoc = web.Load(BankURL);

            var node = htmlDoc.DocumentNode.SelectNodes("//td[@class='tygia_mua']")[0].InnerText.Trim().Replace(",00", "").Replace(".", "");

            usdExchangeRate = float.Parse(node);

            return usdExchangeRate;
        }
    }
}
