﻿using HtmlAgilityPack;
using Vienauto.Updation.Exchange.Enum;

namespace Vienauto.Updation.Exchange.Provider
{
    public class SacombankProvider : IBankProvider
    {
        public BankNameEnum BankName { get { return BankNameEnum.SacomBank; } }

        public string BankURL { get { return "https://www.sacombank.com.vn/company/Pages/ty-gia.aspx"; } }

        public float GetUSDExchangeRate()
        {
            float usdExchangeRate = 0;

            HtmlWeb web = new HtmlWeb();
            var htmlDoc = web.Load(BankURL);

            var node = htmlDoc.DocumentNode.SelectSingleNode("//td[@class='td-cell02']");

            usdExchangeRate = float.Parse(node.InnerText);

            return usdExchangeRate;
        }
    }
}
