﻿using System;
using Serilog;
using static Vienauto.Updation.Utilities.OutputLog;

namespace Vienauto.Updation.Exchange.Provider
{
    public class BankProvider
    {
        private readonly ILogger _logger;
        private readonly IBankProvider _provider;

        public BankProvider(ILogger logger, IBankProvider provider)
        {
            _provider = provider;
            _logger = logger.BindLogContextForBankProvider(_provider.BankName);
        }

        public float GetBankExchangeRate()
        {
            try
            {
                return _provider.GetUSDExchangeRate();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, PrintErrorLog($"find exchange rate on bank web page. Link: {_provider.BankURL}"));
                return 0;
            }
        }
    }
}
