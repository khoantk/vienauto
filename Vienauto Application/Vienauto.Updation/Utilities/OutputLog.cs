﻿using Serilog;
using System.Reflection;
using Vienauto.Updation.Exchange.Enum;
using Vienauto.Updation.Exchange.Provider;

namespace Vienauto.Updation.Utilities
{
    public static class OutputLog
    {
        public enum IntializationType
        {
            StartStop,
            Initialize
        }

        public static string PrintStartLog(this string intializeObject, bool isEnd = false)
        {
            return PrintLog(intializeObject, IntializationType.StartStop, isEnd);
        }

        public static string PrintInitializeLog(this string intializeObject, bool isEnd = false)
        {
            return PrintLog(intializeObject, IntializationType.Initialize, isEnd);
        }

        public static string PrintLog(this string intializeObject, IntializationType initializationType, bool isEnd)
        {
            var initializeLogMessage = string.Empty;
            if(initializationType == IntializationType.StartStop)
                initializeLogMessage = isEnd ? "STOPPED " : "STARTING ";

            if (initializationType == IntializationType.Initialize)
                initializeLogMessage = isEnd ? "INITIALIZED " : "INITIALIZING ";

            var outputLogMessage = initializeLogMessage + intializeObject;
            return outputLogMessage;
        }

        public static string PrintErrorLog(string message)
        {
            return "Error happens when " + message;
        }

        public static ILogger BindLogContext<T>(this ILogger logger) where T : class
        {
            logger = logger.ForContext("Source", $"{nameof(T)}.cs");
            return logger;
        }

        public static ILogger BindLogContextForBankProvider(this ILogger logger, BankNameEnum bankName)
        {
            string className = "";

            if (bankName == BankNameEnum.BIDV)
                className = nameof(BIDVProvider);

            if (bankName == BankNameEnum.SacomBank)
                className = nameof(SacombankProvider);

            if (bankName == BankNameEnum.Vietcombank)
                className = nameof(VietcomBankProvider);

            logger = logger.ForContext("Source", $"{className}.cs");
            return logger;
        }
    }
}
