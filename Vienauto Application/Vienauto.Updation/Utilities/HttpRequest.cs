﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Vienauto.Updation.Utilities
{
    public class HttpRequest
    {
        public string Post(string url, string values)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
                        
            var data = Encoding.UTF8.GetBytes(values);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return responseString.Trim();
        }
    }
}
