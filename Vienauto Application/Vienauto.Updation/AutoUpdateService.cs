﻿using System;
using Serilog;
using NHibernate;
using System.Diagnostics;
using Vienauto.Entity.Maps;
using System.ServiceProcess;
using Common.Core.Extension;
using Vienauto.Updation.Exchange;
using Common.Core.Nhibernate.Context;
using static Vienauto.Updation.Utilities.OutputLog;

namespace Vienauto.Updation
{
    public partial class AutoUpdateService : ServiceBase
    {
        private readonly ILogger _logger;

        ExchangeEngine exchangeRateEngine;

        bool enableExchangeEngine;

        public AutoUpdateService()
        {
            _logger = new LoggerConfiguration()
                            .ReadFrom.AppSettings()
                            .CreateLogger()
                            .BindLogContext<AutoUpdateService>();

            InitializeComponent();
            InitializeNHibernate();
            InitializeEngine();
            InitializeEnableEngine();
        }

        static void Main()
        {
            var canDebug = ConfigExtensions<bool>.GetValue("Debug");
            if (canDebug)
            {
                AutoUpdateService service = new AutoUpdateService();
                service.OnStart(null);
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new AutoUpdateService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                _logger.Verbose("Auto Update Service starts");

                if (enableExchangeEngine)
                    exchangeRateEngine.Start();

            }
            catch (Exception ex)
            {
                _logger.Error(ex, PrintErrorLog("start service"));
                EventLog.WriteEntry(ex.ToString());
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (!enableExchangeEngine)
                    exchangeRateEngine.Stop();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, PrintErrorLog("stop service"));
                EventLog.WriteEntry(ex.ToString());
            }
        }

        private void InitializeEngine()
        {
            _logger.Debug("Engines".PrintInitializeLog());

            exchangeRateEngine = new ExchangeEngine(_logger);

            _logger.Debug("Engines".PrintInitializeLog(true));
        }

        private void InitializeEnableEngine()
        {
            _logger.Debug("EnableEngines".PrintInitializeLog());

            enableExchangeEngine = ConfigExtensions<bool>.GetValue("EnableExchangeEngine");
            _logger.Information("ENABLED Exchange Engine");

            _logger.Debug("EnableEngines".PrintInitializeLog(true));
        }

        private void InitializeNHibernate()
        {
            _logger.Debug("NhibernateInitialization".PrintInitializeLog());

            var configFile = ConfigExtensions<string>.GetValue("NhibernateConfig");
            var mappingAssembly = typeof(ManufacturerMap).Assembly;
            NhibernateConfiguration.Init(configFile, mappingAssembly);

            _logger.Debug("NhibernateInitialization".PrintInitializeLog(true));
        }
    }
}
