﻿using Common.Core.Extension;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Vienauto.Test
{
    [TestClass]
    public class MethodTests
    {
        [TestMethod]
        public void TestInteger()
        {
            var phoneLength = IntegerExtensions.GetLength(0938632061);
            Assert.AreEqual(10, phoneLength);
        }
    }
}
