﻿using System.Web.Mvc;
using Castle.Windsor;
using Vienauto.Business;
using Common.Core.Dependency;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;

namespace Vienauto.Mobile.Test
{
    public class TestInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                                      .BasedOn<IController>()
                                      .LifestyleScoped<WindsorHybridScopeAccessor>());

            container.Register(Classes.FromAssemblyNamed("Vienauto.Business")
                                      .BasedOn<BaseService>()
                                      .WithServiceAllInterfaces()
                                      .LifestyleTransient());
        }
    }
}
