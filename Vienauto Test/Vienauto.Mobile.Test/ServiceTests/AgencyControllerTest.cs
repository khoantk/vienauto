using System;
using Vienauto.Interface.Dto;
using Vienauto.Business.AsyncServices;
using Vienauto.Interface.AsyncServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common.Core.Data.Transformation;

namespace Vienauto.Mobile.Test.ServiceTests
{
    [TestClass]
    public class AgencyControllerTest : BaseControllerTest
    {
        private readonly IAgencyAsyncService _agencyAsyncService;

        public AgencyControllerTest() : base() { }

        public AgencyControllerTest(IAgencyAsyncService agencyAsyncService)
        {
            _agencyAsyncService = agencyAsyncService;
        }

        [TestMethod]
        public void CanReviewAgent()
        {
            var agentReviews = new AgencyDetailReviewDto()
            {
                UserId = 91302,                
                Name = "lehoangnha",                
                Email = "lehoangnha@gmail.com",
                Phone = "0123",
                AgentId = 196,
                ManufacturerId = 152,
                Overall = 3.5,
                AgentName = "Auto Car Hoàng Văn Thụ",
                ProvinceName = "TP.HCM",
                ManufacturerName = "Acura",
                DatetimeReview = DateTime.Now,
                TitleReview = "Agent Review",
                ContentReview = "Good",
                CustomerService = 3,
                BuyingProcess = 3,
                QualityOfRepair = 3,
                Facilities = 3,
                AverageReviews = 3,
                TotalReviews = 4
            };

            var result = _agencyAsyncService.AddAgencyReviewAsync(agentReviews);
        }
                
    }
        
}
