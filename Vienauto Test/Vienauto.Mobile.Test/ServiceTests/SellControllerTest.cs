﻿using System;
using System.Threading.Tasks;
using Common.Core.Data.Paging;
using System.Collections.Generic;
using Vienauto.Business.AsyncServices;
using Vienauto.Interface.AsyncServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Vienauto.Mobile.Test.ServiceTests
{
    [TestClass]
    public class SellControllerTest : BaseControllerTest
    {
        private readonly ISellAsyncService _sellAsyncService;

        public SellControllerTest()
        {
            _sellAsyncService = new SellAsyncService();
        }

        [TestMethod]
        public async Task CanGetCarForSellPaging()
        {
            var pagingOption = new PagingOption
            {
                PageIndex = 1,
                PageSize = 10,
                SortName = "DatePost",
                OrderName = "DESC",
                Action = PagingAction.Current
            };

            var sellPaging = new PagingRequest<Dictionary<string, object>>(pagingOption);
            sellPaging.FilterConditions = new Dictionary<string, object>
            {
                //["Manufacturer"] = new int[] { 94, 98, 152 },
                ["Price"] = new float[] { 50000, 100000 }
            };

            var result = await _sellAsyncService.GetCarsWithPagingAsync(sellPaging);
        }

        [TestMethod]
        public async Task CanGetDetailCarForSell()
        {
            var productId = 12494;
            var result = await _sellAsyncService.GetDetailInfoAndUpdateViewTimesAsync(productId);
            var validResult = Assert.Equals(result.HasErrors, false);
            if (validResult)
            {
                Assert.IsNotNull(result.Target);
                Assert.AreEqual(productId, result.Target.ProductId);
                Assert.AreEqual("TOY12494", result.Target.ProductCode);
                Assert.AreEqual("Sport  3.5L AT AWD ", result.Target.CarName);
                Assert.AreEqual(DateTime.Parse("2012-06-15 09:46:48.383"), result.Target.DatePost);
                Assert.AreEqual("Toyota Highlander SUV 2010 Sport  3.5L AT AWD", result.Target.ProductName);
            }
        }

        [TestMethod]
        public async Task GetSellDetailOverview()
        {
            var productId = 13247;
            var result = await _sellAsyncService.GetSellDetailOverview(productId);
            var validResult = Assert.Equals(result.HasErrors, false);
            if (validResult)
            {
                Assert.IsNotNull(result.Target);                
            }
        }
                
        [TestMethod]
        public async Task GetSellDetailEngine()
        {
            var productId = 13247;
            var result = await _sellAsyncService.GetSellDetailEngine(productId);
            var validResult = Assert.Equals(result.HasErrors, false);
            if (validResult)
            {
                Assert.IsNotNull(result.Target);
            }
        }
        
        [TestMethod]
        public async Task GetSellDetailSafety()
        {
            var productId = 13247;
            var result = await _sellAsyncService.GetSellDetailSafety(productId);
            var validResult = Assert.Equals(result.HasErrors, false);
            if (validResult)
            {
                Assert.IsNotNull(result.Target);
            }
        }
        
        [TestMethod]
        public async Task GetSellDetailEntertainment()
        {
            var productId = 13247;
            var result = await _sellAsyncService.GetSellDetailEntertainment(productId);
            var validResult = Assert.Equals(result.HasErrors, false);
            if (validResult)
            {
                Assert.IsNotNull(result.Target);
            }
        }        

        [TestMethod]
        public async Task GetSellDetailInterior()
        {
            var productId = 13247;
            var result = await _sellAsyncService.GetSellDetailInterior(productId);
            var validResult = Assert.Equals(result.HasErrors, false);
            if (validResult)
            {
                Assert.IsNotNull(result.Target);
            }
        }
        
        [TestMethod]
        public async Task GetSellDetailExterior()
        {
            var productId = 13247;
            var result = await _sellAsyncService.GetSellDetailExterior(productId);
            var validResult = Assert.Equals(result.HasErrors, false);
            if (validResult)
            {
                Assert.IsNotNull(result.Target);
            }
        }

        [TestMethod]
        public async Task GetSellDetailColor()
        {
            var productId = 13247;
            var result = await _sellAsyncService.GetSellDetailColor(productId);
            var validResult = Assert.Equals(result.HasErrors, false);
            if (validResult)
            {
                Assert.IsNotNull(result.Target);
            }
        }
        
        [TestMethod]
        public async Task GetSellDetailImage()
        {
            var productId = 13247;
            var result = await _sellAsyncService.GetSellDetailImage(productId);
            var validResult = Assert.Equals(result.HasErrors, false);
            if (validResult)
            {
                Assert.IsNotNull(result.Target);
            }
        }
    }
}
