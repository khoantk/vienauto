﻿using System.Web.Mvc;
using System.Configuration;
using Vienauto.Entity.Maps;
using Castle.Windsor.Installer;
using Vienauto.DependencyInjection;
using Common.Core.Nhibernate.Context;
using System.Web.Hosting;

namespace Vienauto.Mobile.Test.ServiceTests
{
    public class BaseControllerTest : Controller
    {
        public BaseControllerTest()
        {
            InitializeNhibernate();
            //DependencyConfig.Setup(FromAssembly.This());
        }

        public void InitializeNhibernate()
        {
            var configFile = ConfigurationManager.AppSettings["nhibernateConfig"];
            var mappingAssembly = typeof(ManufacturerMap).Assembly;
            NhibernateConfiguration.Init(configFile, mappingAssembly);
        }
    }
}
