﻿using Vienauto.Business.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Vienauto.Interface.AsyncServices;
using Vienauto.Business.AsyncServices;
using System.Threading.Tasks;
using Vienauto.Interface.Dto;

namespace Vienauto.Mobile.Test.ServiceTests
{
    [TestClass]
    public class ModelControllerTest : BaseControllerTest
    {
        private readonly ModelService modelService;
        private readonly IModelAsyncService _modelAsyncService;

        public ModelControllerTest(IModelAsyncService modelAsyncService)
        {
            _modelAsyncService = modelAsyncService;
        }

        [TestMethod]
        public void CanShowSameVersionCars()
        {
            var result = modelService.GetSameVersionCars("Acura", "MDX", "2015");
        }

        public void CanShowSafetyModelDetail()
        {
            //var result = modelService.GetModelDetailSafety(94, 725, 1438);
        }

        [TestMethod]
        public void CanReviewModel()
        {
            ModelReviewsDto modelReviews = new ModelReviewsDto()
            {
                UserId = 91302,
                Brand = "acura",
                Model = "ilx",
                Year = "2015",
                Content = "Good",
                DatePost = DateTime.Now,
                PointBody = 3,
                PointSafe = 3,
                PointOperation = 3,
                PointPrice = 3,
                Name = "lehoangnha",
                Title = "Review",
                Email = "lehoangnha@gmail.com",
                Phone = "0123"
            };

            var result = _modelAsyncService.AddModelReviewAsync(modelReviews);
        }
    }

}
