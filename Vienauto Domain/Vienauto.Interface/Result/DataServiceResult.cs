﻿namespace Vienauto.Interface.Result
{
    public class DataServiceResult<TData> : ErrorHelper
    {
        public TData Target { get; set; }

        public bool HasErrors
        {
            get { return Errors.Count > 0 ? true : false; }
        }
    }
}
