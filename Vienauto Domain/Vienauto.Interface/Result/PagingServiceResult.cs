﻿namespace Vienauto.Interface.Result
{
    public class PagingServiceResult<TPaging, TData> : DataServiceResult<TData>
    {
        public TPaging Paging { get; set; }
    }
}
