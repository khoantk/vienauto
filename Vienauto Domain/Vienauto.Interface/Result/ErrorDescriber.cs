﻿using System;
using Vienauto.Entity.Enums;
using System.Collections.Generic;

namespace Vienauto.Interface.Result
{
    public class ErrorDescriber
    {
        public ErrorCode Code { get; set; }
        public Exception Exception { get; set; }

        public ErrorDescriber()
        {
            Code = ErrorCode.None;
            Exception = null;
        }

        public ErrorDescriber(ErrorCode code, Exception exception)
        {
            Code = code;
            Exception = exception;
        }
    }

    public class ErrorHelper
    {
        public List<ErrorDescriber> Errors { get; set; }

        public ErrorHelper()
        {
            Errors = new List<ErrorDescriber>();
        }

        public List<ErrorDescriber> AddErrorRange(IEnumerable<ErrorDescriber> ErrorsRange)
        {
            Errors.AddRange(ErrorsRange);
            return Errors;
        }

        public List<ErrorDescriber> AddError(ErrorCode error, Exception exception)
        {
            Errors.Add(new ErrorDescriber { Code = error, Exception = exception });
            return Errors;
        }

        public List<ErrorDescriber> AddError(ErrorCode error)
        {
            return AddError(error, null);
        }

        public List<ErrorDescriber> AddError(Exception exception)
        {
            return AddError(ErrorCode.None, exception);
        }
    }
}
