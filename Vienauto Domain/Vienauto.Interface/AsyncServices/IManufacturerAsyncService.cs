﻿using System.Threading.Tasks;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;

namespace Vienauto.Interface.AsyncServices
{
    public interface IManufacturerAsyncService
    {
        Task<DataServiceResult<IList<Manufacturer>>> GetAllManufacturerAsync();
    }
}
