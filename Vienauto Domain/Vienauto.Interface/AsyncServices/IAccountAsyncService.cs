﻿using System.Threading.Tasks;
using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;

namespace Vienauto.Interface.AsyncServices
{
    public interface IAccountAsyncService : IBaseService
    {
        Task<DataServiceResult<UserDto>> AuthenticateUserAsync(string userName, string passWord, bool enableEncryptPassword = false);
        Task<DataServiceResult<RegisterDto>> SignUpUserAsync(RegisterDto registerDto);
        Task<DataServiceResult<RegisterDto>> SignUpAgentUserAsync(RegisterDto registerDto);
    }
}
