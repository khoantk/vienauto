﻿using System.Threading.Tasks;
using Vienauto.Interface.Dto;
using Common.Core.Data.Paging;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;

namespace Vienauto.Interface.AsyncServices
{
    public interface IModelAsyncService
    {
        Task<DataServiceResult<IList<Model>>> GetModelByManufacturerAsync(int manufacturerId);
        Task<DataServiceResult<ModelDetailReviewDto>> GetModelReviewDetailAsync(int manufactureId, int modelId, int yearId, int userId);
        Task<PagingServiceResult<PagingResponse, List<ModelReviewsDto>>> GetModelsReviewsWithPagingAsync(PagingRequest<Dictionary<string, object>> pagingRequest);
        Task<DataServiceResult<ModelDto>> GetModelStandardInformationAsync(string manufacturerName, string modelName, string yearName, int carId, int userId = 0);
        Task<DataServiceResult<IList<ModelDto>>> GetSameVersionCarsAsync(string manufacturerName, string modelName, string yearName);
        Task<PagingServiceResult<PagingResponse, IList<ModelDto>>> GetSameStyleCarsWithModelPagingAsync(PagingRequest<Dictionary<string, object>> pagingRequest);
        Task<DataServiceResult<bool>> CheckCanWriteReviewModelAsync(string manufacturer, string model, string year, int userId);
        Task<DataServiceResult<bool>> AddModelReviewAsync(ModelReviewsDto modelReview);
        Task<DataServiceResult<ModelDetailOverviewDto>> GetOverviewAsync(int carId);
        Task<DataServiceResult<ModelDetailEngineDto>> GetEngineAsync(int carId);
        Task<DataServiceResult<ModelDetailSafetyDto>> GetSafetyAsync(int carId);
        Task<DataServiceResult<ModelDetailEntertainmentDto>> GetEntertainmentAsync(int carId);
        Task<DataServiceResult<ModelDetailInteriorDto>> GetInteriorAsync(int carId);
        Task<DataServiceResult<ModelDetailExteriorDto>> GetExteriorAsync(int carId);
        Task<DataServiceResult<ModelDetailColorDto>> GetColorAsync(int carId);
        Task<DataServiceResult<ModelDetailImageDto>> GetImageAsync(int carId);
    }
}
