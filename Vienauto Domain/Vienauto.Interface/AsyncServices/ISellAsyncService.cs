﻿using System.Threading.Tasks;
using Vienauto.Interface.Dto;
using Common.Core.Data.Paging;
using Vienauto.Interface.Result;
using System.Collections.Generic;

namespace Vienauto.Interface.AsyncServices
{
    public interface ISellAsyncService : IBaseService
    {
        Task<PagingServiceResult<PagingResponse, List<CarForSellDetailDto>>> GetCarsWithPagingAsync(PagingRequest<Dictionary<string, object>> pagingRequest);
        Task<DataServiceResult<CarForSellDetailDto>> GetDetailInfoAndUpdateViewTimesAsync(int productId);
        Task<DataServiceResult<ModelDetailOverviewDto>> GetSellDetailOverview(int productId);
        Task<DataServiceResult<ModelDetailEngineDto>> GetSellDetailEngine(int productId);
        Task<DataServiceResult<ModelDetailSafetyDto>> GetSellDetailSafety(int productId);
        Task<DataServiceResult<ModelDetailEntertainmentDto>> GetSellDetailEntertainment(int productId);
        Task<DataServiceResult<ModelDetailInteriorDto>> GetSellDetailInterior(int productId);
        Task<DataServiceResult<ModelDetailExteriorDto>> GetSellDetailExterior(int productId);
        Task<DataServiceResult<ModelDetailColorDto>> GetSellDetailColor(int productId);
        Task<DataServiceResult<ModelDetailImageDto>> GetSellDetailImage(int productId);
    }
}
