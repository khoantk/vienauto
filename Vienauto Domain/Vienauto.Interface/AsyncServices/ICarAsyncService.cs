﻿using System.Threading.Tasks;
using Vienauto.Interface.Dto;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;

namespace Vienauto.Interface.AsyncServices
{
    public interface ICarAsyncService
    {
        Task<DataServiceResult<int>> BookingAsync(CarBookingDto carBookingDto);
        Task<DataServiceResult<Car>> GetCarByIdAsync(int carId);
        Task<DataServiceResult<IList<CarDto>>> GetCarByYearAsync(int yearId);
        Task<DataServiceResult<string>> GetAvatarByCarIdAsync(int carId);
    }
}
