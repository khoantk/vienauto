﻿using System.Threading.Tasks;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;

namespace Vienauto.Interface.AsyncServices
{
    public interface IYearAsyncService
    {
        Task<DataServiceResult<IList<Year>>> GetYearByModelAsync(int modelId);
    }
}
