﻿using System.Threading.Tasks;
using Common.Core.Data.Paging;
using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;
using System.Collections.Generic;

namespace Vienauto.Interface.AsyncServices
{
    public interface IAgencyAsyncService : IBaseService
    {
        Task<DataServiceResult<AgencyDetailDto>> GetAgencyDetailAsync(int agencyUserId, int manufacturerId, int agentId);
        Task<DataServiceResult<AgencyDetailReviewDto>> GetAgencyReviewDetailAsync(int agentId, int manufacturerId, int userId);
        Task<DataServiceResult<IList<DealerShipDto>>> GetAllDealerShipsAsync();
        Task<DataServiceResult<IList<AgencyDto>>> GetAgencyAsync(int manufacturerId);
        Task<DataServiceResult<IList<AgencyDto>>> GetAgencyByDealerShipAsync(int manufacturerId);
        Task<PagingServiceResult<PagingResponse, List<AgencyDto>>> GetAgenciesWithPagingAsync(PagingRequest<Dictionary<string, object>> pagingRequest);
        Task<PagingServiceResult<PagingResponse, List<AgencyReviewsDto>>> GetAgenciesReviewsWithPagingAsync(PagingRequest<Dictionary<string, object>> pagingRequest);
        Task<DataServiceResult<bool>> CheckCanWriteReviewAgencyAsync(int agentId, int userId);
        Task<DataServiceResult<bool>> AddAgencyReviewAsync(AgencyDetailReviewDto agencyReview);
    }
}
