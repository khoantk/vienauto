﻿using System.Threading.Tasks;
using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;
using System.Collections.Generic;

namespace Vienauto.Interface.AsyncServices
{
    public interface IOtherAsyncService
    {
        Task<DataServiceResult<IList<QuestionDto>>> GetAllQuestionsAsync();
        Task<DataServiceResult<IList<LocationDto>>> GetAllLocationsAsync();
        Task<DataServiceResult<IList<ProvinceDto>>> GetAllProvincesAsync();
    }
}
