﻿using System;

namespace Vienauto.Interface.Dto
{
    public class ModelReviewsDto : UserDto
    {
        public int IdReviews { get; set; }        
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }              
        public int PointBody { get; set; }
        public int PointSafe { get; set; }
        public int PointOperation { get; set; }
        public int PointPrice { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime DatePost { get; set; }                
    }
}
