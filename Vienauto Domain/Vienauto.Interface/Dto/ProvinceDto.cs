﻿using Vienauto.Entity.Entities;
using System.Collections.Generic;

namespace Vienauto.Interface.Dto
{
    public class ProvinceDto
    {
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }
    }

    public static class ProvinceDtoExtension
    {
        public static ProvinceDto FromEntityToDto(this Province entity)
        {
            return new ProvinceDto
            {
                ProvinceId = entity.Id,
                ProvinceName = entity.Name_TinhThanh
            };
        }

        public static IList<ProvinceDto> FromEntitiesToDtos(this IList<Province> entities)
        {
            var dtos = new List<ProvinceDto>();
            var listEntities = (List<Province>)entities;
            listEntities.ForEach(x =>
            {
                dtos.Add(x.FromEntityToDto());
            });
            return dtos;
        }
    }
}
