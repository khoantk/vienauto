﻿using System.Linq;
using System.Collections.Generic;

namespace Vienauto.Interface.Dto
{
    public class AgencyDetailDto : AgencyDto
    {
        public string FromSales { get; set; }
        public string ToSales { get; set; }
        public string FromServices { get; set; }
        public string ToServices { get; set; }
        public string DisplaySales { get; set; }
        public string DisplayServices { get; set; }

        public Dictionary<string, string> FullAgencySaleTime
        {
            get { return GetAgencySaleTime("fullTime"); }
        }
        public Dictionary<string, string> OpenAgencySaleTime
        {
            get { return GetAgencySaleTime(DisplaySales); }
        }
        public Dictionary<string, string> FullAgencyServiceTime
        {
            get { return GetAgencyServiceTime("fullTime"); }
        }
        public Dictionary<string, string> OpenAgencyServiceTime
        {
            get { return GetAgencyServiceTime(DisplayServices); }
        }

        private Dictionary<string, string> GetAgencySaleTime(string displayDate)
        {
            return GetAgencyDateTime(FromSales, ToSales, displayDate);
        }

        private Dictionary<string, string> GetAgencyServiceTime(string displayDate)
        {
            return GetAgencyDateTime(FromServices, ToServices, displayDate);
        }

        private Dictionary<string, string> GetAgencyDateTime(string fromDate, string toDate, string displayDate)
        {
            var fromDateTime = new Dictionary<string, string>();
            var toDateTime = new Dictionary<string, string>();
            var pairDateTime = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate) ||
                !fromDate.Contains("-") || !toDate.Contains("-"))
                return new Dictionary<string, string>();

            var fromDateSet = fromDate.Split(',');
            var toDateSet = toDate.Split(',');
            if(fromDateSet.Length != toDateSet.Length) 
               return new Dictionary<string, string>();

            for (var dateTimeIndex = 0; dateTimeIndex < fromDateSet.Length; dateTimeIndex++)
            {
                var dateNameInWeek = fromDateSet[dateTimeIndex].Split('-').FirstOrDefault();
                var fromTime = fromDateSet[dateTimeIndex].Split('-').LastOrDefault();
                var toTime = toDateSet[dateTimeIndex].Split('-').LastOrDefault();

                if (displayDate == "fullTime")
                    pairDateTime.Add(GetDateNameInWeekFromNumber(dateNameInWeek), $"{fromTime} - {toTime}");
                else if (displayDate != "")
                {
                    if (displayDate.ToArray().Any(d => d == char.Parse(dateNameInWeek)))
                        pairDateTime.Add(GetDateNameInWeekFromNumber(dateNameInWeek), $"{fromTime} - {toTime}");
                }
            }
            return pairDateTime;
        }

        private string GetDateNameInWeekFromNumber(string dateInWeekNumber)
        {
            switch (dateInWeekNumber)
            {
                case "2":
                    return "Thứ hai";
                case "3":
                    return "Thứ ba";
                case "4":
                    return "Thứ tư";
                case "5":
                    return "Thứ năm";
                case "6":
                    return "Thứ sáu";
                case "7":
                    return "Thứ bảy";
                case "8":
                    return "Chủ nhật";
                default:
                    return "";
            }
        }
    }
}
