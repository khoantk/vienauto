﻿namespace Vienauto.Interface.Dto
{
    public class AgencyDto
    {
        public int AgencyId { get; set; }
        public int UserId { get; set; }
        public int CountryId { get; set; }
        public string FullName { get; set; }
        public string ProvinceName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string NewPhone { get; set; }
        public string OldPhone { get; set; }
        public string Service { get; set; }
        public string WebsiteLink { get; set; }
        public string UserEmail { get; set; }
        public string Coordinates { get; set; }
        public int DealerShipId { get; set; }
        public string ManufacturerAvatar { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public string ZoomMap { get; set; }
        public double MaxPointReview { get; set; }
        public int TotalReview { get; set; }
        public long TotalPages { get; set; }
    }
}
