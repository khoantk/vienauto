﻿using System;
using System.Collections.Generic;

namespace Vienauto.Interface.Dto
{
    public class CarForSellDetailDto
    {
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public int ProductStatus { get; set; }
        public DateTime DatePost { get; set; }
        public int InOut { get; set; }
        public int NewOld { get; set; }
        public string VinNumber { get; set; }
        public string Guarantee { get; set; }
        public int CarId { get; set; }
        public string CarName { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public int ModelId { get; set; }
        public string ModelName { get; set; }
        public int StyleId { get; set; }
        public string StyleName { get; set; }
        public int YearId { get; set; }
        public string YearName { get; set; }
        public string Avatar { get; set; }
        public double Price { get; set; }
        public double FullPrice { get; set; }
        public string UnitName { get; set; }
        public int Expire { get; set; }
        public int ManufacturerDiscount { get; set; }
        public int AgencyDiscount { get; set; }
        public int Discount { get; set; }
        public int Km { get; set; }
        public string MadeIn { get { return InOut == 0 ? "Nhập khẩu" : (InOut == 1 ? "Trong nước" : ""); } }
        public string ProductType { get { return NewOld == 0 ? "Xe cũ" : (InOut == 1 ? "Xe mới" : ""); } }
    }

    public class CarForSellOverviewDto : CarForSellDetailDto
    {
        public string Introduction { get; set; }
    }

    public class CarForSellEngineDto : CarForSellDetailDto
    {
        public string EngineName { get; set; }
        public string EngineType { get; set; }
        public string CompressionRatio { get; set; }
        public string DrivingType { get; set; }
        public string TranmissionType { get; set; }
        public string FuelType { get; set; }
        public string Locking { get; set; }
        public string EngineValvetrain { get; set; }
        public string ESS { get; set; }
        public string EBD { get; set; }
        public string RemoteVehicle { get; set; }
        public string Tranmission { get; set; }
        public string ExteriorLength { get; set; }
        public string ExteriorWidth { get; set; }
        public string ExteriorHeight { get; set; }
        public string HorsePower { get; set; }
        public string Torque { get; set; }
        public string DragCoeficient { get; set; }
        public string TimeSpeed { get; set; }
        public string Km { get; set; }
        public string CurbWeight { get; set; }
        public string GVWR { get; set; }
    }

    public class CarForSellSafetyDto : CarForSellDetailDto
    {
        public string FrontAirbag { get; set; }
        public string SideAirbag { get; set; }
        public string BackAirbag { get; set; }
        public string HeadLights { get; set; }
        public string ExteriorLightControl { get; set; }
        public string DayLights { get; set; }
        public string LedTailLights { get; set; }
        public string ParkingAssist { get; set; }
        public string AlarmSystem { get; set; }
        public string DoorLockSystem { get; set; }
        public string ChildLock { get; set; }
        public string AntiTheftSystem { get; set; }
        public string LowPressureWarning { get; set; }
        public string Brakes { get; set; }
        public string ABSBrakes { get; set; }
        public string BrakeAssist { get; set; }
    }

    public class CarForSellEntertainmentDto : CarForSellDetailDto
    {
        public string Radio { get; set; }
        public string CDPlayer { get; set; }
        public string DVDAudio { get; set; }
        public string SpeechRecognition { get; set; }
        public int Speakers { get; set; }
        public string Amplifier { get; set; }
        public string BluetoothCompatibility { get; set; }
        public string WifiCompatibility { get; set; }
        public string ThreeGCompatibility { get; set; }
        public string GPSCompatibility { get; set; }
    }

    public class CarForSellInteriorDto : CarForSellDetailDto
    {
        public int FrontAirConditioning { get; set; }
        public int BackAirConditioning { get; set; }
        public int AirFilter { get; set; }
        public int Seating { get; set; }
        public string FrontSeatType { get; set; }
        public string Shampoo { get; set; }
        public string Compass { get; set; }
        public string Clocks { get; set; }
        public int DCPowerOutlet { get; set; }
        public string Tachometer { get; set; }
        public string WaterTempGauges { get; set; }
        public string LowFuelWarning { get; set; }
        public string DisplayOutsideTemp { get; set; }
        public string FlashInterior { get; set; }
    }

    public class CarForSellExteriorDto : CarForSellDetailDto
    {
        public string FrontTires { get; set; }
        public string BackTires { get; set; }
        public string Wheels { get; set; }
        public string SunRoof { get; set; }
        public string BackWindowType { get; set; }
        public int NumberOfDoors { get; set; }
        public string RearviewMirror { get; set; }
        public string HeatedDoorMirror { get; set; }
        public string WindshieldWipes { get; set; }
        public string LiftingSystem { get; set; }
        public string FlashExterior { get; set; }
    }

    public class CarForSellColorDto : CarForSellDetailDto
    {
        public string Color { get; set; }
    }

    public class CarForSellVideoDto : CarForSellDetailDto
    {
        public string VideoLink { get; set; }
    }

    public class CarForSellImageDto : CarForSellDetailDto
    {
        public string PathImages { get; set; }
    }

    public class CarForSellDto
    {
        public CarForSellDto() { CarDetails = new List<CarForSellDetailDto>(); }

        public long TotalRows { get; set; }
        public List<CarForSellDetailDto> CarDetails { get; set; }
    }
}
