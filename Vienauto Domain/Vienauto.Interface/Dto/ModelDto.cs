﻿using System.Collections.Generic;

namespace Vienauto.Interface.Dto
{
    public class ModelDto
    {
        public int ManufacturerId { get; set; }
        public int ModelId { get; set; }
        public int YearId { get; set; }
        public int CarId { get; set; }
        public int ProductId { get; set; }
        public double Price { get; set; }
        public string UnitName { get; set; }
        public string Avatar { get; set; }
        public string CarName { get; set; }
        public string ManufacturerName { get; set; }
        public string ModelName { get; set; }
        public string YearName { get; set; }
        public string StyleName { get; set; }
        public string FuelEconomyCity { get; set; }
        public string FuelEconomyHighway { get; set; }
        public long TotalPages { get; set; }
        public string ProductTypeName { get; set; }
        public int PointBody { get; set; }
        public int PointSafe { get; set; }
        public int PointOperation { get; set; }
        public int PointPrice { get; set; }
        public double MaxPointReview
        {
            get
            {
                var maxPoint = (PointBody + PointOperation + PointPrice + PointSafe) / 4;
                if (maxPoint > 0.5 && maxPoint < 1)
                    return 1;
                else if (maxPoint > 1.5 && maxPoint < 2)
                    return 2;
                else if (maxPoint > 2.5 && maxPoint < 3)
                    return 3;
                else if (maxPoint > 3.5 && maxPoint < 4)
                    return 4;
                else if (maxPoint > 4.5 && maxPoint < 5)
                    return 5;
                return maxPoint;
            }
        }
        public int TotalReview { get; set; }
    }

    public class ModelDetailDto : ModelDto { }

    public class ModelDetailOverviewDto : ModelDto
    {
        public string Introduction { get; set; }
    }

    public class ModelDetailEngineDto : ModelDto
    {
        public int SpecificationId { get; set; }
        public string EngineName { get; set; }
        public string EngineType { get; set; }
        public string CompressionRatio { get; set; }
        public string DrivingType { get; set; }
        public string TranmissionType { get; set; }
        public string FuelType { get; set; }
        public string Locking { get; set; }
        public string EngineValvetrain { get; set; }
        public string ESS { get; set; }
        public string EBD { get; set; }
        public string RemoteVehicle { get; set; }
        public string Tranmission { get; set; }
        public string ExteriorLength { get; set; }
        public string ExteriorWidth { get; set; }
        public string ExteriorHeight { get; set; }
        public string HorsePower { get; set; }
        public string Torque { get; set; }
        public string DragCoeficient { get; set; }
        public string TimeSpeed { get; set; }
        public string Km { get; set; }
        public string CurbWeight { get; set; }
        public string GVWR { get; set; }
        public string CC { get; set; }
        public string Payloa { get; set; }
        public string RewriteDrivingType { get; set; }
        public string MPG { get; set; }
    }

    public class ModelDetailSafetyDto : ModelDto
    {
        public int SafetyId { get; set; }
        public string FrontAirbag { get; set; }
        public string SideAirbag { get; set; }
        public string BackAirbag { get; set; }
        public string HeadLights { get; set; }
        public string ExteriorLightControl { get; set; }
        public string DayLights { get; set; }
        public string LedTailLights { get; set; }
        public string ParkingAssist { get; set; }
        public string AlarmSystem { get; set; }
        public string DoorLockSystem { get; set; }
        public string ChildLock { get; set; }
        public string AntiTheftSystem { get; set; }
        public string LowPressureWarning { get; set; }
        public string Brakes { get; set; }
        public string ABSBrakes { get; set; }
        public string BrakeAssist { get; set; }
    }

    public class ModelDetailEntertainmentDto : ModelDto
    {
        public string Radio { get; set; }
        public string CDPlayer { get; set; }
        public string DVDAudio { get; set; }
        public string SpeechRecognition { get; set; }
        public int Speakers { get; set; }
        public string Amplifier { get; set; }
        public string BluetoothCompatibility { get; set; }
        public string WifiCompatibility { get; set; }
        public string ThreeGCompatibility { get; set; }
        public string GPSCompatibility { get; set; }        
    }

    public class ModelDetailInteriorDto : ModelDto
    {
        public int InteriorId { get; set; }
        public int FrontAirConditioning { get; set; }
        public int BackAirConditioning { get; set; }
        public int AirFilter { get; set; }
        public int Seating { get; set; }
        public string FrontSeatType { get; set; }
        public string Shampoo { get; set; }
        public string Compass { get; set; }
        public string Clocks { get; set; }
        public int DCPowerOutlet { get; set; }
        public string Tachometer { get; set; }
        public string WaterTempGauges { get; set; }
        public string LowFuelWarning { get; set; }
        public string DisplayOutsideTemp { get; set; }
        public string FlashInterior { get; set; }
        public string RewriteSeating { get; set; }
    }

    public class ModelDetailExteriorDto : ModelDto
    {
        public int ExteriorId { get; set; } 
        public string FrontTires { get; set; }
        public string BackTires { get; set; }
        public string Wheels { get; set; }
        public string SunRoof { get; set; }
        public string BackWindowType { get; set; }
        public int NumberOfDoors { get; set; }
        public string RearviewMirror { get; set; }
        public string HeatedDoorMirror { get; set; }
        public string WindshieldWipes { get; set; }
        public string LiftingSystem { get; set; }
        public string FlashExterior { get; set; }
    }

    public class ModelDetailColorDto : ModelDto
    {
        public string Color { get; set; }
    }

    public class ModelDetailVideoDto : ModelDto
    {
        public string VideoLink { get; set; }
    }

    public class ModelDetailImageDto : ModelDto
    {
        public string PathImages { get; set; }        
    }
        
}
