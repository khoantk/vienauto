﻿using System;
using Vienauto.Entity.Entities;

namespace Vienauto.Interface.Dto
{
    public class ModelDetailReviewDto
    {
        public int IdReviews { get; set; }
        public int IdUsers { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Year { get; set; } 
        public string ModelName { get; set; }
        public string Image { get; set; }
        public string FuelInCity { get; set; }
        public string FuelInHighWay { get; set; }
        public double Price { get; set; }
        public string Unit { get; set; }
        public int PointBody { get; set; }
        public int PointSafe { get; set; }
        public int PointOperation { get; set; }
        public int PointPrice { get; set; }
        public string Content { get; set; }
        public DateTime Datepost { get; set; }
        public int TotalReview { get; set; }

        public ModelDetailReviewDto Transform(ModelReview modelReview, int totalReview)
        {
            TotalReview = totalReview;
            if(modelReview != null)
            {
                IdReviews = modelReview.IdReviews;               
                PointBody = modelReview.PointBody;
                PointSafe = modelReview.PointSafe;
                PointOperation = modelReview.PointOperation;
                PointPrice = modelReview.PointPrice;
                Content = modelReview.Content;
                Datepost = modelReview.Datepost;
                IdUsers = modelReview.IdUsers;
            }
            return this;
        }
    }
}
