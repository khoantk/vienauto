﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vienauto.Interface.Dto
{
    public class SearchDto
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string SortOrder { get; set; }
        public string Filter { get; set; }
    }
}
