﻿using System;

namespace Vienauto.Interface.Dto
{
    public class AgencyDetailReviewDto
    {
        public AgencyDetailReviewDto()
        { }

        public int ReviewId { get; set; }
        public int AgentId { get; set; }
        public int UserId { get; set; }
        public int ManufacturerId { get; set; }
        public double Overall { get; set; }
        public string AgentName { get; set; }
        public string ProvinceName { get; set; }
        public string ManufacturerName { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime DatetimeReview { get; set; }
        public string TitleReview { get; set; }
        public string ContentReview { get; set; }
        public int CustomerService { get; set; }
        public int BuyingProcess { get; set; }
        public int QualityOfRepair { get; set; }
        public int Facilities { get; set; }
        public int AverageReviews { get; set; }
        public int TotalReviews { get; set; }
    }
}
