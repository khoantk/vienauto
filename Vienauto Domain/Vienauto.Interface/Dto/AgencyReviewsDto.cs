﻿namespace Vienauto.Interface.Dto
{
    public class AgencyReviewsDto : UserDto
    {
        public int ReviewId { get; set; }
        public int CustomerService { get; set; }
        public int BuyingProcess { get; set; }
        public int QualityRepair { get; set; }
        public int Facilities { get; set; }
        public double Overall { get; set; }
        public string Content { get; set; }
        public System.DateTime DatePost { get; set; }
        public double MaxPointReview { get; set; }    
    }
}
