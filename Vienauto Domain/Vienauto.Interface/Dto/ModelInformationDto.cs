﻿using Vienauto.Entity.Entities;
using System.Collections.Generic;

namespace Vienauto.Interface.Dto
{
    public class ModelInformationDto
    {
        public int ModelId { get; set; }
        public IList<Year> Years { get; set; }
        public Style Style { get; set; }
        public string Name { get; set; }
        public string RewriteName { get; set; }

        public ModelInformationDto Transform(Model model)
        {
            if(model != null)
            {
                ModelId = model.Id;
                Years = model.Years;
                Style = model.Style;
                Name = model.Name;
                RewriteName = model.RewriteName;
            }
            return this;
        }
    }
}
