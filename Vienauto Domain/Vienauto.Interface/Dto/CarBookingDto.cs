﻿using System;
using Vienauto.Entity.Entities;

namespace Vienauto.Interface.Dto
{
    public class CarBookingDto
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Content { get; set; }
        public bool IsNew { get; set; }
        public string Map { get; set; }
        public string CarImage { get; set; }
        public string CarColor { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public string ContactTime { get; set; }
        public string ContactForm { get; set; }
        public int CarId { get; set; }
        public int UserId { get; set; }
        public int FollowerId { get; set; }
        public int LocationId { get; set; }
        public int ManufacturerId { get; set; }
    }

    public static class CarBookingDtoExtension
    {
        public static CarBooking FromDtoToEntity(this CarBookingDto dto, CarBooking entity)
        {
            entity.FullName = dto.FullName;
            entity.Email = dto.Email;
            entity.Address = dto.Address;
            entity.Phone = dto.Phone;
            entity.NoiDung = dto.Content;
            entity.Datepost = DateTime.Now;
            entity.Active = 0;
            entity.New_Old = dto.IsNew ? 1 : 0;
            entity.Map = dto.Map;
            entity.Anh_Xe = dto.CarImage;
            entity.Mau_Xe = dto.CarColor;
            entity.Tuoi = dto.Age;
            entity.Gioi_Tinh = dto.Gender;
            entity.Thoi_Gian = dto.ReceiveDate.GetValueOrDefault();
            entity.Thoi_Gian_LH = dto.ContactTime;
            entity.Hinh_Thuc_LH = dto.ContactForm;
            return entity;
        }
    }
}
