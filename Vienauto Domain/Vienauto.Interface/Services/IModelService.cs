﻿using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;
using Common.Core.Data.Paging;
using System.Collections.Generic;
using Vienauto.Entity.Entities;

namespace Vienauto.Interface.Services
{
    public interface IModelService : IBaseService
    {
        DataServiceResult<IList<Model>> GetModelByManufacturer(int manufacturerId);
        DataServiceResult<ModelDetailDto> GetModelStandardInformation(int userId, string manufacturerName, string modelName, string yearName, int carId);
        DataServiceResult<ModelDto> GetModelCommonInformation(string manufacturerName, string modelName, string yearName, int carId);
        DataServiceResult<IList<ModelDto>> GetSameVersionCars(string manufacturerName, string modelName, string yearName);
        DataServiceResult<ModelDetailReviewDto> GetModelReviewDetail(int manufactureId, int modelId, int yearId, int userId);
        DataServiceResult<bool> CheckCanWriteReviewModel(string manufacture, string model, string year, int userId);
        DataServiceResult<bool> AddModelReview(ModelReviewsDto agencyReview);

        PagingServiceResult<PagingResponse, List<ModelReviewsDto>> GetModelsReviewsWithPaging(PagingRequest<Dictionary<string, object>> pagingInput);
        PagingServiceResult<PagingResponse, IList<ModelDetailDto>> GetSameStyleCarsWithModelPaging(PagingRequest<Dictionary<string, object>> pagingRequest);
        DataServiceResult<ModelDetailOverviewDto> GetModelDetailOverview(int carId);
        DataServiceResult<ModelDetailEngineDto> GetModelDetailEngine(int carId);
        DataServiceResult<ModelDetailSafetyDto> GetModelDetailSafety(int carId);
        DataServiceResult<ModelDetailEntertainmentDto> GetModelDetailEntertainment(int carId);
        DataServiceResult<ModelDetailInteriorDto> GetModelDetailInterior(int carId);
        DataServiceResult<ModelDetailExteriorDto> GetModelDetailExterior(int carId);
        DataServiceResult<ModelDetailColorDto> GetModelDetailColor(int carId);
        DataServiceResult<ModelDetailImageDto> GetModelDetailImage(int carId);
    }
}
