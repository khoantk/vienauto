﻿using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;

namespace Vienauto.Interface.Services
{
    public interface IAccountService : IBaseService
    {
        DataServiceResult<UserDto> AuthenticateUser(string userName, string passWord);
        DataServiceResult<RegisterDto> SignUpUser(RegisterDto registerDto);
        DataServiceResult<RegisterDto> SignUpAgentUser(RegisterDto registerDto);
    }
}
