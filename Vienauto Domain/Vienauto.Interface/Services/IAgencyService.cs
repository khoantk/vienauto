﻿using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;
using Common.Core.Data.Paging;
using System.Collections.Generic;

namespace Vienauto.Interface.Services
{
    public interface IAgencyService : IBaseService
    {
        DataServiceResult<AgencyDetailDto> GetAgencyDetail(int agencyUserId, int manufacturerId, int agentId);
        DataServiceResult<IList<DealerShipDto>> GetAllDealerShips();
        DataServiceResult<IList<AgencyDto>> GetAgency(int manufacturerId);
        DataServiceResult<IList<AgencyDto>> GetAgencyByDealerShip(int manufacturerId);
        DataServiceResult<AgencyDetailReviewDto> GetAgencyReviewDetail(int agentId, int manufacturerId, int userId);

        DataServiceResult<bool> CheckCanWriteReviewAgency(int agentId, int userId);
        DataServiceResult<bool> AddAgencyReview(AgencyDetailReviewDto agencyReview);

        PagingServiceResult<PagingResponse, List<AgencyDto>> GetAgenciesWithPaging(PagingRequest<Dictionary<string, object>> pagingInput);
        PagingServiceResult<PagingResponse, List<AgencyReviewsDto>> GetAgenciesReviewsWithPaging(PagingRequest<Dictionary<string, object>> pagingRequest);
    }
}