﻿using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;
using System.Collections.Generic;

namespace Vienauto.Interface.Services
{
    public interface IOtherService : IBaseService
    {
        DataServiceResult<IList<QuestionDto>> GetAllQuestions();
        DataServiceResult<IList<LocationDto>> GetAllLocations();
        DataServiceResult<IList<ProvinceDto>> GetAllProvinces();
    }
}
