﻿using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Entity.Entities;

namespace Vienauto.Interface.Services
{
    public interface ICarService : IBaseService
    {
        DataServiceResult<int> Booking(CarBookingDto carBookingDto);
        DataServiceResult<IList<CarDto>> GetCarByYear(int yearId);
        DataServiceResult<Car> GetCarById(int carId);
        DataServiceResult<string> GetAvatarByCarId(int carId);
    }
}
