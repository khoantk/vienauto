﻿using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Entity.Entities;

namespace Vienauto.Interface.Services
{
    public interface IYearService : IBaseService
    {
        DataServiceResult<IList<Year>> GetYearByModel(int modelId);
    }
}
