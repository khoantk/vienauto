﻿using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Entity.Entities;

namespace Vienauto.Interface.Services
{
    public interface IManufacturerService : IBaseService
    {
        DataServiceResult<IList<Manufacturer>> GetAllManufacturer();
    }
}
