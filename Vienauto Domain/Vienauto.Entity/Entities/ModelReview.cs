﻿using System;

namespace Vienauto.Entity.Entities
{
    public class ModelReview
    {
        public virtual int IdReviews { get; set; }
        public virtual int IdUsers { get; set; }
        public virtual string Brand { get; set; }
        public virtual string Model { get; set; }
        public virtual string Year { get; set; }
        public virtual int PointBody { get; set; }
        public virtual int PointSafe { get; set; }
        public virtual int PointOperation { get; set; }
        public virtual int PointPrice { get; set; }
        public virtual string Name { get; set; }
        public virtual string Email { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Title { get; set; }
        public virtual string Content { get; set; }
        public virtual DateTime Datepost { get; set; }
    }
}
