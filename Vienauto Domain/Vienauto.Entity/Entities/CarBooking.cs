﻿using System;

namespace Vienauto.Entity.Entities
{
    public class CarBooking : Entity
    {
        public virtual string FullName { get; set; }
        public virtual string Email { get; set; }
        public virtual string Address { get; set; }
        public virtual string Phone { get; set; }
        public virtual string NoiDung { get; set; }
        public virtual string Xe { get; set; }
        public virtual DateTime? Datepost { get; set; }
        public virtual int Active { get; set; }
        public virtual int New_Old { get; set; }
        public virtual string Map { get; set; }
        public virtual string Anh_Xe { get; set; }
        public virtual string Mau_Xe { get; set; }
        public virtual string Tuoi { get; set; }
        public virtual string Gioi_Tinh { get; set; }
        public virtual DateTime Thoi_Gian { get; set; }
        public virtual string Thoi_Gian_LH { get; set; }
        public virtual string Hinh_Thuc_LH { get; set; }
        public virtual int theodoi { get; set; }
        public virtual Car Car { get; set; }
        public virtual User User { get; set; }
        public virtual User Follower { get; set; }
        public virtual Location Location { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
    }
}
