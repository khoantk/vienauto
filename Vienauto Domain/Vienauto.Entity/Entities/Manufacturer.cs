﻿using System.Collections.Generic;

namespace Vienauto.Entity.Entities
{
    public class Manufacturer : Entity
    {
        public Manufacturer()
        {
            Styles = new List<Style>();
            Products = new List<Product>();
            DealerShips = new List<DealerShip>();
            CarBookings = new List<CarBooking>();
            Avatars = new List<ModelAvatar>();
        }

        public virtual string Name { get; set; }
        public virtual string RewriteName { get; set; }
        public virtual string Logo { get; set; }
        public virtual IList<Style> Styles { get; set; }
        public virtual IList<Product> Products { get; set; }
        public virtual IList<DealerShip> DealerShips { get; set; }
        public virtual IList<CarBooking> CarBookings { get; set; }
        public virtual IList<ModelAvatar> Avatars { get; set; }
    }
}
