﻿namespace Vienauto.Entity.Entities
{
    public class Setting : Entity
    {
        public virtual float Exchange { get; set; }
        public virtual int Start_ChangePoint { get; set; }
        public virtual string Acount_Bank { get; set; }
        public virtual string Title_web { get; set; }
        public virtual string description_web { get; set; }
        public virtual string keyword_web { get; set; }
        public virtual float? chitieu_doanhthu { get; set; }
        public virtual float? luongcoban { get; set; }
        public virtual float? fuel_price { get; set; }
        public virtual string seopass { get; set; }
    }
}
