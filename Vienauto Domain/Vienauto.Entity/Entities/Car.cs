﻿using System;
using System.Collections.Generic;

namespace Vienauto.Entity.Entities
{
    public class Car : Entity
    {
        public Car()
        {
            Reviews = new List<CarReview>();
            Products = new List<Product>();
            CarBookings = new List<CarBooking>();
        }

        public virtual string Name { get; set; }
        public virtual string Html_Exterior { get; set; }
        public virtual string Html_Interior { get; set; }
        public virtual string Html_Color { get; set; }
        public virtual string Link_Video { get; set; }
        public virtual string Introlduction { get; set; }
        public virtual string Km { get; set; }
        public virtual int Rate_Value { get; set; }
        public virtual int Rate_Num { get; set; }
        public virtual DateTime? LastReviews { get; set; }
        public virtual string PathImages { get; set; }
        public virtual Year Year { get; set; }
        public virtual IList<CarReview> Reviews { get; set; }
        public virtual IList<Product> Products { get; set; }
        public virtual IList<CarBooking> CarBookings { get; set; }
        public virtual Avatar Avatar { get; set; }
        public virtual CarSafety Safety { get; set; }
        public virtual CarInterior Interior { get; set; }
        public virtual CarExterior Exterior { get; set; }
        public virtual CarEntertainment Entertainment { get; set; }
        public virtual CarSpecification Specification { get; set; }
    }
}
