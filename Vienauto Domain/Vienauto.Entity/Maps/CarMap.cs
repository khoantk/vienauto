﻿using NHibernate.Mapping.ByCode;
using Vienauto.Entity.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Vienauto.Entity.Maps
{
    public class CarMap : ClassMapping<Car>
    {
        public CarMap()
        {
            Schema("hdt");
            Table("MName");
            Lazy(true);
            Id(x => x.Id, map =>
            {
                map.Column("Id_MName");
                map.Generator(Generators.Identity);
            });
            Property(x => x.Km);
            Property(x => x.Rate_Num);
            Property(x => x.Html_Color);
            Property(x => x.Link_Video);
            Property(x => x.Rate_Value);
            Property(x => x.PathImages);
            Property(x => x.LastReviews);
            Property(x => x.Html_Exterior);
            Property(x => x.Html_Interior);
            Property(x => x.Introlduction);
            Property(x => x.Name, map => map.Column("Name_MName"));
            ManyToOne(x => x.Year, map => map.Column("Id_Year"));
            Bag(x => x.Reviews, map => map.Key(k => k.Column("Id_MName")), m => m.OneToMany());
            Bag(x => x.Products, map => map.Key(k => k.Column("Id_MName")), m => m.OneToMany());
            OneToOne(x => x.Avatar, map =>
            {
                map.PropertyReference(typeof(Avatar).GetProperty("Car"));
                map.Cascade(Cascade.All);
            });
            OneToOne(x => x.Specification, map =>
            {
                map.PropertyReference(typeof(CarSpecification).GetProperty("Car"));
                map.Cascade(Cascade.All);
            });
            OneToOne(x => x.Safety, map =>
            {
                map.PropertyReference(typeof(CarSafety).GetProperty("Car"));
                map.Cascade(Cascade.All);
            });
            OneToOne(x => x.Interior, map =>
            {
                map.PropertyReference(typeof(CarInterior).GetProperty("Car"));
                map.Cascade(Cascade.All);
            });
            OneToOne(x => x.Exterior, map =>
            {
                map.PropertyReference(typeof(CarExterior).GetProperty("Car"));
                map.Cascade(Cascade.All);
            });
            OneToOne(x => x.Entertainment, map =>
            {
                map.PropertyReference(typeof(CarEntertainment).GetProperty("Car"));
                map.Cascade(Cascade.All);
            });
        }
    }
}
