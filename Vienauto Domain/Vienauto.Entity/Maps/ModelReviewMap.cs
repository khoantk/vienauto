﻿using NHibernate.Mapping.ByCode;
using Vienauto.Entity.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Vienauto.Entity.Maps
{
    public class ModelReviewMap : ClassMapping<ModelReview>
    {
        public ModelReviewMap()
        {
            Table("Reviews_ModelDetail");
            Schema("hdt");
            Lazy(true);
            Id(x => x.IdReviews, map => { map.Column("Id_Reviews"); map.Generator(Generators.Identity); });
            Property(x => x.Email);
            Property(x => x.Phone);
            Property(x => x.Content);
            Property(x => x.Brand, map => map.NotNullable(true));
            Property(x => x.Model, map => map.NotNullable(true));
            Property(x => x.Year, map => map.NotNullable(true));
            Property(x => x.IdUsers, map => { map.Column("Id_Users"); map.NotNullable(true); });
            Property(x => x.PointBody, map => { map.Column("Point_Body"); map.NotNullable(true); });
            Property(x => x.PointSafe, map => { map.Column("Point_Safe"); map.NotNullable(true); });
            Property(x => x.PointOperation, map => { map.Column("Point_Operation"); map.NotNullable(true); });
            Property(x => x.PointPrice, map => { map.Column("Point_Price"); map.NotNullable(true); });
            Property(x => x.Name, map => map.NotNullable(true));
            Property(x => x.Title, map => map.NotNullable(true));
            Property(x => x.Datepost, map => map.NotNullable(true));
        }
    }
}
