﻿using NHibernate.Mapping.ByCode;
using Vienauto.Entity.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Vienauto.Entity.Maps
{
    public class CarBookingMap : ClassMapping<CarBooking>
    {
        public CarBookingMap()
        {
            Schema("hdt");
            Table("DatXe_Online");
            Lazy(true);
            Id(x => x.Id, map =>
            {
                map.Column("Id_DatXe_Online");
                map.Generator(Generators.Identity);
            });
            Property(x => x.FullName);
            Property(x => x.Email);
            Property(x => x.Address);
            Property(x => x.Phone);
            Property(x => x.NoiDung);
            Property(x => x.Xe);
            Property(x => x.Datepost);
            Property(x => x.Active);
            Property(x => x.New_Old);
            Property(x => x.Map);
            Property(x => x.Anh_Xe);
            Property(x => x.Mau_Xe);
            Property(x => x.Tuoi);
            Property(x => x.Gioi_Tinh);
            Property(x => x.Thoi_Gian);
            Property(x => x.Thoi_Gian_LH);
            Property(x => x.Hinh_Thuc_LH);
            Property(x => x.theodoi);
            ManyToOne(x => x.Car, map => map.Column("Id_MName"));
            ManyToOne(x => x.User, map => map.Column("Id_Users"));
            ManyToOne(x => x.Follower, map => map.Column("id_usertheodoi"));
            ManyToOne(x => x.Location, map => map.Column("Id_Location"));
            ManyToOne(x => x.Manufacturer, map => map.Column("Id_Hang"));
        }
    }
}
