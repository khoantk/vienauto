﻿using NHibernate.Mapping.ByCode;
using Vienauto.Entity.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Vienauto.Entity.Maps
{
    public class ModelAvatarMap : ClassMapping<ModelAvatar>
    {
        public ModelAvatarMap()
        {
            Schema("hdt");
            Table("AvatarMode");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.PathImage);
            ManyToOne(x => x.Manufacturer, map => map.Column("BrandId"));
            ManyToOne(x => x.Model, map => map.Column("ModeId"));
        }
    }
}
