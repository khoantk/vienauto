﻿using NHibernate.Mapping.ByCode;
using Vienauto.Entity.Entities;
using NHibernate.Mapping.ByCode.Conformist;

namespace Vienauto.Entity.Maps
{
    public class SettingMap : ClassMapping<Setting>
    {
        public SettingMap()
        {
            Schema("hdt");
            Table("Setting");
            Lazy(true);
            Id(x => x.Id, map =>
            {
                map.Column("Id_Setting");
                map.Generator(Generators.Identity);
            });
            Property(x => x.Exchange);
            Property(x => x.Start_ChangePoint);
            Property(x => x.Acount_Bank);
            Property(x => x.Title_web);
            Property(x => x.description_web);
            Property(x => x.keyword_web);
            Property(x => x.chitieu_doanhthu);
            Property(x => x.luongcoban);
            Property(x => x.fuel_price);
            Property(x => x.seopass);
        }
    }
}
