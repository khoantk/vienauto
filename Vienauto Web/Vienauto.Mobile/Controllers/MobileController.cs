﻿using System.Web.Mvc;
using Common.Core.Mvc;
using System.Collections.Generic;
using Common.Core.Extension.Html;
using System.Threading.Tasks;
using Vienauto.Interface.Services;
using Vienauto.Interface.AsyncServices;

namespace Vienauto.Mobile.Controllers
{
    public class MobileController : BaseController
    {
        private readonly ICarService _carService;
        private readonly IYearService _yearService;
        private readonly IModelService _modelService;
        private readonly IOtherService _otherService;
        private readonly IAgencyService _agencyService;
        private readonly IAccountService _accountService;
        private readonly IManufacturerService _manufacturerService;

        private readonly ICarAsyncService _carAsyncService;
        private readonly IYearAsyncService _yearAsyncService;
        private readonly IModelAsyncService _modelAsyncService;
        private readonly IOtherAsyncService _otherAsyncService;
        private readonly IAgencyAsyncService _agencyAsyncService;
        private readonly IAccountAsyncService _accountAsyncService;
        private readonly IManufacturerAsyncService _manufacturerAsyncService;

        //public MobileController(IManufacturerService manufacturerService, IModelService modelService, 
        //                        IYearService yearService, ICarService carService,
        //                        IOtherService otherService, IAccountService accountService, 
        //                        IAgencyService agencyService)
        //{
        //    _manufacturerService = manufacturerService;
        //    _modelService = modelService;
        //    _yearService = yearService;
        //    _carService = carService;
        //    _otherService = otherService;
        //    _agencyService = agencyService;
        //    _accountService = accountService;
        //}

        public MobileController(ICarAsyncService carAsyncService, IYearAsyncService yearAsyncService,
                                IModelAsyncService modelAsyncService, IOtherAsyncService otherAsyncService, 
                                IAgencyAsyncService agencyAsyncService, IAccountAsyncService accountAsyncService,
                                IManufacturerAsyncService manufacturerAsyncService)
        {
            _manufacturerAsyncService = manufacturerAsyncService;
            _modelAsyncService = modelAsyncService;
            _yearAsyncService = yearAsyncService;
            _carAsyncService = carAsyncService;
            _otherAsyncService = otherAsyncService;
            _agencyAsyncService = agencyAsyncService;
            _accountAsyncService = accountAsyncService;
        }
        
        public async Task<JsonResult> GetModelByManufacturer(int id)
        {
            //var result = _modelService.GetModelByManufacturer(id);
            var result = await _modelAsyncService.GetModelByManufacturerAsync(id);
            if (result.HasErrors)
                return JsonError("", false);
            return JsonSuccess("", result.Target);
        }

        public async Task<JsonResult> GetYearByModel(int id)
        {
            //var result = _yearService.GetYearByModel(id);
            var result = await _yearAsyncService.GetYearByModelAsync(id);
            if (result.HasErrors)
                return JsonError("", false);
            return JsonSuccess("", result.Target);
        }

        public async Task<JsonResult> GetCarByYear(int id)
        {
            //var result = _carService.GetCarByYear(id);
            var result = await _carAsyncService.GetCarByYearAsync(id);
            if (result.HasErrors)
                return JsonError("", false);
            return JsonSuccess("", result.Target);
        }
        
        public async Task<JsonResult> GetDealerShips()
        {
            //var result = _agencyService.GetAllDealerShips();
            var result = await _agencyAsyncService.GetAllDealerShipsAsync();
            if (result.HasErrors)
                return JsonError("", false);
            return JsonSuccess("", result.Target);
        }

        public async Task<JsonResult> GetProvinces()
        {
            //var result = _otherService.GetAllProvinces();
            var result = await _otherAsyncService.GetAllProvincesAsync();
            if (result.HasErrors)
                return JsonError("", false);
            return JsonSuccess("", result.Target);
        }

        public async Task<List<SelectListItem>> LoadManufacturers()
        {
            //var manufacturerDtos = _manufacturerService.GetAllManufacturer();
            var manufacturerDtos = await _manufacturerAsyncService.GetAllManufacturerAsync();
            if (!manufacturerDtos.HasErrors)
                return manufacturerDtos.Target.ToSelectList(m => m.Name, 
                                                            m => m.Id.ToString(), 
                                                            "Chọn hãng xe");
            return new List<SelectListItem>();
        }

        public async Task<List<SelectListItem>> LoadQuestions()
        {
            //var questionDtos = _otherService.GetAllQuestions();
            var questionDtos = await _otherAsyncService.GetAllQuestionsAsync();
            if (!questionDtos.HasErrors)
                return questionDtos.Target.ToSelectList(q => q.QuestionName, 
                                                        q => q.QuestionId.ToString(), 
                                                        "Chọn câu hỏi");
            return new List<SelectListItem>();
        }

        public async Task<List<SelectListItem>> LoadDealerShips()
        {
            //var dealerShipDtos = _agencyService.GetAllDealerShips();
            var dealerShipDtos = await _agencyAsyncService.GetAllDealerShipsAsync();
            if (!dealerShipDtos.HasErrors)
                return dealerShipDtos.Target.ToSelectList(ds => ds.ManufacturerName, 
                                                          ds => ds.ManufacturerId.ToString(), 
                                                          "Chọn hãng phân phối");
            return new List<SelectListItem>();
        }

        public async Task<List<SelectListItem>> LoadLocations()
        {
            //var locationDtos = _otherService.GetAllLocations();
            var locationDtos = await _otherAsyncService.GetAllLocationsAsync();
            if (!locationDtos.HasErrors)
            {
                return locationDtos.Target.ToSelectList(l => l.LocationName,
                                                        l => l.LocationId.ToString(),
                                                        "Chọn vị trí");
            }
            return new List<SelectListItem>();
        }

        public async Task<List<SelectListItem>> LoadProvinces()
        {
            //var provinceDtos = _otherService.GetAllProvinces();
            var provinceDtos = await _otherAsyncService.GetAllProvincesAsync();
            if (!provinceDtos.HasErrors)
                return provinceDtos.Target.ToSelectList(p => p.ProvinceName,
                                                        p => p.ProvinceId.ToString(),
                                                        "Chọn tỉnh thành");
            return new List<SelectListItem>();
        }
    }
}
