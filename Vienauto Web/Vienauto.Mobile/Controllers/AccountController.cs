﻿using Vendare.Utils;
using System.Web.Mvc;
using Common.Core.Mvc;
using Common.Core.Extension;
using System.Threading.Tasks;
using System.Security.Claims;
using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Mobile.Models.Form;
using Vienauto.Interface.Services;
using Vienauto.Interface.AsyncServices;
using Common.Core.Extension.Configuration;
using Common.Core.Mvc.Attribute;

namespace Vienauto.Mobile.Controllers
{
    public class AccountController : BaseController
    {
        private MobileController _mobileController;
        private readonly IOtherService _otherService;
        private readonly IAgencyService _agencyService;
        private readonly IAccountService _accountService;

        private readonly IOtherAsyncService _otherAsyncService;
        private readonly IAgencyAsyncService _agencyAsyncService;
        private readonly IAccountAsyncService _accountAsyncService;

        public AccountController(IOtherService othersService, IAccountService accountService, 
                                 IAgencyService agencyService, IOtherAsyncService otherAsyncService,
                                 IAccountAsyncService accountAsyncService, IAgencyAsyncService agencyAsyncService)
        {
            _otherService = othersService;
            _accountService = accountService;
            _agencyService = agencyService;

            _otherAsyncService = otherAsyncService;
            _agencyAsyncService = agencyAsyncService;
            _accountAsyncService = accountAsyncService;

            _mobileController = new MobileController(null, null, null, _otherAsyncService, 
                                                     _agencyAsyncService, _accountAsyncService, null);
        }

        public ActionResult SignIn(string returnUrl)
        {
            var accountModel = new Account();
            ViewBag.ReturnUrl = returnUrl;
            return View(accountModel);
        }
        
        public ActionResult RedirectSignIn(Account accounModel)
        {
            return View(accounModel);
        }

        public async Task<ActionResult> Register()
        {
            var registerModel = new Register();
            await LoadDropdownListRegisterModel(registerModel);
            return View(registerModel);
        }
        
        [HttpPost]
        [ValidateForgeryToken]
        public async Task<ActionResult> SignIn(Account model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                //var result = _accountService.AuthenticateUser(model.UserName, model.PassWord);
                var result = await _accountAsyncService.AuthenticateUserAsync(model.UserName, model.PassWord);
                if (!result.HasErrors)
                {
                    var claims = new Dictionary<string, string>()
                    {
                        [ClaimTypes.Name] = result.Target.FullName,
                        [ClaimTypes.Email] = result.Target.UserName,
                        [ClaimTypes.NameIdentifier] = result.Target.UserId.ToString()
                    };
                    LogIn(claims, false);
                    return RedirectToLocal(returnUrl);                    
                }
                else
                {
                    ModelState.AddModelError("Login", "Đăng nhập thất bại!");
                    return View(model);
                }
            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Register(Register registerModel)
        {
            if (ModelState.IsValid)
            {
                //var result = new DataServiceResult<RegisterDto>();
                //var registerDto = registerModel.TransForm();
                //if (registerModel.IsRegsiterAgent)
                //    //result = _accountService.SignUpAgentUser(registerDto);
                //    result = await _accountAsyncService.SignUpAgentUserAsync(registerDto);
                //else
                //    //result = _accountService.SignUpUser(registerDto);
                //    result = await _accountAsyncService.SignUpUserAsync(registerDto);

                var registerDto = registerModel.TransForm();
                var result = await _accountAsyncService.SignUpUserAsync(registerDto);
                if (!result.HasErrors)
                {
                    return PartialView("_RedirectSignIn", new Account
                    {
                        RememberMe = true,
                        UserName = registerModel.Email,
                        PassWord = registerModel.Password,
                        IsUserRegistered = true //registerModel.IsRegsiterAgent
                    });
                }
                ViewBag.ErrorMessage = result.Errors;
                await LoadDropdownListRegisterModel(registerModel);
                return View(registerModel);
            }
            await LoadDropdownListRegisterModel(registerModel);
            return View(registerModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignOut()
        {
            LogOut();
            return new RedirectResult("/");
        }

        public async Task<ActionResult> GetAgencyDealerShip(int dealerShipId)
        {
            //var agents = _agencyService.GetAgencyByDealerShip(dealerShipId);
            var agents = await _agencyAsyncService.GetAgencyByDealerShipAsync(dealerShipId);
            if (agents.HasErrors)
                return JsonError("Lỗi hiển thị đại lý. Liên hệ admin.", false);
            return JsonSuccess("Hiển thị đại lý thành công.", agents.Target);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("Index", "Home");
        }

        private async Task LoadDropdownListRegisterModel(Register registerFormModel)
        {
            registerFormModel.Questions = await _mobileController.LoadQuestions();
            registerFormModel.DealerShips = await _mobileController.LoadDealerShips();
            registerFormModel.Locations = await _mobileController.LoadLocations();
            registerFormModel.Agents = await LoadAgentsDealership(registerFormModel.DealerShipId);
            registerFormModel.TotalBranches = ConfigSection.GetDropDownList("TotalBranches", "Account");
            registerFormModel.NumberCarTransactions = ConfigSection.GetDropDownList("NumberCarTransactions", "Account");
            registerFormModel.CarDistributions = ConfigSection.GetDropDownList("CarDistributions", "Account");
            registerFormModel.IntroduceCustomer = ConfigSection.GetDropDownList("IntroduceCustomer", "Account");
            registerFormModel.YourCustomer = ConfigSection.GetDropDownList("YourCustomer", "Account");
            registerFormModel.HowToKnowUs = ConfigSection.GetDropDownList("HowToKnowUs", "Account");
        }

        private async Task<List<SelectListItem>> LoadAgentsDealership(int dealerShipId)
        {
            var selectList = new List<SelectListItem>();
            //var agentDtos = _agencyService.GetAgencyByDealerShip(dealerShipId);
            var agentDtos = await _agencyAsyncService.GetAgencyByDealerShipAsync(dealerShipId);
            if (!agentDtos.HasErrors)
                agentDtos.Target.ForEach(agent =>
                { selectList.Add(new SelectListItem { Text = agent.FullName, Value = agent.AgencyId.ToString() }); });
            return selectList;
        }
    }
}
