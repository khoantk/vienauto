﻿using System.Web.Mvc;
using Common.Core.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using Vienauto.Mobile.Models.View;
using Vienauto.Mobile.Configuration;
using Vienauto.Interface.AsyncServices;
using Vienauto.Entity.Enums;
using Vienauto.Mobile.Models.View.CarForSell;
using System.IO;
using Vienauto.Mobile.Configuration.SEO.Model;
using Vienauto.Mobile.Configuration.SEO;

namespace Vienauto.Mobile.Controllers
{
    public class SellController : BaseController
    {
        private readonly ISellAsyncService _sellAsyncService;

        public SellController(ISellAsyncService sellAsyncService)
        {
            _sellAsyncService = sellAsyncService;
        }
                
        public ActionResult Index(string manufacture, string model, string year)
        {
            return View();
        }

        public ActionResult Detail()
        {
            //var carForSellDetailDto = await _sellAsyncService.GetDetailInfoAndUpdateViewTimesAsync(productId);
            //if (!carForSellDetailDto.HasErrors)
            //{
            //    return View();
            //}
            //else if (carForSellDetailDto.Errors.Exists(e => e.Code == ErrorCode.UndefinedCarForSell))
            //{ 
            //    return ErrorContent("Không tồn tại xe đã chọn.");
            //}
            var car = new CarForSellDetail();
            return View(car);
        }

        public async Task<ActionResult> GetSellDetailOverview(int productId)
        {
            var error = string.Empty;
            var modelDetailOverviewViewModel = new OverviewModelDetail();
            var modelDetailOverviewDto = await _sellAsyncService.GetSellDetailOverview(productId);
            if (!modelDetailOverviewDto.HasErrors)
                return PartialView("_SellDetailOverviewContent", modelDetailOverviewViewModel.TransForm(modelDetailOverviewDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetSellDetailEngine(int productId)
        {
            var error = string.Empty;
            var modelDetailEngineViewModel = new EngineModelDetail();
            var modelDetailEngineDto = await _sellAsyncService.GetSellDetailEngine(productId);
            if (!modelDetailEngineDto.HasErrors)
                return PartialView("_SellDetailEngineContent", modelDetailEngineViewModel.TransForm(modelDetailEngineDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetSellDetailSafety(int productId)
        {
            var error = string.Empty;
            var modelDetailSafetyViewModel = new SafetyModelDetail();
            var modelDetailSafetyDto = await _sellAsyncService.GetSellDetailSafety(productId);
            if (!modelDetailSafetyDto.HasErrors)
                return PartialView("_SellDetailSafetyContent", modelDetailSafetyViewModel.TransForm(modelDetailSafetyDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetSellDetailEntertainment(int productId)
        {
            var error = string.Empty;
            var modelDetailEntertainmentViewModel = new EntertainmentModelDetail();
            var modelDetailEntertainmentDto = await _sellAsyncService.GetSellDetailEntertainment(productId);
            if (!modelDetailEntertainmentDto.HasErrors)
                return PartialView("_SellDetailEntertainmentContent", modelDetailEntertainmentViewModel.TransForm(modelDetailEntertainmentDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetSellDetailInterior(int productId)
        {
            var error = string.Empty;
            var modelDetailInteriorViewModel = new InteriorModelDetail();
            var modelDetailInteriorDto = await _sellAsyncService.GetSellDetailInterior(productId);
            if (!modelDetailInteriorDto.HasErrors)
                return PartialView("_SellDetailInteriorContent", modelDetailInteriorViewModel.TransForm(modelDetailInteriorDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetSellDetailExterior(int productId)
        {
            var error = string.Empty;
            var modelDetailExteriorViewModel = new ExteriorModelDetail();
            var modelDetailExteriorDto = await _sellAsyncService.GetSellDetailExterior(productId);
            if (!modelDetailExteriorDto.HasErrors)
                return PartialView("_SellDetailExteriorContent", modelDetailExteriorViewModel.TransForm(modelDetailExteriorDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetSellDetailColor(int productId)
        {
            var error = string.Empty;
            var modelDetailColorViewModel = new ColorModelDetail();
            var modelDetailColorDto = await _sellAsyncService.GetSellDetailColor(productId);
            if (!modelDetailColorDto.HasErrors)
                return PartialView("_SellDetailColorContent", modelDetailColorViewModel.TransForm(modelDetailColorDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetSellDetailImage(int productId)
        {
            var error = string.Empty;
            var modelDetailImageViewModel = new ImageModelDetail();
            var modelDetailImageDto = await _sellAsyncService.GetSellDetailImage(productId);
            if (!modelDetailImageDto.HasErrors)
            {
                modelDetailImageViewModel = modelDetailImageViewModel.TransForm(modelDetailImageDto.Target);
                int dem1 = GetNumberFilesInFolder("data-xe/" + modelDetailImageViewModel.PathImages);
                if (dem1 > 0)
                {
                    modelDetailImageViewModel.ImageList = FetchSlideImage("data-xe/" + modelDetailImageViewModel.PathImages, "");
                }
                else
                {
                    int dem2 = GetNumberFilesInFolder("data-dongxe-serv/" + modelDetailImageViewModel.PathImages);
                    modelDetailImageViewModel.ImageList = FetchSlideImage("data-dongxe-serv/" + modelDetailImageViewModel.PathImages, "");
                }
                return PartialView("_SellDetailImageContent", modelDetailImageViewModel);
            }
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        #region Private Method

        public List<string> FetchSlideImage(string _path, string _nameproduct)
        {
            string folder = Server.MapPath("~/Upload/" + _path);
            string _pathfile = "/Upload/" + _path + "/";
            List<string> imageLinks = new List<string>();
            if (Directory.Exists(folder))
            {
                string id = Request["kjhewusa"];
                string strFilter = "*.jpg;*.png;*.gif";
                string[] m_arExt = strFilter.Split(';');
                DirectoryInfo dir = new DirectoryInfo(folder);
                int control = 0;
                foreach (string filter in m_arExt)
                {
                    FileInfo[] strFiles = dir.GetFiles(filter, SearchOption.AllDirectories);
                    foreach (FileInfo Name in strFiles)
                    {
                        control++;
                        if (control < 21)
                            imageLinks.Add(string.Concat(AppSetting.RootURL, _pathfile, Name));
                    }
                }
                dir.Refresh();
            }
            return imageLinks;
        }

        private int GetNumberFilesInFolder(string path)
        {
            string folder = Server.MapPath("~/Upload/" + path);
            if (Directory.Exists(folder))
            {
                var count = 0;
                string strFilter = "*.jpg;*.png;*.gif";
                string[] m_arExt = strFilter.Split(';');
                DirectoryInfo dir = new DirectoryInfo(folder);
                foreach (string filter in m_arExt)
                    count += dir.GetFiles(filter, SearchOption.AllDirectories).Length;
                dir.Refresh();
                if (count > 0)
                    return count;
            }
            return 0;
        }

        private void AssignSEOInfo(ModelModule modelDetailParameter, ISEOSetting<ModelModule> modelDetailSetting)
        {
            var SEOSetting = new SEOSetting<ModelModule>(modelDetailSetting);
            SEOSetting.entity = new ModelModule
            {
                Manufacturer = modelDetailParameter.Manufacturer,
                Model = modelDetailParameter.Model,
                Year = modelDetailParameter.Year
            };
            SEOInformation = SEOSetting.GetSEOInformation();
        }

        #endregion

    }
}
