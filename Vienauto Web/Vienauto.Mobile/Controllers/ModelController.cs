﻿using System;
using System.IO;
using System.Web.Mvc;
using Common.Core.Mvc;
using Vienauto.Entity.Enums;
using Vienauto.Interface.Dto;
using System.Threading.Tasks;
using Common.Core.Data.Paging;
using Common.Core.Mvc.Attribute;
using System.Collections.Generic;
using Vienauto.Interface.Services;
using Vienauto.Mobile.Models.View;
using Vienauto.Mobile.Models.Form;
using Vienauto.Mobile.Configuration;
using Vienauto.Interface.AsyncServices;
using Vienauto.Mobile.Configuration.SEO;
using Vienauto.Mobile.Configuration.SEO.Model;

namespace Vienauto.Mobile.Controllers
{
    public class ModelController : BaseController
    {
        private readonly IModelService _modelService;
        private readonly IModelAsyncService _modelAsyncService;

        public ModelController(IModelService modelService, IModelAsyncService modelAsyncService)
        {
            _modelService = modelService;
            _modelAsyncService = modelAsyncService;
        }

        public async Task<ActionResult> Index(string manufacturer, string model, string year)
        {
            var error = string.Empty;
            var modelInformationView = new ModelDetail();
            var modelInformationDto = await _modelAsyncService.GetModelStandardInformationAsync(DecodeUrl(manufacturer), DecodeUrl(model), DecodeUrl(year), 0, UserId);
            if (!modelInformationDto.HasErrors)
            {
                //Embbed SEO
                var modelDetailParameter = new ModelModule
                {
                    Manufacturer = manufacturer,
                    Model = model,
                    Year = year
                };
                AssignSEOInfo(modelDetailParameter, new SEOModelIndex());
                EmbbedSEO();
                //End Embbed SEO
                return View(modelInformationView.TransForm(modelInformationDto.Target));
            }
            else if (modelInformationDto.Errors.Exists(e => e.Code == ErrorCode.UndefinedModel))
                error = "Không tồn tại dòng xe đã chọn.";
            return ErrorContent(error);
        }

        public async Task<ActionResult> Review(int manufacturerId, int modelId, int yearId)
        {
            var modelDetail = await _modelAsyncService.GetModelReviewDetailAsync(manufacturerId, modelId, yearId, UserId);
            if (!modelDetail.HasErrors)
            {
                //Embbed SEO
                var modelDetailParameter = new ModelModule
                {
                    Manufacturer = modelDetail.Target.Brand,
                    Model = modelDetail.Target.Model,
                    Year = modelDetail.Target.Year
                };
                AssignSEOInfo(modelDetailParameter, new SEOModelReview());
                EmbbedSEO();
                //End Embbed SEO
                var detailModeModel = new ModelReviewDetail().TransForm(modelDetail.Target);
                return View(detailModeModel);
            }
            return View(new ModelReviewDetail());
        }

        public async Task<ActionResult> Detail(string manufacturer, string model, string year, int carId = 0)
        {
            var modelInformationView = new ModelDetail();
            var modelInformationDto = await _modelAsyncService.GetModelStandardInformationAsync(DecodeUrl(manufacturer), DecodeUrl(model), DecodeUrl(year), carId, UserId);
            if (!modelInformationDto.HasErrors)
            {
                //Embbed SEO
                var modelDetailParameter = new ModelModule
                {
                    Manufacturer = manufacturer,
                    Model = model,
                    Year = year
                };
                AssignSEOInfo(modelDetailParameter, new SEOModelDetail());
                EmbbedSEO();
                //End Embbed SEO
                return View(modelInformationView.TransForm(modelInformationDto.Target));
            }
            else if (modelInformationDto.Errors.Exists(e => e.Code == ErrorCode.UndefinedModel))
            {
                return ErrorContent("Không tồn tại dòng xe đã chọn.");
            }
            return View();
        }

        public async Task<ActionResult> GetModelDetailOverview(int carId)
        {
            var error = string.Empty;
            var modelDetailOverviewViewModel = new OverviewModelDetail();
            var modelDetailOverviewDto = await _modelAsyncService.GetOverviewAsync(carId);
            if (!modelDetailOverviewDto.HasErrors)
                return PartialView("_ModelDetailOverviewContent", modelDetailOverviewViewModel.TransForm(modelDetailOverviewDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetModelDetailEngine(int carId)
        {
            var error = string.Empty;
            var modelDetailEngineViewModel = new EngineModelDetail();
            var modelDetailEngineDto = await _modelAsyncService.GetEngineAsync(carId);
            if (!modelDetailEngineDto.HasErrors)
                return PartialView("_ModelDetailEngineContent", modelDetailEngineViewModel.TransForm(modelDetailEngineDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetModelDetailSafety(int carId)
        {
            var error = string.Empty;
            var modelDetailSafetyViewModel = new SafetyModelDetail();
            var modelDetailSafetyDto = await _modelAsyncService.GetSafetyAsync(carId);
            if (!modelDetailSafetyDto.HasErrors)
                return PartialView("_ModelDetailSafetyContent", modelDetailSafetyViewModel.TransForm(modelDetailSafetyDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetModelDetailEntertainment(int carId)
        {
            var error = string.Empty;
            var modelDetailEntertainmentViewModel = new EntertainmentModelDetail();
            var modelDetailEntertainmentDto = await _modelAsyncService.GetEntertainmentAsync(carId);
            if (!modelDetailEntertainmentDto.HasErrors)
                return PartialView("_ModelDetailEntertainmentContent", modelDetailEntertainmentViewModel.TransForm(modelDetailEntertainmentDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetModelDetailInterior(int carId)
        {
            var error = string.Empty;
            var modelDetailInteriorViewModel = new InteriorModelDetail();
            var modelDetailInteriorDto = await _modelAsyncService.GetInteriorAsync(carId);
            if (!modelDetailInteriorDto.HasErrors)
                return PartialView("_ModelDetailInteriorContent", modelDetailInteriorViewModel.TransForm(modelDetailInteriorDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetModelDetailExterior(int carId)
        {
            var error = string.Empty;
            var modelDetailExteriorViewModel = new ExteriorModelDetail();
            var modelDetailExteriorDto = await _modelAsyncService.GetExteriorAsync(carId);
            if (!modelDetailExteriorDto.HasErrors)
                return PartialView("_ModelDetailExteriorContent", modelDetailExteriorViewModel.TransForm(modelDetailExteriorDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetModelDetailColor(int carId)
        {
            var error = string.Empty;
            var modelDetailColorViewModel = new ColorModelDetail();
            var modelDetailColorDto = await _modelAsyncService.GetColorAsync(carId);
            if (!modelDetailColorDto.HasErrors)
                return PartialView("_ModelDetailColorContent", modelDetailColorViewModel.TransForm(modelDetailColorDto.Target));
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> GetModelDetailImage(int carId)
        {
            var error = string.Empty;
            var modelDetailImageViewModel = new ImageModelDetail();
            var modelDetailImageDto = await _modelAsyncService.GetImageAsync(carId);
            if (!modelDetailImageDto.HasErrors)
            {
                modelDetailImageViewModel = modelDetailImageViewModel.TransForm(modelDetailImageDto.Target);
                int dem1 = GetNumberFilesInFolder("data-xe/" + modelDetailImageViewModel.PathImages);
                if (dem1 > 0)
                {
                    modelDetailImageViewModel.ImageList = FetchSlideImage("data-xe/" + modelDetailImageViewModel.PathImages, "");
                }
                else
                {
                    int dem2 = GetNumberFilesInFolder("data-dongxe-serv/" + modelDetailImageViewModel.PathImages);
                    modelDetailImageViewModel.ImageList = FetchSlideImage("data-dongxe-serv/" + modelDetailImageViewModel.PathImages, "");
                }
                return PartialView("_ModelDetailImageContent", modelDetailImageViewModel);
            }
            else
                error = ErrorMessage.ERROR_CONTACT_ADMIN;
            return ErrorContent(error);
        }

        public async Task<ActionResult> SameVersion(string manufacturer, string model, string year, int carId = 0)
        {
            var sameStyleViewModel = new ModelSameVersion();
            var modelDto = await _modelAsyncService.GetModelStandardInformationAsync(DecodeUrl(manufacturer), DecodeUrl(model), DecodeUrl(year), carId, UserId);
            if (!modelDto.HasErrors)
            {
                //Embbed SEO
                var modelDetailParameter = new ModelModule
                {
                    Manufacturer = manufacturer,
                    Model = model,
                    Year = year
                };
                AssignSEOInfo(modelDetailParameter, new SEOModelDetail());
                EmbbedSEO();
                //End Embbed SEO
                sameStyleViewModel.modelInformation = new ModelDetail().TransForm(modelDto.Target);
                var sameVersionsCarDto = await _modelAsyncService.GetSameVersionCarsAsync(manufacturer, model, year);
                if (!sameVersionsCarDto.HasErrors)
                    sameStyleViewModel.sameVersionCars = new ModelDetail().TransFormMany(sameVersionsCarDto.Target as List<ModelDto>);
            }
            return View(sameStyleViewModel);
        }

        public async Task<ActionResult> SameStyle(string manufacturer, string model, string year, int carId = 0)
        {
            var sameStyleModel = new ModelDetail();
            var modelDto = await _modelAsyncService.GetModelStandardInformationAsync(DecodeUrl(manufacturer), DecodeUrl(model), DecodeUrl(year), carId, UserId);
            if (!modelDto.HasErrors)
            {
                //Embbed SEO
                var modelDetailParameter = new ModelModule
                {
                    Manufacturer = manufacturer,
                    Model = model,
                    Year = year
                };
                AssignSEOInfo(modelDetailParameter, new SEOModelDetail());
                EmbbedSEO();
                //End Embbed SEO
                return View(sameStyleModel.TransForm(modelDto.Target));
            }
            return View(sameStyleModel);
        }

        public async Task<ActionResult> GetModelSameStylePaging(string manufacturer, string model, string year, string style,
                                                                string navigation, int page = 1, int size = 10, string sort = "",
                                                                int nextPage = 1, int prevPage = 1)
        {
            var modelPaging = new PagingRequest<Dictionary<string, object>>
                (
                    new PagingOption
                    {
                        PageIndex = page,
                        PageSize = size,
                        SortName = sort,
                        Action = (PagingAction)Enum.Parse(typeof(PagingAction), navigation)
                    }
                );

            modelPaging.FilterConditions = new Dictionary<string, object>
            {
                ["manufacturerName"] = DecodeUrl(manufacturer),
                ["modelName"] = DecodeUrl(model),
                ["yearName"] = DecodeUrl(year),
                ["styleName"] = DecodeUrl(style)
            };

            //var sameStyleModels = _modelService.GetSameStyleCarsWithModelPaging(modelPaging);
            var sameStyleModels = await _modelAsyncService.GetSameStyleCarsWithModelPagingAsync(modelPaging);
            if (!sameStyleModels.HasErrors)
            {
                var sameStyleModelsPaged = new PagingDataComponent<ModelDetail>();
                sameStyleModelsPaged.PageIndex = sameStyleModels.Paging.PageIndex;
                sameStyleModelsPaged.NextPage = sameStyleModels.Paging.NextPage;
                sameStyleModelsPaged.PreviousPage = sameStyleModels.Paging.PreviousPage;
                sameStyleModelsPaged.PageSize = sameStyleModels.Paging.PageSize;
                sameStyleModelsPaged.TotalPages = sameStyleModels.Paging.TotalPages;
                sameStyleModelsPaged.DataList = new ModelDetail().TransFormMany(sameStyleModels.Target as List<ModelDto>);

                if (sameStyleModelsPaged.DataList != null && sameStyleModelsPaged.DataList.Count > 0)
                    return PartialView("_ModelSameStyleContent", sameStyleModelsPaged);
                else
                    return JsonError("Không có xe nào được tìm thấy");
            }
            return JsonError("Lỗi xảy ra. Vui lòng liên hệ admin");
        }

        public async Task<ActionResult> GetModelReviewsPaging(string manufacturer, string model, string year,
                                                              string navigation, int page = 1,
                                                              int size = 10, string sort = "",
                                                              int nextPage = 1, int prevPage = 1)
        {
            var modelPaging = new PagingRequest<Dictionary<string, object>>
                (
                    new PagingOption
                    {
                        PageIndex = page,
                        PageSize = size,
                        SortName = sort,
                        Action = (PagingAction)Enum.Parse(typeof(PagingAction), navigation)
                    }
                );

            modelPaging.FilterConditions = new Dictionary<string, object>
            {
                { "manufacture", DecodeUrl(manufacturer) },
                { "model", DecodeUrl(model) },
                { "year", DecodeUrl(year) }
            };

            var modelsReviews = await _modelAsyncService.GetModelsReviewsWithPagingAsync(modelPaging);
            if (!modelsReviews.HasErrors)
            {
                var modelsReviewsPaged = new PagingDataComponent<ModelsReviewsViewModel>
                {
                    PageIndex = modelsReviews.Paging.PageIndex,
                    NextPage = modelsReviews.Paging.NextPage,
                    PreviousPage = modelsReviews.Paging.PreviousPage,
                    PageSize = modelsReviews.Paging.PageSize,
                    TotalPages = modelsReviews.Paging.TotalPages,
                    DataList = new ModelsReviewsViewModel().TransFormMany(modelsReviews.Target)
                };
                if (modelsReviewsPaged.DataList != null && modelsReviewsPaged.DataList.Count > 0)
                    return PartialView("_ModelsReviewsContent", modelsReviewsPaged);
                else
                    return JsonError("Hiện tại chưa có đánh giá nào");
            }
            return JsonError("Lỗi xảy ra. Vui lòng liên hệ admin");
        }

        public async Task<ActionResult> ShowModelReviewForm(string manufacturer, string model, string year)
        {
            var result = await _modelAsyncService.CheckCanWriteReviewModelAsync(DecodeUrl(manufacturer), DecodeUrl(model), DecodeUrl(year), UserId);
            if (!result.HasErrors)
                return PartialView("_ModelReviewsForm", new ReviewInputModel());
            else if (result.Errors.Exists(e => e.Code == ErrorCode.ExistedReviewModel))
                return JsonError("Dòng xe này đã được đánh giá");
            //else if (result.Errors.Exists(e => e.Code == ErrorCode.FailToCheckCanWriteReviewModel))
            //    return JsonError("Xảy ra lỗi khi kiểm tra đánh giá");
            return JsonError(ErrorMessage.ERROR_CONTACT_ADMIN);
        }

        [ValidateForgeryToken]
        public async Task<ActionResult> SaveModelReview(ReviewInputModel modelReviews)
        {
            if (ModelState.IsValid)
            {
                modelReviews.UserId = UserId;
                modelReviews.DatePost = DateTime.Now;
                modelReviews.Name = User.Identity.IsAuthenticated ? @User.Identity.Name : modelReviews.Name;
                var modelReviewsDto = modelReviews.TransForm();
                var result = await _modelAsyncService.AddModelReviewAsync(modelReviewsDto);
                if (!result.HasErrors)
                    return JsonSuccess("Cám ơn bạn đã viết đánh giá");
                else
                    return JsonError(ErrorMessage.ERROR_CONTACT_ADMIN);
            }
            return JsonError("Vui lòng kiểm tra lại thông tin đánh giá");
        }

        #region Private Method

        public List<string> FetchSlideImage(string _path, string _nameproduct)
        {
            string folder = Server.MapPath("~/Upload/" + _path);
            string _pathfile = "/Upload/" + _path + "/";
            List<string> imageLinks = new List<string>();
            if (Directory.Exists(folder))
            {
                string id = Request["kjhewusa"];
                string strFilter = "*.jpg;*.png;*.gif";
                string[] m_arExt = strFilter.Split(';');
                DirectoryInfo dir = new DirectoryInfo(folder);
                int control = 0;
                foreach (string filter in m_arExt)
                {
                    FileInfo[] strFiles = dir.GetFiles(filter, SearchOption.AllDirectories);
                    foreach (FileInfo Name in strFiles)
                    {
                        control++;
                        if (control < 21)
                            imageLinks.Add(string.Concat(AppSetting.RootURL, _pathfile, Name));
                    }
                }
                dir.Refresh();
            }
            return imageLinks;
        }

        private int GetNumberFilesInFolder(string path)
        {
            string folder = Server.MapPath("~/Upload/" + path);
            if (Directory.Exists(folder))
            {
                var count = 0;
                string strFilter = "*.jpg;*.png;*.gif";
                string[] m_arExt = strFilter.Split(';');
                DirectoryInfo dir = new DirectoryInfo(folder);
                foreach (string filter in m_arExt)
                    count += dir.GetFiles(filter, SearchOption.AllDirectories).Length;
                dir.Refresh();
                if (count > 0)
                    return count;
            }
            return 0;
        }

        private void AssignSEOInfo(ModelModule modelDetailParameter, ISEOSetting<ModelModule> modelDetailSetting)
        {
            var SEOSetting = new SEOSetting<ModelModule>(modelDetailSetting);
            SEOSetting.entity = new ModelModule
            {
                Manufacturer = modelDetailParameter.Manufacturer,
                Model = modelDetailParameter.Model,
                Year = modelDetailParameter.Year
            };
            SEOInformation = SEOSetting.GetSEOInformation();
        }

        #endregion
    }
}
