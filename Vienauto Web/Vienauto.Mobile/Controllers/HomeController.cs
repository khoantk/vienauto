﻿using System.Linq;
using System.Web.Mvc;
using Common.Core.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using Vienauto.Mobile.Models.Form;
using Vienauto.Mobile.Models.View;
using Vienauto.Interface.Services;
using Vienauto.Interface.AsyncServices;
using Common.Core.Extension.Configuration;

namespace Vienauto.Mobile.Controllers
{
    public class HomeController : BaseController
    {
        private MobileController _mobileController;
        private readonly ICarService _carService;
        private readonly IAgencyService _agencyService;
        private readonly IOtherService _otherService;
        private readonly IManufacturerService _manufacturerService;

        private readonly ICarAsyncService _carAsyncService;
        private readonly IOtherAsyncService _otherAsyncService;
        private readonly IAgencyAsyncService _agencyAsyncService;
        private readonly IManufacturerAsyncService _manufactureAsyncService;
        
        public HomeController(ICarService carService, IAgencyService agencyService,
                              IManufacturerService manufacturerService, IOtherService othersService,
                              ICarAsyncService carAsyncService, IOtherAsyncService otherAsyncService,
                              IAgencyAsyncService agencyAsyncService,
                              IManufacturerAsyncService manufactureAsyncService)
        {
            _carService = carService;
            _otherService = othersService;
            _agencyService = agencyService;
            _manufacturerService = manufacturerService;

            _carAsyncService = carAsyncService;
            _otherAsyncService = otherAsyncService;
            _agencyAsyncService = agencyAsyncService;
            _manufactureAsyncService = manufactureAsyncService;

            _mobileController = new MobileController(_carAsyncService, null, null, _otherAsyncService,
                                                     _agencyAsyncService, null, _manufactureAsyncService);
        }   

        public async Task<ActionResult> Index()
        {
            var homeModel = new BookingCarInput();
            homeModel.UserId = UserId;
            await LoadDropDownListHomePage(homeModel);
            return View(homeModel);
        }
        
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BookCar(BookingCarInput homeFormModel)
        {
            var carBookingDto = homeFormModel.TransForm();
            //var result = _carService.Booking(carBookingDto);
            var result = await _carAsyncService.BookingAsync(carBookingDto);

            if (result.HasErrors)
                ViewBag.ErrorMessage = result.Errors;
            ViewBag.TriggerNotification = true;

            await LoadDropDownListHomePage(homeFormModel);
            return View("Index", homeFormModel);
        }

        public async Task<ActionResult> GetAvatarByCar(int id)
        {
            //var result = _carService.GetAvatarByCarId(id);
            var result = await _carAsyncService.GetAvatarByCarIdAsync(id);
            if (result.HasErrors)
                return JsonError(string.Empty, false);
            return JsonSuccess(string.Empty, result.Target);
        }

        private async Task LoadDropDownListHomePage(BookingCarInput homeFormModel)
        {
            homeFormModel.Locations = await _mobileController.LoadLocations();
            homeFormModel.Manufacturers = await _mobileController.LoadManufacturers();
            homeFormModel.Models = new List<SelectListItem> { new SelectListItem { Text = "Chọn dòng xe", Value = "0" } };
            homeFormModel.Years = new List<SelectListItem> { new SelectListItem { Text = "Chọn năm", Value = "0" } };
            homeFormModel.Cars = new List<SelectListItem> { new SelectListItem { Text = "Chọn phiên bản", Value = "0" } };

            homeFormModel.InitialTabLoading = new InitialTabLoading();
            var carPrices = ConfigSection.GetDropDownList("CarPrice", "Home");
            homeFormModel.InitialTabLoading.CarFromPrices = carPrices.Where(p => p.Text != "&gt;300.000$").ToList();
            homeFormModel.InitialTabLoading.CarToPrices = carPrices.Where(p => p.Text != "&lt;20.000$").ToList();
            homeFormModel.InitialTabLoading.Manufacturers = homeFormModel.Manufacturers;
            homeFormModel.InitialTabLoading.Dealerships = await _mobileController.LoadDealerShips();
            homeFormModel.InitialTabLoading.Provinces = await _mobileController.LoadProvinces();
        }
    }
}
