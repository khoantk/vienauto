﻿using System;
using System.Web.Mvc;
using Common.Core.Mvc;
using Vienauto.Entity.Enums;
using System.Threading.Tasks;
using Common.Core.Data.Paging;
using Common.Core.Mvc.Attribute;
using System.Collections.Generic;
using Vienauto.Mobile.Models.View;
using Vienauto.Mobile.Models.Form;
using Vienauto.Interface.Services;
using Vienauto.Mobile.Configuration;
using Vienauto.Interface.AsyncServices;
using Vienauto.Mobile.Configuration.SEO;
using Vienauto.Mobile.Configuration.SEO.Agency;

namespace Vienauto.Mobile.Controllers
{
    public class AgencyController : BaseController
    {
        private readonly IAgencyService _agencyService;
        private readonly IAgencyAsyncService _agencyAsyncService;

        public AgencyController(IAgencyService agencyService, IAgencyAsyncService agencyAsyncService)
        {
            _agencyService = agencyService;
            _agencyAsyncService = agencyAsyncService;
        }

        public async Task<ActionResult> Detail(int agencyUserId, int manufacturerId, int agentId)
        {
            var agencyDetail = await _agencyAsyncService.GetAgencyDetailAsync(agencyUserId, manufacturerId, agentId);
            //Embbed SEO
            var agencyParameter = new AgencyModule
            {
                DealerShip = agencyDetail.Target.ManufacturerName,
                Province = agencyDetail.Target.ProvinceName
            };
            AssignSEOInfo(agencyParameter, new SEOAgencyDetail());
            EmbbedSEO();
            //End Embbed SEO
            return View(new AgencyOpenTime().TransForm(agencyDetail.Target));

            //if (!agencyDetail.HasErrors)
            //{
            //    //Embbed SEO
            //    var agencyParameter = new AgencyModule
            //    {
            //        DealerShip = agencyDetail.Target.ManufacturerName,
            //        Province = agencyDetail.Target.ProvinceName
            //    };
            //    AssignSEOInfo(agencyParameter, new SEODetailOfAgency());
            //    EmbbedSEO();
            //    //End Embbed SEO
            //    return View(new AgencyDetailViewModel().TransForm(agencyDetail.Target));
            //}
            //return View(new AgencyDetailViewModel());
        }

        public ActionResult Index(string dealership, string province)
        {
            //Embbed SEO
            var agencyParameter = new AgencyModule
            {
                DealerShip = dealership,
                Province = province
            };
            AssignSEOInfo(agencyParameter, new SEOAgencyIndex());
            EmbbedSEO();
            //End Embbed SEO
            return View(new PagingDataComponent<AgencyDetail>());
        }

        public async Task<ActionResult> Review(int agentId, int manufacturerId)
        {
            var agencyDetail = await _agencyAsyncService.GetAgencyReviewDetailAsync(agentId, manufacturerId, UserId);
            if (!agencyDetail.HasErrors)
            {
                //Embbed SEO
                var agencyParameter = new AgencyModule
                {
                    DealerShip = agencyDetail.Target.ManufacturerName,
                    Province = agencyDetail.Target.ProvinceName
                };
                AssignSEOInfo(agencyParameter, new SEOAgencyReview());
                EmbbedSEO();
                //End Embbed SEO
                var detailAgencyModel = new AgencyReviewDetail().TransForm(agencyDetail.Target);
                return View(detailAgencyModel);
            }
            return View(new AgencyReviewDetail());
        }

        public async Task<ActionResult> GetAgenciesPaging(string dealershipId, string provinceId, string navigation,
                                                          int page = 1, int size = 10, string sort = "", 
                                                          int nextPage = 1, int prevPage = 1)
        {
            var pagingOption = new PagingOption
            {
                PageIndex = page,
                PageSize = size,
                SortName = sort,
                Action = (PagingAction)Enum.Parse(typeof(PagingAction), navigation)
            };
            var angencyPaging = new PagingRequest<Dictionary<string, object>>(pagingOption);

            angencyPaging.FilterConditions = new Dictionary<string, object>
            {
                ["provinceId"] = provinceId,
                ["dealershipId"] = dealershipId
            };
            
            //var agenciesReviews = _agencyService.GetAgenciesWithPaging(angencyPaging);
            var agenciesReviews = await _agencyAsyncService.GetAgenciesWithPagingAsync(angencyPaging);
            if (!agenciesReviews.HasErrors)
            {
                var agenciesPaged = new PagingDataComponent<AgencyDetail>
                {
                    PageIndex = agenciesReviews.Paging.PageIndex,
                    NextPage = agenciesReviews.Paging.NextPage,
                    PreviousPage = agenciesReviews.Paging.PreviousPage,
                    PageSize = agenciesReviews.Paging.PageSize,
                    TotalPages = agenciesReviews.Paging.TotalPages,
                    DataList = new AgencyDetail().TransFormMany(agenciesReviews.Target)
                };
                if (agenciesPaged.DataList != null && agenciesPaged.DataList.Count > 0)
                    return PartialView("_AgenciesContent", agenciesPaged);
                else
                    return JsonError("Không có đại lý nào được tìm thấy");
            }
            return JsonError(ErrorMessage.ERROR_CONTACT_ADMIN);
        }

        public async Task<ActionResult> GetAgenciesReviewsPaging(int dealershipId, int manufacturerId,
                                                                 string navigation, int page = 1,
                                                                 int size = 10, string sort = "",
                                                                 int nextPage = 1, int prevPage = 1)
        {
            var pagingOption = new PagingOption
            {
                PageIndex = page,
                PageSize = size,
                SortName = sort,
                Action = (PagingAction)Enum.Parse(typeof(PagingAction), navigation)
            };
            var angencyPaging = new PagingRequest<Dictionary<string, object>>(pagingOption);

            angencyPaging.FilterConditions = new Dictionary<string, object>
            {
                { "agentId", dealershipId },
                { "manufacturerId", manufacturerId }
            };

            var agentsReviews = await _agencyAsyncService.GetAgenciesReviewsWithPagingAsync(angencyPaging);
            if (!agentsReviews.HasErrors)
            {
                var agenciesReviewsPaged = new PagingDataComponent<AgencyReviewViewModel>
                {
                    PageIndex = agentsReviews.Paging.PageIndex,
                    NextPage = agentsReviews.Paging.NextPage,
                    PreviousPage = agentsReviews.Paging.PreviousPage,
                    PageSize = agentsReviews.Paging.PageSize,
                    TotalPages = agentsReviews.Paging.TotalPages,
                    DataList = new AgencyReviewViewModel().TransFormMany(agentsReviews.Target)
                };
                if (agenciesReviewsPaged.DataList != null && agenciesReviewsPaged.DataList.Count > 0)
                    return PartialView("_AgenciesReviewsContent", agenciesReviewsPaged);
                else
                    return JsonError("Hiện tại chưa có đánh giá nào");
            }
            return JsonError(ErrorMessage.ERROR_CONTACT_ADMIN);
        }

        public async Task<ActionResult> ShowAgencyReviewForm(int agentId, string dealerShip)
        {
            var result = await _agencyAsyncService.CheckCanWriteReviewAgencyAsync(agentId, UserId);
            if (!result.HasErrors)
            {
                var model = new ReviewInputAgency();
                model.AgentName = dealerShip;
                return PartialView("_AgenciesReviewsForm", model);
            }
            else if (result.Errors.Exists(e => e.Code == ErrorCode.ExistedReviewAgency))
                return JsonError("Đại lý này đã được đánh giá");
            //else if (result.Errors.Exists(e => e.Code == ErrorCode.FailToCheckCanWriteReviewAgency))
            //    return JsonError("Xảy ra lỗi khi kiểm tra đánh giá");
            return JsonError(ErrorMessage.ERROR_CONTACT_ADMIN);
        }

        [ValidateForgeryToken]
        public async Task<ActionResult> SaveAgencyReview(ReviewInputAgency agentReviews)
        {
            if (ModelState.IsValid)
            {
                agentReviews.UserId = UserId;
                var agencyReviewsDto = agentReviews.TransForm();
                var result = await _agencyAsyncService.AddAgencyReviewAsync(agencyReviewsDto);
                if (!result.HasErrors)
                    return JsonSuccess("Cám ơn bạn đã viết đánh giá");
                else
                    return JsonError("Rất tiếc!!! Đã có lỗi xảy ra khi đánh giá");
            }
            return JsonError(ErrorMessage.ERROR_CONTACT_ADMIN);
        }

        private void AssignSEOInfo(AgencyModule agencyParameter, ISEOSetting<AgencyModule> agencySetting)
        {
            var SEOSetting = new SEOSetting<AgencyModule>(agencySetting);
            SEOSetting.entity = new AgencyModule
            {
                DealerShip = agencyParameter.DealerShip,
                Province = agencyParameter.Province
            };
            SEOInformation = SEOSetting.GetSEOInformation();
        }
    }
}
