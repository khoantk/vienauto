﻿using System.Web.Mvc;
using System.Collections.Generic;
using Common.Core.Extension.Html;

namespace Vienauto.Mobile.Models
{
    public static class BaseModel
    {
        public static string ViewRatings(double ratingValue)
        {
            var ratingPosition = "";
            if (ratingValue == 0.5)
                ratingPosition = "-71px";
            else if (ratingValue <= 1.0 && ratingValue > 0.5)
                ratingPosition = "-63px";
            else if (ratingValue == 1.5)
                ratingPosition = "-55px";
            else if (ratingValue <= 2.0 && ratingValue > 1.5)
                ratingPosition = "-47px";
            else if (ratingValue == 2.5)
                ratingPosition = "-39px";
            else if (ratingValue <= 3.0 && ratingValue > 2.5)
                ratingPosition = "-33px";
            else if (ratingValue == 3.5)
                ratingPosition = "-23px";
            else if (ratingValue <= 4.0 && ratingValue > 3.5)
                ratingPosition = "-15px";
            else if (ratingValue == 4.5)
                ratingPosition = "-7px";
            else if (ratingValue <= 5.0 && ratingValue > 4.5)
                ratingPosition = "0px";
            else
                ratingPosition = "-80px";
            return ratingPosition;
        }

        public static string ViewProgress(double value)
        {
            return ((16.0 / 80 * value) * 100).ToString() + "%";
        }

        public static string ViewPrice(double value, string unit)
        {
            return value != 0 ? $"{value.ToString()} {unit}" : "N/A";
        }

        public static string ViewPrice(string value, string unit)
        {
            return $"{value.ToString()} {unit}";
        }

        public static List<SelectListItem> DisplayItemsPerPage(string noOfItems = "")
        {
            return new List<string> { "5", "10", "15" }.ToSelectList<string>(x => x, x => x, noOfItems);
        }
    }
}
