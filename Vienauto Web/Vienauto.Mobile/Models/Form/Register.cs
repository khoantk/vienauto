﻿using System;
using System.Web.Mvc;
using Vienauto.Interface.Dto;
using System.Collections.Generic;
using Common.Core.Data.Transformation;
using System.ComponentModel.DataAnnotations;
using static Common.Core.Mvc.Security.PersistentXSS;

namespace Vienauto.Mobile.Models.Form
{
    public class Register : BaseTransform<Register, RegisterDto>
    {
        public Register()
        {
            QuestionId = 0;
            DealerShipId = 0;
            AgentId = 0;
            LocationId = 0;
            TotalBranchId = 2;
            IsRegsiterAgent = true;
            NumberCarTransactionId = "nhỏ hơn 5 chiếc";
            CarDistributionId = "Trong nước";
            IntroduceCustomerId = "Trong nước";
            YourCustomerId = "Tiếp thị quảng cáo";
            HowToKnowUsId = "Email quảng cáo";
        }

        public bool IsRegsiterAgent { get; set; }
        [Required(ErrorMessage = "Chưa nhập tên")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Chưa nhập họ cua ban")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Chưa nhập Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "Chưa nhập password")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Chưa xác nhận password")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password", ErrorMessage = "Mật khẩu không khớp")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Chưa trả lời câu hỏi")]
        public string Answer { get; set; }
        public string HomePhone { get; set; }
        public string Mobile { get; set; }
        public string AgentMobile { get; set; }
        public string AgentPhone { get; set; }
        public string AgentEmail { get; set; }
        public bool NeedConsultMore { get; set; }
        public bool IsUser { get; set; }
        public bool CreateOrders { get; set; }
        public bool AgreeWithUs { get; set; }
        [Required(ErrorMessage = "Chưa chọn câu hỏi")]
        public int QuestionId { get; set; }
        public int DealerShipId { get; set; }
        public int AgentId { get; set; }
        public int LocationId { get; set; }
        public int TotalBranchId { get; set; }
        public string NumberCarTransactionId { get; set; }
        public string CarDistributionId { get; set; }
        public string IntroduceCustomerId { get; set; }
        public string YourCustomerId { get; set; }
        public string HowToKnowUsId { get; set; }
        public List<SelectListItem> Questions { get; set; }        
        public List<SelectListItem> DealerShips { get; set; }
        public List<SelectListItem> Agents { get; set; }
        public List<SelectListItem> Locations { get; set; }
        public List<SelectListItem> TotalBranches { get; set; }
        public List<SelectListItem> NumberCarTransactions { get; set; }
        public List<SelectListItem> CarDistributions { get; set; }
        public List<SelectListItem> IntroduceCustomer { get; set; }
        public List<SelectListItem> YourCustomer { get; set; }
        public List<SelectListItem> HowToKnowUs { get; set; }

        public override RegisterDto TransForm()
        {
            ValidateAllInputsInModel(this);
            return new RegisterDto
            {
                //User
                Active = 1, //default is active
                LevelId = 5, //default is member
                Avatar = "",
                Discount = 0,
                ChangeSub = 0,
                ProvinceId = 1, //default is TP.HCM
                Mobile = this.Mobile,
                UserName = this.Email,
                JoinDate = DateTime.Now,
                Phone = this.HomePhone,
                PassWord = this.Password,
                QuestionId = this.QuestionId,
                AnswerQuestion = this.Answer,
                FullName = string.Join(" ", new string[] { this.FirstName, this.LastName }),
                //Agent
                //CompanyName = this.CompanyName,
                //TransactionAddress = this.TransactionAddress,
                //TaxNumber = this.TaxNumber,
                //DeputyFullName = this.Deputy,
                EmailVerification = this.Email,
                Location = this.LocationId.ToString(),
                TotalBranches = this.TotalBranchId,
                NumberCarTransaction = this.NumberCarTransactionId,
                CarDistribution = this.CarDistributionId,
                NeedConsultMore = this.NeedConsultMore,
                IntroduceCustomer = this.IntroduceCustomerId,
                YourCustomer = this.YourCustomerId,
                HowToKnowUs = this.HowToKnowUsId,
                IsUser = this.IsUser,
                CreateOrders = this.CreateOrders,
                AgentPhone = this.HomePhone
            };
        }
    }
}
