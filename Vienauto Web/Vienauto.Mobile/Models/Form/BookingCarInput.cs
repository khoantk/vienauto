﻿using System;
using System.Web.Mvc;
using System.Globalization;
using Vienauto.Interface.Dto;
using Common.Core.Mvc.Attribute;
using System.Collections.Generic;
using Vienauto.Mobile.Models.View;
using Common.Core.Data.Transformation;
using System.ComponentModel.DataAnnotations;
using static Common.Core.Mvc.Security.PersistentXSS;

namespace Vienauto.Mobile.Models.Form
{
    public class ContactTime
    {
        public string Name { get; set; }
        public string Label { get; set; }
    }

    public class BookingCarInput : BaseTransform<BookingCarInput, CarBookingDto>
    {
        public BookingCarInput()
        {
            CarId = 0;
            YearId = 0;
            ModelId = 0;
            IsNew = true;
            Gender = true;
            LocationId = 0;
            ManufacturerId = 0;
            CarImg = "http://www.vienauto.com/images/files/car_unavaluabel.png";
        }

        [Required(ErrorMessage = "Chưa nhập họ và tên")]
        public string FullName { get; set; }
        [EmailAddress(ErrorMessage = "Địa chỉ email không tồn tại")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Chưa nhập địa chỉ")]
        public string Address { get; set; }
        [VietnamPhone]
        public string Phone { get; set; }
        public string CarImg { get; set; }
        public bool IsNew { get; set; }
        public int LocationId { get; set; }
        //public string AgeId { get; set; }
        public int UserId { get; set; }
        public int FollowerId { get; set; }
        [Range(1, 99999, ErrorMessage = "Chưa chọn hãng xe")]
        public int ManufacturerId { get; set; }
        [Range(1, 99999, ErrorMessage = "Chưa chọn dòng xe")]
        public int ModelId { get; set; }
        [Range(1, 99999, ErrorMessage = "Chưa chọn năm sản xuất")]
        public int YearId { get; set; }
        [Range(1, 99999, ErrorMessage = "Chưa chọn phiên bản")]
        public int CarId { get; set; }
        [Required(ErrorMessage = "Chưa chọn ngày nhận xe")]
        public string ReceiveDate { get; set; }
        [MinLength(30, ErrorMessage = "Nội dung liên hệ quá ngắn")]
        public string Content { get; set; }
        public bool Gender { get; set; }
        public List<SelectListItem> Locations { get; set; }
        //public List<SelectListItem> Ages { get; set; }
        public List<SelectListItem> Manufacturers { get; set; }
        public List<SelectListItem> Models { get; set; }
        public List<SelectListItem> Years { get; set; }
        public List<SelectListItem> Cars { get; set; }

        //TabSearchingViewModel
        public InitialTabLoading InitialTabLoading { get; set; }

        public override CarBookingDto TransForm()
        {
            ValidateAllInputsInModel<BookingCarInput>(this);
            return new CarBookingDto
            {
                FullName = this.FullName,
                Email = this.Email,
                Address = this.Address,
                Phone = this.Phone,
                IsNew = this.IsNew,
                Content = this.Content,
                CarId = this.CarId,
                UserId = this.UserId,
                FollowerId = this.FollowerId,
                LocationId = this.LocationId,
                ManufacturerId = this.ManufacturerId,
                Map = string.Empty,
                CarImage = this.CarImg,
                Gender = this.Gender ? "Nam" : "Nữ",
                CarColor = string.Empty,
                ContactTime = string.Empty,
                ContactForm = string.Empty,
                ReceiveDate = DateTime.ParseExact(this.ReceiveDate, "mm/dd/yyyy", new CultureInfo("en-US")),
                //Age = ConfigSection.GetTextValueDropDownList(this.AgeId, "Age", "Home", DDLSearchTermType.Text)
            };
        }
    }

}
