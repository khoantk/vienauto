﻿namespace Vienauto.Mobile.Models.Form
{
    public enum CarType
    {
        New,
        Old
    }

    public class CarForSellFilter
    {
        public int Seat { get; set; }
        public string[] Price { get; set; }
        public string[] YearName { get; set; }
        public int[] StyleId { get; set; }
        public int[] LocationId { get; set; }
        public int[] ManufacturerId { get; set; }
        public CarType Type { get; set; }
    }
}