﻿using System;
using Vienauto.Interface.Dto;
using Common.Core.Data.Transformation;
using System.ComponentModel.DataAnnotations;
using static Common.Core.Mvc.Security.PersistentXSS;

namespace Vienauto.Mobile.Models.Form
{
    public class ReviewInputAgency : BaseTransform<ReviewInputAgency, AgencyDetailReviewDto>
    {
        public int UserId { get; set; }
        public int AgentId { get; set; }
        public string AgentName { get; set; }
        public DateTime DatetimeReview { get; set; }
        [Required(ErrorMessage = "Bạn chưa đánh giá tiêu chí chăm sóc khách hàng")]
        public int CustomerService { get; set; }
        [Required(ErrorMessage = "Bạn chưa đánh giá tiêu chí thủ tục mua bán")]
        public int BuyingProcess { get; set; }
        [Required(ErrorMessage = "Bạn chưa đánh giá tiêu chí chất lượng bảo trì")]
        public int QualityRepair { get; set; }
        [Required(ErrorMessage = "Bạn chưa đánh giá tiêu chí tiện lợi")]
        public int Facilities { get; set; }
        //[Required(ErrorMessage = "Tiêu đề đánh giá không được để trống")]
        [StringLength(150, ErrorMessage = "Tiêu đề đánh giá quá dài")]
        public string TitleReview { get; set; }
        //[Required(ErrorMessage = "Nội dung đánh giá không được để trống")]
        //[StringLength(4000, ErrorMessage = "Nội dung đánh giá quá dài")]
        public string ContentReview { get; set; }
        [Required(ErrorMessage = "Tên không được để trống")]
        [StringLength(150, ErrorMessage = "Tên đánh giá quá dài")]
        public string Name { get; set; }        
        [EmailAddress(ErrorMessage = "Email không hợp lệ")]
        public string Email { get; set; }
        [RegularExpression("([0-9]+)", ErrorMessage = "Số điện thoại không hợp lệ")]
        public string Phone { get; set; }

        public override AgencyDetailReviewDto TransForm()
        {
            ValidateAllInputsInModel(this);
            return new AgencyDetailReviewDto()
            {
                AgentId = this.AgentId,
                CustomerService = this.CustomerService,
                BuyingProcess = this.BuyingProcess,
                QualityOfRepair = this.QualityRepair,
                Facilities = this.Facilities,
                TitleReview = this.TitleReview,
                ContentReview = this.ContentReview,
                Name = this.Name,
                Email = this.Email,
                Phone = this.Phone,
                UserId = this.UserId,
                DatetimeReview = this.DatetimeReview
            };
        }
    }
}
