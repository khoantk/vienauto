﻿using System;
using Vienauto.Interface.Dto;
using Common.Core.Data.Transformation;
using System.ComponentModel.DataAnnotations;
using static Common.Core.Mvc.Security.PersistentXSS;

namespace Vienauto.Mobile.Models.Form
{
    public class ReviewInputModel : BaseTransform<ReviewInputModel, ModelReviewsDto>
    {
        public int UserId { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public string Content { get; set; }
        public DateTime DatePost { get; set; }
        [Required(ErrorMessage = "Bạn chưa đánh giá kiểu dáng thiết kế")]
        public int PointBody { get; set; }
        [Required(ErrorMessage = "Bạn chưa đánh giá độ an toàn")]
        public int PointSafe { get; set; }
        [Required(ErrorMessage = "Bạn chưa đánh giá sự vận hành")]
        public int PointOperation { get; set; }
        [Required(ErrorMessage = "Bạn chưa đánh giá về giá cả hợp lý")]
        public int PointPrice { get; set; }
        [Required(ErrorMessage = "Tên không được để trống")]
        [StringLength(150, ErrorMessage = "Tên đánh giá quá dài")]
        public string Name { get; set; }
        [StringLength(150, ErrorMessage = "Tiêu đề đánh giá quá dài")]
        public string Title { get; set; }
        [EmailAddress(ErrorMessage = "Email không hợp lệ")]
        public string Email { get; set; }
        [RegularExpression("([0-9]+)", ErrorMessage = "Số điện thoại không hợp lệ")]
        public string Phone { get; set; }

        public override ModelReviewsDto TransForm()
        {
            ValidateAllInputsInModel(this);
            return new ModelReviewsDto()
            {
                Brand = this.Brand,
                Model = this.Model,
                Year = this.Year,
                PointBody = this.PointBody,
                PointSafe = this.PointSafe,
                PointOperation = this.PointOperation,
                PointPrice = this.PointPrice,
                DatePost = this.DatePost,
                UserId = this.UserId,
                Title = this.Title,
                Content = this.Content,
                Name = this.Name,
                Email = this.Email,
                Phone = this.Phone
            };
        }
    }
    
}