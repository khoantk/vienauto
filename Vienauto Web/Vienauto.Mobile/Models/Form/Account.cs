﻿using System.ComponentModel.DataAnnotations;

namespace Vienauto.Mobile.Models.Form
{
    public class Account
    {
        [Required(ErrorMessage = "Chưa nhập tên đăng nhập")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Chưa nhập mật khẩu")]
        public string PassWord { get; set; }
        [Display(Name = "Remember me?")]
        public bool? RememberMe { get; set; }
        public bool IsUserRegistered { get; set; }
    }
}