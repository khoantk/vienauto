﻿namespace Vienauto.Mobile.Models.View
{
    public class ProvinceDetail
    {
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }
    }
}
