﻿namespace Vienauto.Mobile.Models.View
{
    public class DealerShipModel
    {
        public int DealerShipId { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
    }
}
