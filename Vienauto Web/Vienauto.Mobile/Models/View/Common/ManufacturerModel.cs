﻿namespace Vienauto.Mobile.Models.View
{
    public class ManufacturerModel
    {
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
    }
}
