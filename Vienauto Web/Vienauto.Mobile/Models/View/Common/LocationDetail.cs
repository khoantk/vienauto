﻿namespace Vienauto.Mobile.Models.View
{
    public class LocationDetail
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
    }
}