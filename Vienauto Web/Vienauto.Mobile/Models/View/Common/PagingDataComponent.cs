﻿using System.Collections.Generic;

namespace Vienauto.Mobile.Models.View
{
    public class PagingComponent
    {
        public long TotalPages { get; set; }
        public int NextPage { get; set; }
        public int PreviousPage { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Sort { get; set; }
    }
    
    public class PagingDataComponent<TList> : PagingComponent where TList : class, new()
    {
        public IList<TList> DataList { get; set; }
    }
}
