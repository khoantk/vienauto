﻿namespace Vienauto.Mobile.Models.View
{
    public class QuestionDetail
    {
        public int QuestionId { get; set; }
        public string QuestionName { get; set; }
    }
}