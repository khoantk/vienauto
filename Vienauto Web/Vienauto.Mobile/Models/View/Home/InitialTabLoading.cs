﻿using System.Web.Mvc;
using System.Collections.Generic;

namespace Vienauto.Mobile.Models.View
{
    public class InitialTabLoading
    {
        public InitialTabLoading()
        {
            CarFromPrices = new List<SelectListItem>();
            CarToPrices = new List<SelectListItem>();
            Manufacturers = new List<SelectListItem>();
            Dealerships = new List<SelectListItem>();
            Provinces = new List<SelectListItem>();
        }
        public List<SelectListItem> CarFromPrices { get; set; }
        public List<SelectListItem> CarToPrices { get; set; }
        public List<SelectListItem> Manufacturers { get; set; }
        public List<SelectListItem> Dealerships { get; set; }
        public List<SelectListItem> Provinces { get; set; }
    }
}
