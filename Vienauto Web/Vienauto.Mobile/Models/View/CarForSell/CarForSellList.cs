﻿using Vienauto.Mobile.Models.Form;

namespace Vienauto.Mobile.Models.View
{
    public class CarForSellList
    {
        public CarForSellFilter Filter { get; set; }
    }
}