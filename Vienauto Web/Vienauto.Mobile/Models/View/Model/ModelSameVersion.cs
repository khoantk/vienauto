﻿using System.Collections.Generic;

namespace Vienauto.Mobile.Models.View
{
    public class ModelSameVersion
    {
        public ModelSameVersion()
        {
            sameVersionCars = new List<ModelDetail>();
        }
        public ModelDetail modelInformation { get; set; }
        public List<ModelDetail> sameVersionCars { get; set; }
    }
}