﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vienauto.Interface.Dto;
using Common.Core.Data.Transformation;

namespace Vienauto.Mobile.Models.View
{
    public class ModelsReviewsViewModel : BaseTransform<ModelReviewsDto, ModelsReviewsViewModel>
    {
        public int IdReviews { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public int PointBody { get; set; }
        public int PointSafe { get; set; }
        public int PointOperation { get; set; }
        public int PointPrice { get; set; }
        public double Overall { get { return (PointBody + PointSafe + PointPrice + PointOperation) / 4; } }
        public string Content { get; set; }
        public DateTime DatePost { get; set; }        
        public string BodyReview { get { return BaseModel.ViewProgress(PointBody); } }
        public string SafeReview { get { return BaseModel.ViewProgress(PointSafe); } }
        public string OperationReview { get { return BaseModel.ViewProgress(PointOperation); } }
        public string PriceReview { get { return BaseModel.ViewProgress(PointPrice); } }
        public string OverallReview { get { return BaseModel.ViewRatings(Overall); } }

        public override ModelsReviewsViewModel TransForm(ModelReviewsDto dto)
        {
            return new ModelsReviewsViewModel
            {
                IdReviews = dto.IdReviews,
                UserId = dto.UserId,
                UserName = dto.UserName,
                Brand = dto.Brand,
                Model = dto.Model,
                Year = dto.Year,
                PointBody = dto.PointBody,
                PointSafe = dto.PointSafe,
                PointOperation = dto.PointOperation,
                PointPrice = dto.PointPrice,                
                Content = dto.Content,
                DatePost = dto.DatePost                
            };
        }
    }
}