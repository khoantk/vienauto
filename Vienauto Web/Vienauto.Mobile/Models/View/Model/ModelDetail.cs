﻿using System;
using System.Globalization;
using Vienauto.Interface.Dto;
using System.Collections.Generic;
using Common.Core.Data.Transformation;
using static Vienauto.Mobile.Models.BaseModel;
using static Vienauto.Mobile.Configuration.AppSetting;

namespace Vienauto.Mobile.Models.View
{
    public class ModelDetail : BaseTransform<ModelDto, ModelDetail>
    {
        public int ManufacturerId { get; set; }
        public int ModelId { get; set; }
        public int YearId { get; set; }
        public int CarId { get; set; }
        public int ProductId { get; set; }
        public string Price { get; set; }        
        public string UnitName { get; set; }
        public string Avatar { get; set; }
        public string CarName { get; set; }
        public string ManufacturerName { get; set; }
        public string ModelName { get; set; }
        public string YearName { get; set; }
        public string StyleName { get; set; }
        public string FuelEconomyCity { get; set; }
        public string FuelEconomyHighway { get; set; }
        public int TotalReview { get; set; }
        public double MaxReview { get; set; }
        public string MaxRating { get { return ViewRatings(MaxReview); } }

        public override ModelDetail TransForm(ModelDto modelDto)
        {
            var detailModel = new ModelDetail();
            if (modelDto != null)
            {
                detailModel.CarId = modelDto.CarId;
                detailModel.YearId = modelDto.YearId;
                detailModel.ModelId = modelDto.ModelId;
                detailModel.ProductId = modelDto.ProductId;
                detailModel.ManufacturerId = modelDto.ManufacturerId;
                detailModel.UnitName = modelDto.UnitName;
                detailModel.CarName = modelDto.CarName;
                detailModel.ManufacturerName = modelDto.ManufacturerName;
                detailModel.ModelName = modelDto.ModelName;
                detailModel.YearName = modelDto.YearName;
                detailModel.StyleName = modelDto.StyleName;
                detailModel.FuelEconomyCity = modelDto.FuelEconomyCity;
                detailModel.FuelEconomyHighway = modelDto.FuelEconomyHighway;
                detailModel.MaxReview = modelDto.MaxPointReview;
                detailModel.TotalReview = modelDto.TotalReview;
                detailModel.Price = ViewPrice(modelDto.Price, modelDto.UnitName);
                //detailModel.Price = (modelDetailDto.Price != 0)? ViewPrice(StringExtensions.CurrencyToText(StringExtensions.SlipArray(modelDetailDto.Price.ToString())), modelDetailDto.UnitName) : "N/A";
                detailModel.Avatar = !string.IsNullOrEmpty(modelDto.Avatar) ?
                                      $"{RootURL}Upload{modelDto.Avatar}" :
                                      $"{RootURL}images/files/car_unavaluabel.png";
            }
            return detailModel;
        }
    }

    public class OverviewModelDetail : BaseTransform<ModelDetailOverviewDto, OverviewModelDetail>
    {
        public string Introduction { get; set; }

        public override OverviewModelDetail TransForm(ModelDetailOverviewDto dto)
        {
            var modelOverview = new OverviewModelDetail();
            if(dto != null)
            {
                modelOverview.Introduction = dto.Introduction;
            }
            return modelOverview;
        }
    }

    public class EngineModelDetail : BaseTransform<ModelDetailEngineDto, EngineModelDetail>
    {
        public string EngineName { get; set; }
        public string EngineType { get; set; }
        public string CompressionRatio { get; set; }
        public string DrivingType { get; set; }
        public string TranmissionType { get; set; }
        public string FuelType { get; set; }
        public string FuelEconomyCity { get; set; }
        public string FuelEconomyHighway { get; set; }
        public string Locking { get; set; }
        public string EngineValvetrain { get; set; }
        public string ESS { get; set; }
        public string EBD { get; set; }
        public string RemoteVehicle { get; set; }
        public string Tranmission { get; set; }
        public string ExteriorLength { get; set; }
        public string ExteriorWidth { get; set; }
        public string ExteriorHeight { get; set; }
        public string HorsePower { get; set; }
        public string Torque { get; set; }
        public string DragCoeficient { get; set; }
        public string TimeSpeed { get; set; }
        public string Km { get; set; }
        public string CurbWeight { get; set; }
        public string GVWR { get; set; }

        public override EngineModelDetail TransForm(ModelDetailEngineDto dto)
        {
            var modelEngine = new EngineModelDetail();
            if (dto != null)
            {
                modelEngine.EngineName = dto.EngineName;
                modelEngine.EngineType = dto.EngineType;
                modelEngine.CompressionRatio = dto.CompressionRatio;
                modelEngine.DrivingType = dto.DrivingType;
                modelEngine.TranmissionType = dto.TranmissionType;
                modelEngine.FuelType = dto.FuelType;
                modelEngine.FuelEconomyCity = dto.FuelEconomyCity;
                modelEngine.FuelEconomyHighway = dto.FuelEconomyHighway;
                modelEngine.Locking = Math.Round(double.Parse(dto.Locking, CultureInfo.InvariantCulture), 2).ToString();
                modelEngine.EngineValvetrain = dto.EngineValvetrain;
                modelEngine.ESS = dto.ESS;
                modelEngine.EBD = dto.EBD;
                modelEngine.RemoteVehicle = dto.RemoteVehicle;
                modelEngine.Tranmission = dto.Tranmission;
                modelEngine.ExteriorLength = Math.Round(double.Parse(dto.ExteriorLength, CultureInfo.InvariantCulture), 2).ToString();
                modelEngine.ExteriorWidth = Math.Round(double.Parse(dto.ExteriorWidth, CultureInfo.InvariantCulture), 2).ToString();
                modelEngine.ExteriorHeight = Math.Round(double.Parse(dto.ExteriorHeight, CultureInfo.InvariantCulture), 2).ToString();
                modelEngine.HorsePower = dto.HorsePower;
                modelEngine.Torque = dto.Torque;
                modelEngine.DragCoeficient = dto.DragCoeficient;
                modelEngine.TimeSpeed = dto.TimeSpeed;
                modelEngine.Km = dto.Km;
                modelEngine.CurbWeight = dto.CurbWeight;
                modelEngine.GVWR = dto.GVWR;
            }
            return modelEngine;
        }
    }

    public class SafetyModelDetail : BaseTransform<ModelDetailSafetyDto, SafetyModelDetail>
    {
        public string FrontAirbag { get; set; }
        public string SideAirbag { get; set; }
        public string BackAirbag { get; set; }
        public string HeadLights { get; set; }
        public string ExteriorLightControl { get; set; }
        public string DayLights { get; set; }
        public string LedTailLights { get; set; }
        public string ParkingAssist { get; set; }
        public string AlarmSystem { get; set; }
        public string DoorLockSystem { get; set; }
        public string ChildLock { get; set; }
        public string AntiTheftSystem { get; set; }
        public string LowPressureWarning { get; set; }
        public string Brakes { get; set; }
        public string ABSBrakes { get; set; }
        public string BrakeAssist { get; set; }

        public override SafetyModelDetail TransForm(ModelDetailSafetyDto dto)
        {
            var modelSafety = new SafetyModelDetail();
            if (dto != null)
            {
                modelSafety.FrontAirbag = dto.FrontAirbag == "1" ? "Có" :
                                         (dto.FrontAirbag == "0" ? "Không" : "");
                modelSafety.SideAirbag = dto.SideAirbag == "1" ? "Có" :
                                        (dto.SideAirbag == "0" ? "Không" : "");
                modelSafety.BackAirbag = dto.BackAirbag;
                modelSafety.HeadLights = dto.HeadLights;
                modelSafety.ExteriorLightControl = dto.ExteriorLightControl;
                modelSafety.DayLights = dto.DayLights;
                modelSafety.LedTailLights = dto.LedTailLights;
                modelSafety.ParkingAssist = dto.ParkingAssist;
                modelSafety.AlarmSystem = dto.AlarmSystem;
                modelSafety.DoorLockSystem = dto.DoorLockSystem;
                modelSafety.ChildLock = dto.ChildLock;
                modelSafety.AntiTheftSystem = dto.AntiTheftSystem;
                modelSafety.LowPressureWarning = dto.LowPressureWarning;
                modelSafety.Brakes = dto.Brakes;
                modelSafety.ABSBrakes = dto.ABSBrakes;
                modelSafety.BrakeAssist = dto.BrakeAssist;                
            }
            return modelSafety;
        }
    }

    public class EntertainmentModelDetail : BaseTransform<ModelDetailEntertainmentDto, EntertainmentModelDetail>
    {
        public string Radio { get; set; }
        public string CDPlayer { get; set; }
        public string DVDAudio { get; set; }
        public string SpeechRecognition { get; set; }
        public int Speakers { get; set; }
        public string Amplifier { get; set; }
        public string BluetoothCompatibility { get; set; }
        public string WifiCompatibility { get; set; }
        public string ThreeGCompatibility { get; set; }
        public string GPSCompatibility { get; set; }

        public override EntertainmentModelDetail TransForm(ModelDetailEntertainmentDto dto)
        {
            var modelEntertainment = new EntertainmentModelDetail();
            if (dto != null)
            {
                modelEntertainment.Radio = dto.Radio;
                modelEntertainment.CDPlayer = dto.CDPlayer;
                modelEntertainment.DVDAudio = dto.DVDAudio;
                modelEntertainment.SpeechRecognition = dto.SpeechRecognition;
                modelEntertainment.Speakers = dto.Speakers;
                modelEntertainment.Amplifier = dto.Amplifier;
                modelEntertainment.BluetoothCompatibility = dto.BluetoothCompatibility;
                modelEntertainment.WifiCompatibility = dto.WifiCompatibility;
                modelEntertainment.ThreeGCompatibility = dto.ThreeGCompatibility;
                modelEntertainment.GPSCompatibility = dto.GPSCompatibility;                
            }
            return modelEntertainment;
        }
    }

    public class InteriorModelDetail : BaseTransform<ModelDetailInteriorDto, InteriorModelDetail>
    {
        public int FrontAirConditioning { get; set; }
        public int BackAirConditioning { get; set; }
        public int AirFilter { get; set; }
        public int Seating { get; set; }
        public string FrontSeatType { get; set; }
        public string Shampoo { get; set; }
        public string Compass { get; set; }
        public string Clocks { get; set; }
        public int DCPowerOutlet { get; set; }
        public string Tachometer { get; set; }
        public string WaterTempGauges { get; set; }
        public string LowFuelWarning { get; set; }
        public string DisplayOutsideTemp { get; set; }
        public string FlashInterior { get; set; }

        public override InteriorModelDetail TransForm(ModelDetailInteriorDto dto)
        {
            var modelInterior = new InteriorModelDetail();
            if (dto != null)
            {
                modelInterior.FrontAirConditioning = dto.FrontAirConditioning;
                modelInterior.BackAirConditioning = dto.BackAirConditioning;
                modelInterior.AirFilter = dto.AirFilter;
                modelInterior.Seating = dto.Seating;
                modelInterior.FrontSeatType = dto.FrontSeatType;
                modelInterior.Shampoo = dto.Shampoo;
                modelInterior.Compass = dto.Compass;
                modelInterior.Clocks = dto.Clocks;
                modelInterior.DCPowerOutlet = dto.DCPowerOutlet;
                modelInterior.Tachometer = dto.Tachometer;
                modelInterior.WaterTempGauges = dto.WaterTempGauges;
                modelInterior.LowFuelWarning = dto.LowFuelWarning;
                modelInterior.DisplayOutsideTemp = dto.DisplayOutsideTemp;
                modelInterior.FlashInterior = dto.FlashInterior;
            }
            return modelInterior;
        }
    }

    public class ExteriorModelDetail : BaseTransform<ModelDetailExteriorDto, ExteriorModelDetail>
    {
        public string FrontTires { get; set; }
        public string BackTires { get; set; }
        public string Wheels { get; set; }
        public string SunRoof { get; set; }
        public string BackWindowType { get; set; }
        public int NumberOfDoors { get; set; }
        public string RearviewMirror { get; set; }
        public string HeatedDoorMirror { get; set; }
        public string WindshieldWipes { get; set; }
        public string LiftingSystem { get; set; }
        public string FlashExterior { get; set; }

        public override ExteriorModelDetail TransForm(ModelDetailExteriorDto dto)
        {
            var modelExterior = new ExteriorModelDetail();
            if (dto != null)
            {
                modelExterior.FrontTires = dto.FrontTires;
                modelExterior.BackTires = dto.BackTires;
                modelExterior.Wheels = dto.Wheels;
                modelExterior.SunRoof = dto.SunRoof;
                modelExterior.BackWindowType = dto.BackWindowType;
                modelExterior.NumberOfDoors = dto.NumberOfDoors;
                modelExterior.RearviewMirror = dto.RearviewMirror;
                modelExterior.HeatedDoorMirror = dto.HeatedDoorMirror;
                modelExterior.WindshieldWipes = dto.WindshieldWipes;
                modelExterior.LiftingSystem = dto.LiftingSystem;
                modelExterior.FlashExterior = dto.FlashExterior;                
            }
            return modelExterior;
        }
    }

    public class ColorModelDetail : BaseTransform<ModelDetailColorDto, ColorModelDetail>
    {
        public string Color { get; set; }

        public override ColorModelDetail TransForm(ModelDetailColorDto dto)
        {
            var modelColor = new ColorModelDetail();
            if(dto != null)
            {
                modelColor.Color = dto.Color;
            }
            return modelColor;
        }
    }

    public class ModelDetailVideoViewModel : BaseTransform<ModelDetailVideoDto, ModelDetailVideoViewModel>
    {
        public string ManufacturerName { get; set; }
        public string ModelName { get; set; }
        public string YearName { get; set; }
        public string VideoLink { get; set; }

        public override ModelDetailVideoViewModel TransForm(ModelDetailVideoDto dto)
        {
            var modelVideo = new ModelDetailVideoViewModel();
            if (dto != null)
            {
                modelVideo.VideoLink = dto.VideoLink;
                modelVideo.ManufacturerName = dto.ManufacturerName;
                modelVideo.ModelName = dto.ModelName;
                modelVideo.YearName = dto.YearName;
            }
            return modelVideo;
        }
    }

    public class ImageModelDetail : BaseTransform<ModelDetailImageDto, ImageModelDetail>
    {
        public string ManufacturerName { get; set; }
        public string ModelName { get; set; }
        public string YearName { get; set; }
        public string PathImages { get; set; }
        public List<string> ImageList { get; set; }

        public override ImageModelDetail TransForm(ModelDetailImageDto dto)
        {
            var modelImage = new ImageModelDetail();
            if (dto != null)
            {
                modelImage.ManufacturerName = dto.ManufacturerName;
                modelImage.ModelName = dto.ModelName;
                modelImage.YearName = dto.YearName;
                modelImage.PathImages = dto.PathImages;
            }
            return modelImage;
        }
    }
}
