﻿using System;
using Vienauto.Interface.Dto;
using Common.Core.Data.Transformation;
using static Vienauto.Mobile.Models.BaseModel;
using static Vienauto.Mobile.Configuration.AppSetting;

namespace Vienauto.Mobile.Models.View
{
    public class ModelReviewDetail : BaseTransform<ModelDetailReviewDto, ModelReviewDetail>
    {
        public int IdReviews { get; set; }
        public int IdUsers { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public string ModelName { get; set; }
        public string Image { get; set; }
        public string FuelInCity { get; set; }
        public string FuelInHighWay { get; set; }
        public string Price { get; set; }
        public string UnitName { get; set; }
        public int PointBody { get; set; }
        public int PointSafe { get; set; }
        public int PointOperation { get; set; }
        public int PointPrice { get; set; }
        public double Overall { get { return (PointBody + PointSafe + PointPrice + PointOperation) / 4; } }
        public string Content { get; set; }
        public DateTime Datepost { get; set; }
        public double TotalReview { get; set; }
        public string BodyReview { get { return BaseModel.ViewProgress(PointBody); } }
        public string SafeReview { get { return BaseModel.ViewProgress(PointSafe); } }
        public string OperationReview { get { return BaseModel.ViewProgress(PointOperation); } }
        public string PriceReview { get { return BaseModel.ViewProgress(PointPrice); } }
        public string OverallReview { get { return BaseModel.ViewRatings(Overall); } }

        public override ModelReviewDetail TransForm(ModelDetailReviewDto modelDto)
        {
            return new ModelReviewDetail
            {
                IdReviews = modelDto.IdReviews,
                IdUsers = modelDto.IdUsers,
                Brand = modelDto.Brand,
                Model = modelDto.Model,
                Year = modelDto.Year,
                ModelName = modelDto.ModelName,
                FuelInCity = modelDto.FuelInCity,
                FuelInHighWay = modelDto.FuelInHighWay,
                Price = ViewPrice(modelDto.Price, modelDto.Unit),
                PointBody = modelDto.PointBody,
                PointSafe = modelDto.PointSafe,
                PointOperation = modelDto.PointOperation,
                PointPrice = modelDto.PointPrice,
                Content = modelDto.Content,
                Datepost = modelDto.Datepost,
                TotalReview = modelDto.TotalReview,
                Image = !string.IsNullOrEmpty(modelDto.Image) ?
                                $"{RootURL}Upload{modelDto.Image}" :
                                $"{RootURL}images/files/car_unavaluabel.png"
            };
        }
    }
}