﻿using Vienauto.Interface.Dto;
using Common.Core.Data.Transformation;

namespace Vienauto.Mobile.Models.View
{
    public class AgencyReviewDetail : BaseTransform<AgencyDetailReviewDto, AgencyReviewDetail>
    {
        public int ReviewId { get; set; }
        public int AgentId { get; set; }
        public int UserId { get; set; }
        public int ManufacturerId { get; set; }
        public string AgentName { get; set; }
        public string Avatar { get; set; }
        public int CustomerService { get; set; }
        public int BuyingProcess { get; set; }
        public int QualityRepair { get; set; }
        public int Facilities { get; set; }
        public double Overall { get; set; }
        public string CustomerServicePoint { get; set; }
        public string BuyingProcessPoint { get; set; }
        public string QualityRepairPoint { get; set; }
        public string FacilitiesPoint { get; set; }
        public string OverallPoint { get; set; }
        public int TotalReviews { get; set; }

        public override AgencyReviewDetail TransForm(AgencyDetailReviewDto dto)
        {
            return new AgencyReviewDetail
            {
                UserId = dto.UserId,
                AgentId = dto.AgentId,
                ReviewId = dto.ReviewId,
                ManufacturerId = dto.ManufacturerId,
                Avatar = dto.Avatar,
                AgentName = dto.AgentName,
                CustomerService = dto.CustomerService,
                BuyingProcess = dto.BuyingProcess,
                QualityRepair = dto.QualityOfRepair,
                Facilities = dto.Facilities,
                Overall = dto.Overall,
                TotalReviews = dto.TotalReviews,
                CustomerServicePoint = BaseModel.ViewProgress(dto.CustomerService),
                BuyingProcessPoint = BaseModel.ViewProgress(dto.BuyingProcess),
                QualityRepairPoint = BaseModel.ViewProgress(dto.QualityOfRepair),
                FacilitiesPoint = BaseModel.ViewProgress(dto.Facilities),
                OverallPoint = BaseModel.ViewRatings(dto.Overall)
            };
        }
    }
}