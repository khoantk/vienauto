﻿using System;
using Vienauto.Interface.Dto;
using Common.Core.Data.Transformation;

namespace Vienauto.Mobile.Models.View
{
    public class AgencyReviewViewModel : BaseTransform<AgencyReviewsDto, AgencyReviewViewModel>
    {
        public int ReviewId { get; set; }
        public int CustomerServicePoint { get; set; }
        public int BuyingProcessPoint { get; set; }
        public int QualityRepairPoint { get; set; }
        public int FacilitiesPoint { get; set; }
        public string Content { get; set; }
        public DateTime DatePost { get; set; }
        public double Overall { get; set; }        
        public string UserName { get; set; }        
        public double MaxOverall { get; set; }        
        public string MaxRating { get { return BaseModel.ViewRatings(MaxOverall); } }         

        public override AgencyReviewViewModel TransForm(AgencyReviewsDto dto)
        {
            return new AgencyReviewViewModel
            {
                ReviewId = dto.ReviewId,
                CustomerServicePoint = dto.CustomerService,
                BuyingProcessPoint = dto.BuyingProcess,
                QualityRepairPoint = dto.QualityRepair,
                FacilitiesPoint = dto.Facilities,
                Content = dto.Content,
                DatePost = dto.DatePost,
                Overall = dto.Overall,                
                UserName = dto.UserName,
                MaxOverall = dto.MaxPointReview                        
            };
        }
    }

}
