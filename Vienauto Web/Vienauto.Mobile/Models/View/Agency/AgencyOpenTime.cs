﻿using Common.Core.Data.Transformation;
using System.Collections.Generic;
using Vienauto.Interface.Dto;
using System;

namespace Vienauto.Mobile.Models.View
{
    public class AgencyOpenTime : AgencyDetail, IBaseTransform<AgencyDetailDto, AgencyOpenTime>
    {
        public Dictionary<string, string> DateTimeOpenSale { get; set; }
        public Dictionary<string, string> DateTimeFullSale { get; set; }
        public Dictionary<string, string> DateTimeOpenService { get; set; }
        public Dictionary<string, string> DateTimeFullService { get; set; }

        public AgencyOpenTime()
        {
            DateTimeOpenSale = new Dictionary<string, string>();
            DateTimeFullSale = new Dictionary<string, string>();
            DateTimeOpenService = new Dictionary<string, string>();
            DateTimeFullService = new Dictionary<string, string>();
        }

        public AgencyOpenTime TransForm(AgencyDetailDto agencyDto)
        {
            var agencyDetail = new AgencyOpenTime();
            if (agencyDto != null)
            {
                agencyDetail.AgencyId = agencyDto.AgencyId;
                agencyDetail.CompanyName = agencyDto.CompanyName;
                agencyDetail.ProvinceName = agencyDto.ProvinceName;
                agencyDetail.Address = agencyDto.Address;
                agencyDetail.NewPhone = agencyDto.NewPhone;
                agencyDetail.OldPhone = agencyDto.OldPhone;
                agencyDetail.Service = agencyDto.Service;
                agencyDetail.Coordinates = agencyDto.Coordinates;
                agencyDetail.Avatar = agencyDto.ManufacturerAvatar;
                agencyDetail.ManufacturerId = agencyDto.ManufacturerId;
                agencyDetail.CountryId = agencyDto.CountryId;
                agencyDetail.DealerShipId = agencyDto.DealerShipId;
                agencyDetail.UserId = agencyDto.UserId;
                agencyDetail.ZoomMap = agencyDto.ZoomMap;
                agencyDetail.MaxOverall = agencyDto.MaxPointReview;
                agencyDetail.TotalReviews = agencyDto.TotalReview;
                agencyDetail.DateTimeOpenSale = agencyDto.OpenAgencySaleTime;
                agencyDetail.DateTimeFullSale = agencyDto.FullAgencySaleTime;
                agencyDetail.DateTimeOpenService = agencyDto.OpenAgencyServiceTime;
                agencyDetail.DateTimeFullService = agencyDto.FullAgencyServiceTime;
            }
            return agencyDetail;
        }                
    }
}
