﻿using Vienauto.Interface.Dto;
using Common.Core.Data.Transformation;

namespace Vienauto.Mobile.Models.View
{
    public class AgencyDetail : BaseTransform<AgencyDto, AgencyDetail>
    {
        public int AgencyId { get; set; }
        public int UserId { get; set; }
        public int CountryId { get; set; }
        public string Avatar { get; set; }
        public string ProvinceName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string NewPhone { get; set; }
        public string OldPhone { get; set; }
        public string Service { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Coordinates { get; set; }
        public int DealerShipId { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public string ZoomMap { get; set; }
        public int TotalReviews { get; set; }
        public double MaxOverall { get; set; }
        public string MaxRating { get { return BaseModel.ViewRatings(MaxOverall); } }

        public override AgencyDetail TransForm(AgencyDto dto)
        {
            return new AgencyDetail
            {
                AgencyId = dto.AgencyId,
                CountryId = dto.CountryId,
                DealerShipId = dto.DealerShipId,
                ManufacturerId = dto.ManufacturerId,
                ManufacturerName = dto.ManufacturerName,
                Avatar = dto.ManufacturerAvatar,
                CompanyName = dto.CompanyName,
                ProvinceName = dto.ProvinceName,
                Address = dto.Address,
                NewPhone = dto.NewPhone,
                OldPhone = dto.OldPhone,
                Service = dto.Service,
                Email = dto.UserEmail,
                Website = dto.WebsiteLink,
                Coordinates = dto.Coordinates,
                UserId = dto.UserId,
                ZoomMap = dto.ZoomMap,
                MaxOverall = dto.MaxPointReview,
                TotalReviews = dto.TotalReview
            };
        }
    }
}
