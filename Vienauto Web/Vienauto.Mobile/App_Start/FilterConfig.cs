﻿using System.Web.Mvc;
using Common.Core.Mvc.Attribute;

namespace Vienauto.Mobile.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ErrorLogExceptionAttribute());
        }
    }
}
