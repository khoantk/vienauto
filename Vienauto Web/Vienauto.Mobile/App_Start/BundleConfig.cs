﻿using System.Linq;
using System.Web.Optimization;
using Common.Core.Mvc.Helper;
using System.Collections.Generic;
using Vienauto.Mobile.Configuration;

namespace Vienauto.Mobile.App_Start
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {            
            //css 
            var coreCss = BundleConfigHelper.GetVirtualPaths(BundConfigType.Css, "bundleCoreDirectory");
            var coreCssBundle = new StyleBundle(AppSetting.BundleCssCorePath).Include(coreCss);
            coreCssBundle.Orderer = new CoreCssBundleOrder();
            bundles.Add(coreCssBundle);

            var boostrapCss = BundleConfigHelper.GetVirtualPaths(BundConfigType.Css, "bundleBootstrapDirectory");
            var bootstrapCssBundle = new StyleBundle(AppSetting.BundleCssBootstrapPath).Include(boostrapCss);
            bootstrapCssBundle.Orderer = new BootstrapCssBundleOrder();
            bundles.Add(bootstrapCssBundle);

            var loginCss = BundleConfigHelper.GetVirtualPaths(BundConfigType.Css, "bundleLoginDirectory");
            bundles.Add(new StyleBundle(AppSetting.BundleCssLoginPath).Include(loginCss));

            var registerCss = BundleConfigHelper.GetVirtualPaths(BundConfigType.Css, "bundleRegisterDirectory");
            bundles.Add(new StyleBundle(AppSetting.BundleCssRegisterPath).Include(registerCss));

            var homeCss = BundleConfigHelper.GetVirtualPaths(BundConfigType.Css, "bundleHomeDirectory");
            bundles.Add(new StyleBundle(AppSetting.BundleCssHomePath).Include(homeCss));

            var agencyCss = BundleConfigHelper.GetVirtualPaths(BundConfigType.Css, "bundleAgencyDirectory");
            bundles.Add(new StyleBundle(AppSetting.BundleCssAgencyPath).Include(agencyCss));

            var modelCss = BundleConfigHelper.GetVirtualPaths(BundConfigType.Css, "bundleModelDirectory");
            bundles.Add(new StyleBundle(AppSetting.BundleCssModelPath).Include(modelCss));

            var sellCss = BundleConfigHelper.GetVirtualPaths(BundConfigType.Css, "bundleSellDirectory");
            bundles.Add(new StyleBundle(AppSetting.BundleCssSellPath).Include(sellCss));

            var errorServerCss = BundleConfigHelper.GetVirtualPaths(BundConfigType.Css, "bundleErrorDirectory");
            bundles.Add(new StyleBundle(AppSetting.BundleCssErrorPath).Include(errorServerCss));

            //js            
            var bootstrapRatingJs = BundleConfigHelper.GetVirtualPaths(BundConfigType.Js, "bundleBootstrapRatingDirectory");
            bundles.Add(new ScriptBundle(AppSetting.BundleJsBootstrapRatingPath).Include(bootstrapRatingJs));

            var jqueryJs = BundleConfigHelper.GetVirtualPaths(BundConfigType.Js, "bundleJqueryDirectory");
            bundles.Add(new ScriptBundle(AppSetting.BundleJsJqueryPath).Include(jqueryJs));

            var jqueryValidateJs = BundleConfigHelper.GetVirtualPaths(BundConfigType.Js, "bundleJqueryValidateDirectory");
            bundles.Add(new ScriptBundle(AppSetting.BundleJsJqueryValidatePath).Include(jqueryValidateJs));

            var coreJs = BundleConfigHelper.GetVirtualPaths(BundConfigType.Js, "bundleCoreDirectory");
            var coreJsBundle = new ScriptBundle(AppSetting.BundleJsCorePath).Include(coreJs);
            coreJsBundle.Orderer = new CoreJsBundleOrderer();
            bundles.Add(coreJsBundle);

            var loginJs = BundleConfigHelper.GetVirtualPaths(BundConfigType.Js, "bundleLoginDirectory");
            bundles.Add(new ScriptBundle(AppSetting.BundleJsLoginPath).Include(loginJs));

            var registerJs = BundleConfigHelper.GetVirtualPaths(BundConfigType.Js, "bundleRegisterDirectory");
            bundles.Add(new ScriptBundle(AppSetting.BundleJsRegisterPath).Include(registerJs));

            var homeJs = BundleConfigHelper.GetVirtualPaths(BundConfigType.Js, "bundleHomeDirectory");
            bundles.Add(new ScriptBundle(AppSetting.BundleJsHomePath).Include(homeJs));

            var agnencyJs = BundleConfigHelper.GetVirtualPaths(BundConfigType.Js, "bundleAgencyDirectory");
            bundles.Add(new ScriptBundle(AppSetting.BundleJsAgencyPath).Include(agnencyJs));

            var modelJs = BundleConfigHelper.GetVirtualPaths(BundConfigType.Js, "bundleModelDirectory");
            bundles.Add(new ScriptBundle(AppSetting.BundleJsModelPath).Include(modelJs));

            var sellJs = BundleConfigHelper.GetVirtualPaths(BundConfigType.Js, "bundleSellDirectory");
            bundles.Add(new ScriptBundle(AppSetting.BundleJsSellPath).Include(sellJs));

            BundleTable.EnableOptimizations = false;
        }
    }
    
    public class CoreJsBundleOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files.OrderBy(file => file.VirtualFile.Name.Contains("mobile"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("core"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("scotchPanel"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("jquery."))
                        .ThenBy(file => file.VirtualFile.Name.Contains("ajax"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("scripts"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("toastr"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("moment"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("respond"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("bootstrap"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("modernizr"));
        }
    }

    public class BootstrapCssBundleOrder : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files.OrderBy(file => file.VirtualFile.Name.Contains("datepicker"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("theme"));
        }
    }

    public class CoreCssBundleOrder : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files.OrderBy(file => file.VirtualFile.Name.Contains("core"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("vienauto-mobile"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("toastr"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("font-awesome"))
                        .ThenBy(file => file.VirtualFile.Name.Contains("jquery.mobile.structure-1.2.0"));
        }
    }
}
