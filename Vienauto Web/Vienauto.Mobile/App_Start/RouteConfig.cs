﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Vienauto.Mobile.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "AgencyDetail",
            //    url: "daily/{id}",
            //    defaults: new { controller = "Agency", action = "Detail", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "SignIn",
            //    url: "dangnhap.html",
            //    defaults: new { controller = "Account", action = "SignIn" }
            //);

            //routes.MapRoute(
            //    name: "Register",
            //    url: "dangky",
            //    defaults: new { controller = "Account", action = "Register" }
            //);
        }
    }
}
