﻿using Owin;
using Microsoft.Owin;

[assembly: OwinStartupAttribute(typeof(Vienauto.Mobile.Startup))]
namespace Vienauto.Mobile
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
