﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using Castle.Windsor.Installer;
using Vienauto.Mobile.App_Start;
using Vienauto.DependencyInjection;

namespace Vienauto.Mobile
{
    public class MvcApplication : HttpApplication
    {
        //public override void Init()
        //{
        //base.Init();
        //ApplicationConfig.BindNHibernateSessionPerRequest(this);
        //ApplicationConfig.InitSessionFactory();
        //}

        protected void Application_Start()
        {
            ApplicationConfig.InitializeNhibernate();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DependencyConfig.Setup(FromAssembly.This());
        }
    }
}
