﻿using Common.Core.SignalR.Hubs;
using Microsoft.AspNet.SignalR;

namespace Vienauto.Mobile.Configuration
{
    public class MobileHub : StationHub
    {
        public static void ReloadSells()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<MobileHub>();
            context.Clients.All.refreshRealTimeSells();
        }
    }
}