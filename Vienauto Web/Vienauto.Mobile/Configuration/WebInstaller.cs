﻿using Castle.Windsor;
using System.Web.Mvc;
using Vienauto.Business;
using Common.Core.Dependency;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;

namespace Vienauto.Mobile.Configuration.CastleWindsor
{
    public class WebInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                                      .BasedOn<IController>()
                                      .LifestyleScoped<WindsorHybridScopeAccessor>());

            container.Register(Classes.FromAssemblyNamed("Vienauto.Business")
                                      .BasedOn<BaseService>()
                                      .WithServiceAllInterfaces()
                                      .LifestyleTransient());
        }
    }
}
