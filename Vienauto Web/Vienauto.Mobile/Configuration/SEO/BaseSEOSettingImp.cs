﻿using System.Collections.Generic;

namespace Vienauto.Mobile.Configuration.SEO
{
    public interface ISEOSetting<TEntity>
    {
        Dictionary<string, string> GetContent(TEntity entity);
    }

    public abstract class BaseSEOSettingImp<TEntity> : ISEOSetting<TEntity>
    {
        public abstract Dictionary<string, string> GetContent(TEntity agency);
    }
}
