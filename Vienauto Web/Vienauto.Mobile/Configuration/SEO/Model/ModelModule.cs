﻿namespace Vienauto.Mobile.Configuration.SEO.Model
{
    public class ModelModule
    {
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
    }
}
