﻿using System.Collections.Generic;
using Common.Core.Extension.DesignPattern.AbstractFactory.SEO.Concrete;

namespace Vienauto.Mobile.Configuration.SEO.Model
{
    public class SEOModelIndex : BaseSEOSettingImp<ModelModule>
    {
        public override Dictionary<string, string> GetContent(ModelModule modelDetail)
        {
            var module = new ImplementModule();
            var title = module.GetTitle().DisplayHeader
            (
                $@"{modelDetail.Manufacturer} {modelDetail.Model} năm {modelDetail.Year} - 
                thông số đánh giá và động cơ của 
                {modelDetail.Manufacturer} {modelDetail.Model} năm {modelDetail.Year} | Vienauto.com"
            );

            var description = module.GetDescription().DisplayContent
            (
                $@"{modelDetail.Manufacturer} {modelDetail.Model} năm {modelDetail.Year} - 
                Chi tiết đánh giá xe, động cơ và giá bán 
                {modelDetail.Manufacturer} {modelDetail.Model} năm {modelDetail.Year} 
                tại Việt Nam | Vienauto.com"
            );

            return new Dictionary<string, string>
            {
                ["Title"] = title,
                ["Description"] = description
            };
        }
    }
}