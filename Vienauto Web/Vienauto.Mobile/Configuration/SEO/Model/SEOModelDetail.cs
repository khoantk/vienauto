﻿using System.Collections.Generic;
using Common.Core.Extension.DesignPattern.AbstractFactory.SEO.Concrete;

namespace Vienauto.Mobile.Configuration.SEO.Model
{
    public class SEOModelDetail : BaseSEOSettingImp<ModelModule>
    {
        public override Dictionary<string, string> GetContent(ModelModule modelDetail)
        {
            var module = new ImplementModule();
            var title = module.GetTitle().DisplayHeader
            (
                $@"{modelDetail.Manufacturer} {modelDetail.Model} năm {modelDetail.Year} - 
                Đánh giá {modelDetail.Manufacturer} {modelDetail.Model} {modelDetail.Year}, 
                động cơ, giá bán {modelDetail.Manufacturer} {modelDetail.Model} {modelDetail.Year} 
                | Vienauto.com"
            );

            var description = module.GetDescription().DisplayContent
            (
                $@"{modelDetail.Manufacturer} {modelDetail.Model} năm {modelDetail.Year} - 
                Thông số kỹ thuật, hình ảnh, nội ngoại thất Chi tiết xe 
                {modelDetail.Manufacturer} {modelDetail.Model} năm {modelDetail.Year} | Vienauto.com"
            );

            return new Dictionary<string, string>
            {
                ["Title"] = title,
                ["Description"] = description
            };
        }
    }
}