﻿using System.Collections.Generic;
using Common.Core.Extension.DesignPattern.AbstractFactory.SEO.Concrete;

namespace Vienauto.Mobile.Configuration.SEO.Model
{
    public class SEOModelReview : BaseSEOSettingImp<ModelModule>
    {
        public override Dictionary<string, string> GetContent(ModelModule modelDetail)
        {
            var module = new ImplementModule();
            var title = module.GetTitle().DisplayHeader
            (
                $@"Đánh giá xe {modelDetail.Manufacturer} {modelDetail.Model} {modelDetail.Year} - 
                Đánh giá kiểu dáng, độ an toàn, vận hành, giá xe 
                {modelDetail.Manufacturer} {modelDetail.Model} {modelDetail.Year} 
                tại Việt Nam | Vienauto.com"
            );

            var description = module.GetDescription().DisplayContent
            (
                $@"Đánh giá xe {modelDetail.Manufacturer} {modelDetail.Model} {modelDetail.Year} - 
                Có tổng + (số lượng đánh giá thay đổi của dòng xe đó) người dùng đánh giá xe 
                {modelDetail.Manufacturer} {modelDetail.Model} {modelDetail.Year} 
                tại Việt Nam dựa trên tiêu chí kiểu dáng, độ an toàn, tính năng vận hành và giá | 
                Vienauto.com"
            );

            return new Dictionary<string, string>
            {
                ["Title"] = title,
                ["Description"] = description
            };
        }
    }
}