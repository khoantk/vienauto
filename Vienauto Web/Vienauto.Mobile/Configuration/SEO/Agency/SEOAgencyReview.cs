﻿using System.Collections.Generic;
using Common.Core.Extension.DesignPattern.AbstractFactory.SEO.Concrete;

namespace Vienauto.Mobile.Configuration.SEO.Agency
{
    public class SEOAgencyReview : BaseSEOSettingImp<AgencyModule>
    {
        public override Dictionary<string, string> GetContent(AgencyModule agency)
        {
            var module = new ImplementModule();
            var title = module.GetTitle().DisplayHeader
            (
                $@"Đánh giá đại lý xe {agency.DealerShip} {agency.Province} - 
                Chi tiết đánh giá, chất lượng dịch vụ, chất lượng bảo trì và tiện ích tại 
                {agency.DealerShip} {agency.Province} | Vienauto.com"
            );

            var description = module.GetDescription().DisplayContent
            (
                $@"Đánh giá đại lý xe {agency.DealerShip} {agency.Province} - 
                Có tổng + (số lượng đánh giá thay đổi của đại lý đó) 
                lượt khách hàng đánh giá, đánh giá chất lượng dịch vụ, 
                chất lượng bảo trì và tiện ích tại 
                {agency.DealerShip} {agency.Province} | Vienauto.com"
            );

            return new Dictionary<string, string>
            {
                ["Title"] = title,
                ["Description"] = description
            };
        }
    }
}