﻿namespace Vienauto.Mobile.Configuration.SEO.Agency
{
    public class AgencyModule
    {
        public string Province { get; set; }
        public string DealerShip { get; set; }
    }
}