﻿using System.Collections.Generic;
using Common.Core.Extension.DesignPattern.AbstractFactory.SEO.Concrete;

namespace Vienauto.Mobile.Configuration.SEO.Agency
{
    public class SEOAgencyIndex : BaseSEOSettingImp<AgencyModule>
    {
        public override Dictionary<string, string> GetContent(AgencyModule agency)
        {
            var module = new ImplementModule();
            var title = module.GetTitle().DisplayHeader
            (
                $@"Đại lý hãng xe {agency.DealerShip} - Tất cả đại lý xe {agency.DealerShip} tại Việt Nam, 
                viết đánh giá đại lý xe {agency.DealerShip} | Vienauto.com"
            );

            var description = module.GetDescription().DisplayContent
            (
                $@"Đại lý hãng xe {agency.DealerShip} - Đánh giá chi tiết đại lý xe {agency.DealerShip}, 
                địa chỉ thông tin đại lý xe {agency.DealerShip} tại Việt Nam | Vienauto.com"
            );

            return new Dictionary<string, string>
            {
                ["Title"] = title,
                ["Description"] = description
            };
        }
    }
}
