﻿using System.Collections.Generic;
using Common.Core.Extension.DesignPattern.AbstractFactory.SEO.Concrete;

namespace Vienauto.Mobile.Configuration.SEO.Agency
{
    public class SEOAgencyDetail : BaseSEOSettingImp<AgencyModule>
    {
        public override Dictionary<string, string> GetContent(AgencyModule agency)
        {
            var module = new ImplementModule();
            var title = module.GetTitle().DisplayHeader
            (
                $@"{agency.DealerShip} + {agency.Province} - 
                Đại lý xe {agency.DealerShip} tại + {agency.Province}, 
                đánh giá chi tiết đại lý, thông tin liên hệ đại lý tư vấn xe 
                {agency.DealerShip} tại {agency.Province} | Vienauto.com"
            );

            var description = module.GetDescription().DisplayContent
            (
                $@"{agency.DealerShip} + {agency.Province} - 
                Đại lý xe {agency.DealerShip} tại + {agency.Province}, 
                đánh giá chi tiết đại lý, thông tin đại lý, 
                đại lý mua bán ô tô {agency.DealerShip} tại + {agency.Province} | Vienauto.com"
            );

            return new Dictionary<string, string>
            {
                ["Title"] = title,
                ["Description"] = description
            };
        }
    }
}