﻿using System.Collections.Generic;

namespace Vienauto.Mobile.Configuration.SEO
{
    public class SEOSetting<TEntity>
    {
        private TEntity _entity;
        public TEntity entity
        {
            get { return _entity; }
            set { _entity = value; }
        }

        private ISEOSetting<TEntity> _setting;

        public SEOSetting(ISEOSetting<TEntity> setting)
        {
            _setting = setting;
        }

        public Dictionary<string, string> GetSEOInformation()
        {
            var content = _setting.GetContent(_entity);
            return content;
        }
    }
}
