﻿using Common.Core.Extension;

namespace Vienauto.Mobile.Configuration
{
    public static class AppSetting
    {
        //Get URL
        public static string RootURL
        {
            get { return ConfigExtensions<string>.GetValue("rootURL"); }
        }
        
        //Get Nhibernate Config
        public static string NhibernateConfig
        {
            get { return ConfigExtensions<string>.GetValue("nhibernateConfig"); }
        }
        
        //Get Mail Config
        public static string MailFrom
        {
            get { return ConfigExtensions<string>.GetValue("mailFrom"); }
        }

        public static string MailTo
        {
            get { return ConfigExtensions<string>.GetValue("mailTo"); }
        }

        public static string[] Cc
        {
            get { return ConfigExtensions<string[]>.GetValue("cc"); }
        }

        //Get Css 
        public static string BundleCssCorePath
        {
            get { return ConfigExtensions<string>.GetValue("bundleCssCorePath"); }
        }

        public static string BundleCssBootstrapPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleCssBootstrapPath"); }
        }

        public static string BundleCssLoginPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleCssLoginPath"); }
        }

        public static string BundleCssRegisterPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleCssRegisterPath"); }
        }

        public static string BundleCssHomePath
        {
            get { return ConfigExtensions<string>.GetValue("bundleCssHomePath"); }
        }

        public static string BundleCssAgencyPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleCssAgencyPath"); }
        }

        public static string BundleCssModelPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleCssModelPath"); }
        }

        public static string BundleCssSellPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleCssSellPath"); }
        }

        public static string BundleCssErrorPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleCssErrorPath"); }
        }

        //Get Js Path
        public static string BundleJsBootstrapRatingPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleJsBootstrapRatingPath"); }
        }

        public static string BundleJsJqueryPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleJsJqueryPath"); }
        }

        public static string BundleJsJqueryValidatePath
        {
            get { return ConfigExtensions<string>.GetValue("bundleJsJqueryValidatePath"); }
        }

        public static string BundleJsCorePath
        {
            get { return ConfigExtensions<string>.GetValue("bundleJsCorePath"); }
        }

        public static string BundleJsLoginPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleJsLoginPath"); }
        }

        public static string BundleJsRegisterPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleJsRegisterPath"); }
        }

        public static string BundleJsHomePath
        {
            get { return ConfigExtensions<string>.GetValue("bundleJsHomePath"); }
        }

        public static string BundleJsAgencyPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleJsAgencyPath"); }
        }

        public static string BundleJsModelPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleJsModelPath"); }
        }

        public static string BundleJsSellPath
        {
            get { return ConfigExtensions<string>.GetValue("bundleJsSellPath"); }
        }
    }
}
