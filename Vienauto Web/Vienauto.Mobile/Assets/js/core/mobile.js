﻿$(document).ready(function () {
    $(".ajax-manufacturer").loadDropDownListAjax();
    $(".ajax-model").loadDropDownListAjax();
    $(".ajax-year").loadDropDownListAjax();

    $("#togglenav-button").click(function (event) {
        var menuPosition = $(".global-navbar").position();
        if (menuPosition.top === -492)
            $(".global-navbar").animate({ "top": "36px" }, 500);
        else
            $(".global-navbar").animate({ "top": "-492px" }, 500);
    });

    showTab();

    $('#superwidget-inventory-button').click(function () {
        !$(this).hasClass('ui-btn-active') && $(this).addClass('ui-btn-active');
        $('#superwidget-inventory-container').show();

        $('#superwidget-research-button').removeClass('ui-btn-active');
        $('#superwidget-research-container').hide();

        $('#superwidget-dealers-button').removeClass('ui-btn-active');
        $('#superwidget-dealers-container').hide();
    });
    $('#superwidget-research-button').click(function () {
        !$(this).hasClass('ui-btn-active') && $(this).addClass('ui-btn-active');
        $('#superwidget-research-container').show();

        $('#superwidget-inventory-button').removeClass('ui-btn-active');
        $('#superwidget-inventory-container').hide();

        $('#superwidget-dealers-button').removeClass('ui-btn-active');
        $('#superwidget-dealers-container').hide();
    });
    $('#superwidget-dealers-button').click(function () {
        !$(this).hasClass('ui-btn-active') && $(this).addClass('ui-btn-active');
        $('#superwidget-dealers-container').show();

        $('#superwidget-inventory-button').removeClass('ui-btn-active');
        $('#superwidget-inventory-container').hide();

        $('#superwidget-research-button').removeClass('ui-btn-active');
        $('#superwidget-research-container').hide();
    });

    $(".js-toggle").toggleAjaxRightMenu();
    $(".js-toggle-close").closeToggleAjaxRightMenu();

    $(".ajax-item-manufacturer-menu-sell").loadDropDownListAjaxForTab();
    $(".ajax-item-manufacturer-menu-model").loadDropDownListAjaxForTab();
    $(".ajax-item-dealership-menu-agent").loadDropDownListAjaxForTab();
    $(".ajax-item-province-menu-agent").loadDropDownListAjaxForTab();
    $(".item-from-price-menu-sell").loadDropDownListAjaxForTab();
    $(".item-to-price-menu-sell").loadDropDownListAjaxForTab();

    $(".restrict-phone-input").keypress(function (key) {
        if (key.charCode < 48 || key.charCode > 57) return false;
    });

    var is_root = location.pathname === "/";

    $(".bookvalue").click(function () {
        if (!is_root)
            location.href = "/#xemoi";
        $("#superwidget-inventory-button").trigger("click");
        $("input[name=stkTyp]").eq(0).removeAttr("checked");
        $("input[name=stkTyp]").eq(1).attr("checked", "checked");
        $(".global-navbar").animate({ "top": "-492px" }, 500);
    });

    $(".sellandtrade").click(function () {
        if (!is_root)
            location.href = "/#xecu";
        $("#superwidget-inventory-button").trigger("click");
        $("input[name=stkTyp]").eq(1).removeAttr("checked");
        $("input[name=stkTyp]").eq(0).attr("checked", "checked");
        $(".global-navbar").animate({ "top": "-492px" }, 500);
    });

    $(".research").click(function () {
        if (!is_root)
            location.href = "/#dongxe";
        $("#superwidget-research-button").trigger("click");
        $(".global-navbar").animate({ "top": "-492px" }, 500);
    });

    $(".findadealer").click(function () {
        if (!is_root)
            location.href = "/#daily";
        $("#superwidget-dealers-button").trigger("click");
        $(".global-navbar").animate({ "top": "-492px" }, 500);
    });

    var is_Oldcar = location.hash === "#xecu";
    var is_Newcar = location.hash === "#xemoi";
    var is_Research = location.hash === "#dongxe";
    var is_Dealer = location.hash === "#daily";

    if (is_Newcar) {
        $("#superwidget-inventory-button").trigger("click");
        $("input[name=stkTyp]").eq(0).removeAttr("checked");
        $("input[name=stkTyp]").eq(1).attr("checked", "checked");
        $(".global-navbar").animate({ "top": "-492px" }, 500);
    }
    if (is_Oldcar) {
        $("#superwidget-inventory-button").trigger("click");
        $("input[name=stkTyp]").eq(1).removeAttr("checked");
        $("input[name=stkTyp]").eq(0).attr("checked", "checked");
        $(".global-navbar").animate({ "top": "-492px" }, 500);
    }
    if (is_Research) {
        $("#superwidget-research-button").trigger("click");
        $(".global-navbar").animate({ "top": "-492px" }, 500);
    }
    if (is_Dealer) {
        $("#superwidget-dealers-button").trigger("click");
        $(".global-navbar").animate({ "top": "-492px" }, 500);
    }
});

function showTab() {
    if (!$('#superwidget-inventory-button').hasClass('ui-btn-active')) {
        $('#superwidget-inventory-button').addClass('ui-btn-active');
        $('#superwidget-inventory-container').show();

        $('#superwidget-research-button').removeClass('ui-btn-active');
        $('#superwidget-research-container').hide();

        $('#superwidget-dealers-button').removeClass('ui-btn-active');
        $('#superwidget-dealers-container').hide();
    }
}

function openAgencyPage() {
    var dealershipId = $("#dealersearch-form #dealership-id-menu-agent").val();
    var provinceId = $("#dealersearch-form #province-id-menu-agent").val();
    Jquery.Function.OpenUrl("/Agency/Index?dealership={1}&province={2}", [dealershipId, provinceId]);
}

function openModelPage() {
    var manufacturer = $("#research-form .manufacturer-current-value-menu-model").html();
    var model = $("#research-form .model-current-value-menu-model").html();
    var year = $("#research-form .year-current-value-menu-model").html();
    Jquery.Function.OpenUrl("/Model/Index?manufacturer={1}&model={2}&year={3}", [manufacturer, model, year]);
}

function openInventoryPage() {
    var manufacturer = $("#inventory-form .manufacturer-current-value-menu-sell").html();
    var model = $("#inventory-form .model-current-value-menu-sell").html();
    var year = $("#inventory-form .year-current-value-menu-sell").html();
    Jquery.Function.OpenUrl("/Sell/Index?manufacturer={1}&model={2}&year={3}", [manufacturer, model, year]);
}
