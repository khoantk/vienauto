﻿(function ($) {
    $.fn.loadDropDownListAjax = function () {
        this.on("change", function () {
            var dataUrl = $(this).data("url");
            var dataBind = $(this).data("bind");
            var dataDefaultOption = $(this).data("default-option");
            var $elementToBind = $(dataBind);

            Ajax.bind(dataUrl, { id: $(this).val() }, $elementToBind,
                    function ($elementToBind, response) {
                        $elementToBind.html("");
                        var result = response.data;
                        if (dataDefaultOption)
                            $elementToBind.append("<option value='0'>" + dataDefaultOption + "</option>");
                        $.each(result, function (index, item) {
                            $elementToBind.append("<option value='" + item.Id + "'>" + item.Name + "</option>");
                        });
                    });
        });
    };

    $.fn.loadDropDownListAjaxForTab = function () {
        this.on("click", function () {
            var $parentContainer = $(this).closest(".ajax-tab");
            var selectedValue = $(this).data("id");
            var currentId = $parentContainer.data("current-id");
            var currentValue = $parentContainer.data("current-value");
            var currentOption = $parentContainer.data("current-option");

            var isClose = $parentContainer.data("close");
            if (Boolean(isClose)) {
                //$parentContainer.closest(".sliding-pane-body").find(".js-toggle-close").trigger("click");
                $(".js-toggle-close").trigger("click");
            }
            else {
                var isUnBind = $parentContainer.data("unbind");
                if (!Boolean(isUnBind)) {
                    var ajaxUrl = $parentContainer.data("url");
                    var bindItem = $parentContainer.data("bind-item");
                    var $bindContainer = $(".".concat($parentContainer.data("bind-container")));
                    Ajax.bind(ajaxUrl, { id: selectedValue }, $bindContainer,
                            function ($bindContainer, response) {
                                $bindContainer.html("<dt></dt>");
                                $.each(response.data, function (index, item) {
                                    $bindContainer.append("".concat("<a class='", bindItem, "' data-id='", item.Id, "'",
                                                          "data-text='", item.Name, "' href='javascript:void(0);'><dd>", item.Name, "</dd></a>"));
                                });
                                if (bindItem != "")
                                    $(".".concat(bindItem)).loadDropDownListAjaxForTab();
                            });
                }
                $parentContainer.toggleAjaxRightMenu();
                $(".js-toggle-close").closeToggleAjaxRightMenu();
            }
            if (currentId)
                $("#".concat(currentId)).val($(this).data("id"));
            $(".".concat(currentValue)).html($(this).data("text"));
            $(".".concat(currentOption)).removeClass("disabled");
        });
    };

    $.fn.toggleAjaxRightMenu = function () {
        this.on("click", function (e) {
            var $self = $(this);
            var toogleMenu = $self.data("toggle-menu");
            var $toggleMenuToOpen = $("#".concat(toogleMenu));

            $toggleMenuToOpen.addClass("slide-open");
            $toggleMenuToOpen.animate({ "left": "0" }, 100);
        });
    };

    $.fn.closeToggleAjaxRightMenu = function () {
        this.on("click", function (e) {
            var $self = $(this);
            var toogleMenu = $self.data("toggle-menu");
            var $toggleMenuToClose = $("#".concat(toogleMenu));
            $toggleMenuToClose.animate({ "left": "1349px" }, 100);
            setTimeout(function () {
                $toggleMenuToClose.removeClass("slide-open");
            }, 500);
        });
    };
}(jQuery));

var Jquery = {};
Jquery.Function = {
    FormatString: function () {
        var args = [].slice.call(arguments);
        if (args.length == 2) {
            var paramters = args[1];
            args = args.slice(0, 1);
            paramters.forEach(function (value) {
                args.push(value);
            });
        }

        if (this.toString() != '[object Object]')
            args.unshift(this.toString());

        var pattern = new RegExp('{([1-' + args.length + '])}', 'g');
        return String(args[0]).replace(pattern, function (match, index) { return args[index]; });
    },
    CheckIsNumber: function () {
        if (value != "" && value != null)
            return (isNaN(Number(value))) ? false : true;
        return false;
    },
    OpenUrl: function (urlFormat, parameters) {
        var url = this.FormatString(urlFormat, parameters);
        window.open(url, "_self");
    }
}
