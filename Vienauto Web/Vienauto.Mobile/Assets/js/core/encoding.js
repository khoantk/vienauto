﻿var $keyTable = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

function createCode(length) {
    var $code = '';
    var $temp = 0;
    for (var $i = 0; $i < length; $i++) {
        $temp = Math.floor(Math.random() * (26 - 1) + 1);
        if ($i === 0)
            $code += $temp;
        else
            $code += '#' + $temp;
    }
    return $code;
};

function getKey($code) {
    var $key = '';
    var $arr = [];
    $arr = $code.split('#').map(Number);
    var $length = $arr.length;
    for (var $i = 0; $i < $length; $i++) {
        $key += $keyTable[$arr[$i]];
    }

    return $key;
};

function createKeyCode(length) {
    var $key = ''; var $code = '';
    try {
        $code = createCode(length);
        $key = getKey($code);
    } catch ($err) {
        // TODO Auto-generated catch block	
        return $err;
    }

    return $key;
};

function encrypt($string) {
    $string = $string.toUpperCase();
    var $length = $string.length;
    var $key = createKeyCode($length);
    var $y = $key.length;
    var $c = '';
    var $n = 0;
    var $kt = 0;
    var $temp = 0;
    for (var $i = 0; $i < $length; $i++) {
        $n = ($string[$i]).charCodeAt(0) - ('A').charCodeAt(0);
        for (var $j = $i; $j < $y; $j++) {
            if ($string[$j] < 'A' || $string[$j] > 'Z') {
                $c += $string[$i];
                break;
            }
            if ($j < $length) {
                if ((($key[$j]).charCodeAt(0) + $n) > 90) {
                    $temp = 90 - ($key[$j].charCodeAt(0));
                    $n -= $temp;
                    $c += String.fromCharCode(('A').charCodeAt(0) + $n - 1);
                    break;
                }
                else {
                    $kt = ($key[$j]).charCodeAt(0) + $n;
                    $c += String.fromCharCode($kt);
                    break;
                }
            }
            else if ($j >= $length)
                break;
        }
        if ($i >= $y) {
            $c += $string[$i];
        }
    }

    $c = $c.toLowerCase();
    $key = $key.toLowerCase();
    return $c + $key;
}; 