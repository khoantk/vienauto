﻿function loadMap() {
    var toado_map = $("#toadomap").val().split(',');
    var zoom_map = parseInt($("#zoommap").val());

    var latitude = toado_map[0];
    var longtitude = toado_map[1];
    var position = new google.maps.LatLng(latitude, longtitude);

    var options = {
        zoom: 16,
        center: position,
        zoomControl: true,
        scaleControl: true,
        rotateControl: true,
        mapTypeControl: true,
        streetViewControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var mapGoogle = new google.maps.Map(document.getElementById("map"), options);

    var marker = new google.maps.Marker({
        title: "",
        //map: mapGoogle,
        position: position
    });

    marker.setMap(mapGoogle);
}
