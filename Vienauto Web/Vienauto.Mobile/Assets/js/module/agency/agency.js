﻿$(document).ready(function () {
    if ($('#agency-list-page').length)
        showAgencies("Current");
    else if ($('#agency-review-page').length)
        showAgenciesReviewPaging("Current");
    else if ($('#agency-detail-page').length)
        google.maps.event.addDomListener(window, 'load', loadMap);

    $('#items-per-page').change(function () {
        $('#current-page-size').val($(this).val());
        showAgencies("Current");
    });

    $('#sort-by').change(function () {
        $('#current-sort').val($(this).val());
        $('.agency-sort-name span').html($('#sort-by :selected').text());
        showAgencies("Current");
    });

    $("body").on("click", "#agency-list-page a.page-navigation", function (e) {
        e.preventDefault();
        if ($(this).data("disable") == "False") {
            showAgencies($(this).data("navigation"));
        }
    });

    $("body").on("click", "#agency-review-page a.page-navigation", function (e) {
        e.preventDefault();
        if ($(this).data("disable") == "False") {
            showAgenciesReviewPaging($(this).data("navigation"));
        }
    });

    $('.collapse-link:not(.binded)').addClass("binded").click(function () {
        var ibox = $(this).closest('div.content-calendar');
        var button = $(this).find('i');
        var content = ibox.find('div.content-hours');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    $('#customer_service').on('change', function () {
        $(this).next('.label').text($(this).val());
    });
    $('#buying_process').on('change', function () {
        $(this).next('.label').text($(this).val());
    });
    $('#quality_of_repair').on('change', function () {
        $(this).next('.label').text($(this).val());
    });
    $('#overall_facilities').on('change', function () {
        $(this).next('.label').text($(this).val());
    });

    $('#write-to-reviews').click(function () {
        Ajax.bind('/Agency/ShowAgencyReviewForm', {
            agentId: $('#current-dealerShip').val(),
            dealerShip: $('#dealerShip-name').val()
        }, $('#form-reviews'), function ($target, response) {
            if (response.success != undefined && !response.success)
                window.notificationInfo(response.message);
            else
                $target.html(response);
        });
    });

    Ajax.post($('#form-reviews-agent'), $('#btn_submit_reviews'),
        function () {
            return {                
                AgentId: $('#current-dealerShip').val(),
                CustomerService: $('#customer_service').val(),
                BuyingProcess: $('#buying_process').val(),
                QualityRepair: $('#quality_of_repair').val(),
                Facilities: $('#overall_facilities').val(),
                TitleReview: $('#title_reviews').val(),
                ContentReview: $('#content_reviews').val(),
                Name: $('#fullname').val(),
                Email: $('#email').val(),
                Phone: $('#phone').val()
            }
        });

    $('#btn_submit_reviews').on("ajaxSuccess", function (target, response) {
        if (response.success != undefined && response.success) {
            $('#modal-reviews-agent').hide();
            showAgenciesReviewPaging("Current");            
            $("#agency-total-reviews").html(parseInt($("#agency-total-reviews").html()) + 1);
        }
    });

    $('.modal-close').click(function () {
        $('#modal-reviews-agent').hide();
    });
});

function showAgencies(navigation) {
    Ajax.bind('/Agency/GetAgenciesPaging/',
    {
        page: $('#page-index').val(),
        size: $('#items-per-page').val(),
        sort: $('#sort-by').val(),
        provinceId: $('#current-province').val(),
        dealerShipId: $('#current-dealerShip').val(),
        navigation: navigation,
        nextPage: $('#next-page-index').val(),
        prevPage: $('#previous-page-index').val()
    },
    $('#agency-list'), function ($target, response) {
        if (response.success != undefined && !response.success)
            $target.html("<h3 class='no-data'>" + response.message + "</h3>");
        else
            $target.html(response);
    });
}

function showAgenciesReviewPaging(navigation) {
    Ajax.bind('/Agency/GetAgenciesReviewsPaging/',
    {
        page: $('#ModelPageIndex').val(),
        size: 10,
        sort: "",
        dealerShipId: $('#current-dealerShip').val(),
        manufacturerId: $('#current-manufacturer').val(),
        navigation: navigation,
        nextPage: $('#next-page-index').val(),
        prevPage: $('#previous-page-index').val()
    },
    $('#agent-review-list'), function ($target, response) {
        if (response.success != undefined && !response.success)
            $target.html("<h3 class='no-data'>" + response.message + "</h3>");
        else
            $target.html(response);
    });
}
