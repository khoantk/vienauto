﻿$(document).ready(function () {
    $(".ajax-car").click(function () {
        var carId = $(this).val();
        if(carId > 0)
            Ajax.bind("/home/getavatarbycar", { id: carId }, $(".agentImage img"),
            function ($avatar, response) {
                if (response.success) {
                    $avatar.attr("src", "http://vienauto.com/Upload" + response.data);
                    setTimeout(function () {
                        $("#your-car-information").fadeOut(250);
                        $("#your-information").delay(250).fadeIn(300);
                    }, 3000)
                }
            });
    });

    var ismobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);
    if (ismobile)
        $("#datepicker input").addClass("no-grey").prop("readonly", true);

    $("#datepicker").datepicker({
        autoclose: true,
        format: "mm/dd/yyyy",
        todayHighlight: true
    }).datepicker("update", new Date());

    var errorMessage = $("#errorMessage").val();
    var triggerNotification = $("#triggerNotification").val();
    if (triggerNotification.length > 0 && triggerNotification) {
        if (errorMessage.length > 0 && errorMessage != "")
            window.notificationError(errorMessage);
        else
            window.notificationSuccess("Bạn đã đặt xe thành công. \
            Mọi chi tiết chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.");
    }   
});
