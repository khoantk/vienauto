﻿$(document).ready(function () {
    $('.collapse-link:not(.binded)').addClass("binded").click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    $('#show-filter-brand').addClass("binded").click(function () {
        var button = $(this).find('i');
        var content = $('#srp-list-content-brand');
        content.slideToggle(500);
        button.toggleClass('glyphicon-chevron-up').toggleClass('glyphicon-chevron-down');
    });

    $('#show-filter-price').addClass("binded").click(function () {
        var button = $(this).find('i');
        var content = $('#srp-list-content-price');
        content.slideToggle(500);
        button.toggleClass('glyphicon-chevron-up').toggleClass('glyphicon-chevron-down');
    });

    $('#show-filter-model').addClass("binded").click(function () {
        var button = $(this).find('i');
        var content = $('#srp-list-content-model');
        content.slideToggle(500);
        button.toggleClass('glyphicon-chevron-up').toggleClass('glyphicon-chevron-down');
    });

    $('#show-filter-seat').addClass("binded").click(function () {
        var button = $(this).find('i');
        var content = $('#srp-list-content-seat');
        content.slideToggle(500);
        button.toggleClass('glyphicon-chevron-up').toggleClass('glyphicon-chevron-down');
    });

    $('#show-filter-fuel').addClass("binded").click(function () {
        var button = $(this).find('i');
        var content = $('#srp-list-content-fuel');
        content.slideToggle(500);
        button.toggleClass('glyphicon-chevron-up').toggleClass('glyphicon-chevron-down');
    });

    $('#show-filter-tranmission-type').addClass("binded").click(function () {
        var button = $(this).find('i');
        var content = $('#srp-list-content-tranmission-type');
        content.slideToggle(500);
        button.toggleClass('glyphicon-chevron-up').toggleClass('glyphicon-chevron-down');
    });

    $('#show-filter-year').addClass("binded").click(function () {
        var button = $(this).find('i');
        var content = $('#srp-list-content-year');
        content.slideToggle(500);
        button.toggleClass('glyphicon-chevron-up').toggleClass('glyphicon-chevron-down');
    });

    $('#show-filter-gear').addClass("binded").click(function () {
        var button = $(this).find('i');
        var content = $('#srp-list-content-gear');
        content.slideToggle(500);
        button.toggleClass('glyphicon-chevron-up').toggleClass('glyphicon-chevron-down');
    });

    $('#show-filter-distribution').addClass("binded").click(function () {
        var button = $(this).find('i');
        var content = $('#srp-list-content-distribution');
        content.slideToggle(500);
        button.toggleClass('glyphicon-chevron-up').toggleClass('glyphicon-chevron-down');
    });

    if ($('#section-overview').length) {
        showSellDetailInform('/Sell/GetSellDetailOverview', $('#section-overview'));
    }
    if ($('#section-engine').length) {
        showSellDetailInform('/Sell/GetSellDetailEngine', $('#section-engine'));
    }
    if ($('#section-safety').length) {
        showSellDetailInform('/Sell/GetSellDetailSafety', $('#section-safety'));
    }
    if ($('#section-entertainment').length) {
        showSellDetailInform('/Sell/GetSellDetailEntertainment', $('#section-entertainment'));
    }
    if ($('#section-interior').length) {
        showSellDetailInform('/Sell/GetSellDetailInterior', $('#section-interior'));
    }
    if ($('#section-exterior').length) {
        showSellDetailInform('/Sell/GetSellDetailExterior', $('#section-exterior'));
    }
    if ($('#section-color').length) {
        showSellDetailInform('/Sell/GetSellDetailColor', $('#section-color'));
    }
    if ($('#section-video').length) {
        showSellDetailInform('/Sell/GetSellDetailVideo', $('#section-video'));
    }
    if ($('#section-image').length) {
        showSellDetailInform('/Sell/GetSellDetailImage', $('#section-image'));
    }
});

function showSellDetailInform(urlRequest, element) {
    Ajax.bind(urlRequest,
    {
        productId: $('#current-productId').val()
    },
    element, function ($target, response) {
        $target.html(response);
    });
}