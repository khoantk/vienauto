﻿$(document).ready(function () {
    if ($('#model-review-list').length) {
        showModelsReviewPaging("Current");
    }
    else if ($('#model-same-style-list').length) {
        showSameStyleModels("Current");

        $('#items-per-page').change(function () {
            $('#current-page-size').val($(this).val());
            showSameStyleModels("Current");
        });

        $("body").on("click", "#model-same-style-list a.page-navigation", function (e) {
            e.preventDefault();
            if ($(this).data("disable") == "False") {
                showSameStyleModels($(this).data("navigation"));
            }
        });
    }

    if ($('#section-overview').length) {
        showModelDetailInform('/Model/GetModelDetailOverview', $('#section-overview'));
    }
    if ($('#section-engine').length) {
        showModelDetailInform('/Model/GetModelDetailEngine', $('#section-engine'));
    }
    if ($('#section-safety').length) {
        showModelDetailInform('/Model/GetModelDetailSafety', $('#section-safety'));
    }
    if ($('#section-entertainment').length) {
        showModelDetailInform('/Model/GetModelDetailEntertainment', $('#section-entertainment'));
    }
    if ($('#section-interior').length) {
        showModelDetailInform('/Model/GetModelDetailInterior', $('#section-interior'));
    }
    if ($('#section-exterior').length) {
        showModelDetailInform('/Model/GetModelDetailExterior', $('#section-exterior'));
    }
    if ($('#section-color').length) {
        showModelDetailInform('/Model/GetModelDetailColor', $('#section-color'));
    }
    if ($('#section-video').length) {
        showModelDetailInform('/Model/GetModelDetailVideo', $('#section-video'));
    }
    if ($('#section-image').length) {
        showModelDetailInform('/Model/GetModelDetailImage', $('#section-image'));
    }

    $("body").on("click", "#model-review-page a.page-navigation", function (e) {
        e.preventDefault();
        if ($(this).data("disable") == "False") {
            showModelsReviewPaging($(this).data("navigation"));
        }
    });

    $('#point_body').on('change', function () {
        $(this).next('.label').text($(this).val());
    });
    $('#point_safe').on('change', function () {
        $(this).next('.label').text($(this).val());
    });
    $('#point_operation').on('change', function () {
        $(this).next('.label').text($(this).val());
    });
    $('#point_price').on('change', function () {
        $(this).next('.label').text($(this).val());
    });

    $('#write-to-model-reviews').click(function () {
        Ajax.bind('/Model/ShowModelReviewForm', {
            manufacture: $('#current-brand').val(),
            model: $('#current-model').val(),
            year: $('#current-year').val()
        }, $('#form-reviews'), function ($target, response) {
            if (response.success != undefined && !response.success)
                window.notificationInfo(response.message);
            else
                $target.html(response);
        });
    });

    Ajax.post($('#form-reviews-model'), $('#btn_submit_reviews'),
    function () {
        return {            
            Brand: $('#current-brand').val(),
            Model: $('#current-model').val(),
            Year: $('#current-year').val(),
            PointBody: $('#point_body').val(),
            PointSafe: $('#point_safe').val(),
            PointOperation: $('#point_operation').val(),
            PointPrice: $('#point_price').val(),
            Content: $('#content_reviews').val(),
            Title: $('#title_reviews').val(),
            Name: $('#fullname').val(),
            Email: $('#email').val(),
            Phone: $('#phone').val()
        }
    });

    $('#btn_submit_reviews').on("ajaxSuccess", function (target, response) {
        if (response.success != undefined && response.success) {
            $('#modal-reviews-model').hide();
            showModelsReviewPaging("Current");
            $("#model-total-reviews").html(parseInt($("#model-total-reviews").html()) + 1);
            //document.location.reload();
        }
    });

    $('.modal-close').click(function () {
        $('#modal-reviews-model').hide();
    });
});

function showSameVersionCars(manufacturer, model, year) {
    var url = Jquery.Function.FormatString("/Model/SameVersion?manufacturer={1}&model={2}&year={3}",
                                           manufacturer, model, year);
    location.href = url;
}

function showSameStyleCars(manufacturer, model, year, style) {
    var url = Jquery.Function.FormatString("/Model/SameStyle?manufacturer={1}&model={2}&year={3}&style={4}",
                                           manufacturer, model, year, style);
    location.href = url;
}

function showModelsReviewPaging(navigation) {
    Ajax.bind('/Model/GetModelReviewsPaging',
    {
        page: $('#ModelPageIndex').val(),
        size: 10,
        sort: "",
        manufacturer: $('#current-brand').val(),
        model: $('#current-model').val(),
        year: $('#current-year').val(),
        navigation: navigation,
        nextPage: $('#next-page-index').val(),
        prevPage: $('#previous-page-index').val()
    },
    $('#model-review-list'), function ($target, response) {
        if (response.success != undefined && !response.success)
            $target.html("<h3 class='no-data'>" + response.message + "</h3>");
        else
            $target.html(response);
    });
}

function showSameStyleModels(navigation) {
    Ajax.bind('/Model/GetModelSameStylePaging',
    {
        page: $('#page-index').val(),
        size: $('#items-per-page').val(),
        sort: "",
        manufacturer: $('#current-manufacturer').val(),
        model: $('#current-model').val(),
        year: $('#current-year').val(),
        style: $('#current-style').val(),
        navigation: navigation,
        nextPage: $('#next-page-index').val(),
        prevPage: $('#previous-page-index').val()
    },
    $('#model-same-style-list'), function ($target, response) {
        if (response.success != undefined && !response.success)
            $target.html("<h3 class='no-data'>" + response.message + "</h3>");
        else
            $target.html(response);
    });
}

function showModelDetailInform(urlRequest, element) {
    Ajax.bind(urlRequest,
    {
        carId: $('#current-carId').val()
    },
    element, function ($target, response) {
        $target.html(response);
    });
}
