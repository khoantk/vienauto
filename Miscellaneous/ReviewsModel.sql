
ALTER TABLE hdt.Reviews_ModelDetail
ADD Title NVARCHAR(500) DEFAULT('')

ALTER TABLE hdt.Reviews_ModelDetail
ADD Name NVARCHAR(100) DEFAULT('')

ALTER TABLE hdt.Reviews_ModelDetail
ADD Email NVARCHAR(500) DEFAULT('')

ALTER TABLE hdt.Reviews_ModelDetail
ADD Phone NVARCHAR(50) DEFAULT('')

