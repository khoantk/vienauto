﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SearchAgent : System.Web.UI.Page
{
    Methods method = new Methods();
    Connect_Data connect = new Connect_Data();

    private int top, hang;
    private string tinhthanh, filter;
    protected IList<DiscountAgent> DiscountAgents;

    public string _filter
    {
        get
        {
            return filter;
        }
        set
        {
            filter = value;
        }
    }

    public int _top
    {
        get
        {
            if (top == 0)
                return 50;
            return top;
        }
        set
        {
            top = value;
        }
    }

    public int _hang
    {
        get
        {
            return hang;
        }
        set
        {
            hang = value;
        }
    }

    public string _tenhang
    {
        get
        {
            return GetManufacturerNameById(_hang);
        }
    }

    public string _tinhthanh
    {
        get
        {
            return tinhthanh;
        }
        set
        {
            tinhthanh = value;
        }
    }

    public SearchAgent()
    {
        DiscountAgents = new List<DiscountAgent>();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _top = method.ConvertType<int>(Request.QueryString["numb"]);
        _hang = method.ConvertType<int>(Request.QueryString["man"]);
        _filter = method.ConvertType<string>(Request.QueryString["filter"]);
        _tinhthanh = method.ConvertType<string>(Request.QueryString["country"]);

        LoadListDiscount();
        FilterDiscounts();
        LoadSeo();
    }

    private void LoadSeo()
    {
        string urlString = method.Get_rootlink() + Request.RawUrl;
        DataTable _dtseo = connect.getTableSProcedure("aa_fillmeta_seolink", urlString);
        if (_dtseo.Rows.Count > 0)
        {
            if (_dtseo.Rows[0]["result"].ToString() == "1")
            {
                this.Page.Header.Title = _dtseo.Rows[0]["title"].ToString();
                description.Text = _dtseo.Rows[0]["description"].ToString();
                keyword.Text = _dtseo.Rows[0]["keyword"].ToString();
            }
            else
            {
                var _title = "Tìm kiếm đại lý";
                var _description = "";
                var _keyword = "Tìm kiếm, Đại lý";

                this.Page.Header.Title = _title;
                keyword.Text = _keyword;
                description.Text = _description;
            }
        }
    }

    private void LoadListDiscount()
    {
        try
        {
            var query = @"select top({0}) u.Id_Users, d.Id_dangki, d.Ten_CTY,  d.Diachi_giaodich,  
                            t.Name_TinhThanh, u.Phone, u.Mobile, u.Fax, u.Avatar, t.Id_TinhThanh, 
                            u.ToaDoMap, u.ZoomMap from hdt.Hang_Phanphoi h 
                            join hdt.Users u on u.Id_Users = h.Id_DaiLy
                            join hdt.Dangki_daili_online d on d.Id_user = u.Id_Users
                            left join hdt.TinhThanh t on u.TinhThanh = t.Id_TinhThanh
                            where u.Id_Level = 4 and h.Id_Manufacturer = {1}";

            var queryParams = new object[3];
            queryParams[0] = _top;
            queryParams[1] = _hang;
            if (!string.IsNullOrEmpty(_tinhthanh))
            {
                queryParams[2] = _tinhthanh;
                query += " and t.Id_TinhThanh in ({2})";
            }
            var sqlQuery = string.Format(query, queryParams);

            var dt = connect.GetTable(sqlQuery);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    var agentId = int.Parse(dt.Rows[i]["Id_dangki"].ToString());

                    var discountAgent = new DiscountAgent()
                    {
                        AgentId = agentId,
                        UserId = int.Parse(dt.Rows[i]["Id_Users"].ToString()),
                        CountryId = int.Parse(dt.Rows[i]["Id_TinhThanh"].ToString()),
                        CountryName = dt.Rows[i]["Name_TinhThanh"].ToString(),
                        Avatar = dt.Rows[i]["Avatar"].ToString(),
                        CompanyName = dt.Rows[i]["Ten_CTY"].ToString(),
                        Address = dt.Rows[i]["Diachi_giaodich"].ToString(),
                        NewPhone = dt.Rows[i]["Phone"].ToString(),
                        OldPhone = dt.Rows[i]["Mobile"].ToString(),
                        Service = dt.Rows[i]["Fax"].ToString(),
                        Coordinates = dt.Rows[i]["ToaDoMap"].ToString(),
                        TotalReview = GetTotalReviewsOfAgent(agentId),
                        MaxOverall = GetMaxReviewOfAgent(agentId)
                    };
                    DiscountAgents.Add(discountAgent);
                }
            }
        }
        catch (Exception ex)
        { }
    }

    private void FilterDiscounts()
    {
        if (!string.IsNullOrEmpty(_filter))
        {
            switch (_filter)
            {
                case "tt":
                    DiscountAgents = DiscountAgents.OrderBy(da => da.CountryName).ToList();
                    break;
                case "dl":
                    DiscountAgents = DiscountAgents.OrderBy(da => da.CompanyName).ToList();
                    break;
                case "dg":
                    DiscountAgents = DiscountAgents.OrderBy(da => da.MaxOverall).ToList();
                    break;
                default:
                    DiscountAgents = DiscountAgents.OrderBy(da => da.CompanyName).ToList();
                    break;
            }
        }
    }

    private int GetTotalReviewsOfAgent(int agentId)
    {
        try
        {
            var count = connect.getItem("TongReviewDaily", agentId);
            return int.Parse(count);
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    private string GetMaxReviewOfAgent(int agentId)
    {
        try
        {
            var max = connect.getItem("MaxReviewDaily", agentId);
            var showMax = ShowMaxReviewAgent(double.Parse(max));
            return showMax;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    private string ShowMaxReviewAgent(double rateValue)
    {
        var result = "-79px";
        if (rateValue == 0.5)
            result = "-72px";
        if (rateValue == 1.0)
            result = "-64px";
        if (rateValue == 1.5)
            result = "-56px";
        if (rateValue == 2.0)
            result = "-48px";
        if (rateValue == 2.5)
            result = "-40px";
        if (rateValue == 3.0)
            result = "-32px";
        if (rateValue == 3.5)
            result = "-24px";
        if (rateValue == 4.0)
            result = "-16px";
        if (rateValue == 4.5)
            result = "-8px";
        if (rateValue == 5.0)
            result = "0px";

        return result;
    }

    private string GetManufacturerNameById(int manufacturerId)
    {
        try
        {
            var name = connect.getOnlyItem(@"select Name_Manufacturer from hdt.Manufacturer 
                                                    where Id_Manufacturer = " + manufacturerId);
            return name;
        }
        catch (Exception ex)
        {
            return "";
        }
    }
}

public class DiscountAgent
{
    public int UserId { get; set; }
    public int AgentId { get; set; }
    public int CountryId { get; set; }
    public string CountryName { get; set; }
    public string Avatar { get; set; }
    public string CompanyName { get; set; }
    public string Address { get; set; }
    public string NewPhone { get; set; }
    public string OldPhone { get; set; }
    public string Service { get; set; }
    public string Coordinates { get; set; }
    public int TotalReview { get; set; }
    public string MaxOverall { get; set; }
}