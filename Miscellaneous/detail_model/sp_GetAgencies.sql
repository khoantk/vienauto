USE [carbank]
GO

/****** Object:  StoredProcedure [hdt].[sp_GetAgencies]    Script Date: 08/04/2017 6:37:50 CH ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [hdt].[sp_GetAgencies]
	@dealershipId INT = 0,
	@provinceId INT = 0,
	@sort NVARCHAR(200) = '',
	@page INT = 1,
	@size INT = 5
AS
BEGIN
	DECLARE @Query NVARCHAR(MAX)
	SET @Query = CONCAT(
	'SELECT
		d.Id_user AS UserId,
		d.Id_dangki AS AgencyId,
		d.Ten_CTY AS CompanyName,
		d.Diachi_giaodich AS [Address],
		t.Name_TinhThanh AS ProvinceName,
		u.Phone AS NewPhone,
		u.Mobile AS OldPhone,
		u.Fax AS [Service],
		t.Id_TinhThanh AS CountryId,
		u.ToaDoMap AS Coordinates,
		h.Id_Hang_PhanPhoi AS DealerShipId,
		m.logo AS ManufacturerAvatar,
		m.Id_Manufacturer AS ManufacturerId,
		m.Name_Manufacturer AS ManufacturerName,
		MAX(ra.Overall) AS MaxPointReview,
		COUNT(ra.Id_Reviews) AS TotalReview,
		TotalPages = (COUNT(*) OVER () + ', @size ,' - 1) / ', @size,
	'FROM hdt.Dangki_daili_online d
	JOIN hdt.Users u
		ON u.Id_Users = d.Id_user
	JOIN hdt.Hang_Phanphoi h
		ON h.Id_Daily = u.Id_Users
	JOIN hdt.Manufacturer m
		ON m.Id_Manufacturer = h.Id_Manufacturer
	JOIN hdt.TinhThanh t
		ON u.TinhThanh = t.Id_TinhThanh
	LEFT JOIN hdt.Reviews_Agent ra
		ON ra.Id_Agent = d.Id_dangki
	WHERE u.Id_Level = 4')

	IF @provinceId <> 0
		SET @Query = CONCAT(@Query, ' AND t.Id_TinhThanh = ', @provinceId)
	IF @dealershipId <> 0
		SET @Query = CONCAT(@Query, ' AND m.Id_Manufacturer = ', @dealershipId)

	SET @Query = CONCAT(@Query, 
	' GROUP BY d.Id_user,
			   d.Id_dangki,
			   d.Ten_CTY,
			   d.Diachi_giaodich,
			   t.Name_TinhThanh,
			   u.Phone,
			   u.Mobile,
			   u.Fax,
			   t.Id_TinhThanh,
			   u.ToaDoMap,
			   h.Id_Hang_PhanPhoi,
			   m.logo,
			   m.Id_Manufacturer,
			   m.Name_Manufacturer
	ORDER BY ProvinceName, CompanyName 
	OFFSET ', @size , ' * (', @page, ' - 1) ROWS FETCH NEXT ', @size ,' ROWS ONLY')
	
	PRINT @Query

	EXECUTE sp_executesql @Query
END

GO


