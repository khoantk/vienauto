USE [carbank]
GO
/****** Object:  StoredProcedure [hdt].[sp_GetModelCommonInformation]    Script Date: 25/04/2017 11:28:17 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Khoa
-- Create date: 2017-03-07
-- Description:	Get Common Model Detail
-- =============================================
alter PROCEDURE [hdt].[sp_GetModelCommonInformation] 
	@manufacturerName NVARCHAR(50),
	@modelName NVARCHAR(50),
	@yearName NVARCHAR(10),
	@carId INT = 0
AS
BEGIN
	SELECT TOP 1
	  manufacturer.Id_Manufacturer AS ManufacturerId,
	  model.Id_Mode_Product AS ModelId,
	  [year].Id_Year AS YearId,
	  car.Id_MName AS CarId,
	  product.Id_Product AS ProductId,
	  product.Price_Product AS Price,
	  unit.Name_Unit AS UnitName,
	  avatar.PathAvatar AS Avatar,
	  car.Name_MName AS CarName,
	  manufacturer.Name_Manufacturer AS ManufacturerName,
	  model.Name_Mode_Product AS ModelName,
	  CONVERT(NVARCHAR(10), [year].Name_Year) AS YearName,
	  style.Name_TypeProduct AS StyleName,
	  specification.Fuel_Economy_City AS FuelEconomyCity,
	  specification.Fuel_Economy_Highway AS FuelEconomyHighway
	FROM MName car
	JOIN [Year] [year]
	  ON [year].Id_Year = car.Id_Year
	JOIN Mode_Product model
	  ON model.Id_Mode_Product = [year].Id_Mode_Product
	JOIN Type_Product style
	  ON style.Id_TypeProduct = model.Id_TypeProduct
	JOIN Manufacturer manufacturer
	  ON manufacturer.Id_Manufacturer = style.Id_Manufacturer
	LEFT JOIN Avatar avatar
	  ON avatar.Id_MName = car.Id_MName
	LEFT JOIN Product product
	  ON product.Id_MName = car.Id_MName
	LEFT JOIN Unit unit
	  ON product.Id_Unit = unit.Id_Unit
	LEFT JOIN Specifications specification
	  ON specification.Id_MName = car.Id_MName
	WHERE car.Id_MName = 
	CASE WHEN @carId = 0 THEN 
	(
		SELECT TOP 1 car.Id_MName 
		FROM MName car
		JOIN [Year] [year]
			ON [year].Id_Year = car.Id_Year
		JOIN Mode_Product model
			ON model.Id_Mode_Product = [year].Id_Mode_Product
		JOIN Type_Product style
			ON style.Id_TypeProduct = model.Id_TypeProduct
		JOIN Manufacturer manufacturer
			ON manufacturer.Id_Manufacturer = style.Id_Manufacturer
		WHERE manufacturer.Name_Manufacturer = LTRIM(RTRIM(@manufacturerName))
		AND model.Name_Mode_Product = LTRIM(RTRIM(@modelName))
		AND [year].Name_Year = LTRIM(RTRIM(@yearName))
		ORDER BY car.Id_MName DESC
	)
	ELSE @carId
	END
	ORDER BY car.Id_MName DESC
END
