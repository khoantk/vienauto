﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/scpage.master" AutoEventWireup="true" CodeFile="cardetail.aspx.cs" Inherits="cardetail" EnableViewState="true" %>
<asp:Content ID="Content2" ContentPlaceHolderID="first_head_of_web" Runat="Server">
    <meta name="keywords" content=" <asp:Literal ID="keyword" runat="server"></asp:Literal>" />
    <meta name="description" content=" <asp:Literal ID="description" runat="server"></asp:Literal> " />
    <asp:Literal ID="imageheader" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="lasted_head_meta_of_site" Runat="Server">
    <% Methods method = new Methods(); string _rootlink = method.Get_rootlink(); %>
    <% Random rd = new Random(); double _n = rd.NextDouble();%>
    <% double _n1 = rd.NextDouble(); %>
    <% double _n2 = rd.NextDouble(); %>
    <link rel="stylesheet" type="text/css" href="//graphics.cars.com/core/css/cars/core.css" />
    <!--[if IE 7]>
      <link rel="stylesheet" href="//graphics.cars.com/core/css/cars/ie7.css" />
    <![endif]-->
        <!--[if IE 8]>
      <link rel="stylesheet" href="//graphics.cars.com/core/css/cars/ie8.css" />
    <![endif]-->
        <!--[if IE 9]>
      <link rel="stylesheet" href="//graphics.cars.com/core/css/cars/ie9.css" />
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="//graphics.cars.com/core/css/cars/frontdoors.css">
    <link rel="stylesheet" type="text/css" href="//graphics.cars.com/core/css/cars/frontdoorsMain.css">
    <link rel="stylesheet" type="text/css" href="//graphics.cars.com/core/css/cars/research.css">

    <!--[if lte IE 8]>
        <link rel="Stylesheet" href="/Sc_themes/ie-fix.css" />
    <![endif]-->
        <!--[if lte IE 7]>
        <link rel="Stylesheet" href="/Sc_themes/ie7-fix.css" />
    <![endif]-->
        <!--[if lte IE 9]>
      <script type="text/javascript">
        document.createElement('header');
        document.createElement('nav');
        document.createElement('article');
        document.createElement('footer');
        </script>
    <![endif]-->
        <!--[if lt IE 9]>
        <script src="/Scripts/html5shiv.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="/js/sc/loader/f/hdt-api-min/<%=method.Encode64(method.Encode64(_n.ToString())) %>"></script>
    <script type="text/javascript" src="/js/sc/loader/d/onload-min/<%=method.Encode64(method.Encode64(_n1.ToString())) %>"></script>
    <script src="/js/items/012a_actionbook_share_in.js" type="text/javascript"></script>

    
    <%--<script data-main="/core/js/main.js" src="/core/js/libs/requirejs/require.js"></script>
    <script src="/core/js/libs/cars/carsAdTagGenerator.js"></script>
    <script src="//cdn.optimizely.com/js/7544042.js"></script>

    <script src="https://www.cars.com/static/www/carsBridge.js"></script>
    <meta name="viewport" content="maximum-scale=1.0" />
    <script type="text/javascript">
        var CARS = CARS || {};
    </script>--%>

    <script src="/Scripts/slide_dongxedaidien/jssor.js" type="text/javascript"></script>
    <script src="/Scripts/slide_dongxedaidien/jssor.slider.js" type="text/javascript"></script>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="content_of_site" Runat="Server">
    <% Methods method = new Methods(); string _rootlink = method.Get_rootlink(); %>
    <asp:HiddenField ID="hidTab" runat="server" />
    <asp:HiddenField ID="hidModeId" runat="server" />
    <asp:HiddenField ID="hidModeAgentId" runat="server" />
    <input type="hidden" id="hidBrand" value="<%= _NameBrand %>" />
    <input type="hidden" id="hidMode" value="<%= _NameMode %>" />
    <input type="hidden" id="hidYear" value="<%= _NameYear %>" />
    <input type="hidden" id="hidBrandRewrite" value="<%= _NameBrandRewrite %>" />
    <input type="hidden" id="hidModeRewrite" value="<%= _NameModeRewrite %>" />
    <div id="page" class="mmy-page">
        <!-- this will load the .main-header -->
        <div class="grid-layout">
            <div class="mmy-hero">
                <div class="mmy-breadcrumbs-tools">
                    <ul class="breadcrumbs mmy-breadcrumbs">
                        <li class="crumb">
                            <a href="/"><span >Trang chủ</span> </a></li>
                        <li class="crumb">
                            <a href="/<%= _NameBrandRewrite %>.html"><span class="upperName"><%= _NameBrand %></span> </a></li>
                        <li class="crumb">
                            <a href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>">
                            <span class="upperName"><%= _NameMode %></span> </a></li>      
                        <li class="crumb">                                                 
                            <a href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/">
                            <span class="upperName"><%= _NameYear %></span> </a>
                        </li>
                        <li class="currentCrumb">
                            <span class="page-name">
                                <% 
                                    switch (hidTab.Value.ToLower())
                                    {
                                        case "gioithieu":
                                            %>
                                                Giới thiệu
                                            <%
                                            break;
                                        case "dongco":
                                            %>
                                                Động cơ
                                            <%
                                            break;
                                        case "antoan":
                                            %>
                                                An toàn
                                            <%
                                            break;
                                        case "giaitri":
                                            %>
                                                Giải trí
                                            <%
                                            break;
                                        case "noithat":
                                            %>
                                                Nội thất
                                            <%
                                            break;
                                        case "ngoaithat":
                                            %>
                                                Ngoại thất
                                            <%
                                            break;
                                        case "mausac":
                                            %>
                                                Màu sắc
                                            <%
                                            break;   
                                        case "hinhanh":
                                            %>
                                                Hình ảnh
                                            <%
                                            break;
                                        case "video":
                                            %>
                                                Video
                                            <%
                                            break;
                                        case "danhgia":
                                            %>
                                                Đánh giá
                                            <%
                                            break;
                                        default:
                                            %>
                                                Giới thiệu
                                            <%
                                            break; 
                                    }
                                %>
                            </span>
                        </li>
                    </ul>
                </div>
                <!-- begin jsp/vehicle-research/includes/_header.jsp    ##### -->
                <div class="mmy-title-box">
                    <div class="car-name">
                        <h1 class="mmy-car-title">
                            <span class="upperName"><%= _NameYear %></span>
                            <span class="upperName"><%= _NameBrand %></span>
                            <%= _NameMode %>
                        </h1>
                        <div class="fewughfui">
                            <div id="ih8e7yiu43" class="bookmark" align="left">Theo dõi</div>
                            <div id="KUh837yhwe" class="share" align="left">Chia sẻ</div>
                            <div id="Lireou8943u" class="print" align="left">In </div>
                        </div>
                    </div>
                    <div class="rating consumer" id="detailRating">
                        <%
                            var idProduct = CarDetailInfo != null ? CarDetailInfo.IdSP : 0;
                            var idMName = CarDetailInfo != null ? CarDetailInfo.IdMName : 0;
                            var review = (CarDetailInfo != null && !string.IsNullOrEmpty(CarDetailInfo.Review)) ? CarDetailInfo.Review : "0px";
                        %>
                        <input type="hidden" class="hidReviewed" value="<%= IsReview %>" />
                        <input type="hidden" class="hidDanhGia" value="<%= review %>" />
                        <input type="hidden" class="hidIdCar" value="<%= idMName %>" />
                        <input type="hidden" class="hidIdProduct" value="<%= idProduct %>" />
                        <div class="wrapper">
                            <div class="value">
                            </div>
                        </div>
                        <div class="detail">
                            <a id="buttonReview" href="javascript:void(0);" onclick="gotoReviews();">Viết đánh giá</a>
                        </div>
                    </div>
                </div>
                <!-- end jsp/vehicle-research/includes/_header.jsp    ##### -->
                <!-- begin jsp/vehicle-research/includes/_mmyModule.jsp ##### -->
                <div class="mmy-main-module">
                    <div class="row top">
                        <div class="col45">
                            <div class="photo-box">
                                <div class="main-image">
                                    <img src="<%= CarDetailInfo.Avatar %>" alt="<%= _NameYear %> <%= _NameBrand %> <%= _NameMode %>" width="624" height="300" />
                                </div>
                                <span class="helper-text">
                                    <span>
                                    <%  if (SameTypeCars != null && SameTypeCars.Count > 0)
                                        { %>
                                            <strong>Có <%= SameTypeCars.Count%> xe cùng dòng xe:&nbsp;</strong> 
                                    <%  }
                                        else
                                        { %>
                                            <strong>Không có xe nào cùng dòng xe:&nbsp;</strong>
                                    <%  } %>
                                    <span class="upperName"><%= _NameYear %> <%= _NameBrand %> <%= _NameMode %></span> 
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col15">
                            <div class="prices">
                                <h5>
                                    Giá tham khảo 
                                    <span class="icon-price"></span>
                                </h5>
                                <span class="callout">
                                <% 
                                    var price = CarDetailInfo != null ? CarDetailInfo.Gia + " " + CarDetailInfo.DonVi : "N/A";
                                    var mpgCity = (CarDetailInfo != null && !string.IsNullOrEmpty(CarDetailInfo.THNLTP))
                                                ? CarDetailInfo.THNLTP : string.Empty;
                                    var mpgHighway = (CarDetailInfo != null && !string.IsNullOrEmpty(CarDetailInfo.THNLCT))
                                                ? CarDetailInfo.THNLCT : string.Empty;
                                %>
                                <%= price %>
                                </span>
                            </div>
                            <div class="mmy-mpg">
                                <h5>
                                    Hao nhiên liệu (TP/Cao tốc)
                                    <span class="icon-mpg"></span>
                                </h5>
                                <p>
                                    <span class="callout">
                                        <span class="callout-text">&nbsp;<%= mpgCity %> lít</span>
                                        &nbsp;/&nbsp;<span class="callout-text">&nbsp;<%= mpgHighway %> lít</span>
                                    </span>
                                </p>
                            </div>
                            <div>
                                <div class="button-box">
                                    <a id="comparesbs" href="#" onclick="openInfoForm();" class="button">Gửi yêu cầu tư vấn xe</a>
                                </div>
                                <div class="button-box">
                                    <a id="priceopt" href="/hang-dai-ly.html" class="button secondary">Đại lý của chúng tôi</a>
                                </div>
                                <div class="button-box">
                                    <a class="button secondary" href="<%= _LinkDangBan %>">Đăng bán xe này</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end jsp/vehicle-research/includes/_mmyModule.jsp ##### -->
                <div class="mmy-nav small-top-margin">
                    <div class="tabs">
                        <a id="TongQuan" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/gioithieu">Tổng quan</a> 
                        <a id="ThongSo" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/dongco">Thông số</a> 
                        <a id="GNN" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/giaitri">Giải trí &amp; Nội ngoại thất</a> 
                        <a id="Video" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/video">Video &amp; Hình ảnh</a>
                        <a id="DanhGia" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/danhgia">Đánh giá</a>
                    </div>
                    <% 
                        if (hidTab.Value == "dongco" || hidTab.Value == "antoan")
                        { %>
                    <div class="sub-tabs">
                        <a id="SubDongCo" name="DongCo" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/dongco">Động cơ</a>
                        <a id="SubAnToan" name="AnToan" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/antoan">An toàn</a>
                    </div>
                    <%  }
                        if (hidTab.Value == "giaitri" || hidTab.Value == "noithat" || 
                            hidTab.Value == "ngoaithat" || hidTab.Value == "mausac")
                        {  %>
                    <div class="sub-tabs">
                        <a id="SubGiaiTri" name="GiaiTri" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/giaitri">Giải trí</a>
                        <a id="SubNoiThat" name="NoiThat" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/noithat">Nội thất</a>
                        <a id="SubNgoaiThat" name="NgoaiThat" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/ngoaithat">Ngoại thất</a> 
                        <a id="SubMauSac" name="MauSac" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/mausac">Màu sắc</a>
                    </div>
                    <%  } %>
                    <%  if (hidTab.Value == "hinhanh" || hidTab.Value == "video")
                        { %>
                    <div class="sub-tabs">
                        <a id="SubVideo" name="Video" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/video">Video</a>
                        <a id="SubHinhAnh" name="HinhAnh" href="/<%= _NameBrandRewrite %>/<%= _NameModeRewrite %>/<%= _NameYear %>/hinhanh">Hình ảnh</a>
                    </div>
                    <%  } %>
                </div>
            </div>
            <div class="row tab-specific-content">
                <div class="col41 content-body">
                    <% if (IntroductionInfo != null)
                       { %>
                       <div class="pad-box">
                        <div class="row">
                            &nbsp; &nbsp;
                            <div class="col26">
                                <h2>Giới thiệu</h2>
                            </div>
                        </div>
                        <div class="row-end">
                        </div>
                        <div class="row">
                            <% if (!string.IsNullOrEmpty(IntroductionInfo.GioiThieu))
                                { %>
                                <div class="col40">
                                    <%= IntroductionInfo.GioiThieu%>
                                </div>
                            <%  } else
                                { %>
                                    <div class="js-manufacturerIncentives">
                                        <div class="js-mi-widget">
                                            <div class="alert-message notification">
                                                <span>
                                                Hiện chưa có giới thiệu tổng quan cho dòng xe
                                                </span>
                                                <p>Vienauto.com sẽ cập nhật thông tin sớm nhất có thể.</p>
                                            </div>
                                        </div>
                                    </div>
                            <%  } %>
                        </div>
                    </div>
                    <% }
                       if (EngineInfo != null)
                       {
                    %>
                    <div class="pad-box">
                        <div class="row">
                            &nbsp; &nbsp;
                            <div class="col26">
                                <h2>Động cơ</h2>
                            </div>
                        </div>
                        <div class="row-end">
                        </div>
                        <p class="thongbao">Thông số trên chỉ mang tính chất tham khảo, có thể khác ở từng phiên bản của dòng xe</p>
                        <div class="row">
                            <div class="col12">
                                <ul class="list">
                                    <li>Động cơ: <%= EngineInfo.DongCo %> </li>
                                    <li>Kiểu động cơ: <%= EngineInfo.KieuDongCo %></li>
                                    <li>Tỷ số nén: <%= EngineInfo.TySoNen %></li>
                                    <li>Kiểu dẫn động: <%= EngineInfo.KieuDanDong %></li>
                                    <li>Loại hộp số: <%= EngineInfo.LoaiHopSo %></li>
                                    <li>Nhiên liệu: <%= EngineInfo.NhienLieu %></li>
                                    <li>Tiêu hao nhiên liệu (TP): <%= EngineInfo.TieuHaoNhienLieuThanhPho %> lít</li>
                                    <li>Tiêu hao nhiên liệu (Cao tốc): <%= EngineInfo.TieuHaoNhienLieuCaoToc %> lít</li>
                                    <li>Dung tích bình nhiên liệu: <%= EngineInfo.DungTichBinhNhienLieu %> lít</li>
                                </ul>
                            </div>
                            <div class="col13">
                                <ul class="list">
                                    <li>Cơ cấu van: <%= EngineInfo.CoCauVan %></li>
                                    <li>Hệ thống cân bằng điện tử (ESS): <%= EngineInfo.HeThongCanBangDienTuESS %></li>
                                    <li>Hệ thống phanh điện tử (EBD): <%= EngineInfo.HeThongCanBangDienTuEBD %></li>
                                    <li>Điều khiển từ xa: <%= EngineInfo.HeThongDieuKhienTuXa %></li>
                                    <li>Cấp số: <%= EngineInfo.CapSo %></li>
                                    <li>Chiều dài: <%= EngineInfo.ChieuDai %> mm</li>
                                    <li>Chiều rộng: <%= EngineInfo.ChieuRong %> mm</li>
                                    <li>Chiều cao: <%= EngineInfo.ChieuCao %> mm</li>
                                </ul>
                            </div>
                            <div class="col11">
                                <ul class="list">
                                    <li>Mã lực: <%= EngineInfo.MaLuc %></li>
                                    <li>Mômen xoắn: <%= EngineInfo.MoMenXoan %></li>
                                    <li>Hệ số cản: <%= EngineInfo.HeSoCan %></li>
                                    <li>Thời gian tăng tốc (0-100km/h): <%= EngineInfo.TangToc %> s</li>
                                    <li>Tốc độ tối đa: <%= EngineInfo.TocDoToiDa %> km/h</li>
                                    <li>Trọng lượng không tải: <%= EngineInfo.TrongLuongKhongTai %> kg</li>
                                    <li>Trọng lượng toàn tải: <%= EngineInfo.TrongLuongToanTai %> kg</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <% } %>
                    <% 
                       if (SafetyInfo != null)
                       {
                    %>
                    <div class="pad-box">
                        <div class="row">
                            &nbsp; &nbsp;
                            <div class="col26">
                                <h2>An toàn</h2>
                            </div>
                        </div>
                        <div class="row-end">
                        </div>
                        <p class="thongbao">Thông số trên chỉ mang tính chất tham khảo, có thể khác ở từng phiên bản của dòng xe</p>
                        <div class="row">
                            <div class="col20">
                                <ul class="list">
                                    <li>Túi khí trước: <%= SafetyInfo.TuiKhiTruoc %></li>
                                    <li>Túi khí bên: <%= SafetyInfo.TuiKhiBen %></li>
                                    <li>Túi khí sau: <%= SafetyInfo.TuiKhiSau %></li>
                                    <li>Đèn pha: <span class="upperName"><%= SafetyInfo.DenPha %></span></li>
                                    <li>Kiểm soát ánh sáng: <%= SafetyInfo.KiemSoatAnhSang %></li>
                                    <li>Chiếu sáng ban ngày: <%= SafetyInfo.ChieuSangBanNgay %></li>
                                    <li>Đèn chiếu hậu: <%= SafetyInfo.DenChieuHau %></li>
                                    <li>Hỗ trợ đỗ xe: <%= SafetyInfo.HoTroDoXe %></li>
                                </ul>
                            </div>
                            <div class="col20">
                                <ul class="list">
                                    <li>Hệ thống báo động: <%= SafetyInfo.HeThongBaoDong %></li>
                                    <li>Hệ thống khóa cửa: <%= SafetyInfo.HeThongKhoaCua %></li>
                                    <li>Khóa an toàn: <%= SafetyInfo.KhoaAnToan %></li>
                                    <li>Hệ thống chống trộm: <%= SafetyInfo.HeThongChongTrom %></li>
                                    <li>Cảnh báo áp suất thấp: <%= SafetyInfo.CanhBaoApSuatThap %></li>
                                    <li>Loại phanh: <%= SafetyInfo.LoaiPhanh %></li>
                                    <li>Hệ thống phanh ABS: <%= SafetyInfo.HeThongPhanhABS %></li>
                                    <li>Hỗ trợ phanh: <span class="upperName"><%= SafetyInfo.HoTroPhanh %></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <% }
                        if (EntertainmentInfo != null)
                        {
                    %>
                    <div class="pad-box">
                        <div class="row">
                            &nbsp; &nbsp;
                            <div class="col26">
                                <h2>Giải trí</h2>
                            </div>
                        </div>
                        <div class="row-end">
                        </div>
                        <p class="thongbao">Thông số trên chỉ mang tính chất tham khảo, có thể khác ở từng phiên bản của dòng xe</p>
                        <div class="row">
                            <div class="col20">
                                <ul class="list">
                                    <li>Radio: <%= EntertainmentInfo.Radio %></li>
                                    <li>CD Player: <%= EntertainmentInfo.CDPlayer %></li>
                                    <li>DVD Audio: <%= EntertainmentInfo.DVDAudio %></li>
                                    <li>Nhận dạng giọng nói: <%= EntertainmentInfo.NhanDangGiongNoi %></li>
                                    <li>Loa: <%= EntertainmentInfo.Loa %></li>
                                </ul>
                            </div>
                            <div class="col20">
                                <ul class="list">
                                    <li>Bộ khuếch đại: <%= EntertainmentInfo.BoKhuechDai %></li>
                                    <li>Hỗ trợ Bluetooth: <%= EntertainmentInfo.HoTroBlueTooth %></li>
                                    <li>Hỗ trợ Wifi: <%= EntertainmentInfo.HoTroWifi %></li>
                                    <li>Hỗ trợ 3G: <%= EntertainmentInfo.HoTro3G %></li>
                                    <li>Định vị toàn cầu: <%= EntertainmentInfo.DinhViToanCau%></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <% }
                        if (InteriorInfo != null)
                        {
                       %>
                        <div class="pad-box">
                            <div class="row">
                                &nbsp; &nbsp;
                                <div class="col26">
                                    <h2>Nội thất</h2>
                                </div>
                            </div>
                            <div class="row-end">
                            </div>
                            <% if (!string.IsNullOrEmpty(InteriorInfo.NoiThatFlash))
                               { %>
                            <div class="row">
                                <%= InteriorInfo.NoiThatFlash%>
                            </div>
                            <div class="row-end">
                            </div>
                            <% } %>
                            <p class="thongbao">Thông số trên chỉ mang tính chất tham khảo, có thể khác ở từng phiên bản của dòng xe</p>
                            <div class="row">
                                <div class="col20">
                                    <ul class="list">
                                        <li>Điều hòa trước: 
                                            <% if (InteriorInfo.DieuHoaTruoc == 1) { %> Có <% } else { %> Không <% } %>
                                        </li>
                                        <li>Điều hòa sau: 
                                            <% if (InteriorInfo.DieuHoaSau == 1){ %> Có <% } else { %> Không <% } %>
                                        </li>
                                        <li>Bộ lọc không khí: 
                                            <% if (InteriorInfo.BoLocKhongKhi == 1) { %> Có <% } else { %> Không <% } %>
                                        </li>
                                        <li>Chỗ ngồi: <%= InteriorInfo.ChoNgoi %></li>
                                        <li>Loại ghế trước: <%= InteriorInfo.LoaiGheTruoc %></li>
                                        <li>Gội đầu: <%= InteriorInfo.GoiDau %></li>
                                        <li>La bàn: <%= InteriorInfo.LaBan %></li>
                                    </ul>
                                </div>
                                <div class="col20">
                                    <ul class="list">
                                        <li>Đồng hồ: <%= InteriorInfo.DongHo %></li>
                                        <li>Ổ cắm điện (V): <%= InteriorInfo.OCamDien %></li>
                                        <li>Máy đo tốc độ: <%= InteriorInfo.MayDoTocDo %></li>
                                        <li>Máy đo nhiệt độ nước: <%= InteriorInfo.MayDoNhietDoNuoc %></li>
                                        <li>Máy cảnh báo nhiên liệu: <%= InteriorInfo.MayCanhBaoNhienLieu %></li>
                                        <li>Hiển thị nhiệt độ ngoài: <%= InteriorInfo.HienThiNhietDoNgoai %></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <% }
                       if (ExteriorInfo != null)
                       {
                    %>
                        <div class="pad-box">
                            <div class="row">
                                &nbsp; &nbsp;
                                <div class="col26">
                                    <h2>Ngoại thất</h2>
                                </div>
                            </div>
                            <div class="row-end">
                            </div>
                            <% if (!string.IsNullOrEmpty(ExteriorInfo.NgoaiThatFlash))
                               { %>
                            <div class="row">
                                <%= ExteriorInfo.NgoaiThatFlash%>
                            </div>
                            <div class="row-end">
                            </div>
                            <% } %>
                            <p class="thongbao">Thông số trên chỉ mang tính chất tham khảo, có thể khác ở từng phiên bản của dòng xe</p>
                            <div class="row">
                                <div class="col20">
                                    <ul class="list">
                                        <li>Thông số lốp trước: <%= ExteriorInfo.ThongSoLopTruoc %></li>
                                        <li>Thông số lốp sau: <%= ExteriorInfo.ThongSoLopSau %></li>
                                        <li>Thông số mâm: <%= ExteriorInfo.ThongSoMam %></li>
                                        <li>Cửa sổ trời: <%= ExteriorInfo.CuaSoTroi %></li>
                                        <li>Loại cửa sau: <%= ExteriorInfo.LoaiCuaSau %></li>
                                    </ul>
                                </div>
                                <div class="col20">
                                    <ul class="list">
                                        <li>Sổ cửa: <%= ExteriorInfo.SoCua %></li>
                                        <li>Kính hậu: <%= ExteriorInfo.KinhHau %></li>
                                        <li>Xông kính: <%= ExteriorInfo.XongKinh %></li>
                                        <li>Cảm biến mưa: <%= ExteriorInfo.CamBienMua %></li>
                                        <li>Hệ thống nâng hạ gầm: <%= ExteriorInfo.HeThongNangHaGam %></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <% }
                       if (ColorInfo != null)
                       { %>
                       <div class="pad-box">
                        <div class="row">
                            &nbsp; &nbsp;
                            <div class="col26">
                                <h2>Màu sắc</h2>
                            </div>
                        </div>
                        <div class="row-end">
                        </div>
                        <p class="thongbao">Thông số trên chỉ mang tính chất tham khảo, có thể khác ở từng phiên bản của dòng xe</p>
                        <div class="row">
                            <% if (!string.IsNullOrEmpty(ColorInfo.MauSac))
                                { %>
                                <div class="col25">
                                    <%= ColorInfo.MauSac%>
                                </div>
                            <%  } else
                                { %>
                                    <div class="js-manufacturerIncentives">
                                        <div class="js-mi-widget">
                                            <div class="alert-message notification">
                                                <span>
                                                Hiển thị màu sắc dòng xe hiện chưa có dữ liệu
                                                </span>
                                                <p>Vienauto.com sẽ cập nhật thông tin sớm nhất có thể.</p>
                                            </div>
                                        </div>
                                    </div>
                            <%  } %>
                        </div>
                    </div>
                    <% } 
                       if (VideoInfo != null)
                       { %>
                       <div class="pad-box">
                        <div class="row">
                            &nbsp; &nbsp;
                            <div class="col26">
                                <h2>Video</h2>
                            </div>
                        </div>
                        <div class="row-end">
                        </div>
                        <div class="row">
                            <div class="col40">
                                <iframe id="showYoutubeVideo" align="left" frameborder="1" height="400" width="100%" 
                                src="/ShowYoutube.aspx?key=<%= VideoInfo.YoutubeKey %>" scrolling="no"></iframe>
                            </div>
                        </div>
                    </div>
                    <% } %>
                    <% if (ImageInfo != null)
                       { %>
                        <div class="pad-box">
                            <div class="row">
                                &nbsp; &nbsp;
                                <div class="col26">
                                    <h2>Hình ảnh</h2>
                                </div>
                            </div>
                            <div class="row-end">
                            </div>
                            <div class="row">
                                <% if (ImageInfo.IdDongXeDaiDien != 0)
                                { %>
                                <div class="col30">
                                    <input type="hidden" id="tenSP" value="<%= ImageInfo.TenSP %>" />
                                    <input type="hidden" id="idHinhAnh" value="<%= ImageInfo.IdHinhAnh %>" />
                                    <input type="hidden" id="hidBrandName" value="<%= _NameBrandRewrite %>" />
                                    <input type="hidden" id="hidModeName" value="<%= _NameModeRewrite %>" />
                                    <input type="hidden" id="hidYearName" value="<%= _NameYear %>" />
                                    <!-- slide show -->
                                         <iframe id="slideDongXe" align="left" frameborder="0" height="430" width="500" src="" scrolling="yes"></iframe>
                                    <!-- end slide show -->
                                </div>
                                <%  } else
                                    { %>
                                        <div class="js-manufacturerIncentives">
                                            <div class="js-mi-widget">
                                                <div class="alert-message notification">
                                                    <span>
                                                    Hiển thị hình ảnh dhiện chưa có dữ liệu
                                                    </span>
                                                    <p>Vienauto.com sẽ cập nhật thông tin sớm nhất có thể.</p>
                                                </div>
                                            </div>
                                        </div>
                                <%  } %>
                            </div>
                        </div>
                    <% } %>
                    <% if (ReviewInfo != null)
                       { %>
                       <div class="pad-box">
                            <div class="row">
                                <link type="text/css" rel="stylesheet" href="http://reviews.cars.com/static/5353/bazaarvoice.css" />
                                <link type="text/css" rel="stylesheet" href="http://reviews.cars.com/static/5353/bazaarvoiceUI.css" />
                                <style type="text/css">
                                    .BVRRWidget div.star, .BVRRWidget div.star a
                                    {
                                        background-image: url('http://reviews.cars.com/static/5353/ratingStars_star.gif') !important;
                                    }
                                    .BVRRWidget div.star_on a
                                    {
                                        background-image: url('http://reviews.cars.com/static/5353/sparkle.gif') !important;
                                    }
                                    #BVSUFieldContainerPostTo_facebookID label
                                    {
                                        background-image: url('http://reviews.cars.com/static/5353/link-facebook.gif') !important;
                                    }
                                </style>
                                <link rel="stylesheet" type="text/css" href="//graphics.cars.com/core/css/cars/bazaarVoiceConsumerReviews.css" />
                                <div class="js-conRevWidget">
                                    <div id="js-consumer-reviews-rating">
                                        <div class="pad-box">
                                            <h2 style="color: #7B2B84; margin-bottom: 30px;">Đánh giá</h2>
                                            <span style="float: right;">
                                                <% if (ReviewInfo.CanRemove) 
                                                   { %>
                                                    <a href="javascript:removeReview('<%= ReviewInfo.IdUser %>','<%= _NameBrand %>','<%= _NameMode %>','<%= _NameYear %>');">
                                                        Hủy</a> (xóa đánh giá của bạn)
                                                <% } %>
                                            </span>
                                            <div class="row">
                                                <div class="col30">
                                                    <div class="rating consumer">
                                                        <div class="col6">
                                                            <div class="wrapper">
                                                                <div class="value">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="average-ratings-overall">
                                                        <div class="col3">
                                                            <h4></h4>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <div class="row consumer-reviews-align average-ratings-row">
                                                        <div class="col14">
                                                            <div class="rating overall">
                                                                <div class="wrapper">
                                                                    <input type="hidden" value="<%= ReviewInfo.Body %>" />
                                                                    <div class="value">
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                    Kiểu dáng thiết kế
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col14">
                                                            <div class="rating overall">
                                                                <div class="wrapper">
                                                                    <input type="hidden" value="<%= ReviewInfo.Safe %>" />
                                                                    <div class="value">
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                    Độ an toàn của xe
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row-end">
                                                        </div>
                                                    </div>
                                                    <div class="row consumer-reviews-align average-ratings-row">
                                                        <div class="col14">
                                                            <div class="rating overall">
                                                                <div class="wrapper">
                                                                    <input type="hidden" value="<%= ReviewInfo.Price %>" />
                                                                    <div class="value">
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                    Giá cả hợp lý
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col14">
                                                            <div class="rating overall">
                                                                <div class="wrapper">
                                                                    <input type="hidden" value="<%= ReviewInfo.Operation %>" />
                                                                    <div class="value">
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                    Vận hành
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row-end">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Begin Write a Review button -->
                                                <div class="col8">
                                                    <div class="row">
                                                        <div class="col9">
                                                            <br />
                                                            <a class="button" href="javascript:gotoReviews();" rel="#bv-window" id="js-write-review-modal">
                                                                Viết đánh giá</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                    </div>
                                    <div id="test1" class="module no-border">
                                        <div class="row">
                                            <div class="col19">
                                                <h2>Người tiêu thụ đánh giá</h2>
                                            </div>
                                            <div class="row-end">
                                            </div>
                                        </div>
                                        <br />
                                        <div id="js-consumer-reviews">
                                            <!-- list reviews -->
                                            <% if (Reviews != null && Reviews.Count > 0)
                                               {
                                                   foreach (var r in Reviews)
                                                   { %>
                                                    <div class="aggregrate row">
                                                        <div class="col6">
                                                            <div class="rating consumer">
                                                                <input type="hidden" value="<%= r.Aggregate %>" />
                                                                <div class="wrapper">
                                                                    <div class="value">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <% if(r.CanRemove) 
                                                           { %>
                                                            <a href="javascript:removeReview('<%= r.IdUser %>','<%= _NameBrand %>','<%= _NameMode %>','<%= _NameYear %>');">
                                                                Hủy</a> (xóa đánh giá của thành viên)
                                                        <% } %>
                                                        <p>bởi <%= r.User %> | <%= r.DatePost %></p>
                                                    </div>
                                                    <div class="points row">
                                                        <div class="col24">
                                                            <p style="word-wrap: break-word;"><%= r.Content %></p>
                                                        </div>
                                                        <div class="col13">
                                                            <div class="rating bodyRate overall">
                                                                <input type="hidden" value="<%= r.Body %>" />
                                                                <div class="wrapper">
                                                                    <div class="value">
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                    Kiểu dáng thiết kế
                                                                </div>
                                                            </div>
                                                            <div class="rating safeRate overall">
                                                                <input type="hidden" value="<%= r.Safe %>" />
                                                                <div class="wrapper">
                                                                    <div class="value">
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                    Độ an toàn của xe
                                                                </div>
                                                            </div>
                                                            <div class="rating priceRate overall">
                                                                <input type="hidden" value="<%= r.Price %>" />
                                                                <div class="wrapper">
                                                                    <div class="value">
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                    Giá cả hợp lý
                                                                </div>
                                                            </div>
                                                            <div class="rating operationRate overall">
                                                                <input type="hidden" value="<%= r.Operation %>" />
                                                                <div class="wrapper">
                                                                    <div class="value">
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                    Vận hành
                                                                </div>
                                                            </div>
                                                            <div class="clearfix">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                <% }
                                               } %>
                                            <!-- end list reviews -->
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                       </div>
                    <% } %>
                    <!-- this will load the notableFeatures.jsp widget -->
                    <div class="module premium">
                        <div class="row">
                            <h2>
                                Tất cả xe cùng kiểu dáng</h2>
                            <div class="row">
                                <div class="col39">
                                    <p>
                                        <%--Chọn các xe để so sánh--%>
                                    </p>
                                </div>
                                <div class="row-end">
                                </div>
                            </div>
                        </div>
                        <div class="row-end">
                        </div>
                        <br/>
                        <div class="gallery view3 js-compare-widget" data-default-myid="16401">
                            <div id="slider1_container" style="position: relative; top: 0px; left: -15px; width: 640px; height: 255px; overflow: hidden;">
                                <!-- Loading Screen -->
                                <div u="loading" style="position: absolute;">
                                    <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                                        background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;">
                                    </div>
                                    <div style="position: absolute; display: block; background: url(images/loading.gif) no-repeat center center;
                                        top: 0px; left: 0px;width: 100%;height:100%;">
                                    </div>
                                </div>
                                <!-- Slides Container -->
                                <div u="slides" style="cursor: move; position: absolute; left: 70px; top: 0px; width: 512px; height: 220px; overflow: hidden;">
                                <% if (NearCars != null && NearCars.Count > 0)
                                    { %>
                                    <script type="text/javascript">
                                        jQuery(document).ready(function($) {
                                            var options = {
                                                $AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                                                $AutoPlaySteps: 4,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                                                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                                                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
                                                $OriginalWidth: 512,
                                                $OriginalHeight: 220,
                                                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                                                $SlideDuration: 160,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                                                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                                                $SlideWidth: 160,                                   //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                                                $SlideHeight: 220,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                                                $SpacingX: 16,
                                                $DisplayPieces: 2,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                                                $ParkingPosition: 0,                              //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                                                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                                                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                                                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                                                $ArrowNavigatorOptions: {
                                                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                                                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                                                    $AutoCenter: 2,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                                                    $Steps: 3                                       //[Optional] Steps to go for each navigation request, default value is 1
                                                }
                                            };

                                            var jssor_slider1 = new $JssorSlider$("slider1_container", options);

                                            //responsive code begin
                                            //you can remove responsive code if you don't want the slider scales while window resizes
                                            function ScaleSlider() {
                                                var bodyWidth = document.body.clientWidth;
                                                if (bodyWidth)
                                                    jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 809));
                                                else
                                                    window.setTimeout(ScaleSlider, 30);
                                            }
                                            //ScaleSlider();

                                            //            $(window).bind("load", ScaleSlider);
                                            //            $(window).bind("resize", ScaleSlider);
                                            //            $(window).bind("orientationchange", ScaleSlider);
                                            //responsive code end
                                        });
                                    </script>
                                    <% foreach (var car in NearCars)
                                        {
                                            var _year = car.Nam;
                                            var _mode = car.DongXe;
                                            var _brand = car.HangXe;
                                            var _image = "/Upload/" + car.HinhDaiDien;
                                            var _review = !string.IsNullOrEmpty(car.DanhGia) ? car.DanhGia : "0px";
                                            var _link = "/" + car.HangXeUrl.ToLower() + "/" + car.DongXeUrl + "/" + _year;
                                            var _price = car.Gia != "N/A" ? method.FormatNumberCurrency(uint.Parse(car.Gia)) + " " + car.DonVi : "N/A";
                                    %>
                                    <div>
                                        <a href="<%= _link %>">
                                            <img alt="" src="<%= _image %>" width="160" height="106" style="position: inherit;" />
                                        </a>
                                        <a href="<%= _link %>" style="font-weight: 700; color: #098AE4; top: 105px; position: absolute;">
                                            <%= _year %> <%= _brand %> <%= _mode %>
                                        </a>
                                        <p style="line-height: 20px; color:#333; top: 140px; position: absolute; margin-bottom: 0px;">
                                            <strong>Giá tham khảo</strong>
                                            <br />
                                            <span><%= _price %></span>
                                        </p>
                                        <div class="rating consumer nearCar" style="bottom: 25px; position: absolute;">
                                            <div class="wrapper">
                                                <input class="slideRate" type="hidden" value="<%= _review %>" />
                                                <div class="value"></div>
                                            </div>
                                        </div>
                                        <div class="compare-checkbox" style="position: absolute; bottom: 0;">
                                            <input class="js-compare-checkbox" type="checkbox" value="<%= car.IdDongXeDaiDien %>" />
                                            Chọn
                                        </div>
                                    </div>
                                    <% }
                                    } %>
                                    </div>
                                <!-- Arrow Left -->
                                <span u="arrowleft" class="jssora03l">
                                </span>
                                <!-- Arrow Right -->
                                <span u="arrowright" class="jssora03r">
                                </span>
                                <a class="button secondary small light js-compare-button" 
                                    style="position: absolute; bottom: 0; left: 70px; 
                                    color: #fff !important; background: #4B80B4 !important;
                                    border: none;"
                                    href="javascript:void(0);" onclick="compareCarSlider();">So sánh xe</a>
                                <%--<span class="helper-text button-middle-align" style="position: absolute; left: 180px; bottom: 0">
                                Chọn thêm 4 dòng xe</span>--%>
                            </div>
                            <style type="text/css">
                                .jssora03l, .jssora03r {
                                    display: block;
                                    position: absolute;
                                    width: 32px;
                                    height: 70px;
                                    cursor: pointer;
                                    overflow: hidden;
                                    border: 1px solid #E1E1E1;
                                }
                                .jssora03l {
                                    background: #EEF1F3 url(/images/scroll-left.png) no-repeat center center; 
                                    border-radius: 5px 0px 0px 5px;
                                    top: 0px !important;
                                    left: 20px;
                                }
                                .jssora03r {
                                    background: #EEF1F3 url(/images/scroll-right.png) no-repeat center center; 
                                    border-radius: 0px 5px 5px 0px; 
                                    top: 0px !important;
                                    right: 6px;
                                }
                                .jssora03l:hover { background-color: #fff; }
                                .jssora03r:hover { background-color: #fff; }
                            </style>
                        </div>
                    </div>
                    <!-- this will load the expertSnapshot.jsp widget -->
                    <!-- begin jsp/vehicle-research/widgets/stylesComparePrice.jsp    ##### -->
                    <hr class="no-margin-top" />
                    <div class="module no-border js-compare-widget">
                        <h2 id="trims">
                            Năm 
                                <span class="upperName">
                                    <%= _NameYear %>
                                </span> 
                                <span class="upperName">
                                    <%= _NameBrand %>
                                </span> 
                                <%= _NameMode %>
                            <% if (SameTypeCars != null && SameTypeCars.Count > 0)
                               { %>
                                    có <%= SameTypeCars.Count %> phiên bản
                            <% }
                               else
                               { %>
                                    không có phiên bản nào
                            <% } %>
                        </h2>
                        <div class="row rule-bottom">
                            <div class="col2">
                                &nbsp;</div>
                            <div class="col9">
                                <h5>Kiểu dáng</h5>
                            </div>
                            <div class="col8">
                                <h5>Động cơ</h5>
                                <p></p>
                            </div>
                            <div class="col8">
                                <h5>Hao nhiên liệu</h5>
                                <p>TP/Cao tốc</p>
                            </div>
                            <div class="col8">
                                <h5>Giá tham khảo</h5>
                            </div>
                            <div class="row-end">
                            </div>
                        </div>
                        <%
                            if (SameTypeCars != null && SameTypeCars.Count > 0)
                            {
                                foreach (var car in SameTypeCars)
                                {
                        %>
                            <div class="row rule-bottom light">
                                <div class="col2">
                                    &nbsp;
                                    <input type="checkbox" class="js-compare-checkbox" value="<%= car.IdDongXeDaiDien %>" />
                                </div>
                                <div class="col9">
                                    <p><%= car.TenDongXeDaiDien %></p>
                                </div>
                                <div class="col8">
                                    <%= car.DongCo %>.
                                    <%= car.KieuDanDong %>, <%= car.MaLuc %>
                                    <br /><br />
                                </div>
                                <div class="col8">
                                    <%= car.THNLTP %>/<%= car.THNLCT %>
                                </div>
                                <div class="col8">
                                    <%= car.Gia %>
                                </div>
                                <div class="row-end"></div>
                            </div>
                        <%      }
                            } %>
                        <br/>
                        <div class="row">
                            <div class="col10">
                                <a href="javascript:compareCarList();" 
                                    class="button secondary small light icon-up-left js-trim-compare-button"
                                    style=" color: #fff !important; background: #4B80B4 !important; border: none;">
                                    <span>So sánh xe</span></a>
                            </div>
                            <div class="col27 helper-text js-compare-text center-align">
                            </div>
                            <div class="row-end">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col19 content-rail" data-js="content-rail">
                    <div style="margin-top: 25px; margin-left: 65px;">
                        <h3 class="title">Năm xe:</h3>
                        <div class="select sort-list" style="width: 192px; background: url('/Images/select-arrow.png') no-repeat scroll right 1px #FFF;">
                            <asp:DropDownList ID="ddlBrand" runat="server" Width="192">
                            </asp:DropDownList>
                        </div>
                        <br/>
                        <h3 class="title">Dòng xe:</h3>
                        <div class="select sort-list" style="width: 192px; background: url('/Images/select-arrow.png') no-repeat scroll right 1px #FFF;">
                            <asp:DropDownList ID="ddlMode" runat="server" Width="192">
                            </asp:DropDownList>
                        </div>
                        <br/>
                        <h3 class="title">Năm sản xuất:</h3>
                        <div class="select sort-list" style="width: 192px; background: url('/Images/select-arrow.png') no-repeat scroll right 1px #FFF;">
                            <asp:DropDownList ID="ddlYear" runat="server" Width="192">
                            </asp:DropDownList>
                        </div>
                        <div class="button-box" data-js="compare-sbs-box">
                            <a id="btnDiscovery" href="#" onclick="openModePage();" class="button">Khám phá</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-->
    <!-- footer -->
    <style type="text/css">
        .fewughfui {
            border: 1px solid #CCC;
            margin-right: 40px;
            width: 190px;
            float: right;
        }
        .fewughfui .bookmark {
            background: transparent url("/sc_themes/images/icon_ilowsuewh4.png") no-repeat scroll 0px 0px;
            color: #888;
            float: left;
            cursor: pointer;
            padding: 3px 5px 3px 20px;
        }
        .fewughfui .share {
            background: transparent url("/sc_themes/images/icon_ilowsuewh5.png") no-repeat scroll 0px 0px;
            color: #888;
            float: left;
            cursor: pointer;
            padding: 3px 5px 3px 20px;
        }
        .fewughfui .print {
            background: transparent url("/sc_themes/images/icon_ilowsuewh6.png") no-repeat scroll 0px 0px;
            color: #888;
            float: left;
            cursor: pointer;
            padding: 3px 3px 3px 20px;
        }
        
        .photo-box
        {
            position: relative;        
        }
        .helper-text
        {
            bottom: -30px;
            position: absolute;
        }
        .thongbao
        {
            font-size: 12px;
            margin-top: 5px;
        }
        .icon-price
        {
            background: transparent url("/images/icon-price.png") no-repeat scroll right bottom;
            display: inline-block;
            height: 13px;
            width: 13px;
        }
        .icon-mpg
        {
            background: transparent url("/images/mpg.png") no-repeat scroll right bottom;
            display: inline-block;
            height: 13px;
            width: 13px;
        }
        .col26 h2, .module h2
        {
            color: #7B2B84; 
            font-size: 28px;
            line-height: 30px;
            margin-top: 0px;
        }
        ul.imgMode li
        {
            float: left;    
        }
        ul.list li
        {
            background-image: url("http://graphics.cars.com/images/core/bullet.png");
        }
        .gallery .right
        {
            background-image: url("http://graphics.cars.com/images/core/scroll-right.png"); 
        }
        .gallery .left
        {
            background-image: url("http://graphics.cars.com/images/core/scroll-left.png"); 
        }
        .js-mi-widget .notification
        {
            background-image: url("http://graphics.cars.com/images/core/notification.png");
        }
        #detailRating .wrapper, .nearCar .wrapper
        {
            background-image: url('http://graphics.cars.com/images/core/rating-consumer.png');
        }
        #detailRating .value, .nearCar .value
        {
            background-image: url('http://graphics.cars.com/images/core/rating-stars-cutout.png');
        }      
        .upperName
        {
            display: inline !important;
            text-transform: capitalize;
        }
        .upperAllName
        {
            text-transform: uppercase;
        }
        hr
        {
            border-top: none !important;
            border-bottom: 1px solid #e1e1e1 !important;
            border-color: -moz-use-text-color -moz-use-text-color #E1E1E1 !important;
            -moz-border-top-colors: none;
            -moz-border-right-colors: none;
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            border-image: none;
            margin: 20px 0px;
            clear: both;
        }
        ul#slideCar
        {
            width: 512px;
            height: 270px;
        }
        ul#slideCar li
        {
            list-style: none;
            margin-left: 16px;
            width: 160px;
            float: left;    
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            loadTabActive();

            var rateValueCarMode = $(".hidDanhGia").val();
            //show aggregrate for title review
            $("#detailRating .wrapper").css("backgroundPosition", rateValueCarMode + " top");

            //show aggregrate for tab review
            $("#js-consumer-reviews-rating .consumer .wrapper").css("backgroundPosition", rateValueCarMode + " center");

            //show review for cars slide
            if ($("#slider1_container .nearCar .wrapper").length >= 0) {
                $("#slider1_container .nearCar .wrapper").each(function () {
                    var rateValue = $(this).find(".slideRate").val();
                    $(this).css("backgroundPosition", rateValue + " top");
                });
            }

            //rate point for tab review
            $(".average-ratings-row .value").each(function () {
                var rateValue = $(this).siblings("input[type='hidden']").val();
                $(this).css("width", rateValue);
            });

            //list reviews for tab review
            $("#js-consumer-reviews .aggregrate").each(function () {
                var avgReviews = $(this).find(".consumer");
                var rateAvgValueReviews = avgReviews.find("input:hidden").val();
                avgReviews.find(".wrapper").css("backgroundPosition", rateAvgValueReviews);
            });

            $("#js-consumer-reviews .points").each(function () {
                var bodyRate = $(this).find(".bodyRate");
                var bodyRateValue = bodyRate.find("input:hidden").val();
                bodyRate.find(".value").css("width", bodyRateValue);

                var safeRate = $(this).find(".safeRate");
                var safeRateValue = safeRate.find("input:hidden").val();
                safeRate.find(".value").css("width", safeRateValue);

                var priceRate = $(this).find(".priceRate");
                var priceRateValue = priceRate.find("input:hidden").val();
                priceRate.find(".value").css("width", priceRateValue);

                var operationRate = $(this).find(".operationRate");
                var operationRateValue = operationRate.find("input:hidden").val();
                operationRate.find(".value").css("width", operationRateValue);
            });
        });

        $("#<%= ddlBrand.ClientID %>").change(function() {
        var mode = $.post(_root() + "/jx/chitietdongxehome/ShowModeByBrand.aspx", { brand: $("#<%= ddlBrand.ClientID %> option:selected").text() }, function() { })
                .success(function(data4) {
                    $("#<%= ddlMode.ClientID %>").html(data4);
                });
        });

        $("#<%= ddlMode.ClientID %>").change(function() {
            var mode = $.post(_root() + "/jx/chitietdongxehome/ShowYearByMode.aspx", { mode: $("#<%= ddlMode.ClientID %> option:selected").text() }, function() { })
                .success(function(data4) {
                    $("#<%= ddlYear.ClientID %>").html(data4);
                });
        });

        function openModePage() {
            var brand = $("#<%= ddlBrand.ClientID %>").val();
            var mode = $("#<%= ddlMode.ClientID %>").val();
            var year = $("#<%= ddlYear.ClientID %>").val();
            window.open("/" + brand.toLowerCase() + "/" + mode + "/" + year, "_self");
        }

        function openInfoForm() {
            var url = "/Popup/Information.aspx?brand=" +
                    document.getElementById("hidBrand").value.toLowerCase() +"&mode=" +
                    document.getElementById("hidMode").value.toLowerCase() + "&year=" +
                    document.getElementById("hidYear").value + "&car=" +
                    document.getElementById("<%= hidModeAgentId.ClientID %>").value;

            var x = screen.width / 2 - 600 / 2;
            var y = screen.height / 2 - 620 / 2;
            
            return window.open(url, "Yêu cầu thêm thông tin xe",
            "height=620,width=600,left="+x+",top="+y+",resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=yes,directories=no,status=yes");
        }
        
        function loadIFrame() {
            var tenSanPham = $("#tenSP").val();
            var idHinhAnh = $("#idHinhAnh").val();
            var brand = $("#hidBrandName").val();
//            var brand = $("# hidBrand").val();
            var mode = $("#hidModeName").val();
            var year = $("#hidYearName").val();
            var url = "/SlideModeImages.aspx?brand=" + brand + "&mode=" + mode + "&year=" + year + "&id=" + idHinhAnh + "&sp=" + tenSanPham;
            $("#slideDongXe").attr("src", url);
        }
        
        function loadTabActive() {
            var tabClass = ".tabs";
            var subTabClass = ".sub-tabs";
            var tabValue = $("#<%= hidTab.ClientID %>").val();

            //remove all tabs activated
            $(tabClass + " a").each(function () {
                if ($(this).hasClass("active")) {
                    $(this).removeClass("active");
                }
            });

            $(subTabClass + " a").each(function () {
                if ($(this).hasClass("active")) {
                    $(this).removeClass("active");
                }
            });

            //set active tab
            if (tabValue == "gioithieu" || tabValue == "") {
                $(tabClass + " #TongQuan").addClass("active");
            }
            if (tabValue == "dongco" || tabValue == "antoan") {
                $(tabClass + " #ThongSo").addClass("active");
                
                if (tabValue == "dongco") {
                    $(subTabClass + " #SubDongCo").addClass("active");
                }
                if (tabValue == "antoan") {
                    $(subTabClass + " #SubAnToan").addClass("active");
                }
            }
            if (tabValue == "giaitri" || tabValue == "noithat" || tabValue == "ngoaithat" || tabValue == "mausac") {
                $(tabClass + " #GNN").addClass("active");

                if (tabValue == "giaitri") {
                    $(subTabClass + " #SubGiaiTri").addClass("active");
                }
                if (tabValue == "noithat") {
                    $(subTabClass + " #SubNoiThat").addClass("active");
                }
                if (tabValue == "ngoaithat") {
                    $(subTabClass + " #SubNgoaiThat").addClass("active");
                }
                if (tabValue == "mausac") {
                    $(subTabClass + " #SubMauSac").addClass("active");
                }
            }
            if (tabValue == "video" || tabValue == "hinhanh") {
                $(tabClass + " #Video").addClass("active");

                if (tabValue == "video") {
                    $(subTabClass + " #SubVideo").addClass("active");
                }
                if (tabValue == "hinhanh") {
                    $(subTabClass + " #SubHinhAnh").addClass("active");
                    loadIFrame();
                }
            }
            if (tabValue == "danhgia") {
                $(tabClass + " #DanhGia").addClass("active");
            }
        }

        function compareCarList() {
            var compareCode;
            var arrayCarCodes = new Array();
            
            $(".rule-bottom .js-compare-checkbox").each(function() {
                if($(this).prop("checked")) {
                    arrayCarCodes.push($(this).val());
                }
            });

            if (arrayCarCodes.length > 0) {

                compareCode = arrayCarCodes[0];
                arrayCarCodes.splice(0, 1);

                var url = "/compare_model_detail.aspx?idMName=" + compareCode;
                if (arrayCarCodes.length > 0) {
                    url += "&carList=" + arrayCarCodes;
                }
                window.open(url, "_self");
            }
            else {
                alert("Bạn hãy chọn một xe để bắt đầu so sánh.");
            }
        }

        function compareCarSlider() {
            var index = 0;
            var arrayCarCodes = new Array();
            var compareCode = $("#<%= hidModeAgentId.ClientID %>").val();

            $("#slider1_container .compare-checkbox .js-compare-checkbox").each(function () {
                if ($(this).prop("checked")) {
                    arrayCarCodes.push($(this).val());
                    index++;
                }
            });

            if (index == 0) {
                alert("Bạn hãy chọn một xe để bắt đầu so sánh.");
            }

            if (index <= 4) {
                if (arrayCarCodes.length > 0) {
                    var url = "/compare_model_detail.aspx?idMName=" + compareCode + "&carList=" + arrayCarCodes;
                    window.open(url, "_self");
                }
            }
            else {
                alert("Bạn chỉ được thêm tối đa 4 xe để so sánh.");
            }
        }

        function removeReview(userId, brand, model, year) {
            if (confirm("Bạn có chắc chắn muốn xóa đánh giá này?")) {

                var param = '{userId: "' + userId + '", brand: "' + brand + '", model: "' + model + '", year: "' + year + '"}';

                $.ajax({
                    type: "POST",
                    url: "/jx/items/deletereview.aspx/DeleteReview",
                    data: param,
                    cache: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var rs = data.d;
                        if (rs == "true") {
                            var hidYear = $("#hidYear").val();
                            var tab = $("#<%= hidTab.ClientID %>").val();
                            var hidModeRewrite = $("#hidModeRewrite").val();
                            var hidBrandRewrite = $("#hidBrandRewrite").val();

                            var url = "/" + hidBrandRewrite + "/" + hidModeRewrite + "/" + hidYear + "/" + tab;
                            window.open(url, "_self");
                        }
                        else {
                            show_error(rs, 300, 130);
                        }
                    },
                    error: function (xhr) {
                        show_error("Lỗi xảy ra. Liên hệ admin.", 300, 130);
                    },
                    complete: function () { }
                });
            }
        }

        function gotoReviews() {
            if ($(".hidDanhGia").val() == "-79px") {
                var _check_exists_rate = $.getJSON(_root() + "/jx/items/check_isreviews_detailmodel.aspx?hdtjsoncallback=?", { capt: getCapt(), brand: $("#hidBrand").val(),
                    model: $("#hidMode").val(), year: $("#hidYear").val()
                },
                function () { }).success(function (rs) {
                    if (rs.result == "false") {
                        if (isLogin()) {
                            var _getinfo = $.getJSON(_root() + "/jx/allpage/selectinfomation_userlogin.aspx?hdtjsoncallback=?", { capt: getCapt(), session: getCookie("___sc_session") }, function () { })
                            .success(function (rs) {
                                if (rs.result == "true") {
                                    $("body").creatNewTabed_Pinter({
                                        "w": 450,
                                        "title": "Đánh giá xe",
                                        "link": _root() + "/jx/templates/load_form_reviews.aspx?capt=" + getCapt(),
                                        "af": function () {
                                            $("#product_need_reviews").text($(".name_of_product").text());
                                            $("ul#rate_body li").mouseover(function () {
                                                var _this_index = $(this).index();
                                                $("ul#rate_body li").removeClass("ct");
                                                $(this).addClass("ct");
                                                $("ul#rate_body li").removeClass("sl");
                                                for (var i = 0; i <= _this_index; i++) {
                                                    $("ul#rate_body li").eq(i).addClass("sl");
                                                }
                                            });
                                            $("ul#rate_safe li").mouseover(function () {
                                                var _this_index = $(this).index();
                                                $("ul#rate_safe li").removeClass("ct");
                                                $(this).addClass("ct");
                                                $("ul#rate_safe li").removeClass("sl");
                                                for (var i = 0; i <= _this_index; i++) {
                                                    $("ul#rate_safe li").eq(i).addClass("sl");
                                                }
                                            });
                                            $("ul#rate_operation li").mouseover(function () {
                                                var _this_index = $(this).index();
                                                $("ul#rate_operation li").removeClass("ct");
                                                $(this).addClass("ct");
                                                $("ul#rate_operation li").removeClass("sl");
                                                for (var i = 0; i <= _this_index; i++) {
                                                    $("ul#rate_operation li").eq(i).addClass("sl");
                                                }
                                            });
                                            $("ul#rate_price li").mouseover(function () {
                                                var _this_index = $(this).index();
                                                $("ul#rate_price li").removeClass("ct");
                                                $(this).addClass("ct");
                                                $("ul#rate_price li").removeClass("sl");
                                                for (var i = 0; i <= _this_index; i++) {
                                                    $("ul#rate_price li").eq(i).addClass("sl");
                                                }
                                            });
                                            /* blur */
                                            $("#text_reviews").blur(function () {
                                                var _this_val = $(this).val();
                                                if (isEmpty(_this_val) || _this_val.length < 20) {
                                                    $(this).addClass("error");
                                                    $("#error_content").text("Nội dung đánh giá quá ngắn.");
                                                }
                                                else {
                                                    $(this).removeClass("error");
                                                    $("#error_content").text("");
                                                }
                                            });
                                            /* end blur */
                                            $("#bt_reviews_click").clickPerTimes({
                                                "timer": 1000,
                                                "af": function () {
                                                    var _text_reviews = $("#text_reviews").val();
                                                    if (_text_reviews.length < 20) {
                                                        $("#text_reviews").addClass("error");
                                                        $("#error_content").text("Nội dung đánh giá quá ngắn.");
                                                        return;
                                                    }
                                                    var _body = $("ul#rate_body li.ct").index() + 1;
                                                    var _safe = $("ul#rate_safe li.ct").index() + 1;
                                                    var _operation = $("ul#rate_operation li.ct").index() + 1;
                                                    var _price = $("ul#rate_price li.ct").index() + 1;
                                                    var _num_value = (_body + _safe + _operation + _price);
                                                    var _post_reviews = $.getJSON(_root() + "/jx/items/insert_reviews_detailmodel.aspx?hdtjsoncallback=?",
                                                                        { capt: getCapt(), brand: $("#hidBrand").val(),
                                                                            model: $("#hidMode").val(), year: $("#hidYear").val(),
                                                                            body: _body, safe: _safe, operation: _operation, price: _price,
                                                                            content: _text_reviews, numvalue: _num_value
                                                                        },
                                                    function () { }).success(function (rs) {
                                                        if (rs.result == "true") {
                                                            $("#panel_reviews_notcomplete").html("<div align='center' style='padding:30px 5px 5px 5px'><b>Cảm ơn bạn đưa ra ý kiến đánh giá cho dòng xe này. Để đảm bảo tính khách quan chúng tôi sẽ không thay đổi nội dung đánh giá của bạn nhưng chúng tôi cần kiểm duyệt nội dung trước khi cho phép nó xuất hiện ra bên ngoài. Chúng tôi sẽ thông báo đến bạn khi việc kiểm duyệt hoàn tất.</b></div>");
                                                        }
                                                        else {
                                                            show_error("Phát sinh lỗi khi đánh giá xe, vui lòng thử lại.", 300, 130);
                                                            return;
                                                        }
                                                    });
                                                }

                                            });
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            show_error("Vui lòng đăng nhập để đánh giá. Nếu bạn chưa có tài khoản, hãy đăng ký.", 300, 130);
                            setTimeout(function () {
                                var _href = window.location.href.replace("http://", "").split("#");
                                var _link_direct = _root() + "/dang-nhap.html#return|" + _href[0] + "#danh-gia-xe";
                                window.location = _link_direct;
                            }, 3000);
                        }
                    }
                });
            }
        }

    </script>
</asp:Content>

