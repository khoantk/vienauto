--should not set procedure name has 'sp' because 'sp' has in procedure name of system. 
create proc hdt.HSP_GetCarSpecification
@manufactureId int,
@modelId int,
@yearId int
as
	SELECT top 1 
	specificat5_.Engine as EngineName, 
	specificat5_.Type_Engine as EngineType,
	specificat5_.Compresstion as CompressionRatio, 
	specificat5_.Driving_Type as DrivingType, 
	specificat5_.Transmission_Type as TranmissionType, 
	specificat5_.Fuel_Type as FuelType, 
	specificat5_.Fuel_Economy_City as FuelEconomyCity, 
	specificat5_.Fuel_Economy_Highway as FuelEconomyHighway, 
	specificat5_.Locking as Locking, 
	specificat5_.Engine_Valvetrain as EngineValvetrain, 
	specificat5_.ESS as ESS, 
	specificat5_.EBD as EBD, 
	specificat5_.Remote_Vehice as RemoteVehicle, 
	specificat5_.Transmission as Tranmission, 
	specificat5_.Exterior_Length as ExteriorLength, 
	specificat5_.Exterior_Width as ExteriorWidth, 
	specificat5_.Exterior_Height as ExteriorHeight, 
	specificat5_.Horsepower as HorsePower, 
	specificat5_.Torque as Torque, 
	specificat5_.Drag_Coeficient as DragCoeficient, 
	specificat5_.TimeSpeed as TimeSpeed, 
	specificat5_.Km as Km, 
	specificat5_.Curb_Weight as CurbWeight, 
	specificat5_.GVWR as GVWR 
	FROM hdt.Manufacturer this_ 
	left outer join hdt.Type_Product stylealias1_ on this_.Id_Manufacturer=stylealias1_.Id_Manufacturer 
	left outer join hdt.Mode_Product modelalias2_ on stylealias1_.Id_TypeProduct=modelalias2_.Id_TypeProduct 
	left outer join hdt.[Year] yearalias3_ on modelalias2_.Id_Mode_Product=yearalias3_.Id_Mode_Product 
	left outer join hdt.MName caralias4_ on yearalias3_.Id_Year=caralias4_.Id_Year 
	left outer join hdt.Specifications specificat5_ on caralias4_.Id_MName=specificat5_.Id_MName 
	WHERE ((this_.Id_Manufacturer = @manufactureId and modelalias2_.Id_Mode_Product = @modelId) and yearalias3_.Id_Year = @yearId)
	ORDER BY caralias4_.Id_MName desc
