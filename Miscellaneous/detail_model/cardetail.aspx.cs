﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;

public partial class cardetail : System.Web.UI.Page
{
    #region Properties

    Methods method = new Methods();
    Connect_Data connect = new Connect_Data();

    protected bool IsReview;
    public static CarDetail CarDetailInfo;
    public static Introduction IntroductionInfo;
    public static Engine EngineInfo;
    public static Safety SafetyInfo;
    public static Entertainment EntertainmentInfo;
    public static Interior InteriorInfo;
    public static Exterior ExteriorInfo;
    public static Images ImageInfo;
    public static Color ColorInfo;
    public static Video VideoInfo;
    public static Review ReviewInfo;
    public static IList<SameTypeCar> SameTypeCars;
    public static IList<NearCar> NearCars;
    public static IList<ReviewShow> Reviews;
    public static string _LinkDangBan = "", 
                         _NameModeRewrite = "",
                         _NameBrandRewrite = "",
                         _NameBrand = "", 
                         _NameMode = "", 
                         _NameYear = "";

    #endregion

    #region Class & Constructor

    public class ModeData
    {
        public int IdDongXeDaiDien { get; set; }
        public int IdKieuXe { get; set; }
        public int IdHangXe { get; set; }
        public int IdDongXe { get; set; }
        public int IdNam { get; set; }
        public string DongXe { get; set; }
        public string KieuXe { get; set; }
        public string Nam { get; set; }
        public string Gia { get; set; }
        public string DonVi { get; set; }
    }

    public class CarDetail : ModeData
    {
        public int IdSP { get; set; }
        public int IdMName { get; set; }
        public string Review { get; set; }
        public string THNLTP { get; set; }
        public string THNLCT { get; set; }
        public string Avatar { get; set; }
    }

    public class SameTypeCar : ModeData
    {
        public int IdSP { get; set; }
        public string THNLTP { get; set; }
        public string THNLCT { get; set; }
        public string MaLuc { get; set; }
        public string DongCo { get; set; }
        public string KieuDanDong { get; set; }
        public string TenDongXeDaiDien { get; set; }
    }

    public class Introduction : ModeData
    {
        public string GioiThieu { get; set; }
    }

    public class Engine : ModeData
    {
        public string DongCo { get; set; }
        public string KieuDongCo { get; set; }
        public string TySoNen { get; set; }
        public string KieuDanDong { get; set; }
        public string LoaiHopSo { get; set; }
        public string NhienLieu { get; set; }
        public string TieuHaoNhienLieuThanhPho { get; set; }
        public string TieuHaoNhienLieuCaoToc { get; set; }
        public string DungTichBinhNhienLieu { get; set; }
        public string CoCauVan { get; set; }
        public string HeThongCanBangDienTuESS { get; set; }
        public string HeThongCanBangDienTuEBD { get; set; }
        public string HeThongDieuKhienTuXa { get; set; }
        public string CapSo { get; set; }
        public string ChieuDai { get; set; }
        public string ChieuRong { get; set; }
        public string ChieuCao { get; set; }
        public string MaLuc { get; set; }
        public string MoMenXoan { get; set; }
        public string HeSoCan { get; set; }
        public string TangToc { get; set; }
        public string TocDoToiDa { get; set; }
        public string TrongLuongKhongTai { get; set; }
        public string TrongLuongToanTai { get; set; }
    }

    public class Safety : ModeData
    {
        public string TuiKhiTruoc { get; set; }
        public string TuiKhiBen { get; set; }
        public string TuiKhiSau { get; set; }
        public string DenPha { get; set; }
        public string KiemSoatAnhSang { get; set; }
        public string ChieuSangBanNgay { get; set; }
        public string DenChieuHau { get; set; }
        public string HoTroDoXe { get; set; }
        public string HeThongBaoDong { get; set; }
        public string HeThongKhoaCua { get; set; }
        public string KhoaAnToan { get; set; }
        public string HeThongChongTrom { get; set; }
        public string CanhBaoApSuatThap { get; set; }
        public string LoaiPhanh { get; set; }
        public string HeThongPhanhABS { get; set; }
        public string HoTroPhanh { get; set; }
    }

    public class Entertainment : ModeData
    {
        public string Radio { get; set; }
        public string CDPlayer { get; set; }
        public string DVDAudio { get; set; }
        public string NhanDangGiongNoi { get; set; }
        public string Loa { get; set; }
        public string BoKhuechDai { get; set; }
        public string HoTroBlueTooth { get; set; }
        public string HoTroWifi { get; set; }
        public string HoTro3G { get; set; }
        public string DinhViToanCau { get; set; }
    }

    public class Interior : ModeData
    {
        public int DieuHoaTruoc { get; set; }
        public int DieuHoaSau { get; set; }
        public int BoLocKhongKhi { get; set; }
        public int ChoNgoi { get; set; }
        public string LoaiGheTruoc { get; set; }
        public string GoiDau { get; set; }
        public string LaBan { get; set; }
        public string DongHo { get; set; }
        public int OCamDien { get; set; }
        public string MayDoTocDo { get; set; }
        public string MayDoNhietDoNuoc { get; set; }
        public string MayCanhBaoNhienLieu { get; set; }
        public string HienThiNhietDoNgoai { get; set; }
        public string NoiThatFlash { get; set; }
    }

    public class Exterior : ModeData
    {
        public string ThongSoLopTruoc { get; set; }
        public string ThongSoLopSau { get; set; }
        public string ThongSoMam { get; set; }
        public string CuaSoTroi { get; set; }
        public string LoaiCuaSau { get; set; }
        public int SoCua { get; set; }
        public string KinhHau { get; set; }
        public string XongKinh { get; set; }
        public string CamBienMua { get; set; }
        public string HeThongNangHaGam { get; set; }
        public string NgoaiThatFlash { get; set; }
    }

    public class Color : ModeData
    {
        public string MauSac { get; set; }
    }

    public class Video : ModeData
    {
        public string YoutubeKey { get; set; }
        public string HangXe { get; set; }
    }

    public class Images : ModeData
    {
        public string TenSP { get; set; }
        public string IdHinhAnh { get; set; }
    }

    public class Review : ModeData
    {
        public bool CanRemove { get; set; }
        public int IdUser { get; set; }
        public int IdReview { get; set; }
        public string Body { get; set; }
        public string Safe { get; set; }
        public string Price { get; set; }
        public string Operation { get; set; }
    }

    public class ReviewShow
    {
        public double AggregateNumb { get; set; }
        public string Aggregate { get; set; }
        public string Body { get; set; }
        public string Safe { get; set; }
        public string Price { get; set; }
        public string Operation { get; set; }
        public string Content { get; set; }
        public string DatePost { get; set; }
        public string User { get; set; }
        public int IdUser { get; set; }
        public int IdReview { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public bool CanRemove { get; set; }
    }

    public class ModeImage
    {
        public string ImageName { get; set; }
    }

    public class NearCar
    {
        public int IdSP { get; set; }
        public int IdDongXe { get; set; }
        public int IdKieuXe { get; set; }
        public int IdDongXeDaiDien { get; set; }
        public string HinhDaiDien { get; set; }
        public string HangXe { get; set; }
        public string DongXe { get; set; }
        public string HangXeUrl { get; set; }
        public string DongXeUrl { get; set; }
        public string Gia { get; set; }
        public string DonVi { get; set; }
        public string Nam { get; set; }
        public string DanhGia { get; set; }
    }

    public cardetail()
    {
        IntroductionInfo = null;
        EngineInfo = null;
        SafetyInfo = null;
        EntertainmentInfo = null;
        InteriorInfo = null;
        ExteriorInfo = null;
        CarDetailInfo = null;
        ImageInfo = null;
        ColorInfo = null;
        VideoInfo = null;
        ReviewInfo = null;
        Reviews = new List<ReviewShow>();
        NearCars = new List<NearCar>();
        SameTypeCars = new List<SameTypeCar>();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var brandId = 0;
            var modeId = 0;
            var yearId = 0;
            var price = 0.0;
            var modeAgentId = 0;
            var tab = string.Empty;
            var nameTypeMode = string.Empty;
            //define tabs
            var tabUrl = Request["tab"];
            var brand = Request["brandcar"];
            var mode = Request["mode"];
            var year = Request["year"];

            if (brand != null && mode != null && year != null)
            {
                _NameYear = year;
                _NameModeRewrite = mode;
                _NameBrandRewrite = brand;
                _NameBrand = GetBrandNameByRewriteUrl(brand);

                if (tabUrl == null)
                {
                    tab = "gioithieu";
                }
                else
                {
                    tab = tabUrl.ToString().ToLower();
                }

                switch (tab)
                {
                    case "gioithieu":
                        LoadTabIntroduction();
                        if (IntroductionInfo != null)
                        {
                            price = method.getDoubleFromString(IntroductionInfo.Gia);
                            brandId = IntroductionInfo.IdHangXe;
                            modeId = IntroductionInfo.IdDongXe;
                            yearId = IntroductionInfo.IdNam;
                            hidModeId.Value = IntroductionInfo.IdKieuXe.ToString();
                            modeAgentId = IntroductionInfo.IdDongXeDaiDien;
                            _NameMode = IntroductionInfo.DongXe;
                            nameTypeMode = IntroductionInfo.KieuXe;
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                    case "dongco":
                        LoadTabEngine();
                        if (EngineInfo != null)
                        {
                            price = method.getDoubleFromString(EngineInfo.Gia);
                            brandId = EngineInfo.IdHangXe;
                            modeId = EngineInfo.IdDongXe;
                            yearId = EngineInfo.IdNam;
                            hidModeId.Value = EngineInfo.IdKieuXe.ToString();
                            modeAgentId = EngineInfo.IdDongXeDaiDien;
                            _NameMode = EngineInfo.DongXe;
                            nameTypeMode = EngineInfo.KieuXe;
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                    case "antoan":
                        LoadTabSafety();
                        if (SafetyInfo != null)
                        {
                            price = method.getDoubleFromString(SafetyInfo.Gia);
                            brandId = SafetyInfo.IdHangXe;
                            modeId = SafetyInfo.IdDongXe;
                            yearId = SafetyInfo.IdNam;
                            hidModeId.Value = SafetyInfo.IdKieuXe.ToString();
                            modeAgentId = SafetyInfo.IdDongXeDaiDien;
                            _NameMode = SafetyInfo.DongXe;
                            nameTypeMode = SafetyInfo.KieuXe;
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                    case "giaitri":
                        LoadTabEntertainment();
                        if (EntertainmentInfo != null)
                        {
                            price = method.getDoubleFromString(EntertainmentInfo.Gia);
                            brandId = EntertainmentInfo.IdHangXe;
                            modeId = EntertainmentInfo.IdDongXe;
                            yearId = EntertainmentInfo.IdNam;
                            hidModeId.Value = EntertainmentInfo.IdKieuXe.ToString();
                            modeAgentId = EntertainmentInfo.IdDongXeDaiDien;
                            _NameMode = EntertainmentInfo.DongXe;
                            nameTypeMode = EntertainmentInfo.KieuXe;
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                    case "noithat":
                        LoadTabInterior();
                        if (InteriorInfo != null)
                        {
                            price = method.getDoubleFromString(InteriorInfo.Gia);
                            brandId = InteriorInfo.IdHangXe;
                            modeId = InteriorInfo.IdDongXe;
                            yearId = InteriorInfo.IdNam;
                            hidModeId.Value = InteriorInfo.IdKieuXe.ToString();
                            modeAgentId = InteriorInfo.IdDongXeDaiDien;
                            _NameMode = InteriorInfo.DongXe;
                            nameTypeMode = InteriorInfo.KieuXe;
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                    case "ngoaithat":
                        LoadTabExterior();
                        if (ExteriorInfo != null)
                        {
                            price = method.getDoubleFromString(ExteriorInfo.Gia);
                            brandId = ExteriorInfo.IdHangXe;
                            modeId = ExteriorInfo.IdDongXe;
                            yearId = ExteriorInfo.IdNam;
                            hidModeId.Value = ExteriorInfo.IdKieuXe.ToString();
                            modeAgentId = ExteriorInfo.IdDongXeDaiDien;
                            _NameMode = ExteriorInfo.DongXe;
                            nameTypeMode = ExteriorInfo.KieuXe;
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                    case "mausac":
                        LoadTabColor();
                        if (ColorInfo != null)
                        {
                            price = method.getDoubleFromString(ColorInfo.Gia);
                            brandId = ColorInfo.IdHangXe;
                            modeId = ColorInfo.IdDongXe;
                            yearId = ColorInfo.IdNam;
                            hidModeId.Value = ColorInfo.IdKieuXe.ToString();
                            modeAgentId = ColorInfo.IdDongXeDaiDien;
                            _NameMode = ColorInfo.DongXe;
                            nameTypeMode = ColorInfo.KieuXe;
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                    case "hinhanh":
                        LoadTabImage();
                        if (ImageInfo != null)
                        {
                            price = method.getDoubleFromString(ImageInfo.Gia);
                            brandId = ImageInfo.IdHangXe;
                            modeId = ImageInfo.IdDongXe;
                            yearId = ImageInfo.IdNam;
                            hidModeId.Value = ImageInfo.IdKieuXe.ToString();
                            modeAgentId = ImageInfo.IdDongXeDaiDien;
                            _NameMode = ImageInfo.DongXe;
                            nameTypeMode = ImageInfo.KieuXe;
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                    case "video":
                        LoadTabVideo();
                        if (VideoInfo != null)
                        {
                            price = method.getDoubleFromString(VideoInfo.Gia);
                            brandId = VideoInfo.IdHangXe;
                            modeId = VideoInfo.IdDongXe;
                            yearId = VideoInfo.IdNam;
                            hidModeId.Value = VideoInfo.IdKieuXe.ToString();
                            modeAgentId = VideoInfo.IdDongXeDaiDien;
                            _NameMode = VideoInfo.DongXe;
                            nameTypeMode = VideoInfo.KieuXe;
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                    case "danhgia":
                        LoadTabReview();
                        if (ReviewInfo != null)
                        {
                            price = method.getDoubleFromString(ReviewInfo.Gia);
                            brandId = ReviewInfo.IdHangXe;
                            modeId = ReviewInfo.IdDongXe;
                            yearId = ReviewInfo.IdNam;
                            hidModeId.Value = ReviewInfo.IdKieuXe.ToString();
                            modeAgentId = ReviewInfo.IdDongXeDaiDien;
                            _NameMode = ReviewInfo.DongXe;
                            nameTypeMode = ReviewInfo.KieuXe;
                            Reviews = LoadListReview();
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                    default:
                        //hidTab.Value = "";
                        LoadTabIntroduction();
                        if (IntroductionInfo != null)
                        {
                            price = method.getDoubleFromString(IntroductionInfo.Gia);
                            brandId = IntroductionInfo.IdHangXe;
                            modeId = IntroductionInfo.IdDongXe;
                            yearId = IntroductionInfo.IdNam;
                            hidModeId.Value = IntroductionInfo.IdKieuXe.ToString();
                            modeAgentId = IntroductionInfo.IdDongXeDaiDien;
                            _NameMode = IntroductionInfo.DongXe;
                            nameTypeMode = IntroductionInfo.KieuXe;
                        }
                        else
                        {
                            Response.Redirect("/404.html");
                        }
                        break;
                }
                hidTab.Value = tab;
                hidModeAgentId.Value = modeAgentId.ToString();
                LoadDetailCar(modeAgentId, brandId, modeId, yearId);
                LoadAllNearCars(brandId, modeId, nameTypeMode, price, int.Parse(_NameYear));
                SameTypeCars = LoadCarsSameType();
                _LinkDangBan = CheckRole();
                LoadRightDropDownList();
                Seo(_NameBrand, _NameMode, _NameYear);
            }
            else
            {
                Response.Redirect("/404.html");
            }
        }
    }

    private string CheckRole()
    {
        try
        {
            var linkReturn = string.Empty;
            string _session_id = "0";
            try
            {
                var cookie = Request.Cookies["___sc_session"];
                if (cookie != null)
                {
                    _session_id = method.Decode64(method.Decode64(cookie.Value));
                }
            }
            catch
            {
                return method.Get_rootlink();
            }

            string _result = connect.getItem("checkrole", int.Parse(_session_id), Request.UserHostAddress);

            switch (_result)
            {
                case "superadmin":
                    linkReturn = method.Get_rootlink() + "/quan-ly/sadmin/trang-chu.html";
                    break;
                case "admin":
                    linkReturn = method.Get_rootlink() + "/quan-ly/admin/trang-chu.html";
                    break;
                case "hang":
                    linkReturn = method.Get_rootlink() + "/quan-ly/hang/trang-chu.html";
                    break;
                case "daily":
                    linkReturn = method.Get_rootlink() + "/quan-ly/dai-ly/trang-chu.html";
                    break;
                case "thanhvien":
                    linkReturn = method.Get_rootlink() + "/quan-ly/thanh-vien/trang-chu.html";
                    break;
                case "notlogin":
                    linkReturn = method.Get_rootlink() + "/dang-nhap.html";
                    break;
            }
            return linkReturn;
        }
        catch
        {
            return method.Get_rootlink();
        }
    }

    private string GetBrandNameByRewriteUrl(string brandNameRewrite)
    {
        try
        {
            var sql = string.Format("select Name_Manufacturer from hdt.Manufacturer where ReWrite_Manufacturer = '{0}'", brandNameRewrite);
            return connect.getOnlyItem(sql);
        }
        catch
        {
            return string.Empty;
        }
    }

    private void Seo(string brand, string mode, string year)
    {
        string urlString = method.Get_rootlink() + Request.RawUrl;
        DataTable _dtseo = connect.getTableSProcedure("aa_fillmeta_seolink", urlString);
        if (_dtseo.Rows.Count > 0)
        {
            if (_dtseo.Rows[0]["result"].ToString() == "1")
            {
                this.Page.Header.Title = _dtseo.Rows[0]["title"].ToString();
                description.Text = _dtseo.Rows[0]["description"].ToString();
                keyword.Text = _dtseo.Rows[0]["keyword"].ToString();
            }
            else
            {
                var brandNameUpper = brand.ToUpper();
                var modeNameUpper = mode.ToUpper();
                string _title = brandNameUpper + " " + modeNameUpper + " " + year + " đánh giá, động cơ, giá bán";
                string _description = @"Nghiên cứu và so sánh " + brandNameUpper + "  " + modeNameUpper + " " + year +
                                        " và nhận báo giá tốt nhất từ các đại lý gần bạn, đánh giá từ chuyên gia và người dùng thực tế," +
                                        " hình ảnh, tính năng, ưu và nhược điểm, thiết bị, thông số kỹ thuật, tùy chọn và nhiều hơn nữa.";
                string _keyword = "";
                _keyword += String.Format("{0} {1} {2}", string.Empty, string.Empty, "2016");
                this.Page.Header.Title = _title;
                keyword.Text = _keyword;
                description.Text = _description;
            }
        }
    }

    private void LoadTabIntroduction()
    {
        try
        {
            var dt = connect.getTableSProcedure("thongtinchitietdongxe", _NameBrand, _NameModeRewrite, _NameYear, "GioiThieu");
            if (dt.Rows.Count > 0)
            {
                IntroductionInfo = new Introduction()
                {
                    IdDongXeDaiDien = int.Parse(dt.Rows[0]["IdDongXeDaiDien"].ToString()),
                    IdKieuXe = int.Parse(dt.Rows[0]["IdKieuXe"].ToString()),
                    IdHangXe = int.Parse(dt.Rows[0]["IdHangXe"].ToString()),
                    IdDongXe = int.Parse(dt.Rows[0]["IdDongXe"].ToString()),
                    IdNam = int.Parse(dt.Rows[0]["IdNam"].ToString()),
                    DongXe = dt.Rows[0]["DongXe"].ToString(),
                    KieuXe = dt.Rows[0]["TenKieuXe"].ToString(),
                    Nam = dt.Rows[0]["Nam"].ToString(),
                    Gia = dt.Rows[0]["Gia"].ToString(),
                    GioiThieu = dt.Rows[0]["GioiThieu"].ToString()
                };
            }
        }
        catch
        {

        }
    }

    private void LoadTabEngine()
    {
        try
        {
            var dt = connect.getTableSProcedure("thongtinchitietdongxe", _NameBrand, _NameModeRewrite, _NameYear, "DongCo");
            if (dt.Rows.Count > 0)
            {
                EngineInfo = new Engine()
                {
                    IdDongXeDaiDien = int.Parse(dt.Rows[0]["IdDongXeDaiDien"].ToString()),
                    IdKieuXe = int.Parse(dt.Rows[0]["IdKieuXe"].ToString()),
                    IdHangXe = int.Parse(dt.Rows[0]["IdHangXe"].ToString()),
                    IdDongXe = int.Parse(dt.Rows[0]["IdDongXe"].ToString()),
                    IdNam = int.Parse(dt.Rows[0]["IdNam"].ToString()),
                    DongXe = dt.Rows[0]["DongXe"].ToString(),
                    KieuXe = dt.Rows[0]["TenKieuXe"].ToString(),
                    Nam = dt.Rows[0]["Nam"].ToString(),
                    Gia = dt.Rows[0]["Gia"].ToString(),
                    DonVi = dt.Rows[0]["DonVi"].ToString(),
                    DongCo = dt.Rows[0]["DongCo"].ToString(),
                    KieuDongCo = dt.Rows[0]["KieuDongCo"].ToString(),
                    TySoNen = dt.Rows[0]["TySoNen"].ToString(),
                    KieuDanDong = dt.Rows[0]["KieuDanDong"].ToString(),
                    LoaiHopSo = dt.Rows[0]["LoaiHopSo"].ToString(),
                    NhienLieu = dt.Rows[0]["NhienLieu"].ToString(),
                    TieuHaoNhienLieuThanhPho = dt.Rows[0]["THNLTP"].ToString(),
                    TieuHaoNhienLieuCaoToc = dt.Rows[0]["THNLCT"].ToString(),
                    CoCauVan = dt.Rows[0]["CoCauVan"].ToString(),
                    HeThongCanBangDienTuESS = dt.Rows[0]["HTCanBangDienTu"].ToString(),
                    HeThongCanBangDienTuEBD = dt.Rows[0]["HTCanBangDienTu1"].ToString(),
                    HeThongDieuKhienTuXa = dt.Rows[0]["HTDieuKhienTuXa"].ToString(),
                    CapSo = dt.Rows[0]["CapSo"].ToString(),
                    ChieuDai = Math.Abs(double.Parse(dt.Rows[0]["ChieuDai"].ToString())).ToString(),
                    ChieuRong = Math.Abs(double.Parse(dt.Rows[0]["ChieuRong"].ToString())).ToString(),
                    ChieuCao = Math.Abs(double.Parse(dt.Rows[0]["ChieuCao"].ToString())).ToString(),
                    MaLuc = dt.Rows[0]["MaLuc"].ToString(),
                    MoMenXoan = dt.Rows[0]["MoMenXoan"].ToString(),
                    HeSoCan = dt.Rows[0]["HeSoCan"].ToString(),
                    TangToc = dt.Rows[0]["TangToc"].ToString(),
                    TocDoToiDa = dt.Rows[0]["TocDoToiDa"].ToString(),
                    TrongLuongKhongTai = dt.Rows[0]["TrongLuongKhongTai"].ToString(),
                    TrongLuongToanTai = dt.Rows[0]["TrongLuongToanTai"].ToString(),
                    DungTichBinhNhienLieu = Math.Abs(double.Parse(dt.Rows[0]["DungTichBinhNhienLieu"].ToString())).ToString()
                };
            }
        }
        catch
        { }
    }

    private void LoadTabSafety()
    {
        try
        {
            var dt = connect.getTableSProcedure("thongtinchitietdongxe", _NameBrand, _NameModeRewrite, _NameYear, "AnToan");
            if (dt.Rows.Count > 0)
            {
                SafetyInfo = new Safety()
                {
                    IdDongXeDaiDien = int.Parse(dt.Rows[0]["IdDongXeDaiDien"].ToString()),
                    IdKieuXe = int.Parse(dt.Rows[0]["IdKieuXe"].ToString()),
                    IdHangXe = int.Parse(dt.Rows[0]["IdHangXe"].ToString()),
                    IdDongXe = int.Parse(dt.Rows[0]["IdDongXe"].ToString()),
                    IdNam = int.Parse(dt.Rows[0]["IdNam"].ToString()),
                    DongXe = dt.Rows[0]["DongXe"].ToString(),
                    KieuXe = dt.Rows[0]["TenKieuXe"].ToString(),
                    Nam = dt.Rows[0]["Nam"].ToString(),
                    Gia = dt.Rows[0]["Gia"].ToString(),
                    DonVi = dt.Rows[0]["DonVi"].ToString(),
                    TuiKhiTruoc = dt.Rows[0]["TuiKhiTruoc"].ToString() == "1" ? "Có" : "Không",
                    TuiKhiBen = dt.Rows[0]["TuiKhiBen"].ToString() == "1" ? "Có" : "Không",
                    TuiKhiSau = dt.Rows[0]["TuiKhiSau"].ToString(),
                    DenPha = dt.Rows[0]["DenPha"].ToString(),
                    KiemSoatAnhSang = dt.Rows[0]["KiemSoatAnhSang"].ToString(),
                    ChieuSangBanNgay = dt.Rows[0]["ChieuSangBanNgay"].ToString(),
                    DenChieuHau = dt.Rows[0]["DenChieuHau"].ToString(),
                    HoTroDoXe = dt.Rows[0]["HoTroDoXe"].ToString(),
                    HeThongBaoDong = dt.Rows[0]["HeThongBaoDong"].ToString(),
                    HeThongKhoaCua = dt.Rows[0]["HeThongKhoaCua"].ToString(),
                    KhoaAnToan = dt.Rows[0]["KhoaAnToan"].ToString(),
                    HeThongChongTrom = dt.Rows[0]["HeThongChongTrom"].ToString(),
                    CanhBaoApSuatThap = dt.Rows[0]["CanhBaoApSuatThap"].ToString(),
                    LoaiPhanh = dt.Rows[0]["LoaiPhanh"].ToString(),
                    HeThongPhanhABS = dt.Rows[0]["HeThongPhanhABS"].ToString(),
                    HoTroPhanh = dt.Rows[0]["HoTroPhanh"].ToString()
                };
            }
        }
        catch
        { }
    }

    private void LoadTabEntertainment()
    {
        try
        {
            var dt = connect.getTableSProcedure("thongtinchitietdongxe", _NameBrand, _NameModeRewrite, _NameYear, "GiaiTri");
            if (dt.Rows.Count > 0)
            {
                EntertainmentInfo = new Entertainment()
                {
                    IdDongXeDaiDien = int.Parse(dt.Rows[0]["IdDongXeDaiDien"].ToString()),
                    IdKieuXe = int.Parse(dt.Rows[0]["IdKieuXe"].ToString()),
                    IdHangXe = int.Parse(dt.Rows[0]["IdHangXe"].ToString()),
                    IdDongXe = int.Parse(dt.Rows[0]["IdDongXe"].ToString()),
                    IdNam = int.Parse(dt.Rows[0]["IdNam"].ToString()),
                    DongXe = dt.Rows[0]["DongXe"].ToString(),
                    KieuXe = dt.Rows[0]["TenKieuXe"].ToString(),
                    Nam = dt.Rows[0]["Nam"].ToString(),
                    Gia = dt.Rows[0]["Gia"].ToString(),
                    DonVi = dt.Rows[0]["DonVi"].ToString(),
                    Radio = dt.Rows[0]["Radio"].ToString(),
                    CDPlayer = dt.Rows[0]["CDPlayer"].ToString(),
                    DVDAudio = dt.Rows[0]["DVDAudio"].ToString(),
                    NhanDangGiongNoi = dt.Rows[0]["NhanDangGiongNoi"].ToString(),
                    Loa = dt.Rows[0]["Loa"].ToString(),
                    BoKhuechDai = dt.Rows[0]["BoKhuechDai"].ToString(),
                    HoTroBlueTooth = dt.Rows[0]["HoTroBlueTooth"].ToString(),
                    HoTroWifi = dt.Rows[0]["HoTroWifi"].ToString(),
                    HoTro3G = dt.Rows[0]["HoTro3G"].ToString(),
                    DinhViToanCau = dt.Rows[0]["DinhViToanCau"].ToString(),
                };
            }
        }
        catch
        { }
    }

    private void LoadTabInterior()
    {
        try
        {
            var dt = connect.getTableSProcedure("thongtinchitietdongxe", _NameBrand, _NameModeRewrite, _NameYear, "NoiThat");
            if (dt.Rows.Count > 0)
            {
                InteriorInfo = new Interior()
                {
                    IdDongXeDaiDien = int.Parse(dt.Rows[0]["IdDongXeDaiDien"].ToString()),
                    IdKieuXe = int.Parse(dt.Rows[0]["IdKieuXe"].ToString()),
                    IdHangXe = int.Parse(dt.Rows[0]["IdHangXe"].ToString()),
                    IdDongXe = int.Parse(dt.Rows[0]["IdDongXe"].ToString()),
                    IdNam = int.Parse(dt.Rows[0]["IdNam"].ToString()),
                    DongXe = dt.Rows[0]["DongXe"].ToString(),
                    KieuXe = dt.Rows[0]["TenKieuXe"].ToString(),
                    Nam = dt.Rows[0]["Nam"].ToString(),
                    Gia = dt.Rows[0]["Gia"].ToString(),
                    DonVi = dt.Rows[0]["DonVi"].ToString(),
                    DieuHoaTruoc = int.Parse(dt.Rows[0]["DieuHoaTruoc"].ToString()),
                    DieuHoaSau = int.Parse(dt.Rows[0]["DieuHoaSau"].ToString()),
                    BoLocKhongKhi = int.Parse(dt.Rows[0]["BoLocKhongKhi"].ToString()),
                    ChoNgoi = int.Parse(dt.Rows[0]["ChoNgoi"].ToString()),
                    LoaiGheTruoc = dt.Rows[0]["LoaiGheTruoc"].ToString(),
                    GoiDau = dt.Rows[0]["GoiDau"].ToString(),
                    LaBan = dt.Rows[0]["LaBan"].ToString(),
                    DongHo = dt.Rows[0]["DongHo"].ToString(),
                    OCamDien = int.Parse(dt.Rows[0]["OCamDien"].ToString()),
                    MayDoTocDo = dt.Rows[0]["MayDoTocDo"].ToString(),
                    MayDoNhietDoNuoc = dt.Rows[0]["MayDoNhietDoNuoc"].ToString(),
                    MayCanhBaoNhienLieu = dt.Rows[0]["MayCanhBaoNhienLieu"].ToString(),
                    HienThiNhietDoNgoai = dt.Rows[0]["HienThiNhietDoNgoai"].ToString(),
                    NoiThatFlash = dt.Rows[0]["NoiThatFlash"].ToString()
                };
            }
        }
        catch
        { }
    }

    private void LoadTabExterior()
    {
        try
        {
            var dt = connect.getTableSProcedure("thongtinchitietdongxe", _NameBrand, _NameModeRewrite, _NameYear, "NgoaiThat");
            if (dt.Rows.Count > 0)
            {
                ExteriorInfo = new Exterior()
                {
                    IdDongXeDaiDien = int.Parse(dt.Rows[0]["IdDongXeDaiDien"].ToString()),
                    IdKieuXe = int.Parse(dt.Rows[0]["IdKieuXe"].ToString()),
                    IdHangXe = int.Parse(dt.Rows[0]["IdHangXe"].ToString()),
                    IdDongXe = int.Parse(dt.Rows[0]["IdDongXe"].ToString()),
                    IdNam = int.Parse(dt.Rows[0]["IdNam"].ToString()),
                    DongXe = dt.Rows[0]["DongXe"].ToString(),
                    KieuXe = dt.Rows[0]["TenKieuXe"].ToString(),
                    Nam = dt.Rows[0]["Nam"].ToString(),
                    Gia = dt.Rows[0]["Gia"].ToString(),
                    DonVi = dt.Rows[0]["DonVi"].ToString(),
                    ThongSoLopTruoc = dt.Rows[0]["ThongSoLopTruoc"].ToString(),
                    ThongSoLopSau = dt.Rows[0]["ThongSoLopSau"].ToString(),
                    ThongSoMam = dt.Rows[0]["ThongSoMam"].ToString(),
                    CuaSoTroi = dt.Rows[0]["CuaSoTroi"].ToString(),
                    LoaiCuaSau = dt.Rows[0]["LoaiCuaSau"].ToString(),
                    SoCua = int.Parse(dt.Rows[0]["SoCua"].ToString()),
                    XongKinh = dt.Rows[0]["XongKinh"].ToString(),
                    CamBienMua = dt.Rows[0]["CamBienMua"].ToString(),
                    HeThongNangHaGam = dt.Rows[0]["HeThongNangHaGam"].ToString(),
                    NgoaiThatFlash = dt.Rows[0]["NgoaiThatFlash"].ToString(),
                    KinhHau = dt.Rows[0]["KinhHau"].ToString() == "tu dong" ? "Tự động" : dt.Rows[0]["KinhHau"].ToString()
                };
            }
        }
        catch
        { }
    }

    private void LoadTabColor()
    {
        try
        {
            var dt = connect.getTableSProcedure("thongtinchitietdongxe", _NameBrand, _NameModeRewrite, _NameYear, "MauSac");
            if (dt.Rows.Count > 0)
            {
                ColorInfo = new Color()
                {
                    IdDongXeDaiDien = int.Parse(dt.Rows[0]["IdDongXeDaiDien"].ToString()),
                    IdKieuXe = int.Parse(dt.Rows[0]["IdKieuXe"].ToString()),
                    IdHangXe = int.Parse(dt.Rows[0]["IdHangXe"].ToString()),
                    IdDongXe = int.Parse(dt.Rows[0]["IdDongXe"].ToString()),
                    IdNam = int.Parse(dt.Rows[0]["IdNam"].ToString()),
                    DongXe = dt.Rows[0]["DongXe"].ToString(),
                    KieuXe = dt.Rows[0]["TenKieuXe"].ToString(),
                    Nam = dt.Rows[0]["Nam"].ToString(),
                    Gia = dt.Rows[0]["Gia"].ToString(),
                    DonVi = dt.Rows[0]["DonVi"].ToString(),
                    MauSac = dt.Rows[0]["MauSac"].ToString()
                };
            }
        }
        catch
        { }
    }

    private void LoadTabVideo()
    {
        try
        {
            var dt = connect.getTableSProcedure("thongtinchitietdongxe", _NameBrand, _NameModeRewrite, _NameYear, "Video");
            if (dt.Rows.Count > 0)
            {
                var nam = dt.Rows[0]["Nam"].ToString();
                var hangXe = dt.Rows[0]["HangXe"].ToString();
                var dongXe = dt.Rows[0]["DongXe"].ToString();

                VideoInfo = new Video()
                {
                    Nam = nam,
                    DongXe = dongXe,
                    HangXe = hangXe,
                    Gia = dt.Rows[0]["Gia"].ToString(),
                    DonVi = dt.Rows[0]["DonVi"].ToString(),
                    YoutubeKey = hangXe + " " + dongXe + " " + nam,
                    IdDongXeDaiDien = int.Parse(dt.Rows[0]["IdDongXeDaiDien"].ToString()),
                    IdKieuXe = int.Parse(dt.Rows[0]["IdKieuXe"].ToString()),
                    IdHangXe = int.Parse(dt.Rows[0]["IdHangXe"].ToString()),
                    IdDongXe = int.Parse(dt.Rows[0]["IdDongXe"].ToString()),
                    IdNam = int.Parse(dt.Rows[0]["IdNam"].ToString()),
                    KieuXe = dt.Rows[0]["TenKieuXe"].ToString()
                };
            }
        }
        catch
        { }
    }

    private void LoadTabImage()
    {
        try
        {
            var dt = connect.getTableSProcedure("thongtinchitietdongxe", _NameBrand, _NameModeRewrite, _NameYear, "HinhAnh");
            if (dt.Rows.Count > 0)
            {
                ImageInfo = new Images()
                {
                    IdDongXeDaiDien = int.Parse(dt.Rows[0]["IdDongXeDaiDien"].ToString()),
                    IdKieuXe = int.Parse(dt.Rows[0]["IdKieuXe"].ToString()),
                    IdHangXe = int.Parse(dt.Rows[0]["IdHangXe"].ToString()),
                    IdDongXe = int.Parse(dt.Rows[0]["IdDongXe"].ToString()),
                    IdNam = int.Parse(dt.Rows[0]["IdNam"].ToString()),
                    DongXe = dt.Rows[0]["DongXe"].ToString(),
                    KieuXe = dt.Rows[0]["TenKieuXe"].ToString(),
                    Nam = dt.Rows[0]["Nam"].ToString(),
                    Gia = dt.Rows[0]["Gia"].ToString(),
                    DonVi = dt.Rows[0]["DonVi"].ToString(),
                    TenSP = dt.Rows[0]["TenSP"].ToString(),
                    IdHinhAnh = dt.Rows[0]["IdHinhAnh"].ToString()
                };
            }
        }
        catch
        { }
    }

    private void LoadTabReview()
    {
        try
        {
            var dt = connect.getTableSProcedure("thongtinchitietdongxe", _NameBrand, _NameModeRewrite, _NameYear, "DanhGia");
            if (dt.Rows.Count > 0)
            {
                ReviewInfo = new Review();
                ReviewInfo.IdDongXeDaiDien = int.Parse(dt.Rows[0]["IdDongXeDaiDien"].ToString());
                ReviewInfo.IdKieuXe = int.Parse(dt.Rows[0]["IdKieuXe"].ToString());
                ReviewInfo.IdHangXe = int.Parse(dt.Rows[0]["IdHangXe"].ToString());
                ReviewInfo.IdDongXe = int.Parse(dt.Rows[0]["IdDongXe"].ToString());
                ReviewInfo.IdNam = int.Parse(dt.Rows[0]["IdNam"].ToString());
                ReviewInfo.DongXe = dt.Rows[0]["DongXe"].ToString();
                ReviewInfo.KieuXe = dt.Rows[0]["TenKieuXe"].ToString();
                ReviewInfo.Nam = dt.Rows[0]["Nam"].ToString();
                ReviewInfo.Gia = dt.Rows[0]["Gia"].ToString();
                
                var _session_id = 0;
                var _ip_client = Request.UserHostAddress;

                if (Request.Cookies["___sc_session"] != null)
                {
                    _session_id = int.Parse(method.Decode64(method.Decode64(Request.Cookies["___sc_session"].Value)));
                }

                var query = String.Format(@"SELECT r.Id_Users as IdUser, r.Id_Reviews as IdReview, 
                                            u.FullName as [User], r.Point_Body as Body, r.Point_Safe as [Safe], 
                                            r.Point_Price as Price, r.Point_Operation as Operation,
                                            r.Content as Content, r.DatePost as DatePost
                                            FROM hdt.Reviews_ModelDetail r 
                                            JOIN hdt.Users u ON r.Id_Users = u.Id_Users
                                            JOIN hdt.session_login s ON u.Id_Users = s.id_user
                                            WHERE r.Brand = '{0}' AND r.Model = '{1}' AND r.[Year] = '{2}'
                                            AND s.id_session_login = {3} AND s.ip_client = '{4}'", 
                                            _NameBrand, ReviewInfo.DongXe, _NameYear, _session_id, _ip_client);

                var dtReview = connect.GetTable(query);
                if (dtReview.Rows.Count > 0)
                {
                    ReviewInfo.IdUser = int.Parse(dtReview.Rows[0]["IdUser"].ToString());
                    ReviewInfo.IdReview = int.Parse(dtReview.Rows[0]["IdReview"].ToString());
                    ReviewInfo.CanRemove = CheckRoleToRemoveReview(ReviewInfo.IdReview);
                    ReviewInfo.Body = dtReview.Rows[0]["Body"] != DBNull.Value ?
                                    ShowRateReview(int.Parse(dtReview.Rows[0]["Body"].ToString())) :
                                    ShowRateReview(0);
                    ReviewInfo.Safe = dtReview.Rows[0]["Safe"] != DBNull.Value ?
                                        ShowRateReview(int.Parse(dtReview.Rows[0]["Safe"].ToString())) :
                                        ShowRateReview(0);
                    ReviewInfo.Price = dtReview.Rows[0]["Price"] != DBNull.Value ?
                                        ShowRateReview(int.Parse(dtReview.Rows[0]["Price"].ToString())) :
                                        ShowRateReview(0);
                    ReviewInfo.Operation = dtReview.Rows[0]["Operation"] != DBNull.Value ?
                                        ShowRateReview(int.Parse(dtReview.Rows[0]["Operation"].ToString())) :
                                        ShowRateReview(0);
                }
            }
        }
        catch
        { }
    }

    private bool CheckRoleToRemoveReview(int idReview)
    {
        try
        {
            var _session_id = 0;
            var _ip_client = Request.UserHostAddress;

            if (Request.Cookies["___sc_session"] != null)
            {
                _session_id = int.Parse(method.Decode64(method.Decode64(Request.Cookies["___sc_session"].Value)));
            }
            
            var dt = connect.getTableSProcedure("check_remove_review_model_detail", _session_id, _ip_client, idReview);
            if (dt.Rows.Count > 0)
            {
                var result = dt.Rows[0]["result"].ToString();

                if (result == "user" || result == "admin")
                {
                    return true;
                }
            }
            return false;
        }
        catch
        {
            return false;
        }
    }

    private void LoadRightDropDownList()
    {
        try
        {
            var brandTable = connect.getTableSProcedure("select_allbrand");
            ddlBrand.DataSource = brandTable;
            ddlBrand.DataTextField = "Name_Manufacturer";
            ddlBrand.DataValueField = "ReWrite_Manufacturer";
            ddlBrand.DataBind();
            ddlBrand.Items.Insert(0, "-- Chọn hãng xe --");

            var modelTable = connect.getTableSProcedure("select_model_by_brandname", ddlBrand.SelectedItem.Value);
            ddlMode.DataSource = modelTable;
            ddlMode.DataTextField = "Name_Mode_Product";
            ddlMode.DataValueField = "ReWrite_ModeProduct";
            ddlMode.DataBind();
            ddlMode.Items.Insert(0, "-- Chọn dòng xe --");
        }
        catch
        {
            Response.Redirect(method.Get_rootlink());
        }
    }

    private string ShowAggregateReview(double rateValue)
    {
        var result = "-79px";

        if (rateValue > 0.0 && rateValue <= 0.5)
        {
            result = "-72px";
        }
        if (rateValue > 0.5 && rateValue <= 1.0)
        {
            result = "-64px";
        }
        if (rateValue > 1.0 && rateValue <= 1.5)
        {
            result = "-56px";
        }
        if (rateValue > 1.5 && rateValue <= 2.0)
        {
            result = "-48px";
        }
        if (rateValue > 2.0 && rateValue <= 2.5)
        {
            result = "-40px";
        }
        if (rateValue > 2.5 && rateValue <= 3.0)
        {
            result = "-32px";
        }
        if (rateValue > 3.0 && rateValue <= 3.5)
        {
            result = "-24px";
        }
        if (rateValue > 3.5 && rateValue <= 4.0)
        {
            result = "-16px";
        }
        if (rateValue > 4.0 && rateValue <= 4.5)
        {
            result = "-8px";
        }
        if (rateValue > 4.5 && rateValue <= 5.0)
        {
            result = "0px";
        }

        return result;
    }

    private string ShowRateReview(int rateValue)
    {
        var result = "";
        switch (rateValue)
        {
            case 0:
                break;
            case 1:
                result = "15px";
                break;
            case 2:
                result = "31px";
                break;
            case 3:
                result = "47px";
                break;
            case 4:
                result = "63px";
                break;
            case 5:
                result = "79px";
                break;
        }
        return result;
    }

    private List<SameTypeCar> LoadCarsSameType()
    {
        try
        {
            var Temp = new List<SameTypeCar>();
            var dt = connect.getTableSProcedure("danhsachxecungdongxe", _NameBrand, _NameMode, _NameYear);
            if (dt.Rows.Count > 0)
            {
                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    Temp.Add(new SameTypeCar
                    {
                        Nam = dt.Rows[i]["Nam"].ToString(),
                        THNLTP = dt.Rows[i]["THNLTP"].ToString(),
                        THNLCT = dt.Rows[i]["THNLCT"].ToString(),
                        DongXe = dt.Rows[i]["DongXe"].ToString(),
                        MaLuc = dt.Rows[i]["MaLuc"].ToString(),
                        DongCo = dt.Rows[i]["DongCo"].ToString(),
                        KieuDanDong = dt.Rows[i]["KieuDanDong"].ToString(),
                        IdKieuXe = int.Parse(dt.Rows[i]["IdKieuXe"].ToString()),
                        IdHangXe = int.Parse(dt.Rows[i]["IdHangXe"].ToString()),
                        TenDongXeDaiDien = dt.Rows[i]["TenDongXeDaiDien"].ToString(),
                        IdDongXeDaiDien = int.Parse(dt.Rows[i]["IdDongXeDaiDien"].ToString()),
                        Gia = dt.Rows[i]["Gia"] != DBNull.Value && dt.Rows[i]["Gia"].ToString() != "" ? 
                        method.FormatNumberCurrency(uint.Parse(dt.Rows[i]["Gia"].ToString())) + " VNĐ" : "N/A",
                        IdSP = dt.Rows[i]["IdSP"] != DBNull.Value ? int.Parse(dt.Rows[i]["IdSP"].ToString()) : 0
                    });
                }
            }
            return Temp;
        }
        catch 
        { 
            return new List<SameTypeCar>(); 
        }
    }

    private List<ModeImage> LoadListImages(string id)
    {
        try
        {
            var listImages = new List<ModeImage>();
            var folder = Server.MapPath("~/Upload/Data-dongxe-serv/" + id + "/Thumbnail");
            var strFilter = "*.jpg;*.png;*.gif";
            string[] m_arExt = strFilter.Split(';');
            DirectoryInfo dir = new DirectoryInfo(folder);
            //Response.Write("<br/><ul id='dirdongxe'>");
            foreach (string filter in m_arExt)
            {
                FileInfo[] strFiles = dir.GetFiles(filter, SearchOption.AllDirectories);
                foreach (FileInfo file in strFiles)
                {
                    listImages.Add(new ModeImage()
                    {
                        ImageName = file.Name
                    });
                    //Response.Write("<li><a href='javascript:void(0)' onclick=\"xoahinh('" + id + "','" + Name + "');\"><img src='" + method.Get_rootlink() + "/Upload/Data-dongxe-serv/" + id + "/Thumbnail/" + Name + "' border='0' /></a></li>");
                }
            }
            //Response.Write("</ul><div class='cl'></div>");
            dir.Refresh();
            return listImages;
        }
        catch
        {
            return new List<ModeImage>();
        }
    }

    private List<ReviewShow> LoadListReview()
    {
        var listReview = new List<ReviewShow>();

        try
        {
            var query = String.Format(@"SELECT r.Id_Users as IdUser, r.Id_Reviews as IdReview,
                                        u.FullName as [User], r.Point_Body as Body, r.Point_Safe as [Safe],
                                        r.Point_Price as Price, r.Point_Operation as Operation, r.Content as Content,
                                        r.DatePost as DatePost FROM hdt.Reviews_ModelDetail r 
                                        JOIN hdt.Users u ON r.Id_Users = u.Id_Users
                                        WHERE r.Brand = '{0}' and r.Model = '{1}' and r.[Year] = '{2}'", 
                                        _NameBrand, _NameMode, _NameYear);

            var dt = connect.GetTable(query);
            if (dt.Rows.Count > 0)
            {
                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    var body = dt.Rows[i]["Body"] != DBNull.Value ? int.Parse(dt.Rows[i]["Body"].ToString()) : 0;
                    var safe = dt.Rows[i]["Safe"] != DBNull.Value ? int.Parse(dt.Rows[i]["Safe"].ToString()) : 0;
                    var price = dt.Rows[i]["Price"] != DBNull.Value ? int.Parse(dt.Rows[i]["Price"].ToString()) : 0;
                    var operation = dt.Rows[i]["Operation"] != DBNull.Value ? int.Parse(dt.Rows[i]["Operation"].ToString()) : 0;

                    double rate = 0.0;
                    int countPoint = 4;
                    int sumpoint = body + safe + price + operation;

                    if(sumpoint > 0)
                    {
                        double aggregate = (double)sumpoint / (double)countPoint;
                        rate = aggregate;
                    }

                    var review = new ReviewShow();
                    review.AggregateNumb = rate;
                    review.Body = ShowRateReview(body);
                    review.Safe = ShowRateReview(safe);
                    review.Price = ShowRateReview(price);
                    review.Operation = ShowRateReview(operation);
                    review.Aggregate = ShowAggregateReview(rate);
                    review.Content = dt.Rows[i]["Content"].ToString();
                    review.IdUser = int.Parse(dt.Rows[i]["IdUser"].ToString());
                    review.IdReview = int.Parse(dt.Rows[i]["IdReview"].ToString());
                    review.CanRemove = CheckRoleToRemoveReview(review.IdReview);
                    review.User = dt.Rows[i]["User"] != DBNull.Value ? dt.Rows[i]["User"].ToString() : string.Empty;
                    review.DatePost = String.Format("{0:d/M/yyyy}", DateTime.Parse(dt.Rows[i]["DatePost"].ToString()));
                    
                    listReview.Add(review);
                }
            }
        }
        catch
        { }

        return listReview.OrderByDescending(r => r.AggregateNumb).Take(5).ToList();
    }

    private void LoadAllNearCars(int brandId, int modeId, string typeMode, double price, int yearId)
    {
        try
        {
            var Temp = new List<NearCar>();
            var dt = connect.getTableSProcedure("danhsachtatcaxecungkieu", brandId, modeId, typeMode, yearId);
            if (dt.Rows.Count > 0)
            {
                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    var idSP = dt.Rows[0]["IdSP"] != DBNull.Value ? int.Parse(dt.Rows[0]["IdSP"].ToString()) : 0;
                    var idCar = dt.Rows[0]["IdDongXeDaiDien"] != DBNull.Value ? int.Parse(dt.Rows[i]["IdDongXeDaiDien"].ToString()) : 0;
                    var brand = dt.Rows[i]["HangXe"] != DBNull.Value ? dt.Rows[i]["HangXe"].ToString() : string.Empty;
                    var model = dt.Rows[i]["DongXe"] != DBNull.Value ? dt.Rows[i]["DongXe"].ToString() : string.Empty;
                    var year = dt.Rows[i]["Nam"] != DBNull.Value ? dt.Rows[i]["Nam"].ToString() : string.Empty;

                    Temp.Add(new NearCar
                    {
                        Nam = year,
                        HangXe = brand,
                        DongXe = model,
                        IdDongXeDaiDien = idCar,
                        IdDongXe = int.Parse(dt.Rows[i]["IdDongXe"].ToString()),
                        IdKieuXe = int.Parse(dt.Rows[i]["IdKieuXe"].ToString()),
                        HinhDaiDien = dt.Rows[i]["HinhDaiDien"].ToString(),
                        HangXeUrl = dt.Rows[i]["HangXeUrl"].ToString(),
                        DongXeUrl = dt.Rows[i]["DongXeUrl"].ToString(),
                        DanhGia = LoadAggregrateReviewModelDetail(brand, model, year),
                        //Gia = dt.Rows[i]["Gia"] != DBNull.Value ?
                        //method.FormatNumberCurrency(uint.Parse(dt.Rows[i]["Gia"].ToString())) + " VNĐ" : "N/A",
                        DonVi = dt.Rows[i]["DonVi"].ToString(),
                        Gia = dt.Rows[i]["Gia"] != DBNull.Value && dt.Rows[i]["Gia"].ToString() != "" ?
                        dt.Rows[i]["Gia"].ToString() : "N/A",
                    });
                }
            }

            //price
            if (price > 0)
                Temp = Temp.Where(x => method.getDoubleFromString(x.Gia) >= (price + 5000) && 
                                       method.getDoubleFromString(x.Gia) <= (price + 10000)).ToList();

            //get first car of mode
            Temp = Temp.GroupBy(x => new { x.IdDongXe, x.Nam },
                                    (key, xs) => xs.OrderByDescending(x => x.IdDongXeDaiDien).First()).ToList();

            NearCars = Temp;
        }
        catch { }
    }

    private void LoadDetailCar(int idDongXeDaiDien, int idHangXe, int idDongXe, int idNam)
    {
        try
        {
            var query = String.Format(@"SELECT p.Id_Product as IdSP, mn.Id_MName as IdCar, p.Price_Product as Gia,
                                        u.Name_Unit as DonVi, sp.Fuel_Economy_City as THNLTP, sp.Fuel_Economy_Highway as THNLCT
                                        FROM hdt.MName mn
                                        LEFT JOIN hdt.Product p on p.Id_MName = mn.Id_MName
                                        LEFT JOIN Unit u on p.Id_Unit = u.Id_Unit
                                        LEFT JOIN hdt.Specifications sp on sp.Id_MName = mn.Id_MName
                                        WHERE mn.Id_Mname = {0}", idDongXeDaiDien);

            var dt = connect.GetTable(query);
            if (dt.Rows.Count > 0)
            {
                var idSP = dt.Rows[0]["IdSP"] != DBNull.Value ? int.Parse(dt.Rows[0]["IdSP"].ToString()) : 0;
                var idCar = dt.Rows[0]["IdCar"] != DBNull.Value ? int.Parse(dt.Rows[0]["IdCar"].ToString()) : 0;
                CarDetailInfo = new CarDetail();
                CarDetailInfo.IdSP = idSP;
                CarDetailInfo.IdMName = idCar;
                CarDetailInfo.Review = LoadAggregrateReviewModelDetail();
                CarDetailInfo.Gia = dt.Rows[0]["Gia"] != DBNull.Value ? 
                    method.FormatNumberCurrency(uint.Parse(dt.Rows[0]["Gia"].ToString())) : "N/A";
                CarDetailInfo.DonVi = dt.Rows[0]["DonVi"].ToString();
                CarDetailInfo.THNLTP = dt.Rows[0]["THNLTP"] != DBNull.Value ? dt.Rows[0]["THNLTP"].ToString() : string.Empty;
                CarDetailInfo.THNLCT = dt.Rows[0]["THNLCT"] != DBNull.Value ? dt.Rows[0]["THNLCT"].ToString() : string.Empty;
            }
            //load avatar
            var queryAvatar = String.Format(@"SELECT PathImage FROM hdt.AvatarMode WHERE BrandId = {0} AND ModeId = {1} 
                                              AND YearId = {2}", idHangXe, idDongXe, idNam);

            var imageAvatar = connect.getOnlyItem(queryAvatar);

            if(!string.IsNullOrEmpty(imageAvatar))
                CarDetailInfo.Avatar = "/Upload" + imageAvatar;
            else
                CarDetailInfo.Avatar = "/Images/error_missing_image.jpg";
        }
        catch
        { }
    }

    private string LoadAggregrateReviewModelDetail(string brand, string model, string year)
    {
        double rate = 0.0;

        try
        {
            var _session_id = 0;
            var _ip_client = Request.UserHostAddress;

            if (Request.Cookies["___sc_session"] != null)
            {
                _session_id = int.Parse(method.Decode64(method.Decode64(Request.Cookies["___sc_session"].Value)));
            }

            var query = String.Format(@"SELECT u.FullName as [User], r.Point_Body as Body, r.Point_Safe as [Safe],
                                        r.Point_Price as Price, r.Point_Operation as Operation, r.Content as Content,
                                        r.DatePost as DatePost FROM hdt.Reviews_ModelDetail r 
                                        JOIN hdt.Users u ON r.Id_Users = u.Id_Users
                                        JOIN hdt.session_login s ON u.Id_Users = s.id_user
                                        WHERE r.Brand = '{0}' AND r.Model = '{1}' AND r.[Year] = '{2}'
                                        AND s.id_session_login = {3} AND s.ip_client = '{4}'", brand, model, year, _session_id, _ip_client);

            var dtReview = connect.GetTable(query);
            if (dtReview.Rows.Count > 0)
            {
                var body = dtReview.Rows[0]["Body"] != DBNull.Value ? int.Parse(dtReview.Rows[0]["Body"].ToString()) : 0;
                var safe = dtReview.Rows[0]["Safe"] != DBNull.Value ? int.Parse(dtReview.Rows[0]["Safe"].ToString()) : 0;
                var price = dtReview.Rows[0]["Price"] != DBNull.Value ? int.Parse(dtReview.Rows[0]["Price"].ToString()) : 0;
                var operation = dtReview.Rows[0]["Operation"] != DBNull.Value ? int.Parse(dtReview.Rows[0]["Operation"].ToString()) : 0;

                int countPoint = 4;
                int sumpoint = body + safe + price + operation;

                if (sumpoint > 0)
                {
                    double aggregate = (double)sumpoint / (double)countPoint;
                    rate = aggregate;
                }
            }
        }
        catch
        { }

        if (rate > 0.0)
            IsReview = true;
        else
            IsReview = false;

        return ShowAggregateReview(rate);
    }

    private string LoadAggregrateReviewModelDetail()
    {
        return LoadAggregrateReviewModelDetail(_NameBrand, _NameMode, _NameYear);
    }
}