--should not set procedure name has 'sp' because 'sp' has in procedure name of system. 
create proc hdt.HSP_GetCarEntertainment
@manufactureId int,
@modelId int,
@yearId int
as
SELECT top 1 
this_.Radio as Radio, 
this_.CD_Player as CDPlayer, 
this_.DVD_Audio as DVDAudio, 
this_.Voice_Recognition as SpeechRecognition, 
this_.Speakers as Speakers, 
this_.Amplifier as Amplifier, 
this_.bluetooth_Compatibility as BluetoothCompatibility, 
this_.Wifi_Compatibility as WifiCompatibility, 
this_.[3G_Compatibility] as ThreeGCompatibility, 
this_.GPS_Compability as GPSCompatibility 
FROM hdt.Manufacturer m
		LEFT JOIN hdt.Type_Product t on m.Id_Manufacturer = t.Id_Manufacturer 
		LEFT JOIN hdt.Mode_Product mp on mp.Id_TypeProduct = t.Id_TypeProduct
		LEFT JOIN hdt.[Year] y on y.Id_Mode_Product = mp.Id_Mode_Product
		LEFT JOIN hdt.MName mn on mn.Id_Year = y.Id_Year
		LEFT JOIN hdt.Entertainment this_ on mn.Id_MName = this_.Id_MName
WHERE ((m.Id_Manufacturer = @manufactureId and mp.Id_Mode_Product = @modelId) and y.Id_Year = @yearId) 
ORDER BY mn.Id_MName desc