USE [carbank]
GO
/****** Object:  StoredProcedure [hdt].[sp_GetModelSameStyles]    Script Date: 15/04/2017 9:54:26 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Khoa
-- Create date: 2017-03-04
-- Description:	Get Model Same Style
-- =============================================
CREATE PROCEDURE [hdt].[sp_GetModelSameStyles] 
	@manufacturerName NVARCHAR(50),
	@modelName NVARCHAR(50),
	@yearName NVARCHAR(10),
	@styleName NVARCHAR(50),
	@page INT = 1,
	@size INT = 10
AS
BEGIN
	-- Get same style cars information group by ModelId and YearId
	CREATE TABLE #AllCars
	(
		ManufacturerId INT,
		ModelId INT,
		YearId INT,
		CarId INT,
		ProductId INT,
		Price FLOAT,
		UnitName NVARCHAR(200),
		Avatar NVARCHAR(200),
		CarName NVARCHAR(200),
		ManufacturerName NVARCHAR(200),
		ModelName NVARCHAR(200),
		YearName NVARCHAR(10),
		FuelEconomyCity NVARCHAR(10),
		FuelEconomyHighway NVARCHAR(10)
	)
	INSERT INTO #AllCars
	SELECT CarInfo.*
	FROM
	( 
		(
			SELECT COALESCE(MAX(product.Price_Product), 0) AS Price,
				   COALESCE(MAX(product.Id_Product), 0) AS ProductId,
				   MAX(car.Id_MName) AS TopCarId,
				   model.Id_Mode_Product AS ModelId,
				   [year].Id_Year AS YearId
			FROM hdt.MName car
			JOIN hdt.[Year] [year]
				ON [year].Id_Year = car.Id_Year
			JOIN hdt.Mode_Product model
				ON model.Id_Mode_Product = [year].Id_Mode_Product
			JOIN hdt.Type_Product style
				ON style.Id_TypeProduct = model.Id_TypeProduct
			JOIN hdt.Manufacturer manufacturer
				ON manufacturer.Id_Manufacturer = style.Id_Manufacturer
			LEFT JOIN hdt.Product product
			  ON product.Id_MName = car.Id_MName
			WHERE car.Id_MName IS NOT NULL
			AND manufacturer.Name_Manufacturer <> LTRIM(RTRIM(@manufacturerName))
			AND model.Name_Mode_Product <> LTRIM(RTRIM(@modelName))
			AND style.Name_TypeProduct = LTRIM(RTRIM(@styleName))
			AND [year].Name_Year = LTRIM(RTRIM(@yearName))
			GROUP BY model.Id_Mode_Product,
					 [year].Id_Year
		) GroupCarByModelYear
		JOIN 
		(
			SELECT
				manufacturer.Id_Manufacturer AS ManufacturerId,
				model.Id_Mode_Product AS ModelId,
				[year].Id_Year AS YearId,
				car.Id_MName AS CarId,
				COALESCE(product.Id_Product, 0) AS ProductId,
				COALESCE(product.Price_Product, 0) AS Price,
				COALESCE(unit.Name_Unit, '') AS UnitName,
				COALESCE(avatar.PathAvatar, '') AS Avatar,
				car.Name_MName AS CarName,
				manufacturer.Name_Manufacturer AS ManufacturerName,
				model.Name_Mode_Product AS ModelName,
				CONVERT(NVARCHAR(10), [year].Name_Year) AS YearName,
				specification.Fuel_Economy_City AS FuelEconomyCity,
				specification.Fuel_Economy_Highway AS FuelEconomyHighway
			FROM hdt.MName car
			JOIN hdt.[Year] [year]
			  ON [year].Id_Year = car.Id_Year
			JOIN hdt.Mode_Product model
			  ON model.Id_Mode_Product = [year].Id_Mode_Product
			JOIN hdt.Type_Product style
			  ON style.Id_TypeProduct = model.Id_TypeProduct
			JOIN hdt.Manufacturer manufacturer
			  ON manufacturer.Id_Manufacturer = style.Id_Manufacturer
			LEFT JOIN hdt.Avatar avatar
			  ON avatar.Id_MName = car.Id_MName
			LEFT JOIN hdt.Product product
			  ON product.Id_MName = car.Id_MName
			LEFT JOIN hdt.Unit unit
			  ON product.Id_Unit = unit.Id_Unit
			LEFT JOIN hdt.Specifications specification
			  ON specification.Id_MName = car.Id_MName
		) CarInfo
		ON CarInfo.CarId = GroupCarByModelYear.TopCarId
		AND CarInfo.Price = GroupCarByModelYear.Price
		AND CarInfo.ProductId = GroupCarByModelYear.ProductId
	)

	CREATE TABLE #ReviewCount
	(
		Brand NVARCHAR(200),
		Model NVARCHAR(200),
		[Year] NVARCHAR(200),
		TotalReview INT
	)
	INSERT INTO #ReviewCount
	SELECT Brand, Model, [Year],
			COUNT(*) AS TotalReview
	FROM hdt.Reviews_ModelDetail
	GROUP BY Brand, Model, [Year]

	CREATE TABLE #ReviewPoints
	(
		Brand NVARCHAR(200),
		Model NVARCHAR(200),
		[Year] NVARCHAR(200),
		Point_Body INT,
		Point_Safe INT,
		Point_Operation INT,
		Point_Price INT
	)
	INSERT INTO #ReviewPoints
	SELECT Brand, Model, [Year],
			MAX(Point_Body) Point_Body,
			MAX(Point_Safe) Point_Safe,
			MAX(Point_Operation) Point_Operation,
			MAX(Point_Price) Point_Price
	FROM Reviews_ModelDetail
	GROUP BY Brand, Model, [Year]

	-- Get Total Reviews and Review Points for same style cars
	SELECT #AllCars.*,
			COALESCE(#ReviewPoints.Point_Body, 0) AS PointBody,
			COALESCE(#ReviewPoints.Point_Safe, 0) AS PointSafe,
			COALESCE(#ReviewPoints.Point_Operation, 0) AS PointOperation,
			COALESCE(#ReviewPoints.Point_Price, 0) AS PointPrice,
			COALESCE(#ReviewCount.TotalReview, 0) AS TotalReview,
			TotalPages = (COUNT(*) OVER () + @size - 1) / @size
	FROM #AllCars
	LEFT JOIN #ReviewCount
	ON #ReviewCount.Brand = #AllCars.ManufacturerName
	AND #ReviewCount.Model = #AllCars.ModelName
	AND #ReviewCount.[Year] = #AllCars.YearName
	LEFT JOIN #ReviewPoints
	ON #ReviewPoints.Brand = #AllCars.ManufacturerName
	AND #ReviewPoints.Model = #AllCars.ModelName
	AND #ReviewPoints.[Year] = #AllCars.YearName
	ORDER BY #AllCars.ManufacturerName
	OFFSET @size * (@page - 1) ROWS FETCH NEXT @size ROWS ONLY

	DROP TABLE #AllCars, #ReviewCount, #ReviewPoints
END
