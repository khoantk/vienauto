USE [carbank]
GO

/****** Object:  StoredProcedure [hdt].[sp_GetDetailModelReview]    Script Date: 11/06/2017 5:28:46 CH ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Khoa
-- Create date: 2017-06-10
-- Description:	Get Detail Model Review
-- =============================================
CREATE PROCEDURE [hdt].[sp_GetDetailModelReview]
	@ManufacturerId NVARCHAR(100),
	@ModelId NVARCHAR(100),
	@YearId NVARCHAR(10)
AS
BEGIN
	SELECT TOP 1
		   avatar.PathAvatar as [Image],
		   m.Name_Manufacturer as Brand,
		   mp.Name_Mode_Product as Model,
		   CONVERT(NVARCHAR(10), y.Name_Year) as Year,
		   CONCAT(m.Name_Manufacturer, ' ', CONVERT(varchar(10), y.Name_Year), 
		   		' ', mp.Name_Mode_Product) as ModelName,
		   sp.Fuel_Economy_City as FuelInCity, 
		   sp.Fuel_Economy_Highway as FuelInHighWay,
		   p.Price_Product as Price,
		   u.Name_Unit as Unit,
		   Reviews.PointBody,
		   Reviews.PointOperation,
		   Reviews.PointSafe,
		   Reviews.PointPrice,
		   Reviews.TotalReview
	FROM hdt.Manufacturer m
	LEFT JOIN hdt.Type_Product t on m.Id_Manufacturer = t.Id_Manufacturer
	LEFT JOIN hdt.Mode_Product mp on mp.Id_TypeProduct = t.Id_TypeProduct
	LEFT JOIN hdt.[Year] y on y.Id_Mode_Product = mp.Id_Mode_Product
	LEFT JOIN hdt.MName mn on mn.Id_Year = y.Id_Year
	LEFT JOIN hdt.Avatar avatar on avatar.Id_MName = mn.Id_MName
	LEFT JOIN hdt.Specifications sp on sp.Id_MName = mn.Id_MName
	LEFT JOIN hdt.Product p on p.Id_MName = mn.Id_MName
	LEFT JOIN hdt.Unit u on u.Id_Unit = p.Id_Unit
	LEFT JOIN
	(
		SELECT Brand, Model, [Year],
				TotalReview,
				PointBody, PointSafe, PointOperation, PointPrice
		FROM
		(
			SELECT Brand, Model, [Year],
			CAST(CAST(SUM(Point_Body) AS FLOAT) / CAST(COUNT(Point_Body) AS FLOAT) AS INT) AS PointBody,
			CAST(CAST(SUM(Point_Safe) AS FLOAT) / CAST(COUNT(Point_Safe) AS FLOAT) AS INT) AS PointSafe,
			CAST(CAST(SUM(Point_Operation) AS FLOAT) / CAST(COUNT(Point_Operation) AS FLOAT) AS INT) AS PointOperation,
			CAST(CAST(SUM(Point_Price) AS FLOAT) / CAST(COUNT(Point_Price) AS FLOAT) AS INT) AS PointPrice,
			COUNT(Id_Reviews) AS TotalReview
			FROM Reviews_ModelDetail 
			GROUP BY Brand, Model, [Year]
		) AS SummaryReviews
	) AS Reviews 
	ON Reviews.Brand = m.Name_Manufacturer
	AND Reviews.Model = mp.Name_Mode_Product
	AND Reviews.[Year] = y.Name_Year
	WHERE m.Id_Manufacturer = @ManufacturerId
	AND mp.Id_Mode_Product = @ModelId
	AND y.Id_Year = @YearId
	ORDER BY mn.Id_MName DESC
END

GO


