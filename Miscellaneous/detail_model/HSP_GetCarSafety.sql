--should not set procedure name has 'sp' because 'sp' has in procedure name of system.
create proc hdt.HSP_GetCarSafety
@manufactureId int,
@modelId int,
@yearId int
as
SELECT top 1 
safetyalia5_.Airbags_frontal as FrontAirbag, 
safetyalia5_.Airbags_Impact as SideAirbag, 
safetyalia5_.Airbag_Curtain as BackAirbag, 
safetyalia5_.HeadLights as HeadLights, 
safetyalia5_.Exterior_Light_Control as ExteriorLightControl, 
safetyalia5_.Daytime_Running_Lights as DayLights, 
safetyalia5_.Led_Taillights as LedTailLights, 
safetyalia5_.Parking_Assist as ParkingAssist, 
safetyalia5_.Alarm_System as AlarmSystem, 
safetyalia5_.Door_Locks_System as DoorLockSystem, 
safetyalia5_.Rear_Child as ChildLock, 
safetyalia5_.Content_Theft as AntiTheftSystem, 
safetyalia5_.Low_Tire as LowPressureWarning, 
safetyalia5_.Brakes as Brakes, 
safetyalia5_.Brake_Assist as BrakeAssist 
FROM hdt.Manufacturer this_ 
left outer join hdt.Type_Product stylealias1_ on this_.Id_Manufacturer=stylealias1_.Id_Manufacturer 
left outer join hdt.Mode_Product modelalias2_ on stylealias1_.Id_TypeProduct=modelalias2_.Id_TypeProduct 
left outer join hdt.[Year] yearalias3_ on modelalias2_.Id_Mode_Product=yearalias3_.Id_Mode_Product 
left outer join hdt.MName caralias4_ on yearalias3_.Id_Year=caralias4_.Id_Year 
left outer join hdt.Safety_Security safetyalia5_ on caralias4_.Id_MName=safetyalia5_.Id_Safety_Security 
WHERE ((this_.Id_Manufacturer = @manufactureId and modelalias2_.Id_Mode_Product = @modelId) and yearalias3_.Id_Year = @yearId) ORDER BY caralias4_.Id_MName desc