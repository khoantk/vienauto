USE [carbank]
GO
/****** Object:  StoredProcedure [hdt].[sp_GetModelStandardInformation]    Script Date: 25/04/2017 10:49:54 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Khoa
-- Create date: 2017-03-04
-- Description:	Get Model Detail
-- =============================================
alter PROCEDURE [hdt].[sp_GetModelStandardInformation] 
	@manufacturerName NVARCHAR(50),
	@modelName NVARCHAR(50),
	@yearName NVARCHAR(10),
	@carId INT = 0,
	@userId INT = 0
AS
BEGIN
	SELECT TOP 1
	  manufacturer.Id_Manufacturer AS ManufacturerId,
	  model.Id_Mode_Product AS ModelId,
	  [year].Id_Year AS YearId,
	  car.Id_MName AS CarId,
	  product.Id_Product AS ProductId,
	  product.Price_Product AS Price,
	  unit.Name_Unit AS UnitName,
	  avatar.PathAvatar AS Avatar,
	  car.Name_MName AS CarName,
	  manufacturer.Name_Manufacturer AS ManufacturerName,
	  model.Name_Mode_Product AS ModelName,
	  CONVERT(NVARCHAR(10), [year].Name_Year) AS YearName,
	  style.Name_TypeProduct AS StyleName,
	  specification.Fuel_Economy_City AS FuelEconomyCity,
	  specification.Fuel_Economy_Highway AS FuelEconomyHighway,
	  ISNULL(ReviewPoints.Point_Body, 0) AS PointBody,
	  ISNULL(ReviewPoints.Point_Safe, 0) AS PointSafe,
	  ISNULL(ReviewPoints.Point_Operation, 0) AS PointOperation,
	  ISNULL(ReviewPoints.Point_Price, 0) AS PointPrice,
	  ISNULL(ReviewCount.TotalReview, 0) AS TotalReview
	FROM MName car
	JOIN [Year] [year]
	  ON [year].Id_Year = car.Id_Year
	JOIN Mode_Product model
	  ON model.Id_Mode_Product = [year].Id_Mode_Product
	JOIN Type_Product style
	  ON style.Id_TypeProduct = model.Id_TypeProduct
	JOIN Manufacturer manufacturer
	  ON manufacturer.Id_Manufacturer = style.Id_Manufacturer
	LEFT JOIN Avatar avatar
	  ON avatar.Id_MName = car.Id_MName
	LEFT JOIN Product product
	  ON product.Id_MName = car.Id_MName
	LEFT JOIN Unit unit
	  ON product.Id_Unit = unit.Id_Unit
	LEFT JOIN Specifications specification
	  ON specification.Id_MName = car.Id_MName
	LEFT JOIN (SELECT
	  Brand,
	  Model,
	  [Year],
	  COUNT(*) AS TotalReview
	FROM Reviews_ModelDetail
	WHERE Brand = LTRIM(RTRIM(@manufacturerName))
	AND Model = LTRIM(RTRIM(@modelName))
	AND [Year] = LTRIM(RTRIM(@yearName))
	GROUP BY Brand,
			 Model,
			 [Year]) ReviewCount
	  ON ReviewCount.Brand = manufacturer.Name_Manufacturer
	  AND ReviewCount.Model = model.Name_Mode_Product
	  AND ReviewCount.[Year] = [year].Name_Year
	LEFT JOIN (SELECT TOP 1
	  Brand,
	  Model,
	  [Year],
	  Point_Body,
	  Point_Safe,
	  Point_Operation,
	  Point_Price
	FROM Reviews_ModelDetail
	WHERE Brand = LTRIM(RTRIM(@manufacturerName))
	  AND Model = LTRIM(RTRIM(@modelName))
	  AND [Year] = LTRIM(RTRIM(@yearName))) ReviewPoints
	  ON ReviewCount.Brand = manufacturer.Name_Manufacturer
	  AND ReviewCount.Model = model.Name_Mode_Product
	  AND ReviewCount.[Year] = [year].Name_Year
	WHERE car.Id_MName = 
	CASE WHEN @carId = 0 THEN 
	(
		SELECT TOP 1 car.Id_MName 
		FROM MName car
		JOIN [Year] [year]
			ON [year].Id_Year = car.Id_Year
		JOIN Mode_Product model
			ON model.Id_Mode_Product = [year].Id_Mode_Product
		JOIN Type_Product style
			ON style.Id_TypeProduct = model.Id_TypeProduct
		JOIN Manufacturer manufacturer
			ON manufacturer.Id_Manufacturer = style.Id_Manufacturer
		WHERE manufacturer.Name_Manufacturer = LTRIM(RTRIM(@manufacturerName))
		AND model.Name_Mode_Product = LTRIM(RTRIM(@modelName))
		AND [year].Name_Year = LTRIM(RTRIM(@yearName))
		ORDER BY car.Id_MName DESC
	)
	ELSE @carId
	END
	ORDER BY car.Id_MName DESC
END

