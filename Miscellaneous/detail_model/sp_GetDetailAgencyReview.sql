USE [carbank]
GO

/****** Object:  StoredProcedure [hdt].[sp_GetDetailAgencyReview]    Script Date: 11/06/2017 5:29:33 CH ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Khoa
-- Create date: 2017-06-10
-- Description:	Get Detail Agency Review
-- =============================================

CREATE PROCEDURE [hdt].[sp_GetDetailAgencyReview] 
	@AgentId INT,
	@ManufacturerId INT
AS
BEGIN
	SELECT d.Id_dangki AS AgentId,
			d.Id_user AS UserId,
			m.Id_Manufacturer AS ManufacturerId,
			d.Ten_CTY AS AgentName,
			m.logo AS Avatar,
			t.Name_TinhThanh AS ProvinceName,
			m.Name_Manufacturer AS ManufacturerName,
			Reviews.CustomerService,
			Reviews.BuyingProcess,
			Reviews.QualityOfRepair,
			Reviews.Facilities,
			Reviews.TotalReviews,
			Reviews.Overall
	FROM hdt.Dangki_daili_online d
	LEFT JOIN hdt.Hang_Phanphoi ds
		ON ds.Id_DaiLy = d.Id_user
	LEFT JOIN hdt.Users u
		ON u.Id_Users = d.Id_user
	LEFT JOIN hdt.TinhThanh t
		ON t.Id_TinhThanh = u.TinhThanh
	LEFT JOIN hdt.Manufacturer m
		ON m.Id_Manufacturer = ds.Id_Manufacturer
	LEFT JOIN 
	(
		SELECT Id_Agent,
			   TotalReviews,
			   CustomerService, BuyingProcess, QualityOfRepair, Facilities, 
			  (CustomerService + BuyingProcess + QualityOfRepair + Facilities) / 4 AS Overall
		FROM
		(
			SELECT Id_Agent,
			CAST(CAST(SUM(Customer_Service) AS FLOAT) / CAST(COUNT(Customer_Service) AS FLOAT) AS INT) AS CustomerService,
			CAST(CAST(SUM(Buying_Process) AS FLOAT) / CAST(COUNT(Buying_Process) AS FLOAT) AS INT) AS BuyingProcess,
			CAST(CAST(SUM(Quality_of_Repair) AS FLOAT) / CAST(COUNT(Quality_of_Repair) AS FLOAT) AS INT) AS QualityOfRepair,
			CAST(CAST(SUM(Facilities) AS FLOAT) / CAST(COUNT(Facilities) AS FLOAT) AS INT ) Facilities,
			COUNT(Id_Reviews) AS TotalReviews
			FROM Reviews_Agent 
			GROUP BY Id_Agent
		) AS SummaryReviews
	) AS Reviews ON Reviews.Id_Agent = d.Id_dangki
	WHERE d.Id_dangki = @AgentId
	AND m.Id_Manufacturer = @ManufacturerId
END

GO


