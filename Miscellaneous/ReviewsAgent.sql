USE [carbank]
GO

/****** Object:  Table [hdt].[Reviews_Agent]    Script Date: 1/19/2017 3:19:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [hdt].[Reviews_Agent](
	[Id_Reviews] [int] IDENTITY(1,1) NOT NULL,
	[Id_Agent] [int] NOT NULL,
	[Id_User] [int] NULL,
	[Customer_Service] [int] NOT NULL,
	[Buying_Process] [int] NOT NULL,
	[Quality_of_Repair] [int] NOT NULL,
	[Facilities] [int] NOT NULL,
	[Overall] [float] NOT NULL,
	[Title_Review] [nvarchar](50) NOT NULL,
	[Content_Review] [nvarchar](4000) NULL,
	[Datetime_Review] [datetime] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NULL,
	[Phone] [nvarchar](20) NULL,
 CONSTRAINT [PK_Reviews_Agent] PRIMARY KEY CLUSTERED 
(
	[Id_Reviews] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [hdt].[Reviews_Agent] ADD  DEFAULT ((0)) FOR [Id_User]
GO

ALTER TABLE [hdt].[Reviews_Agent] ADD  CONSTRAINT [DF_Reviews_Agent_Customer_Service]  DEFAULT ((0)) FOR [Customer_Service]
GO

ALTER TABLE [hdt].[Reviews_Agent] ADD  CONSTRAINT [DF_Reviews_Agent_Buying_Process]  DEFAULT ((0)) FOR [Buying_Process]
GO

ALTER TABLE [hdt].[Reviews_Agent] ADD  CONSTRAINT [DF_Reviews_Agent_Quality_of_Repair]  DEFAULT ((0)) FOR [Quality_of_Repair]
GO

ALTER TABLE [hdt].[Reviews_Agent] ADD  CONSTRAINT [DF_Reviews_Agent_Facilities]  DEFAULT ((0)) FOR [Facilities]
GO

ALTER TABLE [hdt].[Reviews_Agent] ADD  CONSTRAINT [DF_Reviews_Agent_Overall]  DEFAULT ((0.0)) FOR [Overall]
GO


