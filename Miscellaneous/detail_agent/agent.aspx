﻿<%@ Page Language="C#"  MasterPageFile="~/App_Master/scpage.master" AutoEventWireup="true" CodeFile="agent.aspx.cs" Inherits="agent" EnableViewState="true" %>
<asp:Content ID="Content2" ContentPlaceHolderID="first_head_of_web" Runat="Server">
    <meta name="keywords" content=" <asp:Literal ID="keyword" runat="server"></asp:Literal>" />
    <meta name="description" content=" <asp:Literal ID="description" runat="server"></asp:Literal> " />
    <asp:Literal ID="imageheader" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="lasted_head_meta_of_site" Runat="Server">
    <% Methods method = new Methods(); string _rootlink = method.Get_rootlink(); %>
    <% Random rd = new Random(); double _n = rd.NextDouble();%>
    <% double _n1 = rd.NextDouble(); %>
    <% double _n2 = rd.NextDouble(); %>
    <% var keyMap = method.Get_keymap(); %>

    <link rel="stylesheet" type="text/css" href="//graphics.cars.com/core/css/cars/core.css" />

    <!--[if IE 7]>
      <link rel="stylesheet" href="//graphics.cars.com/core/css/cars/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
      <link rel="stylesheet" href="//graphics.cars.com/core/css/cars/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
      <link rel="stylesheet" href="//graphics.cars.com/core/css/cars/ie9.css" />
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="//graphics.cars.com/core/css/cars/buy.css" />

    <!--[if lte IE 8]>
        <link rel="Stylesheet" href="/Sc_themes/ie-fix.css" />
    <![endif]-->
        <!--[if lte IE 7]>
        <link rel="Stylesheet" href="/Sc_themes/ie7-fix.css" />
    <![endif]-->
        <!--[if lte IE 9]>
      <script type="text/javascript">
        document.createElement('header');
        document.createElement('nav');
        document.createElement('article');
        document.createElement('footer');
        </script>
    <![endif]-->
        <!--[if lt IE 9]>
        <script src="/Scripts/html5shiv.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="/js/main-js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/main-js/jquery-ui-1.8.19.custom.min.js"></script>
    <script type="text/javascript" src="/js/main-js/hdt-api-min.js"></script>
    <script type="text/javascript" src="/js/sc/loader/d/onload-min/<%=method.Encode64(method.Encode64(_n1.ToString())) %>"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCi3z-pIe5ZeEEFyyivrVh2hJ-DkZxBiJY&language=vi"></script>
    <%--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCi3z-pIe5ZeEEFyyivrVh2hJ-DkZxBiJY&signed_in=true&callback=initMap"></script>--%>
    
    <script type="text/javascript">

        function loadMap() {
            var toado_map = $("#toadomap").val().split(',');
            var zoom_map = parseInt($("#zoommap").val());
            
            var latitude = toado_map[0];
            var longtitude = toado_map[1];
            var position = new google.maps.LatLng(latitude, longtitude);
            
            var options = {
                zoom: 16,
                center: position,
                zoomControl: true,
                scaleControl: true,
                rotateControl: true,
                mapTypeControl: true,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            
            var mapGoogle = new google.maps.Map(document.getElementById("map"), options);

            var marker = new google.maps.Marker({
                title: "",
                //map: mapGoogle,
                position: position
            });

            marker.setMap(mapGoogle);
        }

        google.maps.event.addDomListener(window, 'load', loadMap);

        function loadTabActive() {
            var tabClass = ".tabs";
            var tabValue = $("#tabLoading").val();

            //remove all tabs activated
            $(tabClass + " a").each(function () {
                if ($(this).hasClass("active")) {
                    $(this).removeClass("active");
                }
            });

            //set active tab
            if (tabValue == "gioithieu") {
                $(tabClass + " #tabOverview").addClass("active");
            }
            if (tabValue == "danhgia") {
                $(tabClass + " #tabReview").addClass("active");
            }
        }

        function informReview(reviewId) {
            if (confirm("Bạn chắc chắn muốn báo cáo đánh giá này?")) {
                $.ajax({
                    type: "POST",
                    url: "/jx/items/inform_review_agent.aspx/InformReview",
                    data: '{idReview: "' + reviewId + '", idUser: "' + $("#idCurUser").val() + '"}',
                    cache: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var rs = data.d;
                        if (rs == "true") {
                            $(".inappropriate_" + reviewId).html("<p>Đã gửi báo cáo</p>");
                        }
                        else {
                            show_error(rs, 300, 130);
                        }
                    },
                    error: function (xhr) {
                        show_error("Lỗi xảy ra. Liên hệ admin.", 300, 130);
                    },
                    complete: function () { }
                });
            }
        }

        function removeReview(reviewId) {
            if (confirm("Bạn có chắc chắn muốn xóa đánh giá này?")) {
                $.ajax({
                    type: "POST",
                    url: "/jx/items/deletereview_agent.aspx/DeleteReview",
                    data: '{idReview: "' + reviewId + '"}',
                    cache: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var rs = data.d;
                        if (rs == "true") {
                            var agentId = $("#idAgent").val();
                            var url = "/daily/" + agentId;
                            location.reload();
                        }
                        else {
                            show_error(rs, 300, 130);
                        }
                    },
                    error: function (xhr) {
                        show_error("Lỗi xảy ra. Liên hệ admin.", 300, 130);
                    },
                    complete: function () { }
                });
            }
        }

        function viewInfo() {
            var _nh = '<div style="color: #90bdf0; list-style: none; font-weight: bold; padding: 15px;">';
            _nh += '<div class="fl">Hãy giúp các khách hàng của chúng tôi!</div>';
            _nh += '<div class="fl">Làm thế nào để đánh giá? Đọc hướng dẫn đánh giá của chúng tôi</div>';
            _nh += '<div class="fl">"Khi viết nhận xét của bạn, xin vui lòng xem xét các nguyên tắc sau đây:</div>';
            _nh += '<div class="fl">Tập trung vào các đại lý và kinh nghiệm cá nhân của bạn</div>';
            _nh += '<div class="fl">Cung cấp thông tin chi tiết về những gì bạn thích hoặc không thích về kinh nghiệm của bạn</div>';
            _nh += '<div class="fl">Chúng tôi bảo lưu quyền không gửi nhận xét của bạn nếu nó có chứa bất kỳ các loại sau đây của nội dung bị cấm, hoặc nếu tuỳ ý của chúng tôi, chúng tôi xác định nó nếu không vi phạm chính sách và hướng dẫn của chúng tôi cho Đại lý Nhận xét:</div>';
            _nh += '<div class="fl">Những lời tục tĩu, ngôn ngữ phân biệt đối xử, hoặc ngôn ngữ khác không thích hợp cho một diễn đàn công cộng</div>';
            _nh += '<div class="fl">Phỉ báng, miệt thị, vi phạm, hoặc nhận xét không thích hợp</div>';
            _nh += '<div class="fl">Bất kỳ hình thức quấy rối, đe dọa, tà dâm hay cố chấp</div>';
            _nh += '<div class="fl">Bất kỳ tài liệu tham khảo để định giá chiếc xe cụ thể</div>';
            _nh += '<div class="fl">Bất kỳ tài liệu tham khảo để hành động pháp lý, kiện tụng đang chờ giải quyết, hoặc hoạt động bất hợp pháp</div>';
            _nh += '<div class="fl">Quảng cáo, "spam" nội dung, tài liệu tham khảo cho các đại lý hoặc khác hoặc các trang web khác</div>';
            _nh += '<div class="fl">Địa chỉ email, URL, số điện thoại, địa chỉ vật lý hoặc các hình thức khác của thông tin liên lạc</div>';
            _nh += '<div class="fl">Đánh giá của bạn nên vẫn còn thiên vị. Nếu chúng tôi nghi ngờ rằng việc xem xét bất kỳ được viết bởi bất cứ ai khác hơn là người tiêu dùng, nó sẽ bị từ chối. Đại lý không được phép nộp ý kiến về thay mặt cho người tiêu dùng. Tất cả các ý kiến phải được viết và trình Vienauto.com bởi người tiêu dùng.</div>';
            _nh += '<div class="fl">Ngoài ra, nếu bạn muốn chia sẻ thông tin phản hồi với chúng tôi về nội dung trang web hoặc các vấn đề chất lượng, xin vui lòng không gửi phản hồi này qua đánh giá đại lý. Thay vào đó, hãy liên hệ với chúng tôi trực tiếp.</div>';
            _nh += '<div class="fl">Thưởng thức viết nhận xét!"</div></div>';

            $("body").creatNewTabed_Pintercustom({
                "w": 400,
                "h": 650,
                "title": "Hướng dẫn đánh giá",
                "code": _nh,
                "af": function () {
                }
            });
        }

        function gotoReviews() {
            if ($("#ownerAggregate").val() != "-105px") {
                show_error("Bạn đã đánh giá đại lý này.", 300, 130);
                return;
            }
            var _cs = 0, _bp = 0, _qr = 0, _fal = 0;
            var _name = "", _email = "", _phone = "", _title = "", _content = "";

            var _check_exists_rate = $.getJSON("/jx/items/check_isreviews_agent.aspx?hdtjsoncallback=?", {
                capt: getCapt(), agentId: $("#idAgent").val(), session: getCookie("___sc_session")
            },
            function() { }).success(function(rs) {
                if (rs.result == "false") {
                    var _getinfo = $.getJSON("/jx/allpage/selectinfomation_userlogin.aspx?hdtjsoncallback=?", {
                        capt: getCapt(), session: getCookie("___sc_session")
                    },
                    function() { }).success(function(rs) {
                        if (rs.result == "true") {
                            _name = rs.fullname;
                            _email = rs.email2;
                            _phone = rs.phone;
                        }
                    });

                    $("body").creatNewTabed_Pinter({
                        "w": 450,
                        "title": "Đánh giá đại lý",
                        "link": "/jx/templates/load_form_reviews_agent.aspx?capt=" + getCapt(),
                        "af": function() {
                            $(".fixtomid").css("z-index","99999");
                            $("#product_need_reviews").text($(".name_of_product").text());
                            $("ul#rate_cs li").mouseover(function() {
                                var _this_index = $(this).index();
                                $("ul#rate_cs li").removeClass("ct");
                                $(this).addClass("ct");
                                $("ul#rate_cs li").removeClass("sl");
                                for (var i = 0; i <= _this_index; i++) {
                                    $("ul#rate_cs li").eq(i).addClass("sl");
                                }
                            });
                            $("ul#rate_bp li").mouseover(function() {
                                var _this_index = $(this).index();
                                $("ul#rate_bp li").removeClass("ct");
                                $(this).addClass("ct");
                                $("ul#rate_bp li").removeClass("sl");
                                for (var i = 0; i <= _this_index; i++) {
                                    $("ul#rate_bp li").eq(i).addClass("sl");
                                }
                            });
                            $("ul#rate_qr li").mouseover(function() {
                                var _this_index = $(this).index();
                                $("ul#rate_qr li").removeClass("ct");
                                $(this).addClass("ct");
                                $("ul#rate_qr li").removeClass("sl");
                                for (var i = 0; i <= _this_index; i++) {
                                    $("ul#rate_qr li").eq(i).addClass("sl");
                                }
                            });
                            $("ul#rate_fal li").mouseover(function() {
                                var _this_index = $(this).index();
                                $("ul#rate_fal li").removeClass("ct");
                                $(this).addClass("ct");
                                $("ul#rate_fal li").removeClass("sl");
                                for (var i = 0; i <= _this_index; i++) {
                                    $("ul#rate_fal li").eq(i).addClass("sl");
                                }
                            });

                            var re = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

                            /* blur */
                            $("#title_reviews").blur(function() {
                                var _this_val = $(this).val();
                                if (isEmpty(_this_val)) {
                                    $(this).addClass("error");
                                    $("#error_title").text("Tiêu đề không được để trống.");
                                }
                                else {
                                    $(this).removeClass("error");
                                    $("#error_title").text("");
                                }
                            });

                            $("#fullname").blur(function() {
                                var _this_val = $(this).val();
                                if (isEmpty(_this_val)) {
                                    $(this).addClass("error");
                                    $("#error_name").text("Họ tên không được để trống.");
                                }
                                else {
                                    $(this).removeClass("error");
                                    $("#error_name").text("");
                                }
                            });

                            $("#email").blur(function() {
                                if ($("#email").val() != '' && !re.test($("#email").val())) {
                                    $("#error_email").text("Email không đúng định dạng.");
                                }
                            });

                            $("#phone").blur(function() {
                                if (isNaN($("#phone").val())) {
                                    $("#error_phone").text("Số điện thoại chỉ được nhập số.");
                                }
                            });

                            $("#text_reviews").blur(function() {
                                var _this_val = $(this).val();
                                if (isEmpty(_this_val) || _this_val.length < 20) {
                                    $(this).addClass("error");
                                    $("#error_content").text("Nội dung đánh giá quá ngắn.");
                                }
                                else {
                                    $(this).removeClass("error");
                                    $("#error_content").text("");
                                }
                            });
                            /* end blur */

                            $("#bt_reviews_click").clickPerTimes({
                                "timer": 1000,
                                "af": function() {
                                    _name = $("#fullname").val();
                                    _email = $("#email").val();
                                    _phone = $("#phone").val();
                                    _title = $("#title_reviews").val();
                                    _content = $("#text_reviews").val();

                                    if (_name.length <= 0) {
                                        $("#title_reviews").addClass("error");
                                        $("#error_title").text("Họ tên không được để trống.");
                                        return;
                                    }

                                    if (_title.length <= 0) {
                                        $("#title_reviews").addClass("error");
                                        $("#error_title").text("Tiêu đề không được để trống.");
                                        return;
                                    }

                                    if (_content.length < 20) {
                                        $("#text_reviews").addClass("error");
                                        $("#error_content").text("Nội dung đánh giá quá ngắn.");
                                        return;
                                    }

                                    if ($("#email").val() != '' && !re.test($("#email").val())) {
                                        $("#error_email").text("Email không đúng định dạng.");
                                        return;
                                    }

                                    if (isNaN($("#phone").val())) {
                                        $("#error_phone").text("Số điện thoại chỉ được nhập số.");
                                    }

                                    _cs = $("ul#rate_cs li.ct").index() + 1;
                                    _bp = $("ul#rate_bp li.ct").index() + 1;
                                    _qr = $("ul#rate_qr li.ct").index() + 1;
                                    _fal = $("ul#rate_fal li.ct").index() + 1;

                                    var _post_reviews = $.getJSON("/jx/items/insert_reviews_agent.aspx?hdtjsoncallback=?",
                                    {
                                        capt: getCapt(), agentId: $("#idAgent").val(),
                                        cs: _cs, bp: _bp, qual: _qr, fal: _fal,
                                        title: _title, content: _content,
                                        name: _name, mail: _email, phone: _phone
                                    },
                                    function() { }).success(function(rs) {
                                        if (rs.result == "true") {
                                            $("#panel_reviews_notcomplete").html("<div align='center' style='padding:30px 5px 5px 5px'><b>Cảm ơn bạn đưa ra ý kiến đánh giá cho đại lý này. Để đảm bảo tính khách quan chúng tôi sẽ không thay đổi nội dung đánh giá của bạn nhưng chúng tôi cần kiểm duyệt nội dung trước khi cho phép nó xuất hiện ra bên ngoài. Chúng tôi sẽ thông báo đến bạn khi việc kiểm duyệt hoàn tất.</b></div>");
                                            location.reload();
                                        }
                                        else {
                                            show_error("Phát sinh lỗi khi đánh giá xe, vui lòng thử lại.", 300, 130);
                                            return;
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }

        $(document).ready(function () {

            //loadMap();
            loadTabActive();

            $("#sortBy").val($("#sorting").val());

            var ownerFRateValue = $("#ownerRateF").val();
            var ownerCSRateValue = $("#ownerRateCS").val();
            var ownerBPRateValue = $("#ownerRateBP").val();
            var ownerQRRateValue = $("#ownerRateQR").val();
            var ownerSumRateValue = $("#ownerAggregate").val();
            var ownerSumTitleRateValue = $("#ownerAggregateTitle").val();

            //load owner aggregate review for title
            $("#dealer-profile-header .dealer .wrapper").css("backgroundPosition", ownerSumTitleRateValue);

            //load owner aggregate review for form review
            $("#dealer-average-ratings .dealer .wrapper").css("backgroundPosition", ownerSumRateValue);

            //load owner rate review for review form
            $("#dealer-average-ratings .rate .value").each(function (index) {
                if (index == 0) {
                    $(this).css("width", ownerCSRateValue);
                }
                if (index == 1) {
                    $(this).css("width", ownerBPRateValue);
                }
                if (index == 2) {
                    $(this).css("width", ownerQRRateValue);
                }
                if (index == 3) {
                    $(this).css("width", ownerFRateValue);
                }
            });

            //load list reviews aggregate value for tab review
            $(".dealer-review-instance .dealer").each(function () {
                var aggregateValue = $(this).find("input:hidden").val();
                $(this).find(".wrapper").css("backgroundPosition", aggregateValue);
            });

            //load list reviews rate value for tab review
            $(".dealer-review-instance .rateReview").each(function () {
                var customerService = $(this).find(".cs");
                var customerServiceValue = customerService.find("input:hidden").val();
                customerService.find(".value").css("width", customerServiceValue);

                var buyingProccess = $(this).find(".bp");
                var buyingProccessValue = buyingProccess.find("input:hidden").val();
                buyingProccess.find(".value").css("width", buyingProccessValue);

                var qualityResult = $(this).find(".qr");
                var qualityResultValue = qualityResult.find("input:hidden").val();
                qualityResult.find(".value").css("width", qualityResultValue);

                var facilities = $(this).find(".f");
                var facilitiesValue = facilities.find("input:hidden").val();
                facilities.find(".value").css("width", facilitiesValue);
            });

            $("#sales").click(function () {
                $("#divservice").slideUp();
                $("#divsale").slideDown();
                $("#sales").addClass("active");
                $("#services").removeClass("active");
            });

            $("#services").click(function () {
                $("#divsale").slideUp();
                $("#divservice").slideDown();
                $("#services").addClass("active");
                $("#sales").removeClass("active");
            });

            $("#sortBy").change(function () {
                var selectedValue = $(this).val();

                var userId = $("#idUserAgent").val();
                var tab = $("#tabLoading").val();

                var url = "/daily/" + userId;

                if (tab != '') {
                    url += "/" + tab;
                }
                url += "/" + selectedValue;

                window.open(url, "_self");
            });
        });

    </script>

    <style type="text/css">
        h1, h2, h3, h4 {
            text-align: left;
        }

        .inform p {
            color: red !important;
        }

        .main h2 {
            color: #7B2B84;
            margin-top: 0;
        }

        .main hr {
            border-top: 1px solid #e1e1e1;
            border-bottom: none;
        }
        
        #page {
            position: relative;
            min-width: 980px;
        }

        #outter, .main {
            min-width: 1058px;
        }
            
        .reviewOverall {
            background: url(/images/core/rating-dealer.png) no-repeat scroll top #C7D3D7;
        }
        
        #dealer-profile-header img:first-child {
            height: 150px !important;
            width: 150px !important;
        }
        
        .dealer-map {
            top: 9px;
            right: 50px;
            z-index: 999;
            position: absolute;
        }
        
        #map {
            width: 300px;
            height: 200px;
        }
        
    </style>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="content_of_site" Runat="Server">
    <%
        Methods method = new Methods();
        
        var prefixUrl = "/daily";

        var idAgent = _DetailAgent != null ? _DetailAgent.IdAgent : 0;

        var idUserAgent = _DetailAgent != null ? _DetailAgent.IdUser : 0;
        
        var idUserCurrent = _DetailAgent != null ? _DetailAgent.IdCurrentUser : 0;

        var aggregate = _DetailAgent != null && _DetailAgent.OwnerReview.Overall != null
                        ? _DetailAgent.OwnerReview.Overall : "-105px";

        var aggregateTitle = _DetailAgent != null && _DetailAgent.OwnerReview.OverallTitle != null
                        ? _DetailAgent.OwnerReview.OverallTitle : "-79px";

        var customerService = "0px";
        if (_DetailAgent != null && _DetailAgent.OwnerReview.CustomerService != null)
            customerService = _DetailAgent.OwnerReview.CustomerService;

        var buyingProccess = "0px";
        if (_DetailAgent != null && _DetailAgent.OwnerReview.BuyingProcess != null)
            buyingProccess = _DetailAgent.OwnerReview.BuyingProcess;

        var qualityResult = "0px";
        if (_DetailAgent != null && _DetailAgent.OwnerReview.QualityRepair != null)
            qualityResult = _DetailAgent.OwnerReview.QualityRepair;

        var facilities = "0px";
        if (_DetailAgent != null && _DetailAgent.OwnerReview.Facilities != null)
            facilities = _DetailAgent.OwnerReview.Facilities;

        var tabFocus = _DetailAgent != null && _DetailAgent.Parameter != null
            && !string.IsNullOrEmpty(_DetailAgent.Parameter.Tab) ? _DetailAgent.Parameter.Tab : "gioithieu";

        var sort = _DetailAgent != null && _DetailAgent.Parameter != null
        && !string.IsNullOrEmpty(_DetailAgent.Parameter.Sort) ? _DetailAgent.Parameter.Sort : "h";
            
    %>
    <input type="hidden" id="tabLoading" value="<%= tabFocus %>" />
    <input type="hidden" id="sorting" value="<%= sort %>" />
    <input type="hidden" id="idAgent" value="<%= idAgent %>" />
    <input type="hidden" id="idCurUser" value="<%= idUserCurrent %>" />
    <input type="hidden" id="idUserAgent" value="<%= idUserAgent %>" />
    <input type="hidden" id="ownerAggregate" value="<%= aggregate %>" />
    <input type="hidden" id="ownerAggregateTitle" value="<%= aggregateTitle %>" />
    <input type="hidden" id="ownerRateCS" value="<%= customerService %>" />
    <input type="hidden" id="ownerRateBP" value="<%= buyingProccess %>" />
    <input type="hidden" id="ownerRateQR" value="<%= qualityResult %>" />
    <input type="hidden" id="ownerRateF" value="<%= facilities %>" />
    <div id="page">
        <style type="text/css">
            @font-face {
                font-family: 'Effra Bold';
                src: url("/webfont/effra_bold-webfont.eot");
                src: url("/webfont/effra_bold-webfont.eot?#iefix") format('embedded-opentype'), url("/webfont/effra_bold-webfont.woff") format('woff'), url("/webfont/effra_bold-webfont.svg#webfont") format('svg'), url("/webfont/effra_bold-webfont.ttf") format('truetype');
            }

            @font-face {
                font-family: 'Source Sans Pro';
                src: url("//fonts.gstatic.com/s/sourcesanspro/v9/ODelI1aHBYDBqgeIAH2zlJbPFduIYtoLzwST68uhz_Y.woff2") format('woff2'), url("//fonts.gstatic.com/s/sourcesanspro/v9/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff") format('woff');
            }

            @font-face {
                font-family: 'Source Sans Pro Semibold';
                src: url("//fonts.gstatic.com/s/sourcesanspro/v9/toadOcfmlt9b38dHJxOBGMzFoXZ-Kj537nB_-9jJhlA.woff2") format('woff2'), url("//fonts.gstatic.com/s/sourcesanspro/v9/toadOcfmlt9b38dHJxOBGJ6-ys_j0H4QL65VLqzI3wI.woff") format('woff');
            }
        </style>
        <div class="dealer-map">
            <input type="hidden" id="toadomap" value="<%= _DetailAgent != null && 
            !string.IsNullOrEmpty(_DetailAgent.CoordinatesMap) ? _DetailAgent.CoordinatesMap : string.Empty %>" />
            <input type="hidden" id="zoommap" value="<%= _DetailAgent != null && 
            !string.IsNullOrEmpty(_DetailAgent.ZoomMap) ? _DetailAgent.ZoomMap : string.Empty %>" />
            <div id="map"></div>
        </div>
        <div class="row grid-layout">
            <div class="module tab-module" id="dealer-profile-header">
                <div class="row">
                    <div class="col10">
                        <img alt="" src="/upload/avatarusers/<%= _DetailAgent != null && !string.IsNullOrEmpty(_DetailAgent.Avatar) 
                            ? _DetailAgent.Avatar : string.Empty %>" width="150" height="150">
                    </div>
                    <div class="col29">
                        <h1><%= _DetailAgent != null && !string.IsNullOrEmpty(_DetailAgent.CompanyName) 
                            ? _DetailAgent.CompanyName : string.Empty %></h1>
                        <div class="row">
                            <div class="col13">
                                <div class="rating dealer">
                                    <a href="#">
                                        <div class="wrapper">
                                            <div class="value"></div>
                                        </div>
                                    </a>
                                    <div class="detail">
                                        <a href="<%= prefixUrl %>/<%= idUserAgent %>/danhgia"><%= _ListReviews.Count %> đánh giá</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row-end"></div>
                        </div>
                        <h4>
                            <address style="margin-bottom: 10px;">
							    <span itemprop="streetAddress">
                                    <%= _DetailAgent != null && !string.IsNullOrEmpty(_DetailAgent.Address) 
                                        ? _DetailAgent.Address : "N/A" %>
							    </span>
						    </address>
                        </h4>
                        <div class="row">
                            <div class="col8 new-phone">
                                <strong>Xe mới</strong>
                                <br />
                                <%= _DetailAgent != null && !string.IsNullOrEmpty(_DetailAgent.Phone) ? _DetailAgent.Phone : "N/A" %>
                            </div>
                            <div class="col9 used-phone">
                                <strong>Xe cũ</strong>
                                <br />
                                <%= _DetailAgent != null && !string.IsNullOrEmpty(_DetailAgent.Mobile) ? _DetailAgent.Mobile : "N/A" %>
                            </div>
                            <div class="col10 service-phone">
                                <strong>Dịch vụ</strong>
                                <span class="js-thirdparty-service-phone" style="display: block;">
                                    <%= _DetailAgent != null && !string.IsNullOrEmpty(_DetailAgent.Service) ? _DetailAgent.Service : "N/A" %>
                                </span>
                            </div>
                            <div class="row-end"></div>
                        </div>
                        <p>Sự trải nghiệm khác biệt trong ngày!</p>
                        <div class="row">
                        </div>
                    </div>
                    <%--<div class="col22 dealer-map"></div>--%>
                    <div class="row-end"></div>
                </div>
                <div class="tabs">
                    <a id="tabOverview" name="overview" href="<%= prefixUrl %>/<%= idUserAgent %>/gioithieu" class="active">Tổng Quan</a>
                    <a id="tabReview" name="review" href="<%= prefixUrl %>/<%= idUserAgent %>/danhgia">Đánh Giá</a>
                </div>
            </div>
        </div>
        <div class="row grid-layout">
            <% if (tabFocus == "gioithieu")
               { %>
            <div class="col41">
                <div class="module">
                    <% if (_OverviewInfo != null && 
                           !string.IsNullOrEmpty(_OverviewInfo.Title) &&
                           !string.IsNullOrEmpty(_OverviewInfo.Introduction))
                       { %>
                        <h3 class="secondary"><%= _OverviewInfo.Title %></h3>
                        <div id="dealer-description">
                            <%= _OverviewInfo.Introduction %>
                        </div>
                    <% }
                       else
                       { %>
                        <div class="js-manufacturerIncentives">
                            <div class="js-mi-widget">
                                <div class="alert-message notification">
                                    <span>
                                    Hiện chưa có giới thiệu tổng quan cho đại lý
                                    </span>
                                    <p>Vienauto.com sẽ cập nhật thông tin sớm nhất có thể.</p>
                                </div>
                            </div>
                        </div>
                    <% } %>
                </div>
                <div class="module">
                    <div class="row">
                        <div id="dealer-new-cars" class="col19">
                            <h2>Xe Mới</h2>
                            <p>Cửa hàng chúng tôi có <strong><%= _DetailAgent != null ? _DetailAgent.NewCount : 0 %> xe</strong> mới</p>
                            <div class="row">
                                <div class="col8">
                                    <a class="button" href="/tim-kiem/oto-moi/daily_<%= _DetailAgent != null ? _DetailAgent.IdUser : 0 %>.html">Xe Mới</a>
                                </div>
                                <div class="row-end"></div>
                            </div>
                        </div>
                        <div id="dealer-used-cars" class="col19">
                            <h2>Xe Cũ</h2>
                            <p>Cửa hàng chúng tôi có <strong><%= _DetailAgent != null ? _DetailAgent.OldCount : 0 %> xe</strong> cũ</p>
                            <div class="row">
                                <div class="col8">
                                    <a class="button" href="/tim-kiem/oto-cu/daily_<%= _DetailAgent != null ? _DetailAgent.IdUser : 0 %>.html">Xe Cũ</a>
                                </div>
                                <div class="row-end"></div>
                            </div>
                        </div>
                        <div class="row-end"></div>
                    </div>
                </div>
                <div id="AboutRPC" style="display: none;"></div>
                <div id="dealer-average-ratings" class="module">
                    <div class="row">
                        <div class="col15"><h2>Xếp hạng</h2></div>
                        <div class="col22">
                            <a href="javascript:viewInfo();" style="float: right;">Xem hướng dẫn và điều lệ đánh giá đại lý</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col29">
                            <div class="rating dealer overall">
                                <div class="wrapper">
                                    <div class="value"></div>
                                </div>
                                <h4 class="overall">Tổng thể</h4>
                            </div>
                            <% if (_DetailAgent != null && _DetailAgent.OwnerReview.CanRemove)
                               { %>
                            <span style="float: right;">
                                <a href="javascript:removeReview(<%= _DetailAgent.OwnerReview.IdReview %>);">Hủy (xóa đánh giá của bạn)</a>
                            </span>
                            <% } %>
                            <hr />
                            <div class="row">
                                <div class="col14">
                                    <div class="rating rate overall">
                                        <div class="wrapper">
                                            <div class="value"></div>
                                        </div>
                                        <div class="detail">Dịch vụ khách hàng</div>
                                    </div>
                                </div>
                                <div class="col14">
                                    <div class="rating rate overall">
                                        <div class="wrapper">
                                            <div class="value"></div>
                                        </div>
                                        <div class="detail">Mua hàng</div>
                                    </div>
                                </div>
                                <div class="row-end"></div>
                            </div>
                            <div class="row">
                                <div class="col14">
                                    <div class="rating rate overall">
                                        <div class="wrapper">
                                            <div class="value"></div>
                                        </div>
                                        <div class="detail">Sữa chữa</div>
                                    </div>
                                </div>
                                <div class="col14">
                                    <div class="rating rate overall">
                                        <div class="wrapper">
                                            <div class="value"></div>
                                        </div>
                                        <div class="detail">Thiết bị tổng thể</div>
                                    </div>
                                </div>
                                <div class="row-end"></div>
                            </div>
                        </div>
                        <div class="col9">
                            <a href="javascript:gotoReviews();" class="button cars-write-dealer-review">Viết Đánh Giá</a>
                            <p class="helper-text"><span>Bằng cách viết đánh giá,bạn có thể cung cấp những giá trị đích thực cho những khách hàng khác.</span></p>
                        </div>
                        <div class="row-end"></div>
                    </div>
                    <div id="dealer-featured-review">
                        <h3 class="secondary">Đánh giá tiêu biểu</h3>
                        <% if (_ListReviews.Count > 0)
                           { %>
                        <div class="dealer-review-instance">
                            <div class="rating dealer">
                                <input type="hidden" value="<%= _ListReviews[0].Overall %>" />
                                <div class="wrapper reviewOverall">
                                    <div class="value"></div>
                                </div>
                            </div>
                            <h4><%= _ListReviews[0].Title %></h4>
                            <p>bởi <%= _ListReviews[0].UserName %></p>
                            <p><%= _ListReviews[0].Content %></p>
                        </div>
                        <% } %>
                    </div>
                    <% if (_ListReviews.Count > 0)
                       { %>
                    <p><a href="<%= prefixUrl %>/<%= idUserAgent %>/danhgia" class="secondary">Xem tất cả các đánh giá</a></p>
                    <% } %>
                </div>
            </div>
            <div class="col19">
                <div class="module" id="dealer-hours">
                    <h3>Giờ mở cửa</h3>
                    <div class="sub-tabs">
                        <a id="sales" href="javascript:void(0);" class="active">Bán hàng</a>
                        <a id="services" href="javascript:void(0);">Dịch vụ</a>
                    </div>
                    <div id="divsale">
                        <dl id="hours-sales">
                            <% if (_TimeActiveInfo.DisplaySales.Contains("2"))
                               { %>
                            <dt>Thứ hai</dt>
                            <dd><%= (_TimeActiveInfo.MonSales != null && _TimeActiveInfo.MonSales.Count == 2) 
                                ? _TimeActiveInfo.MonSales[0] : string.Empty %> AM                
                                &ndash;                                                           
                                <%= (_TimeActiveInfo.MonSales != null && _TimeActiveInfo.MonSales.Count == 2) 
                                ? _TimeActiveInfo.MonSales[1] : string.Empty %> PM</dd>
                            <% }
                               if (_TimeActiveInfo.DisplaySales.Contains("3"))
                               { %>    
                            <dt>Thứ ba</dt>                                                       
                            <dd><%= (_TimeActiveInfo.TueSales != null && _TimeActiveInfo.TueSales.Count == 2) 
                                ? _TimeActiveInfo.TueSales[0] : string.Empty %> AM                
                                &ndash;                                                           
                                <%= (_TimeActiveInfo.TueSales != null && _TimeActiveInfo.TueSales.Count == 2) 
                                ? _TimeActiveInfo.TueSales[1] : string.Empty %> PM</dd>
                            <% }
                               if (_TimeActiveInfo.DisplaySales.Contains("4"))
                               { %>           
                            <dt>Thứ tư</dt>                                                       
                            <dd><%= (_TimeActiveInfo.WedSales != null && _TimeActiveInfo.WedSales.Count == 2) 
                                ? _TimeActiveInfo.WedSales[0] : string.Empty %> AM                
                                &ndash;                                                           
                                <%= (_TimeActiveInfo.WedSales != null && _TimeActiveInfo.WedSales.Count == 2)
                                ? _TimeActiveInfo.WedSales[1] : string.Empty %> PM</dd>
                            <% }
                               if (_TimeActiveInfo.DisplaySales.Contains("5"))
                               { %>          
                            <dt>Thứ năm</dt>                                                      
                            <dd><%= (_TimeActiveInfo.ThuSales != null && _TimeActiveInfo.ThuSales.Count == 2)
                                ? _TimeActiveInfo.ThuSales[0] : string.Empty %> AM                
                                &ndash;                                                           
                                <%= (_TimeActiveInfo.ThuSales != null && _TimeActiveInfo.ThuSales.Count == 2)
                                ? _TimeActiveInfo.ThuSales[1] : string.Empty %> PM</dd>
                            <% }
                               if (_TimeActiveInfo.DisplaySales.Contains("6"))
                               { %>           
                            <dt>Thứ sáu</dt>                                                      
                            <dd><%= (_TimeActiveInfo.FriSales != null && _TimeActiveInfo.FriSales.Count == 2) 
                                ? _TimeActiveInfo.FriSales[0] : string.Empty %> AM                
                                &ndash;                                                           
                                <%= (_TimeActiveInfo.FriSales != null && _TimeActiveInfo.FriSales.Count == 2) 
                                ? _TimeActiveInfo.FriSales[1] : string.Empty %> PM</dd>
                            <% }
                               if (_TimeActiveInfo.DisplaySales.Contains("7"))
                               { %>
                            <dt>Thứ bảy</dt>                                                       
                            <dd><%= (_TimeActiveInfo.SatSales != null && _TimeActiveInfo.SatSales.Count == 2) 
                                ? _TimeActiveInfo.SatSales[0] : string.Empty %> AM
                                &ndash;                                                            
                                <%= (_TimeActiveInfo.SatSales != null && _TimeActiveInfo.SatSales.Count == 2) 
                                ? _TimeActiveInfo.SatSales[1] : string.Empty %> PM</dd>
                            <% } 
                               if (_TimeActiveInfo.DisplaySales.Contains("8"))
                               { %>
                            <dt>Chủ nhật</dt>                                                      
                            <dd><%= (_TimeActiveInfo.SunSales != null && _TimeActiveInfo.SunSales.Count == 2) 
                                ? _TimeActiveInfo.SunSales[0] : string.Empty %> AM
                                &ndash;                                                            
                                <%= (_TimeActiveInfo.SunSales != null && _TimeActiveInfo.SunSales.Count == 2) 
                                ? _TimeActiveInfo.SunSales[1] : string.Empty %> PM</dd>
                            <% } %>
                        </dl>
                    </div>                
                    <div id="divservice" style="display:none">   
                        <dl id="hours-service">
                            <% if (_TimeActiveInfo.DisplayServices.Contains("2"))
                               { %>
                            <dt>Thứ hai</dt>
                            <dd><%= (_TimeActiveInfo.MonServices != null && _TimeActiveInfo.MonServices.Count == 2)
                                ? _TimeActiveInfo.MonServices[0] : string.Empty %> AM
                                &ndash; 
                                <%= (_TimeActiveInfo.MonServices != null && _TimeActiveInfo.MonServices.Count == 2) 
                                ? _TimeActiveInfo.MonServices[1] : string.Empty %> PM</dd>
                            <% } 
                               if (_TimeActiveInfo.DisplayServices.Contains("3"))
                               { %>
                            <dt>Thứ ba</dt>                                                              
                            <dd><%= (_TimeActiveInfo.TueServices != null && _TimeActiveInfo.TueServices.Count == 2) 
                                ? _TimeActiveInfo.TueServices[0] : string.Empty %> AM
                                &ndash;                                                                  
                                <%= (_TimeActiveInfo.TueServices != null && _TimeActiveInfo.TueServices.Count == 2) 
                                ? _TimeActiveInfo.TueServices[1] : string.Empty %> PM</dd>
                            <% }
                               if (_TimeActiveInfo.DisplayServices.Contains("4"))
                               { %>
                            <dt>Thứ tư</dt>                                                           
                            <dd><%= (_TimeActiveInfo.WedServices != null && _TimeActiveInfo.WedServices.Count == 2) 
                                ? _TimeActiveInfo.WedServices[0] : string.Empty %> AM
                                &ndash;                                                                  
                                <%= (_TimeActiveInfo.WedServices != null && _TimeActiveInfo.WedServices.Count == 2) 
                                ? _TimeActiveInfo.WedServices[1] : string.Empty %> PM</dd>
                            <% }
                               if (_TimeActiveInfo.DisplayServices.Contains("5"))
                               { %>
                            <dt>Thứ năm</dt>                                                            
                            <dd><%= (_TimeActiveInfo.ThuServices != null && _TimeActiveInfo.ThuServices.Count == 2) 
                                ? _TimeActiveInfo.ThuServices[0] : string.Empty %> AM
                                &ndash;                                                                  
                                <%= (_TimeActiveInfo.ThuServices != null && _TimeActiveInfo.ThuServices.Count == 2) 
                                ? _TimeActiveInfo.ThuServices[1] : string.Empty %> PM</dd>
                            <% } 
                               if (_TimeActiveInfo.DisplayServices.Contains("6"))
                               { %>
                            <dt>Thứ sáu</dt>                                                              
                            <dd><%= (_TimeActiveInfo.FriServices != null && _TimeActiveInfo.FriServices.Count == 2) 
                                ? _TimeActiveInfo.FriServices[0] : string.Empty %> AM
                                &ndash;                                                                  
                                <%= (_TimeActiveInfo.FriServices != null && _TimeActiveInfo.FriServices.Count == 2) 
                                ? _TimeActiveInfo.FriServices[1] : string.Empty %> PM</dd>
                            <% } 
                               if (_TimeActiveInfo.DisplayServices.Contains("7"))
                               { %>
                            <dt>Thứ bảy</dt>                                                            
                            <dd><%= (_TimeActiveInfo.SatServices != null && _TimeActiveInfo.SatServices.Count == 2) 
                                ? _TimeActiveInfo.SatServices[0] : string.Empty %> AM
                                &ndash;                                                                  
                                <%= (_TimeActiveInfo.SatServices != null && _TimeActiveInfo.SatServices.Count == 2) 
                                ? _TimeActiveInfo.SatServices[1] : string.Empty %> PM</dd>
                            <% }
                               if (_TimeActiveInfo.DisplayServices.Contains("8"))
                               { %>
                            <dt>Chủ nhật</dt>                                                              
                            <dd><%= (_TimeActiveInfo.SunServices != null && _TimeActiveInfo.SunServices.Count == 2) 
                                ? _TimeActiveInfo.SunServices[0] : string.Empty %> AM
                                &ndash;                                                                  
                                <%= (_TimeActiveInfo.SunServices != null && _TimeActiveInfo.SunServices.Count == 2) 
                                ? _TimeActiveInfo.SunServices[1] : string.Empty %> PM</dd>
                            <% } %>
                        </dl>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="dealer-contact-directions" class="module">
                    <h3>Liên Hệ</h3>
                    <div class="dealer-car-contact">
                        <h4>Xe mới</h4>
                        <%= _DetailAgent != null && !string.IsNullOrEmpty(_DetailAgent.Phone) ? _DetailAgent.Phone : "N/A" %>
                    </div>
                    <div class="dealer-car-contact">
                        <h4>Xe cũ</h4>
                        <%= _DetailAgent != null && !string.IsNullOrEmpty(_DetailAgent.Mobile) ? _DetailAgent.Mobile : "N/A" %>
                    </div>
                    <h4>Dịch vụ</h4>
                    <span class="js-thirdparty-service-phone">
                        <%= _DetailAgent != null && !string.IsNullOrEmpty(_DetailAgent.Service) ? _DetailAgent.Service : "N/A" %>
                    </span>
                </div>
            </div>
            <% } %>
            <% if (tabFocus == "danhgia")
               { %>
            <div class="row grid-layout">
                <!--  Set Locale to EN_US -->
                <div>
                    <meta itemprop="name" content="<%= _DetailAgent != null && !string.IsNullOrEmpty(_DetailAgent.CompanyName)
                                                       ? _DetailAgent.CompanyName : string.Empty  %>">
                    <div class="module no-border" id="dealer-average-ratings">
                        <div class="row">
                            <div class="col19">
                                <h2>Xếp hạng</h2>
                            </div>
                            <div class="col20">
                                <a href="javascript:viewInfo();">Xem hướng dẫn và điều lệ đánh giá đại lý</a>
                            </div>
                            <div class="col14">
                                <% if (_DetailAgent != null && _DetailAgent.OwnerReview.CanRemove)
                                   { %>
                                <span style="float: right; margin-bottom: 15px;">
                                    <a href="javascript:removeReview(<%= _DetailAgent.OwnerReview.IdReview %>);">Hủy (xóa đánh giá của bạn)</a>
                                </span>
                                <% } %>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col19">
                                <div class="rating dealer overall">
                                    <div class="wrapper">
                                        <div class="value"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col14">
                                <div class="rating rate overall">
                                    <div class="wrapper">
                                        <div class="value"></div>
                                    </div>
                                    <div class="detail">Dịch vụ khách hàng</div>
                                </div>
                                <div class="rating rate overall">
                                    <div class="wrapper">
                                        <div class="value"></div>
                                    </div>
                                    <div class="detail">Mua hàng</div>
                                </div>
                            </div>
                            <div class="col14">
                                <div class="rating rate overall">
                                    <div class="wrapper">
                                        <div class="value"></div>
                                    </div>
                                    <div class="detail">Sữa chữa</div>
                                </div>
                                <div class="rating rate overall">
                                    <div class="wrapper">
                                        <div class="value"></div>
                                    </div>
                                    <div class="detail">Thiết bị tổng thể</div>
                                </div>
                            </div>
                            <div class="row-end"></div>
                        </div>
                        <div class="row">
                            <div class="col9"><a href="javascript:gotoReviews();" class="button cars-write-dealer-review">Viết đánh giá</a> </div>
                            <div class="col30">
                                <p class="helper-text"><span>Bằng cách viết đánh giá,bạn có thể cung cấp những giá trị đích thực cho những khách hàng khác.</span></p>
                            </div>
                            <div class="row-end"></div>
                        </div>
                        <hr />
                    </div>
                    <div id="dealer-reviews">
                        <div class="module no-border" id="review-sort">
                            <form id="sort-filter" method="get" action="#">
                                <div class="row">
                                    <div class="col19">
                                        <label>Hiển thị theo</label>
                                        <div class="select">
                                            <select name="sortBy" id="sortBy">
                                                <option value="h">Đánh giá cao nhất</option>
                                                <option value="l">Đánh giá thấp nhất</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col13">
                                        <div class="simple-pagination">
                                            <span>
                                                <%--1 of 1 review--%>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row-end"></div>
                                </div>
                            </form>
                        </div>
                        <a name="review-1016374"></a>
                        <div id="featuredReview" class="module">
                            <h2>Đánh giá khác</h2>
                            <% if (_ListReviews.Count > 0)
                               {
                                   foreach (var review in _ListReviews)
                                   { %>
                            <div class="dealer-review-instance">
                                <div class="rating dealer">
                                    <input type="hidden" value="<%= review.Overall %>" />
                                    <div class="wrapper reviewOverall">
                                        <div class="value"></div>
                                    </div>
                                    <meta itemprop="worstRating" content="1">
                                </div>
                                <h4><%= review.Title %></h4>
                                <p>bởi <span class="reviewer"><%= review.UserName %></span> 
                                    | <span class="meta"><%= review.DateReview %></span></p>
                                <div class="row">
                                    <div class="col42 dealer-review-body">
                                        <p itemprop="description" style="word-break: break-all;">
                                            <%= review.Content %></p>
                                        <span class="helpful">
                                            <p class="feedback">
                                                <% if (review.CanRemove)
                                                   { %>
                                                    (Xóa đánh giá thành viên)
									                <a href="javascript:removeReview(<%= review.IdReview %>);" class="button secondary small" rel="nofollow">Hủy</a>
                                                <% } %>
                                            </p>
                                        </span>
                                    </div>
                                    <div class="col14 rateReview">
                                        <div class="rating overall cs">
                                            <div class="wrapper">
                                                <input type="hidden" value="<%= review.CustomerService %>" />
                                                <div class="value"></div>
                                            </div>
                                            <div class="detail">Dịch vụ khách hàng</div>
                                        </div>
                                        <div class="rating overall bp">
                                            <div class="wrapper">
                                                <input type="hidden" value="<%= review.BuyingProcess %>" />
                                                <div class="value"></div>
                                            </div>
                                            <div class="detail">Mua hàng</div>
                                        </div>
                                        <div class="rating overall qr">
                                            <div class="wrapper">
                                                <input type="hidden" value="<%= review.QualityRepair %>" />
                                                <div class="value"></div>
                                            </div>
                                            <div class="detail">Sữa chữa</div>
                                        </div>
                                        <div class="rating overall f">
                                            <div class="wrapper">
                                                <input type="hidden" value="<%= review.Facilities %>" />
                                                <div class="value"></div>
                                            </div>
                                            <div class="detail">Thiết bị tổng thể</div>
                                        </div>
                                    </div>
                                    <div class="row-end"></div>
                                </div>
                                <div class="row dealer-review-footer">
                                    <div class="col43">
                                        <p>&nbsp;</p>
                                    </div>
                                    <input type="hidden" value="<%= review.IsInformed %>" />
                                    <div class="col12 inform inappropriate_<%= review.IdReview %>">
                                        <% if (review.IsInformed)
                                           { %>
                                                <p>Báo cáo bởi <%= review.UserInformed %></p>
                                        <%  }
                                           else
                                           {
                                               if (_DetailAgent != null && _DetailAgent.IdCurrentUser > 0)
                                               {
                                               %>
                                                <p>
                                                    <a href="javascript:informReview(<%= review.IdReview %>);" rel="nofollow">Báo cáo nội dung không thích hợp</a>
                                                </p>
                                            <% }
                                           } %>
                                    </div>
                                    <div class="row-end"></div>
                                </div>
                            </div>
                                <% }
                               } %>
                        </div>
                        <div class="col61 reviews-per-page">
                            <div class="col28 flush-right">(Có <strong><%= _ListReviews.Count %></strong> đánh giá)</div>
                            <div class="col20 flush-left">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <% } %>
    </div>
    </div>
</asp:Content>
