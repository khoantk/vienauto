﻿using System;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data;

public partial class agent : System.Web.UI.Page
{
    #region properties

    Methods method;
    Connect_Data connect;
    public static int idAgent = 0;
    public static string sortView = "", tabView = "";

    protected Overview _OverviewInfo;
    protected TimeActive _TimeActiveInfo;
    protected DetailAgent _DetailAgent;
    protected List<Review> _ListReviews;

    #endregion

    #region class

    public agent()
    {
        method = new Methods();
        connect = new Connect_Data();
        _OverviewInfo = new Overview();
        _TimeActiveInfo = new TimeActive
        {
            MonSales = new List<string>(),
            MonServices = new List<string>(),
            TueSales = new List<string>(),
            TueServices = new List<string>(),
            WedSales = new List<string>(),
            WedServices = new List<string>(),
            ThuSales = new List<string>(),
            ThuServices = new List<string>(),
            FriSales = new List<string>(),
            FriServices = new List<string>(),
            SatSales = new List<string>(),
            SatServices = new List<string>(),
            SunSales = new List<string>(),
            SunServices = new List<string>(),
            DisplaySales = new List<string>(),
            DisplayServices = new List<string>()
        };
        _DetailAgent = new DetailAgent();
        _ListReviews = new List<Review>();
    }

    public class Parameter
    {
        public string Tab { get; set; }
        public string Sort { get; set; }
    }

    public class DetailAgent
    {
        public int IdCurrentUser { get; set; }
        public int IdAgent { get; set; }
        public int IdUser { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Service { get; set; }
        public string CoordinatesMap { get; set; }
        public string ZoomMap { get; set; }
        public string Avatar { get; set; }
        public int NewCount { get; set; }
        public int OldCount { get; set; }
        public Parameter Parameter { get; set; }
        public OwnerReview OwnerReview { get; set; }
    }

    public class OwnerReview
    {
        public int IdUser { get; set; }
        public int IdReview { get; set; }
        public string CustomerService { get; set; }
        public string BuyingProcess { get; set; }
        public string QualityRepair { get; set; }
        public string Facilities { get; set; }
        public string Overall { get; set; }
        public string OverallTitle { get; set; }
        public bool CanRemove { get; set; }
    }

    public class Review
    {
        public int IdReview { get; set; }
        public int IdUser { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string UserName { get; set; }
        public string DateReview { get; set; }
        public string CustomerService { get; set; }
        public string BuyingProcess { get; set; }
        public string QualityRepair { get; set; }
        public string Facilities { get; set; }
        public string Overall { get; set; }
        public string UserInformed { get; set; }
        public bool IsInformed { get; set; }
        public bool CanRemove { get; set; }
    }

    public class Overview
    {
        public string Title { get; set; }
        public string Introduction { get; set; }
    }

    public class TimeActive
    {
        public List<string> MonSales { get; set; }
        public List<string> TueSales { get; set; }
        public List<string> WedSales { get; set; }
        public List<string> ThuSales { get; set; }
        public List<string> FriSales { get; set; }
        public List<string> SatSales { get; set; }
        public List<string> SunSales { get; set; }
        public List<string> MonServices { get; set; }
        public List<string> TueServices { get; set; }
        public List<string> WedServices { get; set; }
        public List<string> ThuServices { get; set; }
        public List<string> FriServices { get; set; }
        public List<string> SatServices { get; set; }
        public List<string> SunServices { get; set; }
        public List<string> DisplaySales { get; set; }
        public List<string> DisplayServices { get; set; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        var id = Request["id"];
        var tab = Request["tabAgent"];
        var sort = Request["sort"];

        if (!string.IsNullOrEmpty(id))
        {
            ///Get parent of current userid agent:
            /// - If have parent, get parent as userid
            /// - If have not, get userid
            var idUser = GetParentAgent(int.Parse(id));
            idAgent = GetAgentByUser(idUser); 
        }
        else
            Response.Redirect("/404.html");

        if (!string.IsNullOrEmpty(tab))
            tabView = tab.ToString();
        else
            tabView = "gioithieu";

        if (!string.IsNullOrEmpty(sort))
            sortView = sort.ToString();
        else
            sortView = "h";

        //load detail agent 
        _DetailAgent = LoadDetailAgent();

        if(_DetailAgent == null)
        {
            Response.Redirect("/404.html");
        }

        LoadSeo(_DetailAgent.CompanyName);

        switch (tabView)
        {
            case "gioithieu":
                _OverviewInfo = LoadOverview();
                _TimeActiveInfo = LoadTimeActive(_DetailAgent.IdUser);
                _ListReviews = LoadListReviews(sortView);
                break;
            case "danhgia":
                _ListReviews = LoadListReviews(sortView);
                break;
            default:
                _OverviewInfo = LoadOverview();
                _TimeActiveInfo = LoadTimeActive(_DetailAgent.IdUser);
                _ListReviews = LoadListReviews(sortView);
                break;
        }
    }
    
    private DetailAgent LoadDetailAgent()
    {
        try
        {
            var dt = connect.getTableSProcedure("Detail_Agent", idAgent);
            if (dt.Rows.Count > 0)
            {
                var agent = new DetailAgent();                
                agent.OwnerReview = LoadOwnerReview();
                agent.IdAgent = int.Parse(dt.Rows[0]["Id_Dangki"].ToString());
                agent.IdUser = int.Parse(dt.Rows[0]["Id_user"].ToString());
                agent.CompanyName = method.GetDBValue(dt.Rows[0]["Ten_cty"]);
                agent.Address = method.GetDBValue(dt.Rows[0]["Diachi_giaodich"]);
                agent.Phone = method.GetDBValue(dt.Rows[0]["Dien_thoai"]);
                agent.Mobile = method.GetDBValue(dt.Rows[0]["Didong"]);
                agent.Service = method.GetDBValue(dt.Rows[0]["Fax"]);
                agent.CoordinatesMap = method.GetDBValue(dt.Rows[0]["ToaDoMap"]);
                agent.ZoomMap = method.GetDBValue(dt.Rows[0]["ZoomMap"]);
                agent.Avatar = method.GetDBValue(dt.Rows[0]["Avatar"]);
                agent.NewCount = GetNumberNewOldCar(agent.IdUser, 1);
                agent.OldCount = GetNumberNewOldCar(agent.IdUser, 0);
                agent.Parameter = new Parameter { Tab = tabView, Sort = sortView };

                var _session_id = 0;
                var _ip_client = Request.UserHostAddress;

                if (Request.Cookies["___sc_session"] != null)
                {
                    _session_id = int.Parse(method.Decode64(method.Decode64(Request.Cookies["___sc_session"].Value)));
                }

                var currentUserId = connect.Get_NewID(String.Format(@"select u.Id_Users from Users u, session_login ss 
			                                where u.Id_Users = ss.id_user and ss.id_session_login = {0} 
			                                and ss.ip_client = '{1}'", _session_id, _ip_client));

                agent.IdCurrentUser = currentUserId;

                return agent;
            }
            else
                return null;
        }
        catch
        { 
            return null;
        }
    }

    private OwnerReview LoadOwnerReview()
    {
        var review = new OwnerReview();

        try
        {
            var _session_id = 0;
            var _ip_client = Request.UserHostAddress;

            if (Request.Cookies["___sc_session"] != null)
            {
                _session_id = int.Parse(method.Decode64(method.Decode64(Request.Cookies["___sc_session"].Value)));
            }

            var dt = connect.getTableSProcedure("Owner_Agent_Review", _session_id, _ip_client, idAgent);
            if (dt.Rows.Count > 0)
            {
                review.IdUser = int.Parse(dt.Rows[0]["Id_User"].ToString());
                review.IdReview = int.Parse(dt.Rows[0]["Id_Reviews"].ToString());
                review.BuyingProcess = ShowRateReview(int.Parse(dt.Rows[0]["Buying_Process"].ToString()));
                review.CustomerService = ShowRateReview(int.Parse(dt.Rows[0]["Customer_Service"].ToString()));
                review.QualityRepair = ShowRateReview(int.Parse(dt.Rows[0]["Quality_of_Repair"].ToString()));
                review.Facilities = ShowRateReview(int.Parse(dt.Rows[0]["Facilities"].ToString()));
                review.OverallTitle = ShowAggregateReviewOwnerTitle(double.Parse(dt.Rows[0]["Overall"].ToString()));
                review.Overall = ShowAggregateReviewOwner(double.Parse(dt.Rows[0]["Overall"].ToString()));
                review.CanRemove = CheckRoleRemoveReview(_session_id, _ip_client, review.IdReview);
            }

            return review;
        }
        catch
        {
            return null;
        }
    }

    private List<Review> LoadListReviews(string sort)
    {
        var listReview = new List<Review>();

        try
        {
            var dt = connect.getTableSProcedure("List_Reviews_Agent", idAgent, sort);
            if (dt.Rows.Count > 0)
            {
                var _session_id = 0;
                var _ip_client = Request.UserHostAddress;

                if (Request.Cookies["___sc_session"] != null)
                {
                    _session_id = int.Parse(method.Decode64(method.Decode64(Request.Cookies["___sc_session"].Value)));
                }

                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    var review = new Review();
                    review.Title = dt.Rows[i]["Title_Review"].ToString();
                    review.IdReview = int.Parse(dt.Rows[i]["Id_Reviews"].ToString());
                    review.IdUser = int.Parse(dt.Rows[i]["Id_User"].ToString());
                    review.IsInformed = !string.IsNullOrEmpty(GetUserInformed(review.IdReview)) ? true : false;
                    review.CustomerService = ShowRateReview(int.Parse(dt.Rows[i]["Customer_Service"].ToString()));
                    review.BuyingProcess = ShowRateReview(int.Parse(dt.Rows[i]["Buying_Process"].ToString()));
                    review.QualityRepair = ShowRateReview(int.Parse(dt.Rows[i]["Quality_of_Repair"].ToString()));
                    review.Facilities = ShowRateReview(int.Parse(dt.Rows[i]["Facilities"].ToString()));
                    review.Overall = ShowAggregateReview(double.Parse(dt.Rows[i]["Overall"].ToString()));
                    review.DateReview = String.Format("{0:d/M/yyyy}", DateTime.Parse(dt.Rows[i]["Datetime_Review"].ToString()));
                    review.Content = method.GetDBValue(dt.Rows[i]["Content_Review"]);
                    review.UserName =  method.GetDBValue(dt.Rows[i]["Name"]);
                    review.UserInformed = GetUserInformed(review.IdReview);
                    review.CanRemove = CheckRoleRemoveReview(_session_id, _ip_client, review.IdReview);
                    listReview.Add(review);
                }
            }
        }
        catch
        {
            return new List<Review>();
        }

        return listReview;
    }

    private Overview LoadOverview()
    {
        var overview = new Overview();

        try
        {
            var query = String.Format(@"select td.TieuDe, td.NoiDung from hdt.Tintuc_Daily td
                                        join hdt.Dangki_daili_online d on d.Id_user = td.Id_Daily
                                        where d.Id_dangki = {0}", idAgent);
            
            var dt = connect.GetTable(query);
            if(dt.Rows.Count > 0)
            {
                overview.Title = method.GetDBValue(dt.Rows[0]["TieuDe"]);
                overview.Introduction = method.GetDBValue(dt.Rows[0]["NoiDung"]);
            }

            return overview;
        }
        catch
        {
            return null;
        }
    }

    private TimeActive LoadTimeActive(int idUserAgent)
    {
        try
        {
            var timeActive = new TimeActive
            {
                MonSales = new List<string>(),
                MonServices = new List<string>(),
                TueSales = new List<string>(),
                TueServices = new List<string>(),
                WedSales = new List<string>(),
                WedServices = new List<string>(),
                ThuSales = new List<string>(),
                ThuServices = new List<string>(),
                FriSales = new List<string>(),
                FriServices = new List<string>(),
                SatSales = new List<string>(),
                SatServices = new List<string>(),
                SunSales = new List<string>(),
                SunServices = new List<string>(),
                DisplaySales = new List<string>(),
                DisplayServices = new List<string>()
            };

            var dt = connect.getTableSProcedure("GetAgentTimeActive", idUserAgent);
            if (dt.Rows.Count > 0)
            {
                var fromSales = method.GetDBValue(dt.Rows[0]["FromSales"]);
                var toSales = method.GetDBValue(dt.Rows[0]["ToSales"]);
                var fromServices = method.GetDBValue(dt.Rows[0]["FromServices"]);
                var toServices = method.GetDBValue(dt.Rows[0]["ToServices"]);
                var displaySales = method.GetDBValue(dt.Rows[0]["DisplaySales"]);
                var displayServices = method.GetDBValue(dt.Rows[0]["DisplayServices"]);

                if (displaySales != "")
                {
                    timeActive.DisplaySales = displaySales.Split(new Char[] { ',' }, StringSplitOptions.None).ToList();
                }

                if (displayServices != "")
                {
                    timeActive.DisplayServices = displayServices.Split(new Char[] { ',' }, StringSplitOptions.None).ToList();

                }

                if (fromSales != "" && toSales != "" && fromServices != "" && toServices != "")
                {
                    var arrayFromSales = fromSales.Split(new Char[] {','}, StringSplitOptions.None);
                    timeActive.MonSales.Add(arrayFromSales[0].Split(new Char[] {'-'}, StringSplitOptions.None)[1]);
                    timeActive.TueSales.Add(arrayFromSales[1].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.WedSales.Add(arrayFromSales[2].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.ThuSales.Add(arrayFromSales[3].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.FriSales.Add(arrayFromSales[4].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.SatSales.Add(arrayFromSales[5].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.SunSales.Add(arrayFromSales[6].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);

                    var arrayToSales = toSales.Split(new Char[] { ',' }, StringSplitOptions.None);
                    timeActive.MonSales.Add(arrayToSales[0].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.TueSales.Add(arrayToSales[1].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.WedSales.Add(arrayToSales[2].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.ThuSales.Add(arrayToSales[3].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.FriSales.Add(arrayToSales[4].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.SatSales.Add(arrayToSales[5].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.SunSales.Add(arrayToSales[6].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);

                    var arrayFromServices = fromServices.Split(new Char[] { ',' }, StringSplitOptions.None);
                    timeActive.MonServices.Add(arrayFromServices[0].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.TueServices.Add(arrayFromServices[1].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.WedServices.Add(arrayFromServices[2].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.ThuServices.Add(arrayFromServices[3].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.FriServices.Add(arrayFromServices[4].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.SatServices.Add(arrayFromServices[5].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.SunServices.Add(arrayFromServices[6].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);

                    var arrayToServices = toServices.Split(new Char[] { ',' }, StringSplitOptions.None);
                    timeActive.MonServices.Add(arrayToServices[0].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.TueServices.Add(arrayToServices[1].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.WedServices.Add(arrayToServices[2].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.ThuServices.Add(arrayToServices[3].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.FriServices.Add(arrayToServices[4].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.SatServices.Add(arrayToServices[5].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                    timeActive.SunServices.Add(arrayToServices[6].Split(new Char[] { '-' }, StringSplitOptions.None)[1]);
                }
            }

            return timeActive;
        }
        catch
        {
            return new TimeActive();
        }
    }

    private void LoadSeo(string agentName)
    {
        string urlString = method.Get_rootlink() + Request.RawUrl;
        DataTable _dtseo = connect.getTableSProcedure("aa_fillmeta_seolink", urlString);
        if (_dtseo.Rows.Count > 0)
        {
            if (_dtseo.Rows[0]["result"].ToString() == "1")
            {
                this.Page.Header.Title = _dtseo.Rows[0]["title"].ToString();
                description.Text = _dtseo.Rows[0]["description"].ToString();
                keyword.Text = _dtseo.Rows[0]["keyword"].ToString();
            }
            else
            {
                var _title = @"Đại lý " + agentName;

                var _description = @"Đọc đánh giá từ người dùng về đại lý, bản đồ hướng dẫn, giờ làm việc cụ thể, đặt lịch bảo dưỡng, 
                                    hãy liên hệ với các đại lý, xem hàng tồn kho. Tìm hiểu về " + agentName + " trên Vienauto.com";

                var _keyword = @"Đại lý, " + agentName;

                this.Page.Header.Title = _title;
                keyword.Text = _keyword;
                description.Text = _description;
            }
        }
    }

    private int GetNumberNewOldCar(int idUserAgent, int newOld)
    {
        try
        {
            var query = String.Format(@"select COUNT(*) from Product p join Users u on p.Id_Users = u.Id_Users 
                                        where u.Id_Users = {0} and p.New_Old = {1}", idUserAgent, newOld);

            var result = connect.getOnlyItem(query);
            if (result != "0")
            {
                return int.Parse(result);
            }

            return 0;
        }
        catch
        {
            return 0;
        }
    }

    private int GetParentAgent(int idUserAgent)
    {
        try
        {
            var result = connect.getOnlyItem(String.Format(@"select parent from Users where Id_Users = {0}", idUserAgent));

            if (result != "" && result != "0") //have parent, get parent as agent
                return int.Parse(result);

            return idUserAgent;
        }
        catch
        {
            return 0;
        }
    }

    private int GetAgentByUser(int idUserAgent)
    {
        try
        {
            var result = connect.getOnlyItem(String.Format(@"select Id_dangki from hdt.Dangki_daili_online where Id_user = {0}", idUserAgent));

            if (result != "")
                return int.Parse(result);

            return 0;
        }
        catch
        {
            return 0;
        }
    }

    private string GetUserInformed(int idReview)
    {
        try
        {
            var user = connect.getOnlyItem(String.Format(@"select u.FullName from hdt.Inform_Review i 
	                                                    join hdt.Users u on i.Id_User = u.Id_Users
	                                                    where i.Id_Reviews = {0}", idReview));
            return user;
        }
        catch
        {
            return string.Empty;
        }
    }

    private bool CheckRoleRemoveReview(int sessionId, string ipClient, int idReview)
    {
        try
        {
            var dt = connect.getTableSProcedure("check_remove_review_agent", sessionId, ipClient, idReview);
            if (dt.Rows.Count > 0)
            {
                var result = dt.Rows[0]["result"].ToString();

                if (result == "user" || result == "admin")
                {
                    return true;
                }
            }
            return false;
        }
        catch
        {
            return false;
        }
    }

    private string ShowRateReview(int rateValue)
    {
        var result = "";
        switch (rateValue)
        {
            case 0:
                result = "0px";
                break;
            case 1:
                result = "16px";
                break;
            case 2:
                result = "32px";
                break;
            case 3:
                result = "48px";
                break;
            case 4:
                result = "64px";
                break;
            case 5:
                result = "80px";
                break;
        }
        return result;
    }

    private string ShowAggregateReview(double rateValue)
    {
        var result = "-79px";

        if (rateValue == 0.5)
        {
            result = "-72px";
        }
        if (rateValue == 1.0)
        {
            result = "-64px";
        }
        if (rateValue == 1.5)
        {
            result = "-56px";
        }
        if (rateValue == 2.0)
        {
            result = "-48px";
        }
        if (rateValue == 2.5)
        {
            result = "-40px";
        }
        if (rateValue == 3.0)
        {
            result = "-32px";
        }
        if (rateValue == 3.5)
        {
            result = "-24px";
        }
        if (rateValue == 4.0)
        {
            result = "-16px";
        }
        if (rateValue == 4.5)
        {
            result = "-8px";
        }
        if (rateValue == 5.0)
        {
            result = "0px";
        }

        return result;
    }

    private string ShowAggregateReviewOwner(double rateValue)
    {
        var result = "-105px";

        if (rateValue == 0.5)
        {
            result = "-96px";
        }
        if (rateValue == 1.0)
        {
            result = "-85px";
        }
        if (rateValue == 1.5)
        {
            result = "-74px";
        }
        if (rateValue == 2.0)
        {
            result = "-64px";
        }
        if (rateValue == 2.5)
        {
            result = "-53px";
        }
        if (rateValue == 3.0)
        {
            result = "-42px";
        }
        if (rateValue == 3.5)
        {
            result = "-31px";
        }
        if (rateValue == 4.0)
        {
            result = "-21px";
        }
        if (rateValue == 4.5)
        {
            result = "-10px";
        }
        if (rateValue == 5.0)
        {
            result = "0px";
        }

        return result;
    }

    private string ShowAggregateReviewOwnerTitle(double rateValue)
    {
        var result = "-79px";

        if (rateValue > 0.0 && rateValue <= 0.5)
        {
            result = "-72px";
        }
        if (rateValue > 0.5 && rateValue <= 1.0)
        {
            result = "-64px";
        }
        if (rateValue > 1.0 && rateValue <= 1.5)
        {
            result = "-56px";
        }
        if (rateValue > 1.5 && rateValue <= 2.0)
        {
            result = "-48px";
        }
        if (rateValue > 2.0 && rateValue <= 2.5)
        {
            result = "-40px";
        }
        if (rateValue > 2.5 && rateValue <= 3.0)
        {
            result = "-32px";
        }
        if (rateValue > 3.0 && rateValue <= 3.5)
        {
            result = "-24px";
        }
        if (rateValue > 3.5 && rateValue <= 4.0)
        {
            result = "-16px";
        }
        if (rateValue > 4.0 && rateValue <= 4.5)
        {
            result = "-8px";
        }
        if (rateValue > 4.5 && rateValue <= 5.0)
        {
            result = "0px";
        }

        return result;
    }
}