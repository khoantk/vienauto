﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="load_form_reviews_agent.aspx.cs" Inherits="jx_templates_load_form_reviews_agent" %>
<div style="padding: 5px">
    <div id="panel_reviews_notcomplete">
        <div>Đánh giá của bạn sẽ giúp rất nhiều cho khách hàng của chúng tôi khi mua xe này. Theo nhận định riêng của bạn, dòng xe này đạt bao nhiêu điểm.<span id="product_need_reviews" style="font-weight: bold;"></span>.</div>
        <div style="padding-top: 30px;">
            <div><b>Đánh giá chung:</b></div>
            <ul style="line-height: 30px;">
                <li><span style="display: inline-block; width: 145px;">Dịch vụ khách hàng:</span>
                    <ul class="danhgia" id="rate_cs">
                        <li class="sl ct"></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </li>
                <li><span style="display: inline-block; width: 145px;">Thủ tục mua: </span>
                    <ul class="danhgia" id="rate_bp">
                        <li class="sl ct"></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </li>
                <li><span style="display: inline-block; width: 145px;">Chất lượng bảo trì:</span>
                    <ul class="danhgia" id="rate_qr">
                        <li class="sl ct"></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </li>
                <li><span style="display: inline-block; width: 145px;">Tiện lợi:</span> 
                    <ul class="danhgia" id="rate_fal">
                        <li class="sl ct"></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div>
            <b>Nhận xét</b>
            <div align="center" style="margin-top: 20px; ">
                <input class="input-register" placeholder="Tiêu đề đánh giá" id="title_reviews" type="text" />
                <input class="input-register" placeholder="Họ và tên" id="fullname" type="text" />
                <input class="input-register" placeholder="Email" id="email" type="text" />
                <input class="input-register" placeholder="Số điện thoại" id="phone" type="text" />
                <textarea class="input-register" id="text_reviews" style="height: 100px; width: 400px; padding: 5px;" placeholder="Nhận xét của bạn."></textarea>
                <div id="error_content" class="register_error" style="width: 400px; text-align: left;"></div>
                <div id="bt_reviews_click" style="width:60px; margin-top:10px; padding-top:5px; height:18px; margin-top:20px;" class="u7wu8732hu">Đánh giá</div>
            </div>
        </div>
    </div>
</div>
