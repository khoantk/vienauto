﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/scpage.master" AutoEventWireup="true" CodeFile="SearchAgent.aspx.cs" Inherits="SearchAgent" EnableViewState="true" %>
<asp:Content ID="Content2" ContentPlaceHolderID="first_head_of_web" Runat="Server">
    <meta name="keywords" content=" <asp:Literal ID="keyword" runat="server"></asp:Literal>" />
    <meta name="description" content=" <asp:Literal ID="description" runat="server"></asp:Literal> " />
    <asp:Literal ID="imageheader" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="lasted_head_meta_of_site" Runat="Server">
    <% Methods method = new Methods(); string _rootlink = method.Get_rootlink(); %>
    <% Random rd = new Random(); double _n = rd.NextDouble();%>
    <% double _n1 = rd.NextDouble(); %>
    <% double _n2 = rd.NextDouble(); %>
    <!--[if IE 7]>
      <link rel="stylesheet" href="//graphics.cars.com/core/css/cars/ie7.css" />
    <![endif]-->
    <!--[if IE 8]>
      <link rel="stylesheet" href="//graphics.cars.com/core/css/cars/ie8.css" />
    <![endif]-->
    <!--[if IE 9]>
      <link rel="stylesheet" href="//graphics.cars.com/core/css/cars/ie9.css" />
    <![endif]-->
    <!--[if lte IE 9]>
  <script type="text/javascript">
    document.createElement('header');
    document.createElement('nav');
    document.createElement('article');
    document.createElement('footer');
    </script>
<![endif]-->
    <!--[if lt IE 9]>
    <script src="http://cars.com/core/js/libs/html5shiv/html5shiv.min.js"></script>
<![endif]-->
    <link rel="stylesheet" type="text/css" href="//graphics.cars.com/core/css/cars/core.css" />
    <link rel="stylesheet" type="text/css" href="//graphics.cars.com/core/css/cars/buy.css">
    <link href="http://cars.com/core/css/cars/hyperdrive/dealerSearch.css" rel="stylesheet" type="text/css" />
    <link href="http://cars.com/core/css/cars/dealerSearch.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/sc/loader/f/hdt-api-min/<%=method.Encode64(method.Encode64(_n.ToString())) %>"></script>
    <script type="text/javascript" src="/js/sc/loader/d/onload-min/<%=method.Encode64(method.Encode64(_n1.ToString())) %>"></script>
    <script src="/js/items/012a_actionbook_share_in.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCi3z-pIe5ZeEEFyyivrVh2hJ-DkZxBiJY&language=vi"></script>
    <script type="text/javascript">

        function showPerPage() {
            var path = "/SearchAgent.aspx?numb=" + $(".select select").val() + "&man=<%= _hang %>&country=<%= _tinhthanh %>";
            window.open(path, "_self");
        }

        function loadMap() {
            var getFirstMarkers = new Array();
            $(".dealer").each(function (index, item) {
                var toado = $(this).find("input[name=toado]").val();
                getFirstMarkers.push(toado);
            });
            
            var centerOption = getFirstMarkers.shift().split(",");
            
            var options = {
                zoom: 8,
                zoomControl: true,
                scaleControl: true,
                rotateControl: true,
                mapTypeControl: true,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: new google.maps.LatLng(centerOption[0], centerOption[1])
            };
            var map = new google.maps.Map(document.getElementById("map_canvas"), options);

            var marker, i;
            var infowindow = new google.maps.InfoWindow();

            $(".dealer").each(function () {
                var company = $(this).find("input[name=company]").val();
                var toado = $(this).find("input[name=toado]").val().split(",");
                
                var markerMap = new Array();
                markerMap.push(company);
                markerMap.push(toado[0]);
                markerMap.push(toado[1]);

                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(markerMap[1], markerMap[2]),
                    map: map
                });

                google.maps.event.addListener(marker, "click", (function (marker, i) {
                    return function () {
                        infowindow.setContent(markerMap[0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            });
        }
        google.maps.event.addDomListener(window, "load", loadMap);

        $(document).ready(function () {
            $(".dealer").each(function () {
                var value = $(this).find("input[name=ratingVal]").val();
                $(this).find(".wrapper").css("backgroundPosition", value);
            });

            $(".select select").val(<%= _top %>);
        });

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="content_of_site" Runat="Server">
    <% Methods method = new Methods(); string _rootlink = method.Get_rootlink(); %>
    <div id="page">
        <div class="row grid-layout">
        </div>
        <div class="grid-layout">
            <div id="hyperdrive-ad"></div>
        </div>
        <div class="row grid-layout" id="dealer-search">
            <div class="col50">
                <h1>Hãng <b><%= _tenhang %></b> có <%= method.GetTypeAvoidNull(DiscountAgents.Count) %> đại lý</h1>
                <%--<a class="change-icon change-vehicle flyout" href="#edit-your-search">Edit your search</a>--%>
                <%--<a href="#" class="button secondary light icon js-print" style="position:absolute; right:0; top:8px;">
                    <span class="print"></span>In</a>--%>
                <h4>Tỉnh/Thành phố: <b>Hồ Chí Minh</b></h4>
                <div class="dealer-map" id="map_canvas"></div>
                <table id="locator-results">
                    <colgroup>
                        <col class="locator-results-number">
                        <col class="locator-results-tile">
                        <col class="locator-results-info">
                        <col class="locator-results-distance">
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="2">1 - <%= method.GetTypeAvoidNull(DiscountAgents.Count) %> đại lý</th>
                            <th>Sắp xếp theo: 
                  	            <a href="/SearchAgent.aspx?numb=<%= _top %>&man=<%= _hang %>&country=<%= _tinhthanh %>&filter=dl">Đại lý</a>, 
                                <a href="/SearchAgent.aspx?numb=<%= _top %>&man=<%= _hang %>&country=<%= _tinhthanh %>&filter=tt">Tỉnh thành</a>,
                  	            <a href="/SearchAgent.aspx?numb=<%= _top %>&man=<%= _hang %>&country=<%= _tinhthanh %>&filter=dg">Xếp hạng</a>
                            </th>
                            <th><span class="js-test-a">Tỉnh thành</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% for (var i = 0; i < DiscountAgents.Count; i++)
                            { %>
                        <tr>
                            <td><%= i+1 %></td>
                            <td>
                                <a href="#">
                                    <% if (DiscountAgents[i].Avatar == "/noavatar.gif")
                                        { %>
                                    <img src="/Upload/AvatarUsers/<%= DiscountAgents[i].Avatar %>"
                                        width="125" height="100" style="margin-right: 15px;" 
                                        alt="<%= DiscountAgents[i].CompanyName %>" />
                                    <% }
                                        else
                                        { %>
                                    <img src="/Upload/AvatarUsers/<%= DiscountAgents[i].Avatar %>"
                                         style="margin-right: 15px;" alt="<%= DiscountAgents[i].CompanyName %>" />
                                    <% } %>
                                </a>
                            </td>
                            <td>
                                <a href="/daily/<%= DiscountAgents[i].UserId %>"><%= DiscountAgents[i].CompanyName %></a>
                                <div class="rating dealer">
                                    <a href="#">
                                        <input type="hidden" name="ratingVal" value="<%= DiscountAgents[i].MaxOverall %>" />
                                        <div class="wrapper" style="background-position: 0px 0px;">
                                            <div class="value"></div>
                                        </div>
                                    </a>
                                    <input type="hidden" name="toado" value="<%= DiscountAgents[i].Coordinates %>" />
                                    <input type="hidden" name="company" value="<%= DiscountAgents[i].CompanyName %>" />
                                    <div class="detail"><a href="#"><%= DiscountAgents[i].TotalReview %> đánh giá</a> </div>
                                    <div class="row-end"></div>
                                </div>
                                <address>
                                    <span><%= DiscountAgents[i].Address %></span>
                                    <br />
                                    <strong>Xe mới</strong> <%= DiscountAgents[i].NewPhone %>
                                    <strong>Xe cũ </strong><%= DiscountAgents[i].OldPhone %>
                                    <br />
                                    <strong>Dịch vụ </strong>
                                    <span class="js-thirdparty-service-phone"><%= DiscountAgents[i].Service %></span>
                                </address>
                            </td>
                            <td>
                                <span class="js-test-a"><%= DiscountAgents[i].CountryName %></span>
                                <span class="js-test-b hidden"></span>
                            </td>
                        </tr>
                        <%  } %>
                    </tbody>
                </table>
                <%--<div class="module secondary tight"><strong>Dealers</strong>: <a href="#">Get your listings on cars.com</a></div>
                <a class="snr-link" href="#">Find service and repair centers</a>--%>
                <div class="reviews-per-page">
                    <div class="x-of-results">
                        (<strong>1 - <%= method.GetTypeAvoidNull(DiscountAgents.Count) %></strong> đại lý)
                    </div>
                    <label>Hiển thị</label>
                    <div class="select">
                        <form id="results-per-page" method="get" action="/">
                            <select name="count" onchange="showPerPage();">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50" selected="selected">50</option>
                                <option value="100">100</option>
                                <option value="200">200</option>
                                <option value="500">500</option>
                                <option value="500">1000</option>
                            </select>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col10">
            </div>
            <div class="row-end"></div>
        </div>
    </div>
</asp:Content>

