﻿using NHibernate;
using Vienauto.Interface;
using Common.Core.Data.SqlLog;
using Common.Core.Nhibernate.Base;
using Common.Core.Nhibernate.Context;

namespace Vienauto.Business
{
    public class BaseService : Repository, IBaseService
    {
        public BaseService()
        {
            Session = OpenDefaultSession();
        }

        protected override ISession Session { get; set; }

        protected override ISession OpenDefaultSession()
        {
            var sessionFactory = NhibernateConfiguration.GetDefaultSessionFactory();
#if DEBUG
            return sessionFactory.OpenSession(new SQLDebugOutput());
#else
            return sessionFactory.OpenSession();
#endif
        }

        protected override ISession OpenSession(string factoryKey = "")
        {
            var sessionFactory = NhibernateConfiguration.GetSessionFactory(factoryKey);
#if DEBUG
            return sessionFactory.OpenSession(new SQLDebugOutput());
#else
            return sessionFactory.OpenSession();
#endif
        }
    }
}
