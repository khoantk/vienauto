﻿using System;
using Vienauto.Entity.Enums;
using Vienauto.Interface.Dto;
using System.Threading.Tasks;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Common.Core.Extension.Encode;
using Vienauto.Interface.AsyncServices;

namespace Vienauto.Business.AsyncServices
{
    public class AccountAsyncService : BaseService, IAccountAsyncService
    {
        public Task<DataServiceResult<UserDto>> AuthenticateUserAsync(string userName, string passWord, bool enableEncryptPassword = false)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<UserDto>();

                using (var session = OpenDefaultSession())
                {
                    //var decodedPassword = EncodingExtensions.Vigenere.Decrypt(passWord);
                    var encodedMd5Password = EncodingExtensions.EncodeMD5(passWord);
                    var user = Session.QueryOver<User>()
                                      .Where(u => u.UserName == userName && u.PassWord == encodedMd5Password && u.Active == 1)
                                      .SingleOrDefault();
                    if (user == null)
                        return new DataServiceResult<UserDto>
                        {
                            Errors = new List<ErrorDescriber> { new ErrorDescriber { Code = ErrorCode.WrongUserNameOrPassword } }
                        };

                    //Update last login for user
                    user.LastLogin = DateTime.Now;
                    Session.SaveOrUpdate(user);
                    Session.Flush();

                    var userDto = user.FromEntityToDto();
                    result.Target = userDto;
                }
                return result;
            });
        }

        public Task<DataServiceResult<RegisterDto>> SignUpUserAsync(RegisterDto registerDto)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<RegisterDto>();

                using (var session = OpenDefaultSession())
                {
                    var existedUser = Duplicate<User>(x => x.UserName == registerDto.UserName);
                    if (existedUser)
                        result.AddError(ErrorCode.DuplicateUser);
                    else
                    {
                        var user = new User();
                        user.UserName = registerDto.UserName;
                        user.PassWord = EncodingExtensions.EncodeMD5(registerDto.PassWord);
                        user.FullName = registerDto.FullName;
                        user.Phone = registerDto.Phone;
                        user.Mobile = registerDto.Mobile;
                        user.Active = registerDto.Active;
                        user.Avatar = registerDto.Avatar;
                        user.NgayGiaNhap = registerDto.JoinDate;
                        user.changesub = registerDto.ChangeSub;
                        user.TienChietKhau = registerDto.Discount;
                        user.Province = Get<Province>(registerDto.ProvinceId);
                        user.ToaDoMap = user.Province?.ToaDoMap2;
                        user.ZoomMap = user.Province?.ZoomMap2.ToString();
                        user.Level = Get<Level>(registerDto.LevelId);
                        user.Question = Get<Question>(registerDto.QuestionId);
                        registerDto.UserId = Create(user);
                        CommitChanges();
                    }
                }
                return result;
            });
        }

        public Task<DataServiceResult<RegisterDto>> SignUpAgentUserAsync(RegisterDto registerDto)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<RegisterDto>();

                using (var session = OpenDefaultSession())
                {
                    var existedUser = Duplicate<User>(x => x.UserName == registerDto.UserName);
                    if (existedUser)
                        result.AddError(ErrorCode.DuplicateUser);

                    var user = new User();
                    user.UserName = registerDto.UserName;
                    user.PassWord = EncodingExtensions.EncodeMD5(registerDto.PassWord);
                    user.FullName = registerDto.FullName;
                    user.Phone = registerDto.Phone;
                    user.Mobile = registerDto.Mobile;
                    user.Active = registerDto.Active;
                    user.Avatar = registerDto.Avatar;
                    user.NgayGiaNhap = registerDto.JoinDate;
                    user.changesub = registerDto.ChangeSub;
                    user.TienChietKhau = registerDto.Discount;
                    user.Province = Get<Province>(registerDto.ProvinceId);
                    user.ToaDoMap = user.Province?.ToaDoMap2;
                    user.ZoomMap = user.Province?.ZoomMap2.ToString();
                    user.Level = Get<Level>(registerDto.LevelId);
                    user.Question = Get<Question>(registerDto.QuestionId);
                    registerDto.UserId = Create(user);
                    CommitChanges();

                    if (registerDto.UserId <= 0)
                        result.AddError(ErrorCode.RegisterAgentFail);
                    else
                    {
                        var agent = new Agency();
                        agent.kichhoat = 1;
                        agent.filedinhkem = "";
                        agent.ngaydangki = DateTime.Now;
                        agent.Ten_CTY = registerDto.CompanyName;
                        agent.Diachi_giaodich = registerDto.TransactionAddress;
                        agent.MaSoThue = registerDto.TaxNumber;
                        agent.HoTen_Nguoidaidien = registerDto.DeputyFullName;
                        agent.Dien_thoai = registerDto.AgentPhone;
                        agent.Didong = registerDto.Mobile;
                        agent.Email_kichhoat = registerDto.EmailVerification;
                        agent.Vitri = registerDto.Location;
                        agent.Chinhanh = registerDto.TotalBranches;
                        agent.Soxe_giaodich = registerDto.NumberCarTransaction;
                        agent.Xe_phanphoi = registerDto.CarDistribution;
                        agent.CanTuVanthem = registerDto.NeedConsultMore ? "Có" : "Không";
                        agent.lamsao_cokh = registerDto.IntroduceCustomer;
                        agent.KH_cuaban = registerDto.YourCustomer;
                        agent.Banbietchungtoitudau = registerDto.HowToKnowUs;
                        agent.Banla_TV = registerDto.IsUser ? "rồi" : "chưa";
                        agent.denghi_cungcapdonhan = registerDto.CreateOrders ? "cần" : "không cần";
                        agent.User = Get<User>(registerDto.UserId);
                        CreateOrUpdate(agent);
                    }
                }
                return result;
            });
        }
    }
}
