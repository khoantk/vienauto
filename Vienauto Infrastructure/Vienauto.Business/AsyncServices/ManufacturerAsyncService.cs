﻿using System.Threading.Tasks;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Interface.AsyncServices;

namespace Vienauto.Business.AsyncServices
{
    public class ManufacturerAsyncService : BaseService, IManufacturerAsyncService
    {
        public Task<DataServiceResult<IList<Manufacturer>>> GetAllManufacturerAsync()
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<Manufacturer>>
                {
                    Target = ListAll<Manufacturer>("asc", "Name")
                };
                return result;
            });
        }
    }
}
