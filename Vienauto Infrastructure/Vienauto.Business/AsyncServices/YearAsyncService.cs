﻿using NHibernate.Transform;
using System.Threading.Tasks;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Interface.AsyncServices;

namespace Vienauto.Business.AsyncServices
{
    public class YearAsyncService : BaseService, IYearAsyncService
    {
        public Task<DataServiceResult<IList<Year>>> GetYearByModelAsync(int modelId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<Year>>();

                Year yearAlias = null;
                Model modelAlias = null;

                using (var session = OpenDefaultSession())
                {
                    result.Target = Session.QueryOver<Year>(() => yearAlias)
                                           .JoinAlias(y => y.Model, () => modelAlias)
                                           .Where(m => modelAlias.Id == modelId)
                                           .SelectList(list => list
                                                .SelectGroup(y => yearAlias.Id).WithAlias(() => yearAlias.Id)
                                                .SelectGroup(y => yearAlias.Name).WithAlias(() => yearAlias.Name))
                                           .TransformUsing(Transformers.AliasToBean<Year>())
                                           .OrderBy(x => x.Name).Desc
                                           .List<Year>();
                }
                return result;
            });
        }
    }
}
