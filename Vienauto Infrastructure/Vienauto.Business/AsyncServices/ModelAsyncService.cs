﻿using System;
using System.Text;
using NHibernate.Transform;
using Vienauto.Entity.Enums;
using System.Threading.Tasks;
using Vienauto.Interface.Dto;
using Common.Core.Data.Paging;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Interface.AsyncServices;

namespace Vienauto.Business.AsyncServices
{
    public class ModelAsyncService : BaseService, IModelAsyncService
    {
        public Task<DataServiceResult<IList<Model>>> GetModelByManufacturerAsync(int manufacturerId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<Model>>();

                using (var session = OpenDefaultSession())
                {
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufacturerAlias = null;

                    result.Target = session.QueryOver<Model>(() => modelAlias)
                                  .JoinAlias(m => m.Style, () => styleAlias)
                                  .JoinAlias(s => styleAlias.Manufacturer, () => manufacturerAlias)
                                  .Where(mn => manufacturerAlias.Id == manufacturerId)
                                  .SelectList(list => list
                                        .Select(m => modelAlias.Id).WithAlias(() => modelAlias.Id)
                                        .Select(m => modelAlias.Name).WithAlias(() => modelAlias.Name)
                                        .Select(m => modelAlias.RewriteName).WithAlias(() => modelAlias.RewriteName))
                                  .TransformUsing(Transformers.AliasToBean<Model>())
                                  .OrderBy(x => x.Name).Asc
                                  .List<Model>();
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailReviewDto>> GetModelReviewDetailAsync(int manufactureId, int modelId, int yearId, int userId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailReviewDto>();

                using (var session = OpenDefaultSession())
                {
                    var modelQuery = session.CreateSQLQuery(@"EXEC hdt.sp_GetDetailModelReview 
                                                                       @ManufacturerId=:ManufacturerId, 
                                                                       @ModelId=:ModelId, 
                                                                       @YearId=:YearId")
                                            .SetInt32("ManufacturerId", manufactureId)
                                            .SetInt32("ModelId", modelId)
                                            .SetInt32("YearId", yearId);

                    result.Target = modelQuery.SetResultTransformer(Transformers.AliasToBean<ModelDetailReviewDto>())
                                              .UniqueResult<ModelDetailReviewDto>();
                }
                return result;
            });
        }

        public Task<PagingServiceResult<PagingResponse, List<ModelReviewsDto>>> GetModelsReviewsWithPagingAsync(PagingRequest<Dictionary<string, object>> pagingRequest)
        {
            return Task.Run(() =>
            {
                var result = new PagingServiceResult<PagingResponse, List<ModelReviewsDto>>();

                using (var session = OpenDefaultSession())
                {
                    var query = new StringBuilder();
                    query.Append(@"SELECT Id_Reviews as IdReviews,
                                          Brand, Model, Year,
                                          rmd.Id_Users as UserId,
                                          CASE WHEN rmd.Id_Users = 0 THEN 
                                               ISNULL(rmd.Name, 'Khách')
                                               ELSE u.FullName END as UserName,
                                          Point_Body as PointBody,
                                          Point_Safe as PointSafe,
                                          Point_Operation as PointOperation,
                                          Point_Price as PointPrice,
                                          Content, DatePost
                                    FROM hdt.Reviews_ModelDetail rmd
                                    LEFT JOIN hdt.Users u
                                    ON rmd.Id_Users = u.Id_Users");

                    if (pagingRequest.FilterConditions.Count > 0)
                    {
                        var brand = pagingRequest.FilterConditions["manufacture"].ToString();
                        if (brand != "")
                            query.Append(string.Format(" WHERE Brand = '{0}'", brand));
                        var model = pagingRequest.FilterConditions["model"].ToString();
                        if (model != "")
                            query.Append(string.Format(" AND Model = '{0}'", model));
                        var year = pagingRequest.FilterConditions["year"].ToString();
                        if (year != "")
                            query.Append(string.Format(" AND Year = '{0}'", year));
                    }

                    int totalPages = (int)Math.Ceiling(Convert.ToDouble(session.CreateSQLQuery(
                                                       GetTotalRows(query.ToString()))
                                                       .UniqueResult()) / (double)pagingRequest.PagingOption.PageSize);

                    query = query.Append(" ORDER BY Id_Reviews DESC ");
                    query.AppendFormat("OFFSET {0} * ({1} - 1) ROWS FETCH NEXT {0} ROWS ONLY;",
                                        pagingRequest.PagingOption.PageSize,
                                        pagingRequest.PagingOption.PageIndex);

                    var modelReviewsPaging = Session.CreateSQLQuery(query.ToString())
                                                .SetResultTransformer(Transformers.AliasToBean<ModelReviewsDto>())
                                                .List<ModelReviewsDto>();

                    result.Target = modelReviewsPaging as List<ModelReviewsDto>;
                    result.Paging = new PagingResponse(totalPages, pagingRequest.PagingOption);
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDto>> GetModelStandardInformationAsync(string manufacturerName, string modelName, string yearName, int carId, int userId = 0)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDto>();

                using (var session = OpenDefaultSession())
                {
                    var modelStandardQuery = session.CreateSQLQuery(
                                                @"EXEC hdt.sp_GetModelStandardInformation 
                                                            @manufacturerName=:Manufacturer, 
                                                            @modelName=:Model, 
                                                            @yearName=:Year,
                                                            @carId=:CarId,
                                                            @userId=:UserId")
                                                 .SetString("Manufacturer", manufacturerName)
                                                 .SetString("Model", modelName)
                                                 .SetString("Year", yearName)
                                                 .SetInt32("CarId", carId)
                                                 .SetInt32("UserId", userId);

                    result.Target = modelStandardQuery.SetResultTransformer(Transformers.AliasToBean<ModelDto>())
                                                      .UniqueResult<ModelDto>();

                    if (result.Target == null)
                        result.AddError(ErrorCode.UndefinedModel);
                }
                return result;
            });
        }

        public Task<DataServiceResult<IList<ModelDto>>> GetSameVersionCarsAsync(string manufacturerName, string modelName, string yearName)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<ModelDto>>();

                using (var session = OpenDefaultSession())
                {
                    Car carAlias = null;
                    Unit unitAlias = null;
                    Product productAlias = null;
                    Year yearAlias = null;
                    Style styleAlias = null;
                    Model modelAlias = null;
                    Avatar avatarModelAlias = null;
                    Manufacturer manufacturerAlias = null;
                    CarSpecification specificationAlias = null;
                    ModelDto modelDtoAlias = null;

                    result.Target = session.QueryOver<Manufacturer>(() => manufacturerAlias)
                                           .Left.JoinAlias(x => x.Styles, () => styleAlias)
                                           .Left.JoinAlias(() => styleAlias.Models, () => modelAlias)
                                           .Left.JoinAlias(() => modelAlias.Years, () => yearAlias)
                                           .Left.JoinAlias(() => yearAlias.Cars, () => carAlias)
                                           .Left.JoinAlias(() => carAlias.Products, () => productAlias)
                                           .Left.JoinAlias(() => carAlias.Avatar, () => avatarModelAlias)
                                           .Left.JoinAlias(() => productAlias.Unit, () => unitAlias)
                                           .Left.JoinAlias(() => carAlias.Specification, () => specificationAlias)
                                           .Where(m => manufacturerAlias.Name == manufacturerName &&
                                                       modelAlias.Name == modelName &&
                                                       yearAlias.Name == yearName)
                                           .SelectList(list => list
                                                   .Select(x => x.Id).WithAlias(() => modelDtoAlias.ManufacturerId)
                                                   .Select(() => modelAlias.Id).WithAlias(() => modelDtoAlias.ModelId)
                                                   .Select(() => yearAlias.Id).WithAlias(() => modelDtoAlias.YearId)
                                                   .Select(() => carAlias.Id).WithAlias(() => modelDtoAlias.CarId)
                                                   .Select(() => productAlias.Id).WithAlias(() => modelDtoAlias.ProductId)
                                                   .Select(() => productAlias.Price_Product).WithAlias(() => modelDtoAlias.Price)
                                                   .Select(() => unitAlias.Name_Unit).WithAlias(() => modelDtoAlias.UnitName)
                                                   .Select(() => avatarModelAlias.PathAvatar).WithAlias(() => modelDtoAlias.Avatar)
                                                   .Select(() => carAlias.Name).WithAlias(() => modelDtoAlias.CarName)
                                                   .Select(() => styleAlias.Name).WithAlias(() => modelDtoAlias.StyleName)
                                                   .Select(() => manufacturerAlias.Name).WithAlias(() => modelDtoAlias.ManufacturerName)
                                                   .Select(() => modelAlias.Name).WithAlias(() => modelDtoAlias.ModelName)
                                                   .Select(() => yearAlias.Name).WithAlias(() => modelDtoAlias.YearName)
                                                   .Select(() => specificationAlias.Fuel_Economy_City).WithAlias(() => modelDtoAlias.FuelEconomyCity)
                                                   .Select(() => specificationAlias.Fuel_Economy_Highway).WithAlias(() => modelDtoAlias.FuelEconomyHighway))
                                           .OrderBy(() => carAlias.Id).Desc
                                           .TransformUsing(Transformers.AliasToBean<ModelDto>())
                                           .List<ModelDto>();
                }
                return result;
            });
        }

        public Task<PagingServiceResult<PagingResponse, IList<ModelDto>>> GetSameStyleCarsWithModelPagingAsync(PagingRequest<Dictionary<string, object>> pagingRequest)
        {
            return Task.Run(() =>
            {
                var result = new PagingServiceResult<PagingResponse, IList<ModelDto>>();

                using (var session = OpenDefaultSession())
                {
                    if (pagingRequest.FilterConditions.Count > 0)
                    {
                        var manufacturerName = pagingRequest.FilterConditions["manufacturerName"].ToString();
                        var modelName = pagingRequest.FilterConditions["modelName"].ToString();
                        var yearName = pagingRequest.FilterConditions["yearName"].ToString();
                        var styleName = pagingRequest.FilterConditions["styleName"].ToString();

                        var sameStyleCarsQuery = session.CreateSQLQuery(
                                                 @"EXEC hdt.sp_GetModelSameStyles 
                                                         @manufacturerName=:Manufacturer, 
                                                         @modelName=:Model, 
                                                         @yearName=:Year,
                                                         @styleName=:Style,
                                                         @page=:PageIndex,
                                                         @size=:PageSize")
                                                        .SetString("Manufacturer", manufacturerName)
                                                        .SetString("Model", modelName)
                                                        .SetString("Year", yearName)
                                                        .SetString("Style", styleName)
                                                        .SetInt32("PageIndex", pagingRequest.PagingOption.PageIndex)
                                                        .SetInt32("PageSize", pagingRequest.PagingOption.PageSize);

                        result.Target = sameStyleCarsQuery.SetResultTransformer(Transformers.AliasToBean<ModelDto>())
                                                          .List<ModelDto>() as List<ModelDto>;

                        var totalPages = (result.Target[0]?.TotalPages).GetValueOrDefault();
                        result.Paging = new PagingResponse(totalPages, pagingRequest.PagingOption);
                    }
                }
                return result;
            });
        }

        public Task<DataServiceResult<bool>> CheckCanWriteReviewModelAsync(string manufacturer, string model, string year, int userId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<bool>();

                using (var session = OpenDefaultSession())
                {
                    var canReviewCount = session.QueryOver<ModelReview>()
                                                .Where(x => x.Brand == manufacturer &&
                                                       x.Model == model && x.Year == year &&
                                                       (x.IdUsers != 0 && x.IdUsers == userId))
                                                .RowCount();
                    if (canReviewCount > 0)
                        result.AddError(ErrorCode.ExistedReviewModel);
                    result.Target = true;
                }
                return result;
            });
        }

        public Task<DataServiceResult<bool>> AddModelReviewAsync(ModelReviewsDto modelReview)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<bool>();

                using (var session = OpenDefaultSession())
                {
                    var reviewOfModel = new ModelReview();
                    reviewOfModel.IdUsers = modelReview.UserId;
                    reviewOfModel.Brand = modelReview.Brand;
                    reviewOfModel.Model = modelReview.Model;
                    reviewOfModel.Year = modelReview.Year;
                    reviewOfModel.PointBody = modelReview.PointBody;
                    reviewOfModel.PointSafe = modelReview.PointSafe;
                    reviewOfModel.PointOperation = modelReview.PointOperation;
                    reviewOfModel.PointPrice = modelReview.PointPrice;
                    reviewOfModel.Title = modelReview.Title;
                    reviewOfModel.Content = modelReview.Content;
                    reviewOfModel.Name = modelReview.Name;
                    reviewOfModel.Email = modelReview.Email;
                    reviewOfModel.Phone = modelReview.Phone;
                    reviewOfModel.Datepost = modelReview.DatePost;
                    
                    Create(reviewOfModel);
                    CommitChanges();
                    result.Target = true;
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailOverviewDto>> GetOverviewAsync(int carId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailOverviewDto>();

                using (var session = OpenDefaultSession())
                {
                    Car carAlias = null;
                    Style styleAlias = null;
                    Model modelAlias = null;
                    Year yearAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailOverviewDto modelDto = null;

                    var modelDetailOverview = session.QueryOver<Car>(() => carAlias)
                        .Left.JoinAlias(x => x.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(() => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Introlduction).WithAlias(() => modelDto.Introduction)
                        )
                        .OrderBy(x => x.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailOverviewDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailOverviewDto>();

                    result.Target = modelDetailOverview;
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailEngineDto>> GetEngineAsync(int carId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailEngineDto>();

                using (var session = OpenDefaultSession())
                {
                    CarSpecification specificationAlias = null;
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailEngineDto modelDto = null;

                    var modelDetailEngine = session.QueryOver<CarSpecification>(() => specificationAlias)
                        .Left.JoinAlias(() => specificationAlias.Car, () => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(p => p.Engine).WithAlias(() => modelDto.EngineName)
                            .Select(p => p.Type_Engine).WithAlias(() => modelDto.EngineType)
                            .Select(p => p.Compresstion).WithAlias(() => modelDto.CompressionRatio)
                            .Select(p => p.Driving_Type).WithAlias(() => modelDto.DrivingType)
                            .Select(p => p.Transmission_Type).WithAlias(() => modelDto.TranmissionType)
                            .Select(p => p.Fuel_Type).WithAlias(() => modelDto.FuelType)
                            .Select(p => p.Fuel_Economy_City).WithAlias(() => modelDto.FuelEconomyCity)
                            .Select(p => p.Fuel_Economy_Highway).WithAlias(() => modelDto.FuelEconomyHighway)
                            .Select(p => p.Locking).WithAlias(() => modelDto.Locking)
                            .Select(p => p.Engine_Valvetrain).WithAlias(() => modelDto.EngineValvetrain)
                            .Select(p => p.ESS).WithAlias(() => modelDto.ESS)
                            .Select(p => p.EBD).WithAlias(() => modelDto.EBD)
                            .Select(p => p.Remote_Vehice).WithAlias(() => modelDto.RemoteVehicle)
                            .Select(p => p.Transmission).WithAlias(() => modelDto.Tranmission)
                            .Select(p => p.Exterior_Length).WithAlias(() => modelDto.ExteriorLength)
                            .Select(p => p.Exterior_Width).WithAlias(() => modelDto.ExteriorWidth)
                            .Select(p => p.Exterior_Height).WithAlias(() => modelDto.ExteriorHeight)
                            .Select(p => p.Horsepower).WithAlias(() => modelDto.HorsePower)
                            .Select(p => p.Torque).WithAlias(() => modelDto.Torque)
                            .Select(p => p.Drag_Coeficient).WithAlias(() => modelDto.DragCoeficient)
                            .Select(p => p.TimeSpeed).WithAlias(() => modelDto.TimeSpeed)
                            .Select(p => p.Km).WithAlias(() => modelDto.Km)
                            .Select(p => p.Curb_Weight).WithAlias(() => modelDto.CurbWeight)
                            .Select(p => p.GVWR).WithAlias(() => modelDto.GVWR)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailEngineDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailEngineDto>();

                    result.Target = modelDetailEngine;
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailSafetyDto>> GetSafetyAsync(int carId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailSafetyDto>();

                using (var session = OpenDefaultSession())
                {
                    CarSafety safetyAlias = null;
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailSafetyDto modelDto = null;

                    var modelDetailSafety = session.QueryOver<CarSafety>(() => safetyAlias)
                        .Left.JoinAlias(() => safetyAlias.Car, () => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Airbags_frontal).WithAlias(() => modelDto.FrontAirbag)
                            .Select(x => x.Airbags_Impact).WithAlias(() => modelDto.SideAirbag)
                            .Select(x => x.Airbag_Curtain).WithAlias(() => modelDto.BackAirbag)
                            .Select(x => x.HeadLights).WithAlias(() => modelDto.HeadLights)
                            .Select(x => x.Exterior_Light_Control).WithAlias(() => modelDto.ExteriorLightControl)
                            .Select(x => x.Daytime_Running_Lights).WithAlias(() => modelDto.DayLights)
                            .Select(x => x.Led_Taillights).WithAlias(() => modelDto.LedTailLights)
                            .Select(x => x.Parking_Assist).WithAlias(() => modelDto.ParkingAssist)
                            .Select(x => x.Alarm_System).WithAlias(() => modelDto.AlarmSystem)
                            .Select(x => x.Door_Locks_System).WithAlias(() => modelDto.DoorLockSystem)
                            .Select(x => x.Rear_Child).WithAlias(() => modelDto.ChildLock)
                            .Select(x => x.Content_Theft).WithAlias(() => modelDto.AntiTheftSystem)
                            .Select(x => x.Low_Tire).WithAlias(() => modelDto.LowPressureWarning)
                            .Select(x => x.Brakes).WithAlias(() => modelDto.Brakes)
                            .Select(x => x.ABS).WithAlias(() => modelDto.ABSBrakes)
                            .Select(x => x.Brake_Assist).WithAlias(() => modelDto.BrakeAssist)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailSafetyDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailSafetyDto>();

                    result.Target = modelDetailSafety;
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailEntertainmentDto>> GetEntertainmentAsync(int carId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailEntertainmentDto>();

                using (var session = OpenDefaultSession())
                {
                    CarEntertainment safetyAlias = null;
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailEntertainmentDto modelDto = null;

                    var modelDetailEntertainment = session.QueryOver<CarEntertainment>(() => safetyAlias)
                        .Left.JoinAlias(() => safetyAlias.Car, () => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Radio).WithAlias(() => modelDto.Radio)
                            .Select(x => x.CD_Player).WithAlias(() => modelDto.CDPlayer)
                            .Select(x => x.DVD_Audio).WithAlias(() => modelDto.DVDAudio)
                            .Select(x => x.Voice_Recognition).WithAlias(() => modelDto.SpeechRecognition)
                            .Select(x => x.Speakers).WithAlias(() => modelDto.Speakers)
                            .Select(x => x.Amplifier).WithAlias(() => modelDto.Amplifier)
                            .Select(x => x.bluetooth_Compatibility).WithAlias(() => modelDto.BluetoothCompatibility)
                            .Select(x => x.Wifi_Compatibility).WithAlias(() => modelDto.WifiCompatibility)
                            .Select(x => x.ThreeG_Compatibility).WithAlias(() => modelDto.ThreeGCompatibility)
                            .Select(x => x.GPS_Compability).WithAlias(() => modelDto.GPSCompatibility)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailEntertainmentDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailEntertainmentDto>();

                    result.Target = modelDetailEntertainment;
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailInteriorDto>> GetInteriorAsync(int carId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailInteriorDto>();

                using (var session = OpenDefaultSession())
                {
                    CarInterior interiorAlias = null;
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailInteriorDto modelDto = null;

                    var modelDetailInterior = session.QueryOver<CarInterior>(() => interiorAlias)
                        .Left.JoinAlias(() => interiorAlias.Car, () => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Air_Conditioning_Front).WithAlias(() => modelDto.FrontAirConditioning)
                            .Select(x => x.Air_Conditioning_Rear).WithAlias(() => modelDto.BackAirConditioning)
                            .Select(x => x.Air_Filter).WithAlias(() => modelDto.AirFilter)
                            .Select(x => x.Seating).WithAlias(() => modelDto.Seating)
                            .Select(x => x.Front_Seat_Type).WithAlias(() => modelDto.FrontSeatType)
                            .Select(x => x.Front_Head_Restraints).WithAlias(() => modelDto.Shampoo)
                            .Select(x => x.Compass).WithAlias(() => modelDto.Compass)
                            .Select(x => x.Clock).WithAlias(() => modelDto.Clocks)
                            .Select(x => x.DC_Power_outlet).WithAlias(() => modelDto.DCPowerOutlet)
                            .Select(x => x.Tachometer).WithAlias(() => modelDto.Tachometer)
                            .Select(x => x.Water_Temp_Gauge).WithAlias(() => modelDto.WaterTempGauges)
                            .Select(x => x.Low_Fuel_Warning).WithAlias(() => modelDto.LowFuelWarning)
                            .Select(x => x.Exterior_Temperature).WithAlias(() => modelDto.DisplayOutsideTemp)
                            .Select(() => carAlias.Html_Interior).WithAlias(() => modelDto.FlashInterior)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailInteriorDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailInteriorDto>();

                    result.Target = modelDetailInterior;
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailExteriorDto>> GetExteriorAsync(int carId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailExteriorDto>();

                using (var session = OpenDefaultSession())
                {
                    CarExterior exteriorAlias = null;
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailExteriorDto modelDto = null;

                    var modelDetailExterior = session.QueryOver<CarExterior>(() => exteriorAlias)
                        .Left.JoinAlias(() => exteriorAlias.Car, () => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Front_Tires).WithAlias(() => modelDto.FrontTires)
                            .Select(x => x.Front_rear).WithAlias(() => modelDto.BackTires)
                            .Select(x => x.Wheels).WithAlias(() => modelDto.Wheels)
                            .Select(x => x.Sunroof).WithAlias(() => modelDto.SunRoof)
                            .Select(x => x.Rear_Window_Type).WithAlias(() => modelDto.BackWindowType)
                            .Select(x => x.DoorCount).WithAlias(() => modelDto.NumberOfDoors)
                            .Select(x => x.Mirrows).WithAlias(() => modelDto.RearviewMirror)
                            .Select(x => x.Heated_Door_Mirrows).WithAlias(() => modelDto.HeatedDoorMirror)
                            .Select(x => x.Windshied_Wipes).WithAlias(() => modelDto.WindshieldWipes)
                            .Select(x => x.System).WithAlias(() => modelDto.LiftingSystem)
                            .Select(() => carAlias.Html_Exterior).WithAlias(() => modelDto.FlashExterior)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailExteriorDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailExteriorDto>();

                    result.Target = modelDetailExterior;
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailColorDto>> GetColorAsync(int carId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailColorDto>();

                using (var session = OpenDefaultSession())
                {
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailColorDto modelDto = null;

                    var modelDetailColor = session.QueryOver<Car>(() => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Html_Color).WithAlias(() => modelDto.Color)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailColorDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailColorDto>();

                    result.Target = modelDetailColor;
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailImageDto>> GetImageAsync(int carId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailImageDto>();

                using (var session = OpenDefaultSession())
                {
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailImageDto modelDto = null;

                    var modelDetailImage = session.QueryOver<Car>(() => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.PathImages).WithAlias(() => modelDto.PathImages)
                            .Select(() => manufactureAlias.RewriteName).WithAlias(() => modelDto.ManufacturerName)
                            .Select(() => modelAlias.RewriteName).WithAlias(() => modelDto.ModelName)
                            .Select(() => yearAlias.Name).WithAlias(() => modelDto.YearName)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailImageDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailImageDto>();

                    result.Target = modelDetailImage;
                }
                return result;
            });
        }

        private string GetTotalRows(string selectQuery)
        {
            return string.Concat("SELECT COUNT(*) FROM (", selectQuery, ") AS TotalRow");
        }
    }
}
