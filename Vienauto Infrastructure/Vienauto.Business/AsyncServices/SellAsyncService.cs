﻿using System;
using NHibernate;
using NHibernate.Type;
using System.Reflection;
using NHibernate.Criterion;
using NHibernate.Transform;
using System.Threading.Tasks;
using Vienauto.Interface.Dto;
using Common.Core.Data.Paging;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Interface.AsyncServices;

namespace Vienauto.Business.AsyncServices
{
    /// <summary>
    /// Car For Sell List Functions
    /// </summary>
    public partial class SellAsyncService : BaseService, ISellAsyncService
    {
        public Task<PagingServiceResult<PagingResponse, List<CarForSellDetailDto>>> GetCarsWithPagingAsync(PagingRequest<Dictionary<string, object>> pagingRequest)
        {
            return Task.Run(() =>
            {
                var carInfo = new CarForSellDto();
                var result = new PagingServiceResult<PagingResponse, List<CarForSellDetailDto>>();

                var pagingOption = pagingRequest.PagingOption;
                var filterConditions = pagingRequest.FilterConditions;

                if (filterConditions.Count > 0)
                {
                    using (var session = OpenDefaultSession())
                    {
                        var carProjections = CreateFinalProjectionMembers();
                        var carFilters = CreateFinalFiltersForPaging(filterConditions);
                        carInfo = BuildCarsWithPaging(session, carFilters, carProjections,
                                                      pagingRequest.PagingOption.PageIndex,
                                                      pagingRequest.PagingOption.PageSize,
                                                      pagingRequest.PagingOption.NhibernateOrder);
                    }
                }

                result.Target = carInfo.CarDetails;
                result.Paging = new PagingResponse(carInfo.TotalRows, pagingRequest.PagingOption);
                return result;
            });
        }

        private CarForSellDto BuildCarsWithPaging(ISession session, Conjunction filters, ProjectionList projections, int pageIndex, int pageSize, Order order)
        {
            var carForSell = new CarForSellDto();
            var carsCriteria = session.CreateCriteria<Product>("product")
                                      .CreateAlias("product.User", "user").SetFetchMode("User", FetchMode.Join)
                                      .CreateAlias("product.Unit", "unit").SetFetchMode("Unit", FetchMode.Join)
                                      .CreateAlias("product.Car", "car").SetFetchMode("Unit", FetchMode.Join)
                                      .CreateAlias("product.Location", "location").SetFetchMode("location", FetchMode.Join)
                                      .CreateAlias("car.Avatar", "avatar").SetFetchMode("avatar", FetchMode.Join)
                                      .CreateAlias("car.Interior", "interior").SetFetchMode("interior", FetchMode.Join)
                                      .CreateAlias("car.Specification", "specification").SetFetchMode("specification", FetchMode.Join)
                                      .CreateAlias("car.Year", "year").SetFetchMode("year", FetchMode.Join)
                                      .CreateAlias("year.Model", "model").SetFetchMode("model", FetchMode.Join)
                                      .CreateAlias("model.Style", "style").SetFetchMode("style", FetchMode.Join)
                                      .CreateAlias("style.Manufacturer", "manufacturer").SetFetchMode("manufacturer", FetchMode.Join)
                                      .Add(filters);

            carForSell.TotalRows = carsCriteria.List().Count > 0 ? carsCriteria.List().Count : 0;
            carForSell.CarDetails = carsCriteria.SetFirstResult((pageIndex-1) * pageSize)
                                                .SetMaxResults(pageSize)
                                                .SetProjection(projections)
                                                .AddOrder(order)
                                                .SetResultTransformer(Transformers.AliasToBean<CarForSellDetailDto>())
                                                .List<CarForSellDetailDto>() as List<CarForSellDetailDto>;
            return carForSell;
        }

        private ProjectionList CreateProjectionMembersForPaging()
        {
            var projections = Projections.ProjectionList()
                                         .Add(Projections.Property("car.Id"), "CarId")
                                         .Add(Projections.Property("car.Name"), "CarName")
                                         .Add(Projections.Property("product.Price_Product"), "Price")
                                         .Add(Projections.Property("product.DatePost"), "DatePost");
            return projections;
        }

        private Conjunction CreateFinalFiltersForPaging(Dictionary<string, object> filterConditions)
        {
            var finalFilters = CreateDefaultFilter();
            if (filterConditions.ContainsKey("Type"))
                finalFilters.Add(Restrictions.Eq("product.New_Old", filterConditions["Type"].ToString()));

            AddFilters(filterConditions, finalFilters);

            return finalFilters;
        }

        private Conjunction CreateDefaultFilter()
        {
            var defaultCondition = Restrictions.Conjunction();
            defaultCondition.Add(Restrictions.Eq("product.Status", 1));
            //defaultCondition.Add(Expression.Sql("DateDiff(day,product.DatePost,getdate()) < 60"));
            return defaultCondition;
        }

        private Conjunction CreatePriceFilter(float[] prices)
        {
            var exchangeRate = 21800;
            var fromPrice = prices[0];
            var toPrice = prices[1];
            
            var VNDPriceCondition = Restrictions.Conjunction();
            VNDPriceCondition.Add(Restrictions.Eq("unit.Id", 1));
            VNDPriceCondition.Add(Restrictions.Between("product.Price_Product", (double)(fromPrice * exchangeRate), (double)(toPrice * exchangeRate)));

            var USDPriceCondition = Restrictions.Conjunction();
            USDPriceCondition.Add(Restrictions.Eq("unit.Id", 2));
            USDPriceCondition.Add(Restrictions.Between("product.Price_Product", (double)fromPrice, (double)toPrice));

            var priceCombinationCondition = Restrictions.Disjunction();
            priceCombinationCondition.Add(VNDPriceCondition);
            priceCombinationCondition.Add(USDPriceCondition);

            var priceCondition = Restrictions.Conjunction();
            priceCondition.Add(priceCombinationCondition);

            return priceCondition;
        }

        private Conjunction CreateManufacturerFilter(int[] manufacturerId)
        {
            var manufacturerCondition = Restrictions.Conjunction();
            manufacturerCondition.Add(Restrictions.In("manufacturer.Id", manufacturerId));
            return manufacturerCondition;
        }

        private Conjunction CreateStyleFilter(int[] styleId)
        {
            var styleCondition = Restrictions.Conjunction();
            styleCondition.Add(Restrictions.In("style.Id", styleId));
            return styleCondition;
        }

        private Conjunction CreateSeatFilter(int seats)
        {
            var seatCondition = Restrictions.Conjunction();
            seatCondition.Add(Restrictions.Eq("interior.Seating", seats));
            return seatCondition;
        }

        private Conjunction CreateYearFilter(string[] yearName)
        {
            var yearCondition = Restrictions.Conjunction();
            yearCondition.Add(Restrictions.Eq("year.Name", yearName));
            return yearCondition;
        }

        private Conjunction CreateLocationFilter(int[] locationId)
        {
            var locationCondition = Restrictions.Conjunction();
            locationCondition.Add(Restrictions.Eq("location.Id", locationId));
            return locationCondition;
        }

        private void AddFilters(Dictionary<string, object> filterConditions, Conjunction finalFilters)
        {
            var type = typeof(SellAsyncService);
            var methodsInfo = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (KeyValuePair<string, object> filter in filterConditions)
            {
                object parameter = null;
                var filterKey = filter.Key;

                if (filterKey == "Manufacturer" || filterKey == "Style" || filterKey == "Location")
                    parameter = filterConditions[filterKey] as int[];
                else if (filterKey == "Price")
                    parameter = filterConditions[filterKey] as float[];
                else if (filterKey == "Year")
                    parameter = filterConditions[filterKey] as string[];
                else if (filterKey == "Seat")
                    parameter = filterConditions[filterKey] as int?;

                Array.ForEach(methodsInfo, (method) =>
                {
                    if (method.Name == $"Create{filterKey}Filter")
                    {
                        var result = method.Invoke(new SellAsyncService(), new object[] { parameter }) as Conjunction;
                        finalFilters.Add(result);
                    }
                });
            }
        }
    }
}
