﻿using System;
using System.Text;
using NHibernate.Transform;
using Vienauto.Entity.Enums;
using Vienauto.Interface.Dto;
using System.Threading.Tasks;
using Common.Core.Data.Paging;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Interface.AsyncServices;

namespace Vienauto.Business.AsyncServices
{
    public class AgencyAsyncService : BaseService, IAgencyAsyncService
    {
        public Task<DataServiceResult<AgencyDetailDto>> GetAgencyDetailAsync(int agencyUserId, int manufacturerId, int agentId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<AgencyDetailDto>();
                using (var session = OpenDefaultSession())
                {
                    var query = new StringBuilder();
                    query = query.AppendFormat(@"SELECT d.Id_user AS UserId,
                                                        d.Id_dangki AS AgencyId,
                                                        t.Id_TinhThanh AS CountryId,
                                                        d.Ten_CTY AS CompanyName,
                                                        d.Diachi_giaodich AS [Address],
                                                        t.Name_TinhThanh AS ProvinceName,
                                                        u.Phone AS NewPhone,
                                                        u.Mobile AS OldPhone,
                                                        u.Fax AS [Service],
                                                        u.ToaDoMap AS Coordinates,
                                                        h.Id_Hang_PhanPhoi DealerShipId,
                                                        m.logo AS ManufacturerAvatar,
                                                        m.Id_Manufacturer AS ManufacturerId,
                                                        m.Name_Manufacturer AS ManufacturerName,
                                                        u.ZoomMap AS ZoomMap,
                                                        FromSales,
                                                        ToSales,
                                                        FromServices,
                                                        ToServices,
                                                        DisplaySales,
                                                        DisplayServices
                                                        FROM hdt.Dangki_daili_online d
                                                        LEFT JOIN hdt.Users u ON u.Id_Users = d.Id_user
                                                        LEFT JOIN hdt.AgentTime a ON u.Id_Users = a.IdUser
                                                        LEFT JOIN hdt.Hang_Phanphoi h ON h.Id_Daily = u.Id_Users
                                                        LEFT JOIN hdt.TinhThanh t ON u.TinhThanh = t.Id_TinhThanh
                                                        LEFT JOIN hdt.Manufacturer m ON m.Id_Manufacturer = h.Id_Manufacturer
                                                        WHERE d.Id_user = {0} AND m.Id_Manufacturer = {1}",
                                                    agencyUserId, manufacturerId);

                    var detailAgency = session.CreateSQLQuery(query.ToString())
                                        .SetResultTransformer(Transformers.AliasToBean<AgencyDetailDto>())
                                        .UniqueResult<AgencyDetailDto>();

                    if (detailAgency != null)
                    {
                        var detailReview = session.CreateSQLQuery(@"SELECT MAX(Overall) AS MaxPointReview, 
                                                                COUNT(Id_Reviews) AS TotalReview
                                                                FROM hdt.Reviews_Agent WHERE Id_Agent = " + agentId)
                                                  .SetResultTransformer(Transformers.AliasToBean<AgencyDetailDto>())
                                                  .UniqueResult<AgencyDetailDto>();

                        detailAgency.MaxPointReview = detailReview.MaxPointReview;
                        detailAgency.TotalReview = detailReview.TotalReview;
                    }
                    else
                        result.AddError(ErrorCode.UndefinedAgencyDetail);
                    result.Target = detailAgency;
                }
                return result;
            });
        }

        public Task<DataServiceResult<AgencyDetailReviewDto>> GetAgencyReviewDetailAsync(int agentId, int manufacturerId, int userId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<AgencyDetailReviewDto>();

                using (var session = OpenDefaultSession())
                {
                    var agencyQuery = session.CreateSQLQuery(@"EXEC hdt.sp_GetDetailAgencyReview 
                                                                        @AgentId=:AgentId, 
                                                                        @ManufacturerId=:ManufacturerId")
                                             .SetInt32("AgentId", agentId)
                                             //.SetInt32("ManufacturerId", manufacturerId)
                                             .SetString("ManufacturerId", "a");

                    result.Target = agencyQuery.SetResultTransformer(Transformers.AliasToBean<AgencyDetailReviewDto>())
                                                .UniqueResult<AgencyDetailReviewDto>();
                }
                return result;
            });
        }

        public Task<DataServiceResult<IList<DealerShipDto>>> GetAllDealerShipsAsync()
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<DealerShipDto>>();

                DealerShipDto dto = null;
                DealerShip dealerShipAlias = null;
                Manufacturer manufactureAlias = null;

                using (var session = OpenDefaultSession())
                {
                    result.Target = session.QueryOver<DealerShip>(() => dealerShipAlias)
                                           .JoinAlias(ds => ds.Manufacturer, () => manufactureAlias)
                                           .SelectList(list => list
                                                 .SelectGroup(pc => manufactureAlias.Id).WithAlias(() => dto.ManufacturerId)
                                                 .SelectGroup(pc => manufactureAlias.Name).WithAlias(() => dto.ManufacturerName))
                                           .OrderBy(() => manufactureAlias.Name).Asc
                                           .TransformUsing(Transformers.AliasToBean<DealerShipDto>())
                                           .List<DealerShipDto>();
                }
                return result;
            });
        }

        public Task<DataServiceResult<IList<AgencyDto>>> GetAgencyAsync(int manufacturerId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<AgencyDto>>();
                try
                {
                    AgencyDto dto = null;
                    User userAlias = null;
                    Agency agencyAlias = null;
                    Province provinceAlias = null;
                    DealerShip dealerShipAlias = null;

                    using (var session = OpenDefaultSession())
                    {
                        result.Target = Session.QueryOver<DealerShip>(() => dealerShipAlias)
                                               .JoinAlias(ds => ds.User, () => userAlias)
                                               .JoinAlias(a => dealerShipAlias.User, () => agencyAlias)
                                               .Left.JoinAlias(p => userAlias.Province, () => provinceAlias)
                                               .Where(d => d.Manufacturer.Id == manufacturerId)
                                               .SelectList(list => list
                                                   .SelectGroup(pc => userAlias.Id).WithAlias(() => dto.AgencyId)
                                                   .SelectGroup(pc => userAlias.FullName).WithAlias(() => dto.FullName))
                                               .TransformUsing(Transformers.AliasToBean<AgencyDto>())
                                               .List<AgencyDto>();
                    }
                }
                catch (Exception ex)
                {
                    result.AddError(ErrorCode.FailToListAllDealerShip, ex);
                }
                return result;
            });
        }

        public Task<DataServiceResult<IList<AgencyDto>>> GetAgencyByDealerShipAsync(int manufacturerId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<AgencyDto>>();

                AgencyDto dto = null;
                User userAlias = null;
                DealerShip dealerShipAlias = null;

                using (var session = OpenDefaultSession())
                {
                    result.Target = Session.QueryOver<DealerShip>(() => dealerShipAlias)
                                           .JoinAlias(ds => ds.User, () => userAlias)
                                           .Where(d => d.Manufacturer.Id == manufacturerId)
                                           .SelectList(list => list
                                                .SelectGroup(pc => userAlias.Id).WithAlias(() => dto.AgencyId)
                                                .SelectGroup(pc => userAlias.FullName).WithAlias(() => dto.FullName))
                                           .TransformUsing(Transformers.AliasToBean<AgencyDto>())
                                           .List<AgencyDto>();
                }
                return result;
            });
        }

        public Task<PagingServiceResult<PagingResponse, List<AgencyDto>>> GetAgenciesWithPagingAsync(PagingRequest<Dictionary<string, object>> pagingRequest)
        {
            return Task.Run(() =>
            {
                var result = new PagingServiceResult<PagingResponse, List<AgencyDto>>();
                using (var session = OpenDefaultSession())
                {
                    var province = "";
                    var dealerShip = "";
                    var sortOrder = "";

                    if (pagingRequest.FilterConditions.Count > 0)
                    {
                        province = pagingRequest.FilterConditions["provinceId"].ToString();
                        dealerShip = pagingRequest.FilterConditions["dealershipId"].ToString();

                        if (pagingRequest.PagingOption.SortName == "Agency")
                            sortOrder = " ORDER BY CompanyName ";
                        else if (pagingRequest.PagingOption.SortName == "Rating")
                            sortOrder = " ORDER BY MaxPointReview DESC ";
                        else if (pagingRequest.PagingOption.SortName == "Province")
                            sortOrder = " ORDER BY ProvinceName, CompanyName ";
                    }

                    var agenciesQuery = $@"EXEC hdt.sp_GetAgencies 
                                                    '{dealerShip}',
                                                    '{province}',
                                                    '{sortOrder}',
                                                     {pagingRequest.PagingOption.PageIndex},
                                                     {pagingRequest.PagingOption.PageSize}";

                    var agenciesResult = Session.CreateSQLQuery(agenciesQuery)
                                            .SetResultTransformer(Transformers.AliasToBean<AgencyDto>())
                                            .List<AgencyDto>();

                    result.Target = agenciesResult as List<AgencyDto>;

                    var totalPages = result.Target.Count > 0 ? 
                                    (result.Target[0]?.TotalPages).GetValueOrDefault() : 0;
                    result.Paging = new PagingResponse(totalPages, pagingRequest.PagingOption);
                }
                return result;
            });
        }

        public Task<PagingServiceResult<PagingResponse, List<AgencyReviewsDto>>> GetAgenciesReviewsWithPagingAsync(PagingRequest<Dictionary<string, object>> pagingRequest)
        {
            return Task.Run(() =>
            {
                var result = new PagingServiceResult<PagingResponse, List<AgencyReviewsDto>>();
                using (var session = OpenDefaultSession())
                {
                    var query = new StringBuilder(20);
                    query = query.Append(GetSelectQueryAgenciesReviewsWithPaging());

                    if (pagingRequest.FilterConditions.Count > 0)
                    {
                        var dealerShip = pagingRequest.FilterConditions["agentId"].ToString();
                        if (dealerShip != "")
                            query.Append(" WHERE a.Id_Agent = " + dealerShip);
                    }

                    int totalPages = (int)Math.Ceiling(Convert.ToDouble(Session.CreateSQLQuery(
                                                       GetTotalRows(query.ToString()))
                                                       .UniqueResult()) / (double)pagingRequest.PagingOption.PageSize);

                    query = query.Append(" ORDER BY Id_Reviews DESC ");

                    query.AppendFormat("OFFSET {0} * ({1} - 1) ROWS FETCH NEXT {0} ROWS ONLY",
                                      pagingRequest.PagingOption.PageSize, pagingRequest.PagingOption.PageIndex);

                    var agentReviewsPaging = Session.CreateSQLQuery(query.ToString())
                                                .SetResultTransformer(Transformers.AliasToBean<AgencyReviewsDto>())
                                                .List<AgencyReviewsDto>();

                    result.Target = (List<AgencyReviewsDto>)agentReviewsPaging;
                    result.Paging = new PagingResponse(totalPages, pagingRequest.PagingOption);
                    return result;
                }
            });
        }

        public Task<DataServiceResult<bool>> CheckCanWriteReviewAgencyAsync(int agentId, int userId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<bool>();
                using (var session = OpenDefaultSession())
                {
                    var canReviewCount = session.QueryOver<AgencyReview>()
                                                .Where(x => x.Agency.Id == agentId && x.User.Id == userId)
                                                .RowCount();
                    if (canReviewCount > 0)
                        result.AddError(ErrorCode.ExistedReviewAgency);
                    result.Target = true;
                }
                return result;
            });
        }

        public Task<DataServiceResult<bool>> AddAgencyReviewAsync(AgencyDetailReviewDto agencyReview)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<bool>();
                using (var session = OpenDefaultSession())
                {
                    var reviewOfAgency = new AgencyReview();
                    reviewOfAgency.Agency = Get<Agency>(agencyReview.AgentId);
                    reviewOfAgency.User = Get<User>(agencyReview.UserId);
                    reviewOfAgency.Customer_Service = agencyReview.CustomerService;
                    reviewOfAgency.Buying_Process = agencyReview.BuyingProcess;
                    reviewOfAgency.Quality_of_Repair = agencyReview.QualityOfRepair;
                    reviewOfAgency.Facilities = agencyReview.Facilities;
                    reviewOfAgency.Title_Review = agencyReview.TitleReview;
                    reviewOfAgency.Content_Review = agencyReview.ContentReview;
                    reviewOfAgency.Name = agencyReview.Name;
                    reviewOfAgency.Email = agencyReview.Email;
                    reviewOfAgency.Datetime_Review = DateTime.Now;
                    reviewOfAgency.Overall = GetRelativeRatingPoint(
                                               (reviewOfAgency.Customer_Service +
                                               reviewOfAgency.Buying_Process +
                                               reviewOfAgency.Quality_of_Repair +
                                               reviewOfAgency.Facilities) / 4.0);
                    Create(reviewOfAgency);
                    CommitChanges();
                    result.Target = true;
                }
                return result;
            });
        }

        #region Private method

        private double GetRelativeRatingPoint(double ratingPoint)
        {
            if (ratingPoint > 0.5 && ratingPoint < 1)
                return 1;
            else if (ratingPoint > 1.5 && ratingPoint < 2)
                return 2;
            else if (ratingPoint > 2.5 && ratingPoint < 3)
                return 3;
            else if (ratingPoint > 3.5 && ratingPoint < 4)
                return 4;
            else if (ratingPoint > 4.5 && ratingPoint < 5)
                return 5;
            return ratingPoint;
        }

        private string GetSelectQueryAgenciesReviewsWithPaging()
        {
            return @"SELECT Id_Reviews as ReviewId,
                            Customer_Service as CustomerService,
                            Buying_Process as BuyingProcess,
                            Quality_of_Repair as QualityRepair,
                            Facilities as Facilities,
                            Content_Review as Content,
                            Datetime_Review as DatePost,
                            Overall,
                            Id_User as UserId,
                            FullName as UserName,
                            ReviewOfAgency.MaxPointReview
                    FROM hdt.Reviews_Agent a                                            
                    LEFT JOIN hdt.Users c ON a.Id_User = c.Id_Users
                    LEFT JOIN (SELECT Id_Agent, MAX(Overall) MaxPointReview
                    FROM hdt.Reviews_Agent GROUP BY Id_Agent) ReviewOfAgency
                    ON ReviewOfAgency.Id_Agent = a.Id_Agent";
        }

        private string GetTotalRows(string selectQuery)
        {
            return string.Concat("SELECT COUNT(*) FROM (", selectQuery, ") AS TotalRow");
        }

        #endregion
    }
}
