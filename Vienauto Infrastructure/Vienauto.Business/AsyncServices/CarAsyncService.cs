﻿using System;
using NHibernate;
using NHibernate.Transform;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using Vienauto.Interface.Dto;
using System.Threading.Tasks;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Interface.AsyncServices;

namespace Vienauto.Business.AsyncServices
{
    public class CarAsyncService : BaseService, ICarAsyncService
    {
        public Task<DataServiceResult<int>> BookingAsync(CarBookingDto carBookingDto)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<int>();
                using (var session = OpenDefaultSession())
                {
                    var carBooking = new CarBooking();
                    carBooking.Car = Get<Car>(carBookingDto.CarId);
                    carBooking.Xe = carBooking.Car?.Name;
                    carBooking.User = Get<User>(carBookingDto.UserId);
                    carBooking.Follower = Get<User>(carBookingDto.FollowerId);
                    carBooking.Location = Get<Location>(carBookingDto.LocationId);
                    carBooking.Manufacturer = Get<Manufacturer>(carBookingDto.ManufacturerId);
                    var carBookingEntity = carBookingDto.FromDtoToEntity(carBooking);
                    result.Target = Create(carBookingEntity);
                    CommitChanges();
                }
                return result;
            });
        }

        public Task<DataServiceResult<Car>> GetCarByIdAsync(int carId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<Car>();
                using (var session = OpenDefaultSession())
                {
                    result.Target = session.Get<Car>(carId);
                }
                return result;
            });
        }

        public Task<DataServiceResult<IList<CarDto>>> GetCarByYearAsync(int yearId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<CarDto>>();
                using (var session = OpenDefaultSession())
                {
                    CarDto carDto = null;
                    result.Target = Session.QueryOver<Car>()
                                           .Where(c => c.Year.Id == yearId)
                                           .SelectList(list => list
                                               .SelectGroup(c => c.Id).WithAlias(() => carDto.Id)
                                               .SelectGroup(c => c.Name).WithAlias(() => carDto.Name))
                                           .TransformUsing(Transformers.AliasToBean<CarDto>())
                                           .List<CarDto>();
                }
                return result;
            });
        }

        public Task<DataServiceResult<string>> GetAvatarByCarIdAsync(int carId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<string>();
                using (var session = OpenDefaultSession())
                {
                    var avatarResult = Session.CreateCriteria<Avatar>("avatar")
                                              .CreateAlias("avatar.Car", "car", JoinType.LeftOuterJoin).SetFetchMode("car", FetchMode.Join)
                                              .Add(Restrictions.Eq("car.Id", carId))
                                              .UniqueResult();

                    result.Target = (avatarResult != null ? (Avatar)avatarResult : null)?.PathAvatar;
                }
                return result;
            });
        }
    }
}
