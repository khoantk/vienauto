﻿using System;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using System.Threading.Tasks;
using Vienauto.Interface.Dto;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using Vienauto.Interface.AsyncServices;

namespace Vienauto.Business.AsyncServices
{
    /// <summary>
    /// Car For Sell Tab Detail Functions
    /// </summary>
    public partial class SellAsyncService : BaseService, ISellAsyncService
    {
        public Task<DataServiceResult<ModelDetailOverviewDto>> GetSellDetailOverview(int productId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailOverviewDto>();
                try
                {
                    using (var session = OpenDefaultSession())
                    {
                        Car carAlias = null;                                             
                        Product productAlias = null;

                        var modelDetailOverview = session.QueryOver<Car>(() => carAlias)                            
                            .JoinAlias(x => x.Products, () => productAlias)
                            .Where(() => productAlias.Id == productId)
                            .Select(x => x.Introlduction)
                            .TransformUsing(Transformers.AliasToBean<ModelDetailOverviewDto>())
                            .SingleOrDefault<ModelDetailOverviewDto>();

                        result.Target = modelDetailOverview;
                    }
                }
                catch (Exception ex)
                {
                    result.AddError(ex);
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailEngineDto>> GetSellDetailEngine(int productId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailEngineDto>();
                try
                {
                    using (var session = OpenDefaultSession())
                    {
                        Car carAlias = null;
                        CarSpecification specificationAlias = null;                        
                        Product productAlias = null;

                        var modelDetailEngine = session.QueryOver<CarSpecification>(() => specificationAlias)
                            .JoinAlias(x => x.Car, () => carAlias)  
                            .JoinAlias(() => carAlias.Products, () => productAlias)
                            .Where(() => productAlias.Id == productId)
                            .SelectList(l => l                            
                                .Select(p => p.Id)
                                .Select(p => p.Engine)
                                .Select(p => p.Type_Engine)
                                .Select(p => p.Compresstion)
                                .Select(p => p.CC)
                                .Select(p => p.Driving_Type)
                                .Select(p => p.Transmission_Type)
                                .Select(p => p.Fuel_Type)
                                .Select(p => p.Engine_Valvetrain)
                                .Select(p => p.ESS)
                                .Select(p => p.EBD)
                                .Select(p => p.Remote_Vehice)
                                .Select(p => p.Transmission)
                                .Select(p => p.Exterior_Length)
                                .Select(p => p.Exterior_Width)
                                .Select(p => p.Exterior_Height)
                                .Select(p => p.Horsepower)
                                .Select(p => p.Torque)
                                .Select(p => p.Drag_Coeficient)
                                .Select(p => p.Fuel_Economy_City)
                                .Select(p => p.Fuel_Economy_Highway)
                                .Select(p => p.TimeSpeed)
                                .Select(p => p.Curb_Weight)
                                .Select(p => p.GVWR)
                                .Select(p => p.Payloa)
                                .Select(p => p.Car.Id)
                                .Select(p => p.MPG)
                                .Select(p => p.Km)
                                .Select(p => p.Locking)
                            )
                            .TransformUsing(Transformers.AliasToBean<ModelDetailEngineDto>())
                            .SingleOrDefault<ModelDetailEngineDto>();

                        result.Target = modelDetailEngine;
                    }
                }
                catch (Exception ex)
                {
                    result.AddError(ex);
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailSafetyDto>> GetSellDetailSafety(int productId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailSafetyDto>();
                try
                {
                    using (var session = OpenDefaultSession())
                    {
                        Car carAlias = null;
                        CarSafety safetyAlias = null;                        
                        Product productAlias = null;

                        var modelDetailSafety = session.QueryOver<CarSafety>(() => safetyAlias)
                            .JoinAlias(x => x.Car, () => carAlias)
                            .JoinAlias(() => carAlias.Products, () => productAlias)
                            .Where(() => productAlias.Id == productId)
                            .SelectList(list => list
                                .Select(l => l.Id)
                                .Select(l => l.Airbags_frontal)
                                .Select(l => l.Airbags_Impact)
                                .Select(l => l.Airbag_Curtain)
                                .Select(l => l.HeadLights)
                                .Select(l => l.Exterior_Light_Control)
                                .Select(l => l.Daytime_Running_Lights)
                                .Select(l => l.Led_Taillights)
                                .Select(l => l.Parking_Assist)
                                .Select(l => l.Alarm_System)
                                .Select(l => l.Door_Locks_System)
                                .Select(l => l.Rear_Child)
                                .Select(l => l.Content_Theft)
                                .Select(l => l.Low_Tire)
                                .Select(l => l.Brakes)
                                .Select(l => l.ABS)
                                .Select(l => l.Brake_Assist)
                            )
                            .TransformUsing(Transformers.AliasToBean<ModelDetailSafetyDto>())
                            .SingleOrDefault<ModelDetailSafetyDto>();

                        result.Target = modelDetailSafety;
                    }
                }
                catch (Exception ex)
                {
                    result.AddError(ex);
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailEntertainmentDto>> GetSellDetailEntertainment(int productId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailEntertainmentDto>();
                try
                {
                    using (var session = OpenDefaultSession())
                    {
                        Car carAlias = null;
                        CarEntertainment safetyAlias = null;
                        Product productAlias = null;

                        var modelDetailEntertainment = session.QueryOver<CarEntertainment>(() => safetyAlias)
                        .JoinAlias(x => x.Car, () => carAlias)
                        .JoinAlias(() => carAlias.Products, () => productAlias)
                        .Where(() => productAlias.Id == productId)
                        .SelectList(list => list
                            .Select(l => l.Radio)
                            .Select(l => l.CD_Player)
                            .Select(l => l.DVD_Audio)
                            .Select(l => l.Voice_Recognition)
                            .Select(l => l.Speakers)
                            .Select(l => l.Amplifier)
                            .Select(l => l.bluetooth_Compatibility)
                            .Select(l => l.Wifi_Compatibility)
                            .Select(l => l.ThreeG_Compatibility)
                            .Select(l => l.GPS_Compability)
                        )
                        .TransformUsing(Transformers.AliasToBean<ModelDetailEntertainmentDto>())
                        .SingleOrDefault<ModelDetailEntertainmentDto>();

                        result.Target = modelDetailEntertainment;
                    }
                }
                catch (Exception ex)
                {
                    result.AddError(ex);
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailInteriorDto>> GetSellDetailInterior(int productId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailInteriorDto>();
                try
                {
                    using (var session = OpenDefaultSession())
                    {
                        CarInterior interiorAlias = null;                        
                        Product productAlias = null;
                        Car carAlias = null;

                        var modelDetailInterior = session.QueryOver<CarInterior>(() => interiorAlias)
                            .JoinAlias(x => x.Car, () => carAlias)
                            .JoinAlias(x => carAlias.Products, () => productAlias)
                            .Where(() => productAlias.Id == productId)
                            .SelectList(list => list
                                .Select(l => l.Id)
                                .Select(l => l.Air_Conditioning_Front)
                                .Select(l => l.Air_Conditioning_Rear)
                                .Select(l => l.Air_Filter)
                                .Select(l => l.Seating)
                                .Select(l => l.Front_Seat_Type)
                                .Select(l => l.Front_Head_Restraints)
                                .Select(l => l.Compass)
                                .Select(l => l.Clock)
                                .Select(l => l.DC_Power_outlet)
                                .Select(l => l.Tachometer)
                                .Select(l => l.Water_Temp_Gauge)
                                .Select(l => l.Low_Fuel_Warning)
                                .Select(l => l.Exterior_Temperature)
                                .Select(l => l.Car.Html_Interior)
                                .Select(l => l.Car.Id)
                                .Select(l => l.ReWrite_Seating)
                            )
                            .TransformUsing(Transformers.AliasToBean<ModelDetailInteriorDto>())
                            .SingleOrDefault<ModelDetailInteriorDto>();

                        result.Target = modelDetailInterior;
                    }
                }
                catch (Exception ex)
                {
                    result.AddError(ex);
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailExteriorDto>> GetSellDetailExterior(int productId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailExteriorDto>();
                try
                {
                    using (var session = OpenDefaultSession())
                    {
                        CarExterior exteriorAlias = null;
                        Car carAlias = null;                        
                        Product productAlias = null;

                        var modelDetailExterior = session.QueryOver<CarExterior>(() => exteriorAlias)
                            .JoinAlias(x => x.Car, () => carAlias)
                            .JoinAlias(() => carAlias.Products, () => productAlias)
                            .Where(() => productAlias.Id == productId)
                            .SelectList(list => list
                                .Select(l => l.Id)
                                .Select(l => l.Front_Tires)
                                .Select(l => l.Front_rear)
                                .Select(l => l.Wheels)
                                .Select(l => l.Sunroof)
                                .Select(l => l.Rear_Window_Type)
                                .Select(l => l.DoorCount)
                                .Select(l => l.Mirrows)
                                .Select(l => l.Heated_Door_Mirrows)
                                .Select(l => l.Windshied_Wipes)
                                .Select(l => l.System)
                                .Select(l => l.Car.Html_Exterior)
                            )
                            .TransformUsing(Transformers.AliasToBean<ModelDetailExteriorDto>())
                            .SingleOrDefault<ModelDetailExteriorDto>();

                        result.Target = modelDetailExterior;
                    }
                }
                catch (Exception ex)
                {
                    result.AddError(ex);
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailColorDto>> GetSellDetailColor(int productId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailColorDto>();
                try
                {
                    using (var session = OpenDefaultSession())
                    {
                        Car carAlias = null;                        
                        Product productAlias = null;

                        var modelDetailColor = session.QueryOver<Car>(() => carAlias)                            
                            .JoinAlias(x => x.Products, () => productAlias)
                            .Where(() => productAlias.Id == productId)
                            .Select(l => l.Html_Color)
                            .TransformUsing(Transformers.AliasToBean<ModelDetailColorDto>())
                            .SingleOrDefault<ModelDetailColorDto>();

                        result.Target = modelDetailColor;
                    }
                }
                catch (Exception ex)
                {
                    result.AddError(ex);
                }
                return result;
            });
        }

        public Task<DataServiceResult<ModelDetailImageDto>> GetSellDetailImage(int productId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<ModelDetailImageDto>();
                try
                {
                    using (var session = OpenDefaultSession())
                    {
                        Car carAlias = null;                        
                        Product productAlias = null;

                        var modelDetailImage = session.QueryOver<Car>(() => carAlias)                           
                            .JoinAlias(x => x.Products, () => productAlias)
                            .Where(() => productAlias.Id == productId)
                            .Select(l => l.PathImages)
                            .TransformUsing(Transformers.AliasToBean<ModelDetailImageDto>())
                            .SingleOrDefault<ModelDetailImageDto>();

                        result.Target = modelDetailImage;
                    }
                }
                catch (Exception ex)
                {
                    result.AddError(ex);
                }
                return result;
            });
        }
    }
}
