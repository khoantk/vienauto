﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using Vienauto.Entity.Enums;
using System.Threading.Tasks;
using Vienauto.Interface.Dto;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using Vienauto.Interface.AsyncServices;
using static Common.Core.Nhibernate.CustomDialect.DialectProjectionsExtensions.DateProjections;

namespace Vienauto.Business.AsyncServices
{
    /// <summary>
    /// Car For Sell Detail Functions
    /// </summary>
    public partial class SellAsyncService : BaseService, ISellAsyncService
    {
        public Task<DataServiceResult<CarForSellDetailDto>> GetDetailInfoAndUpdateViewTimesAsync(int productId)
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<CarForSellDetailDto>();
                using (var session = OpenDefaultSession())
                {
                    result.Target = BuildCarDetail(session, productId);
                    if (result.Target != null)
                        UpdateNumberOfViewTimes(session, productId);
                    else
                        result.AddError(ErrorCode.UndefinedCarForSell);
                }
                return result;
            });
        }

        private bool UpdateNumberOfViewTimes(ISession session, int productId)
        {
            var product = session.Load<Product>(productId);
            if (product != null)
            {
                product.SoLanXem += 1;
                session.SaveOrUpdate(product);
                return true;
            }
            return false;
        }

        private CarForSellDetailDto BuildCarDetail(ISession session, int productId)
        {
            var detailProjections = CreateFinalProjectionMembers();
            var carDetail = session.CreateCriteria<Product>("product")
                                   .CreateAlias("product.User", "user").SetFetchMode("User", FetchMode.Join)
                                   .CreateAlias("product.Unit", "unit").SetFetchMode("Unit", FetchMode.Join)
                                   .CreateAlias("product.Car", "car").SetFetchMode("Unit", FetchMode.Join)
                                   .CreateAlias("product.Location", "location").SetFetchMode("location", FetchMode.Join)
                                   .CreateAlias("car.Avatar", "avatar").SetFetchMode("avatar", FetchMode.Join)
                                   .CreateAlias("car.Specification", "specification").SetFetchMode("specification", FetchMode.Join)
                                   .CreateAlias("car.Year", "year").SetFetchMode("year", FetchMode.Join)
                                   .CreateAlias("year.Model", "model").SetFetchMode("model", FetchMode.Join)
                                   .CreateAlias("model.Style", "style").SetFetchMode("style", FetchMode.Join)
                                   .CreateAlias("style.Manufacturer", "manufacturer").SetFetchMode("manufacturer", FetchMode.Join)
                                   .Add(Restrictions.Eq("product.Id", productId))
                                   .SetProjection(detailProjections)
                                   .SetResultTransformer(Transformers.AliasToBean<CarForSellDetailDto>())
                                   .UniqueResult<CarForSellDetailDto>();
            return carDetail;
        }

        private IProjection CreateProjectionForFullPrice()
        {
            var priceProjection = Projections.SqlFunction("GetFullPrice",
                                                          NHibernateUtil.Double,
                                                          Projections.Property("product.Price_Product"),
                                                          Projections.Property("unit.Id"),
                                                          Projections.Constant(21800));
            return priceProjection;
        }

        private IProjection CreateProjectionForProductCode()
        {
            var upperLeftManufacturerProjection = Projections.SqlFunction("Upper", 
                                                                          NHibernateUtil.String,
                                                                          Projections.SqlFunction("Left",
                                                                                 NHibernateUtil.String,
                                                                                 Projections.Property("manufacturer.Name"),
                                                                                 Projections.Constant(3)));

            var castFunctionProductCode = Projections.Cast(NHibernateUtil.String, 
                                                           Projections.Property("product.Id"));

            var productCodeProjection = Projections.SqlFunction("Concat", 
                                                                NHibernateUtil.String,
                                                                upperLeftManufacturerProjection,
                                                                castFunctionProductCode);
            return productCodeProjection;
        }

        private IProjection CreateProjectForExpireTime()
        {
            var dateDiffForDatePost = DateDiff("day", Projections.Property("product.DatePost"),
                                                      Projections.SqlFunction("GetDate", NHibernateUtil.DateTime));

            var expireProjection = Projections.Conditional(
                                            Restrictions.Ge(dateDiffForDatePost, 60),
                                            Projections.Constant(1),
                                            Projections.Conditional(Restrictions.Lt(dateDiffForDatePost, 60), 
                                                                    Projections.Constant(0),
                                                                    Projections.Constant(0)));
            return expireProjection;
        }

        private ProjectionList CreateFinalProjectionMembers()
        {
            var productCodeProjection = CreateProjectionForProductCode();
            var fullPriceProjection = CreateProjectionForFullPrice();
            var expireTimeProjection = CreateProjectForExpireTime();

            var projections = Projections.ProjectionList()
                                         .Add(Projections.Property("product.Id"), "ProductId")
                                         .Add(productCodeProjection, "ProductCode")
                                         .Add(Projections.Property("product.Name_Product"), "ProductName")
                                         .Add(Projections.Property("product.In_Out"), "InOut")
                                         .Add(Projections.Property("product.New_Old"), "NewOld")
                                         .Add(Projections.Property("product.Vin"), "VinNumber")
                                         .Add(Projections.Property("product.Warranty"), "Guarantee")
                                         .Add(Projections.Property("product.Price_Product"), "Price")
                                         .Add(fullPriceProjection, "FullPrice")
                                         .Add(Projections.Property("car.Id"), "CarId")
                                         .Add(Projections.Property("car.Name"), "CarName")
                                         .Add(Projections.Property("manufacturer.Id"), "ManufacturerId")
                                         .Add(Projections.Property("manufacturer.Name"), "ManufacturerName")
                                         .Add(Projections.Property("model.Id"), "ModelId")
                                         .Add(Projections.Property("model.Name"), "ModelName")
                                         .Add(Projections.Property("style.Id"), "StyleId")
                                         .Add(Projections.Property("style.Name"), "StyleName")
                                         .Add(Projections.Property("year.Id"), "YearId")
                                         .Add(Projections.Property("year.Name"), "YearName")
                                         .Add(Projections.Property("avatar.PathAvatar"), "Avatar")
                                         .Add(Projections.Property("unit.Name_Unit"), "UnitName")
                                         .Add(expireTimeProjection, "Expire")
                                         .Add(Projections.Property("product.Status"), "ProductStatus")
                                         .Add(Projections.Property("product.DatePost"), "DatePost")
                                         //.Add(Projections.Property("product.ChietKhauNSX"), "ProducerDiscount")
                                         //.Add(Projections.Property("product.ChietKhauDaiLy"), "ProducerAgent")
                                         //.Add(Projections.Property("product.ChieuKhauSC"), "ProducerSC")
                                         //.Add(Projections.Property("product.GiamGiaSC"), "SaleOffSC")
                                         //.Add(Projections.Property("product.Discount"), "Discount")
                                         //.Add(Projections.Property("product.Km"), "Km")
                                         //.Add(Projections.Property("product.hide_price"), "HidePrice")
                                         ;
            return projections;
        }
    }
}
