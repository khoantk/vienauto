﻿using Vienauto.Entity.Enums;
using Common.Core.Extension;
using Vienauto.Interface.Dto;
using System.Threading.Tasks;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Interface.AsyncServices;
using System.Linq.Expressions;
using System;
using System.Linq;

namespace Vienauto.Business.AsyncServices
{
    public class OtherAsyncService : BaseService, IOtherAsyncService
    {
        public Task<DataServiceResult<IList<QuestionDto>>> GetAllQuestionsAsync()
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<QuestionDto>>();
                using (var session = OpenDefaultSession())
                {
                    var questions = ListAll<Question>("asc", "Name_Question");
                    if (questions == null)
                        return new DataServiceResult<IList<QuestionDto>>
                        {
                            Errors = new List<ErrorDescriber> { new ErrorDescriber { Code = ErrorCode.FailToListAllQuestion } }
                        };

                    result.Target = questions.FromEntitiesToDtos();
                }
                return result;
            });
        }

        public Task<DataServiceResult<IList<LocationDto>>> GetAllLocationsAsync()
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<LocationDto>>();
                using (var session = OpenDefaultSession())
                {
                    var locations = ListAll<Location>("asc", "Name_Location");
                    if (locations == null)
                        return new DataServiceResult<IList<LocationDto>>
                        {
                            Errors = new List<ErrorDescriber>
                            {
                                new ErrorDescriber { Code = ErrorCode.FailToListLocation }
                            }
                        };

                    result.Target = locations.FromEntitiesToDtos();
                }
                return result;
            });
        }

        public Task<DataServiceResult<IList<ProvinceDto>>> GetAllProvincesAsync()
        {
            return Task.Run(() =>
            {
                var result = new DataServiceResult<IList<ProvinceDto>>();
                using (var session = OpenDefaultSession())
                {
                    var provinces = ListAll<Province>("asc", "Name_TinhThanh");

                    var HCMAndHNProvince = new[]{ "TP. Hà Nội", "TP.Hồ Chí Minh" };
                    var provincesOrderedByHCMAndHN = provinces.OrderByDescending(p => HCMAndHNProvince.Contains(p.Name_TinhThanh))
                                                              .ThenBy(p => p.Name_TinhThanh)
                                                              .ToList();

                    if (provincesOrderedByHCMAndHN == null || provincesOrderedByHCMAndHN.Count == 0)
                        return new DataServiceResult<IList<ProvinceDto>>
                        {
                            Errors = new List<ErrorDescriber> { new ErrorDescriber { Code = ErrorCode.FailToListProvince } }
                        };

                    result.Target = provincesOrderedByHCMAndHN.FromEntitiesToDtos();
                }
                return result;
            });
        }
    }
}
