﻿using System;
using System.Text;
using Vendare.Error;
using NHibernate.Transform;
using Vienauto.Entity.Entities;
using Common.Core.Data.Paging;
using System.Collections.Generic;
using System.Collections.Specialized;
using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using Vienauto.Interface.Services;
using Vienauto.Entity.Enums;

namespace Vienauto.Business.Service
{
    public class AgencyService : BaseService, IAgencyService
    {
        public DataServiceResult<AgencyDetailDto> GetAgencyDetail(int agencyUserId, int manufacturerId, int agentId)
        {
            var result = new DataServiceResult<AgencyDetailDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var query = new StringBuilder();
                    query = query.AppendFormat(@"SELECT d.Id_user AS UserId,
                                                        d.Id_dangki AS AgencyId,
                                                        t.Id_TinhThanh AS CountryId,
                                                        d.Ten_CTY AS CompanyName,
                                                        d.Diachi_giaodich AS [Address],
                                                        t.Name_TinhThanh AS ProvinceName,
                                                        u.Phone AS NewPhone,
                                                        u.Mobile AS OldPhone,
                                                        u.Fax AS [Service],
                                                        u.ToaDoMap AS Coordinates,
                                                        h.Id_Hang_PhanPhoi DealerShipId,
                                                        m.logo AS ManufacturerAvatar,
                                                        m.Id_Manufacturer AS ManufacturerId,
                                                        m.Name_Manufacturer AS ManufacturerName,
                                                        u.ZoomMap AS ZoomMap,
                                                        FromSales,
                                                        ToSales,
                                                        FromServices,
                                                        ToServices,
                                                        DisplaySales,
                                                        DisplayServices
                                                        FROM hdt.Dangki_daili_online d
                                                        LEFT JOIN hdt.Users u ON u.Id_Users = d.Id_user
                                                        LEFT JOIN hdt.AgentTime a ON u.Id_Users = a.IdUser
                                                        LEFT JOIN hdt.Hang_Phanphoi h ON h.Id_Daily = u.Id_Users
                                                        LEFT JOIN hdt.TinhThanh t ON u.TinhThanh = t.Id_TinhThanh
                                                        LEFT JOIN hdt.Manufacturer m ON m.Id_Manufacturer = h.Id_Manufacturer
                                                        WHERE d.Id_user = {0} AND m.Id_Manufacturer = {1}",
                                                        agencyUserId, manufacturerId);

                    var detailAgency = session.CreateSQLQuery(query.ToString())
                                        .SetResultTransformer(Transformers.AliasToBean<AgencyDetailDto>())
                                        .UniqueResult<AgencyDetailDto>();

                    if (detailAgency != null)
                    {
                        var detailReview = session.CreateSQLQuery(@"SELECT MAX(Overall) AS MaxPointReview, 
                                                                COUNT(Id_Reviews) AS TotalReview
                                                                FROM hdt.Reviews_Agent WHERE Id_Agent = " + agentId)
                                                  .SetResultTransformer(Transformers.AliasToBean<AgencyDetailDto>())
                                                  .UniqueResult<AgencyDetailDto>();

                        detailAgency.MaxPointReview = detailReview.MaxPointReview;
                        detailAgency.TotalReview = detailReview.TotalReview;
                    }
                    else
                        result.AddError(ErrorCode.UndefinedAgencyDetail);
                    result.Target = detailAgency;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToGetAgencyDetail, ex);
            }
            return result;
        }

        public DataServiceResult<AgencyDetailReviewDto> GetAgencyReviewDetail(int agentId, int manufacturerId, int userId)
        {
            var result = new DataServiceResult<AgencyDetailReviewDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var agencyInfo = session.CreateSQLQuery(String.Format(@"SELECT d.Id_dangki AS AgentId,
                                                                                d.Id_user AS UserId,
                                                                                m.Id_Manufacturer AS ManufacturerId,
                                                                                d.Ten_CTY AS AgentName,
                                                                                m.logo AS Avatar,
                                                                                t.Name_TinhThanh AS ProvinceName,
                                                                                m.Name_Manufacturer AS ManufacturerName
                                                                            FROM hdt.Dangki_daili_online d
                                                                            LEFT JOIN hdt.Hang_Phanphoi ds
                                                                                ON ds.Id_DaiLy = d.Id_user
                                                                            LEFT JOIN hdt.Users u
                                                                                ON u.Id_Users = d.Id_user
                                                                            LEFT JOIN hdt.TinhThanh t
                                                                                ON t.Id_TinhThanh = u.TinhThanh
                                                                            LEFT JOIN hdt.Manufacturer m
                                                                                ON m.Id_Manufacturer = ds.Id_Manufacturer
                                                                            WHERE d.Id_dangki = {0}
                                                                            AND m.Id_Manufacturer = {1};", agentId, manufacturerId))
                                             .SetResultTransformer(Transformers.AliasToBean<AgencyDetailReviewDto>())
                                             .UniqueResult<AgencyDetailReviewDto>();

                    var detailReview = session.CreateSQLQuery(String.Format(@"SELECT sum(Overall) / count(Id_Reviews) as Overall,
		                                                    sum(Customer_Service) / count(Id_Reviews) as Customer_Service,
		                                                    sum(Buying_Process) / count(Id_Reviews) as Buying_Process,
		                                                    sum(Quality_of_Repair) / count(Id_Reviews) as Quality_of_Repair,
		                                                    sum(Facilities) / count(Id_Reviews) as Facilities
	                                                    FROM hdt.Dangki_daili_online d
	                                                    LEFT JOIN hdt.Hang_Phanphoi ds
		                                                    ON ds.Id_DaiLy = d.Id_user	
	                                                    LEFT JOIN hdt.Manufacturer m
		                                                    ON m.Id_Manufacturer = ds.Id_Manufacturer
	                                                    JOIN hdt.Reviews_Agent ra
		                                                    ON ra.Id_Agent = d.Id_dangki
	                                                    WHERE d.Id_dangki = {0}
	                                                    AND m.Id_Manufacturer = {1}", agentId, manufacturerId))
                                                        .SetResultTransformer(Transformers.AliasToBean<AgencyReview>())
                                                        .UniqueResult<AgencyReview>();

                    //var detailReview = session.QueryOver<AgencyReview>()
                    //                          .Where(ar => ar.Agency.Id == agentId && ar.User.Id == userId)
                    //                          .SingleOrDefault();

                    var totalReviews = session.QueryOver<AgencyReview>()
                                           .Where(ar => ar.Agency.Id == agentId)
                                           .RowCount();

                    result.Target = agencyInfo;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToGetReviewOfAnAgent, ex);
            }
            return result;
        }

        public DataServiceResult<IList<DealerShipDto>> GetAllDealerShips()
        {
            var result = new DataServiceResult<IList<DealerShipDto>>();
            try
            {
                DealerShipDto dto = null;
                DealerShip dealerShipAlias = null;
                Manufacturer manufactureAlias = null;

                using (var session = OpenDefaultSession())
                {
                    result.Target = session.QueryOver<DealerShip>(() => dealerShipAlias)
                                           .JoinAlias(ds => ds.Manufacturer, () => manufactureAlias)
                                           .SelectList(list => list
                                                 .SelectGroup(pc => manufactureAlias.Id).WithAlias(() => dto.ManufacturerId)
                                                 .SelectGroup(pc => manufactureAlias.Name).WithAlias(() => dto.ManufacturerName))
                                           .OrderBy(() => manufactureAlias.Name).Asc
                                           .TransformUsing(Transformers.AliasToBean<DealerShipDto>())
                                           .List<DealerShipDto>();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToListAllDealerShip, ex);
            }
            return result;
        }

        public DataServiceResult<IList<AgencyDto>> GetAgency(int manufacturerId)
        {
            var result = new DataServiceResult<IList<AgencyDto>>();
            try
            {
                AgencyDto dto = null;
                User userAlias = null;
                Agency agencyAlias = null;
                Province provinceAlias = null;
                DealerShip dealerShipAlias = null;

                using (var session = OpenDefaultSession())
                {
                    result.Target = Session.QueryOver<DealerShip>(() => dealerShipAlias)
                                           .JoinAlias(ds => ds.User, () => userAlias)
                                           .JoinAlias(a => dealerShipAlias.User, () => agencyAlias)
                                           .Left.JoinAlias(p => userAlias.Province, () => provinceAlias)
                                           .Where(d => d.Manufacturer.Id == manufacturerId)
                                           .SelectList(list => list
                                               .SelectGroup(pc => userAlias.Id).WithAlias(() => dto.AgencyId)
                                               .SelectGroup(pc => userAlias.FullName).WithAlias(() => dto.FullName))
                                           .TransformUsing(Transformers.AliasToBean<AgencyDto>())
                                           .List<AgencyDto>();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToListAllDealerShip, ex);
            }
            return result;
        }

        public DataServiceResult<IList<AgencyDto>> GetAgencyByDealerShip(int manufacturerId)
        {
            var result = new DataServiceResult<IList<AgencyDto>>();
            try
            {
                AgencyDto dto = null;
                User userAlias = null;
                DealerShip dealerShipAlias = null;

                using (var session = OpenDefaultSession())
                {
                    result.Target = Session.QueryOver<DealerShip>(() => dealerShipAlias)
                                           .JoinAlias(ds => ds.User, () => userAlias)
                                           .Where(d => d.Manufacturer.Id == manufacturerId)
                                           .SelectList(list => list
                                                .SelectGroup(pc => userAlias.Id).WithAlias(() => dto.AgencyId)
                                                .SelectGroup(pc => userAlias.FullName).WithAlias(() => dto.FullName))
                                           .TransformUsing(Transformers.AliasToBean<AgencyDto>())
                                           .List<AgencyDto>();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToListAllDealerShip, ex);
            }
            return result;
        }

        public PagingServiceResult<PagingResponse, List<AgencyDto>> GetAgenciesWithPaging(PagingRequest<Dictionary<string, object>> pagingRequest)
        {
            var result = new PagingServiceResult<PagingResponse, List<AgencyDto>>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    string province = "",
                           dealerShip = "",
                           sortOrder = "";

                    if (pagingRequest.FilterConditions.Count > 0)
                    {
                        province = pagingRequest.FilterConditions["provinceId"].ToString();
                        dealerShip = pagingRequest.FilterConditions["dealershipId"].ToString();

                        if (pagingRequest.PagingOption.SortName == "Agency")
                            sortOrder = " ORDER BY CompanyName ";
                        else if (pagingRequest.PagingOption.SortName == "Rating")
                            sortOrder = " ORDER BY MaxPointReview DESC ";
                        else if (pagingRequest.PagingOption.SortName == "Province")
                            sortOrder = " ORDER BY ProvinceName, CompanyName ";
                    }

                    var agenciesQuery = $@"EXEC hdt.sp_GetAgencies 
                                                    '{dealerShip}',
                                                    '{province}',
                                                    '{sortOrder}',
                                                     {pagingRequest.PagingOption.PageIndex},
                                                     {pagingRequest.PagingOption.PageSize}";

                    var agenciesResult = Session.CreateSQLQuery(agenciesQuery)
                                            .SetResultTransformer(Transformers.AliasToBean<AgencyDto>())
                                            .List<AgencyDto>();

                    result.Target = agenciesResult as List<AgencyDto>;
                    var totalPages = (result.Target[0]?.TotalPages).GetValueOrDefault();
                    result.Paging = new PagingResponse(totalPages, pagingRequest.PagingOption);
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToGetAgenciesPaging, ex);
                var detail = new NameValueCollection();
                if (ex.InnerException != null) detail.Add("innerExeption", ex.InnerException.Message);
                new LoggableException(ex, detail);
            }
            return result;
        }

        public PagingServiceResult<PagingResponse, List<AgencyReviewsDto>> GetAgenciesReviewsWithPaging(PagingRequest<Dictionary<string, object>> pagingRequest)
        {
            var result = new PagingServiceResult<PagingResponse, List<AgencyReviewsDto>>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var query = new StringBuilder(20);
                    query = query.Append(GetSelectQueryAgenciesReviewsWithPaging());

                    if (pagingRequest.FilterConditions.Count > 0)
                    {
                        var dealerShip = pagingRequest.FilterConditions["agentId"].ToString();
                        if (dealerShip != "")
                            query.Append(" WHERE a.Id_Agent = " + dealerShip);
                    }

                    int totalPages = (int)Math.Ceiling(Convert.ToDouble(Session.CreateSQLQuery(
                                                       GetTotalRows(query.ToString()))
                                                       .UniqueResult()) / (double)pagingRequest.PagingOption.PageSize);

                    query = query.Append(" ORDER BY Id_Reviews DESC ");

                    query.AppendFormat("OFFSET {0} * ({1} - 1) ROWS FETCH NEXT {0} ROWS ONLY",
                                      pagingRequest.PagingOption.PageSize, pagingRequest.PagingOption.PageIndex);
                    
                    var agentReviewsPaging = Session.CreateSQLQuery(query.ToString())
                                                .SetResultTransformer(Transformers.AliasToBean<AgencyReviewsDto>())
                                                .List<AgencyReviewsDto>();

                    result.Target = (List<AgencyReviewsDto>)agentReviewsPaging;
                    result.Paging = new PagingResponse(totalPages, pagingRequest.PagingOption);
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToGetAgenciesReviews, ex);
            }
            return result;
        }

        public DataServiceResult<bool> CheckCanWriteReviewAgency(int agentId, int userId)
        {
            var result = new DataServiceResult<bool>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var canReviewCount = session.QueryOver<AgencyReview>()
                                                .Where(x => x.Agency.Id == agentId && x.User.Id == userId)
                                                .RowCount();
                    if (canReviewCount > 0)
                        result.AddError(ErrorCode.ExistedReviewAgency);
                    result.Target = true;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToCheckCanWriteReviewAgency, ex);
            }
            return result;
        }

        public DataServiceResult<bool> AddAgencyReview(AgencyDetailReviewDto agencyReview)
        {
            var result = new DataServiceResult<bool>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var agenciesReviews = new AgencyReview();
                    agenciesReviews.Agency = Get<Agency>(agencyReview.AgentId);
                    agenciesReviews.User = Get<User>(agencyReview.UserId);
                    agenciesReviews.Customer_Service = agencyReview.CustomerService;
                    agenciesReviews.Buying_Process = agencyReview.BuyingProcess;
                    agenciesReviews.Quality_of_Repair = agencyReview.QualityOfRepair;
                    agenciesReviews.Facilities = agencyReview.Facilities;
                    agenciesReviews.Title_Review = agencyReview.TitleReview;
                    agenciesReviews.Content_Review = agencyReview.ContentReview;
                    agenciesReviews.Name = agencyReview.Name;
                    agenciesReviews.Email = agencyReview.Email;
                    agenciesReviews.Datetime_Review = DateTime.Now;
                    agenciesReviews.Overall = GetRelativeRatingPoint(
                                               (agenciesReviews.Customer_Service +
                                               agenciesReviews.Buying_Process +
                                               agenciesReviews.Quality_of_Repair +
                                               agenciesReviews.Facilities) / 4.0);
                    session.SaveOrUpdate(agenciesReviews);
                    session.Flush();
                    result.Target = true;
                }
            }
            catch (Exception ex)
            {
                result.Target = false;
                result.AddError(ErrorCode.FailToSaveAgencyReview, ex);
            }
            return result;
        }

        #region Private method

        private double GetRelativeRatingPoint(double ratingPoint)
        {
            if (ratingPoint > 0.5 && ratingPoint < 1)
                return 1;
            else if (ratingPoint > 1.5 && ratingPoint < 2)
                return 2;
            else if (ratingPoint > 2.5 && ratingPoint < 3)
                return 3;
            else if (ratingPoint > 3.5 && ratingPoint < 4)
                return 4;
            else if (ratingPoint > 4.5 && ratingPoint < 5)
                return 5;
            return ratingPoint;
        }

        private string GetSelectQueryAgenciesReviewsWithPaging()
        {
            return @"SELECT Id_Reviews as ReviewId,
                            Customer_Service as CustomerService,
                            Buying_Process as BuyingProcess,
                            Quality_of_Repair as QualityRepair,
                            Facilities as Facilities,
                            Content_Review as Content,
                            Datetime_Review as DatePost,
                            Overall,
                            Id_User as UserId,
                            FullName as UserName,
                            ReviewOfAgency.MaxPointReview
                    FROM hdt.Reviews_Agent a                                            
                    LEFT JOIN hdt.Users c ON a.Id_User = c.Id_Users
                    LEFT JOIN (SELECT Id_Agent, MAX(Overall) MaxPointReview
                    FROM hdt.Reviews_Agent GROUP BY Id_Agent) ReviewOfAgency
                    ON ReviewOfAgency.Id_Agent = a.Id_Agent";
        }

        private string GetTotalRows(string selectQuery)
        {
            return string.Concat("SELECT COUNT(*) FROM (", selectQuery, ") AS TotalRow");
        }

        #endregion
    }
}
