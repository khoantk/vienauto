﻿using System;
using Vienauto.Entity.Enums;
using Vienauto.Interface.Result;
using NHibernate.Transform;
using System.Collections.Generic;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Services;

namespace Vienauto.Business.Service
{
    public class YearService : BaseService, IYearService
    {
        public DataServiceResult<IList<Year>> GetYearByModel(int modelId)
        {
            var result = new DataServiceResult<IList<Year>>();
            try
            {
                Year yearAlias = null;
                Model modelAlias = null;

                using (var session = OpenDefaultSession())
                {
                    result.Target = Session.QueryOver<Year>(() => yearAlias)
                                  .JoinAlias(y => y.Model, () => modelAlias)
                                  .Where(m => modelAlias.Id == modelId)
                                  .SelectList(list => list
                                        .SelectGroup(y => yearAlias.Id).WithAlias(() => yearAlias.Id)
                                        .SelectGroup(y => yearAlias.Name).WithAlias(() => yearAlias.Name))
                                  .TransformUsing(Transformers.AliasToBean<Year>())
                                  .OrderBy(x => x.Name).Desc
                                  .List<Year>();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToListYearByModel, ex);
            }
            return result;
        }
    }
}
