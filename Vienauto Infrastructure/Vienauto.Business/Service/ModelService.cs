﻿using System;
using System.Text;
using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;
using NHibernate.Transform;
using Common.Core.Data.Paging;
using System.Collections.Generic;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Services;
using Vienauto.Entity.Enums;

namespace Vienauto.Business.Service
{
    public class ModelService : BaseService, IModelService
    {
        public DataServiceResult<IList<Model>> GetModelByManufacturer(int manufacturerId)
        {
            var result = new DataServiceResult<IList<Model>>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufacturerAlias = null;

                    result.Target = session.QueryOver<Model>(() => modelAlias)
                                  .JoinAlias(m => m.Style, () => styleAlias)
                                  .JoinAlias(s => styleAlias.Manufacturer, () => manufacturerAlias)
                                  .Where(mn => manufacturerAlias.Id == manufacturerId)
                                  .SelectList(list => list
                                        .Select(m => modelAlias.Id).WithAlias(() => modelAlias.Id)
                                        .Select(m => modelAlias.Name).WithAlias(() => modelAlias.Name)
                                        .Select(m => modelAlias.RewriteName).WithAlias(() => modelAlias.RewriteName))
                                  .TransformUsing(Transformers.AliasToBean<Model>())
                                  .OrderBy(x => x.Name).Asc
                                  .List<Model>();
                }
            }
            catch (Exception ex)
            {
                //result.AddError(ErrorCode.FailToListModelByManufacturer, ex);
            }
            return result;
        }

        public DataServiceResult<ModelDetailReviewDto> GetModelReviewDetail(int manufactureId, int modelId, int yearId, int userId)
        {
            var result = new DataServiceResult<ModelDetailReviewDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var modelInfo = session.CreateSQLQuery(
                                            string.Format(GetSqlQuerySelectModelReviewDetail(), 
                                                          manufactureId, modelId, yearId))
                                    .SetResultTransformer(Transformers.AliasToBean<ModelDetailReviewDto>())
                                    .UniqueResult<ModelDetailReviewDto>();

                    //var detailReview = session.QueryOver<ModelReview>()
                    //                          .Where(x => x.Brand == modelInfo.Brand && 
                    //                                 x.Model == modelInfo.Model && 
                    //                                 x.Year == modelInfo.Year &&
                    //                                 (x.IdUsers != 0 && x.IdUsers == userId))
                    //                          .SingleOrDefault();

                    var detailReview = session.CreateSQLQuery(String.Format(@"SELECT sum(Point_Body) / COUNT(Id_Reviews) as PointBody,
		                                                    sum(Point_Operation) / COUNT(Id_Reviews) as PointOperation,
		                                                    sum(Point_Price) / COUNT(Id_Reviews) as PointPrice,
		                                                    sum(Point_Safe) / COUNT(Id_Reviews) as PointSafe
                                                    FROM hdt.Reviews_ModelDetail		
                                                    WHERE Brand = '{0}'
	                                                    and Model = '{1}'
	                                                    and Year = '{2}'", modelInfo.Brand, modelInfo.Model, modelInfo.Year))
                                                        .SetResultTransformer(Transformers.AliasToBean<ModelReview>())
                                                        .UniqueResult<ModelReview>();
                                                                                
                    var totalReview = session.QueryOver<ModelReview>()
                                              .Where(x => x.Brand == modelInfo.Brand && x.Model == modelInfo.Model && x.Year == modelInfo.Year)
                                              .RowCount();

                    result.Target = modelInfo.Transform(detailReview, totalReview);
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex);
            }
            return result;
        }

        public PagingServiceResult<PagingResponse, List<ModelReviewsDto>> GetModelsReviewsWithPaging(PagingRequest<Dictionary<string, object>> pagingRequest)
        {
            var result = new PagingServiceResult<PagingResponse, List<ModelReviewsDto>>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var query = new StringBuilder();
                    query.Append(@"SELECT Id_Reviews as IdReviews,
                                          Brand, Model, Year,
                                          rmd.Id_Users as UserId,
                                          CASE WHEN rmd.Id_Users = 0 THEN 
                                               ISNULL(rmd.Name, 'Khách')
                                               ELSE u.FullName END as UserName,
                                          Point_Body as PointBody,
                                          Point_Safe as PointSafe,
                                          Point_Operation as PointOperation,
                                          Point_Price as PointPrice,
                                          Content, DatePost
                                    FROM hdt.Reviews_ModelDetail rmd
                                    LEFT JOIN hdt.Users u
                                    ON rmd.Id_Users = u.Id_Users");

                    if (pagingRequest.FilterConditions.Count > 0)
                    {
                        var brand = pagingRequest.FilterConditions["manufacture"].ToString();
                        if (brand != "")
                            query.Append(string.Format(" WHERE Brand = '{0}'", brand));
                        var model = pagingRequest.FilterConditions["model"].ToString();
                        if (model != "")
                            query.Append(string.Format(" AND Model = '{0}'", model));
                        var year = pagingRequest.FilterConditions["year"].ToString();
                        if (year != "")
                            query.Append(string.Format(" AND Year = '{0}'", year));
                    }

                    int totalPages = (int)Math.Ceiling(Convert.ToDouble(session.CreateSQLQuery(
                                                       GetTotalRows(query.ToString()))
                                                       .UniqueResult()) / (double)pagingRequest.PagingOption.PageSize);

                    query = query.Append(" ORDER BY Id_Reviews DESC ");
                    query.AppendFormat("OFFSET {0} * ({1} - 1) ROWS FETCH NEXT {0} ROWS ONLY;",
                                        pagingRequest.PagingOption.PageSize,
                                        pagingRequest.PagingOption.PageIndex);

                    var modelReviewsPaging = Session.CreateSQLQuery(query.ToString())
                                                .SetResultTransformer(Transformers.AliasToBean<ModelReviewsDto>())
                                                .List<ModelReviewsDto>();

                    result.Target = modelReviewsPaging as List<ModelReviewsDto>;
                    result.Paging = new PagingResponse(totalPages, pagingRequest.PagingOption);
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex);
            }
            return result;
        }

        public DataServiceResult<ModelDetailDto> GetModelStandardInformation(int userId, string manufacturerName, string modelName, string yearName, int carId)
        {
            var result = new DataServiceResult<ModelDetailDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var modelStandardQuery = $@"EXEC hdt.sp_GetModelStandardInformation 
                                                    '{manufacturerName}', 
                                                    '{modelName}', 
                                                    '{yearName}',
                                                     {carId}, 
                                                     {userId}";
                    result.Target = session.CreateSQLQuery(modelStandardQuery)
                                               .SetResultTransformer(Transformers.AliasToBean<ModelDetailDto>())
                                               .UniqueResult<ModelDetailDto>();
                    if (result.Target == null)
                        result.AddError(ErrorCode.UndefinedModel);
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToGetModelStandardInformation, ex);
            }
            return result;
        }

        public DataServiceResult<ModelDto> GetModelCommonInformation(string manufacturerName, string modelName, string yearName, int carId)
        {
            var result = new DataServiceResult<ModelDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var modelCommonQuery = $@"EXEC hdt.sp_GetModelCommonInformation 
                                                    '{manufacturerName}', 
                                                    '{modelName}', 
                                                    '{yearName}',
                                                     {carId}";

                    var modelCommon = session.CreateSQLQuery(modelCommonQuery)
                                             .SetResultTransformer(Transformers.AliasToBean<ModelDto>())
                                             .UniqueResult<ModelDto>();

                    if (modelCommon == null)
                        result.AddError(ErrorCode.UndefinedModel);

                    result.Target = modelCommon;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToGetBasicModelInformation, ex);
            }
            return result;
        }

        public DataServiceResult<IList<ModelDto>> GetSameVersionCars(string manufacturerName, string modelName, string yearName)
        {
            var result = new DataServiceResult<IList<ModelDto>>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    Car carAlias = null;
                    Unit unitAlias = null;
                    Product productAlias = null;
                    Year yearAlias = null;
                    Style styleAlias = null;
                    Model modelAlias = null;
                    Avatar avatarModelAlias = null;
                    Manufacturer manufacturerAlias = null;
                    CarSpecification specificationAlias = null;
                    ModelDto modelDtoAlias = null;

                    result.Target = session.QueryOver<Manufacturer>(() => manufacturerAlias)
                                           .Left.JoinAlias(x => x.Styles, () => styleAlias)
                                           .Left.JoinAlias(() => styleAlias.Models, () => modelAlias)
                                           .Left.JoinAlias(() => modelAlias.Years, () => yearAlias)
                                           .Left.JoinAlias(() => yearAlias.Cars, () => carAlias)
                                           .Left.JoinAlias(() => carAlias.Products, () => productAlias)
                                           .Left.JoinAlias(() => carAlias.Avatar, () => avatarModelAlias)
                                           .Left.JoinAlias(() => productAlias.Unit, () => unitAlias)
                                           .Left.JoinAlias(() => carAlias.Specification, () => specificationAlias)
                                           .Where(m => manufacturerAlias.Name == manufacturerName &&
                                                       modelAlias.Name == modelName &&
                                                       yearAlias.Name == yearName)
                                           .SelectList(list => list
                                                   .Select(x => x.Id).WithAlias(() => modelDtoAlias.ManufacturerId)
                                                   .Select(() => modelAlias.Id).WithAlias(() => modelDtoAlias.ModelId)
                                                   .Select(() => yearAlias.Id).WithAlias(() => modelDtoAlias.YearId)
                                                   .Select(() => carAlias.Id).WithAlias(() => modelDtoAlias.CarId)
                                                   .Select(() => productAlias.Id).WithAlias(() => modelDtoAlias.ProductId)
                                                   .Select(() => productAlias.Price_Product).WithAlias(() => modelDtoAlias.Price)
                                                   .Select(() => unitAlias.Name_Unit).WithAlias(() => modelDtoAlias.UnitName)
                                                   .Select(() => avatarModelAlias.PathAvatar).WithAlias(() => modelDtoAlias.Avatar)
                                                   .Select(() => carAlias.Name).WithAlias(() => modelDtoAlias.CarName)
                                                   .Select(() => styleAlias.Name).WithAlias(() => modelDtoAlias.StyleName)
                                                   .Select(() => manufacturerAlias.Name).WithAlias(() => modelDtoAlias.ManufacturerName)
                                                   .Select(() => modelAlias.Name).WithAlias(() => modelDtoAlias.ModelName)
                                                   .Select(() => yearAlias.Name).WithAlias(() => modelDtoAlias.YearName)
                                                   .Select(() => specificationAlias.Fuel_Economy_City).WithAlias(() => modelDtoAlias.FuelEconomyCity)
                                                   .Select(() => specificationAlias.Fuel_Economy_Highway).WithAlias(() => modelDtoAlias.FuelEconomyHighway))
                                           .OrderBy(() => carAlias.Id).Desc
                                           .TransformUsing(Transformers.AliasToBean<ModelDto>())
                                           .List<ModelDto>();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToGetSameVersionCars, ex);
            }
            return result;
        }

        public PagingServiceResult<PagingResponse, IList<ModelDetailDto>> GetSameStyleCarsWithModelPaging(PagingRequest<Dictionary<string, object>> pagingRequest)
        {
            var result = new PagingServiceResult<PagingResponse, IList<ModelDetailDto>>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    if (pagingRequest.FilterConditions.Count > 0)
                    {
                        var manufacturerName = pagingRequest.FilterConditions["manufacturerName"].ToString();
                        var modelName = pagingRequest.FilterConditions["modelName"].ToString();
                        var yearName = pagingRequest.FilterConditions["yearName"].ToString();
                        var styleName = pagingRequest.FilterConditions["styleName"].ToString();

                        var sameStyleCarsQuery = $@"EXEC hdt.sp_GetModelSameStyles 
                                                    '{manufacturerName}','{modelName}',
                                                    '{yearName}','{styleName}',
                                                     {pagingRequest.PagingOption.PageIndex},
                                                     {pagingRequest.PagingOption.PageSize}";

                        result.Target = (session.CreateSQLQuery(sameStyleCarsQuery)
                                                .SetResultTransformer(Transformers.AliasToBean<ModelDetailDto>())
                                                .List<ModelDetailDto>() as List<ModelDetailDto>);

                        var totalPages = (result.Target[0]?.TotalPages).GetValueOrDefault();
                        result.Paging = new PagingResponse(totalPages, pagingRequest.PagingOption);
                    }
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToGetSameStyleCars, ex);
            }
            return result;
        }

        public DataServiceResult<bool> CheckCanWriteReviewModel(string manufacturer, string model, string year, int userId)
        {
            var result = new DataServiceResult<bool>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var canReviewCount = session.QueryOver<ModelReview>()
                                                .Where(x => x.Brand == manufacturer &&
                                                       x.Model == model && x.Year == year &&
                                                       (x.IdUsers != 0 && x.IdUsers == userId))
                                                .RowCount();
                    if (canReviewCount > 0)
                        result.AddError(ErrorCode.ExistedReviewModel);
                    result.Target = true;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToCheckCanWriteReviewModel, ex);
            }
            return result;
        }

        public DataServiceResult<bool> AddModelReview(ModelReviewsDto modelReview)
        {
            var result = new DataServiceResult<bool>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var modelReviews = new ModelReview();
                    modelReviews.IdUsers = modelReview.UserId;
                    modelReviews.Brand = modelReview.Brand;
                    modelReviews.Model = modelReview.Model;
                    modelReviews.Year = modelReview.Year;
                    modelReviews.PointBody = modelReview.PointBody;
                    modelReviews.PointSafe = modelReview.PointSafe;
                    modelReviews.PointOperation = modelReview.PointOperation;
                    modelReviews.PointPrice = modelReview.PointPrice;
                    modelReviews.Title = modelReview.Title;
                    modelReviews.Content = modelReview.Content;
                    modelReviews.Name = modelReview.Name;
                    modelReviews.Email = modelReview.Email;
                    modelReviews.Phone = modelReview.Phone;
                    modelReviews.Datepost = modelReview.DatePost;
                    session.SaveOrUpdate(modelReviews);
                    session.Flush();
                    result.Target = true;
                }
            }
            catch (Exception ex)
            {
                result.Target = false;
                result.AddError(ErrorCode.FailToSaveModelReview, ex);
            }
            return result;
        }

        public DataServiceResult<ModelDetailOverviewDto> GetModelDetailOverview(int carId)
        {
            var result = new DataServiceResult<ModelDetailOverviewDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    Car carAlias = null;
                    Style styleAlias = null;
                    Model modelAlias = null;
                    Year yearAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailOverviewDto modelDto = null;

                    var modelDetailOverview = session.QueryOver<Car>(() => carAlias)
                        .Left.JoinAlias(x => x.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(() => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Introlduction).WithAlias(() => modelDto.Introduction)
                        )
                        .OrderBy(x => x.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailOverviewDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailOverviewDto>();

                    result.Target = modelDetailOverview;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex);
            }
            return result;
        }

        public DataServiceResult<ModelDetailEngineDto> GetModelDetailEngine(int carId)
        {
            var result = new DataServiceResult<ModelDetailEngineDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    CarSpecification specificationAlias = null;
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailEngineDto modelDto = null;

                    var modelDetailEngine = session.QueryOver<CarSpecification>(() => specificationAlias)
                        .Left.JoinAlias(() => specificationAlias.Car, () => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(p => p.Engine).WithAlias(() => modelDto.EngineName)
                            .Select(p => p.Type_Engine).WithAlias(() => modelDto.EngineType)
                            .Select(p => p.Compresstion).WithAlias(() => modelDto.CompressionRatio)
                            .Select(p => p.Driving_Type).WithAlias(() => modelDto.DrivingType)
                            .Select(p => p.Transmission_Type).WithAlias(() => modelDto.TranmissionType)
                            .Select(p => p.Fuel_Type).WithAlias(() => modelDto.FuelType)
                            .Select(p => p.Fuel_Economy_City).WithAlias(() => modelDto.FuelEconomyCity)
                            .Select(p => p.Fuel_Economy_Highway).WithAlias(() => modelDto.FuelEconomyHighway)
                            .Select(p => p.Locking).WithAlias(() => modelDto.Locking)
                            .Select(p => p.Engine_Valvetrain).WithAlias(() => modelDto.EngineValvetrain)
                            .Select(p => p.ESS).WithAlias(() => modelDto.ESS)
                            .Select(p => p.EBD).WithAlias(() => modelDto.EBD)
                            .Select(p => p.Remote_Vehice).WithAlias(() => modelDto.RemoteVehicle)
                            .Select(p => p.Transmission).WithAlias(() => modelDto.Tranmission)
                            .Select(p => p.Exterior_Length).WithAlias(() => modelDto.ExteriorLength)
                            .Select(p => p.Exterior_Width).WithAlias(() => modelDto.ExteriorWidth)
                            .Select(p => p.Exterior_Height).WithAlias(() => modelDto.ExteriorHeight)
                            .Select(p => p.Horsepower).WithAlias(() => modelDto.HorsePower)
                            .Select(p => p.Torque).WithAlias(() => modelDto.Torque)
                            .Select(p => p.Drag_Coeficient).WithAlias(() => modelDto.DragCoeficient)
                            .Select(p => p.TimeSpeed).WithAlias(() => modelDto.TimeSpeed)
                            .Select(p => p.Km).WithAlias(() => modelDto.Km)
                            .Select(p => p.Curb_Weight).WithAlias(() => modelDto.CurbWeight)
                            .Select(p => p.GVWR).WithAlias(() => modelDto.GVWR)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailEngineDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailEngineDto>();

                    result.Target = modelDetailEngine;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex);
            }
            return result;
        }

        public DataServiceResult<ModelDetailSafetyDto> GetModelDetailSafety(int carId)
        {
            var result = new DataServiceResult<ModelDetailSafetyDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    CarSafety safetyAlias = null;
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailSafetyDto modelDto = null;

                    var modelDetailSafety = session.QueryOver<CarSafety>(() => safetyAlias)
                        .Left.JoinAlias(() => safetyAlias.Car, () => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Airbags_frontal).WithAlias(() => modelDto.FrontAirbag)
                            .Select(x => x.Airbags_Impact).WithAlias(() => modelDto.SideAirbag)
                            .Select(x => x.Airbag_Curtain).WithAlias(() => modelDto.BackAirbag)
                            .Select(x => x.HeadLights).WithAlias(() => modelDto.HeadLights)
                            .Select(x => x.Exterior_Light_Control).WithAlias(() => modelDto.ExteriorLightControl)
                            .Select(x => x.Daytime_Running_Lights).WithAlias(() => modelDto.DayLights)
                            .Select(x => x.Led_Taillights).WithAlias(() => modelDto.LedTailLights)
                            .Select(x => x.Parking_Assist).WithAlias(() => modelDto.ParkingAssist)
                            .Select(x => x.Alarm_System).WithAlias(() => modelDto.AlarmSystem)
                            .Select(x => x.Door_Locks_System).WithAlias(() => modelDto.DoorLockSystem)
                            .Select(x => x.Rear_Child).WithAlias(() => modelDto.ChildLock)
                            .Select(x => x.Content_Theft).WithAlias(() => modelDto.AntiTheftSystem)
                            .Select(x => x.Low_Tire).WithAlias(() => modelDto.LowPressureWarning)
                            .Select(x => x.Brakes).WithAlias(() => modelDto.Brakes)
                            .Select(x => x.ABS).WithAlias(() => modelDto.ABSBrakes)
                            .Select(x => x.Brake_Assist).WithAlias(() => modelDto.BrakeAssist)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailSafetyDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailSafetyDto>();

                    result.Target = modelDetailSafety;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex);
            }
            return result;
        }

        public DataServiceResult<ModelDetailEntertainmentDto> GetModelDetailEntertainment(int carId)
        {
            var result = new DataServiceResult<ModelDetailEntertainmentDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    CarEntertainment safetyAlias = null;
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailEntertainmentDto modelDto = null;

                    var modelDetailEntertainment = session.QueryOver<CarEntertainment>(() => safetyAlias)
                        .Left.JoinAlias(() => safetyAlias.Car, () => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Radio).WithAlias(() => modelDto.Radio)
                            .Select(x => x.CD_Player).WithAlias(() => modelDto.CDPlayer)
                            .Select(x => x.DVD_Audio).WithAlias(() => modelDto.DVDAudio)
                            .Select(x => x.Voice_Recognition).WithAlias(() => modelDto.SpeechRecognition)
                            .Select(x => x.Speakers).WithAlias(() => modelDto.Speakers)
                            .Select(x => x.Amplifier).WithAlias(() => modelDto.Amplifier)
                            .Select(x => x.bluetooth_Compatibility).WithAlias(() => modelDto.BluetoothCompatibility)
                            .Select(x => x.Wifi_Compatibility).WithAlias(() => modelDto.WifiCompatibility)
                            .Select(x => x.ThreeG_Compatibility).WithAlias(() => modelDto.ThreeGCompatibility)
                            .Select(x => x.GPS_Compability).WithAlias(() => modelDto.GPSCompatibility)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailEntertainmentDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailEntertainmentDto>();

                    result.Target = modelDetailEntertainment;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex);
            }
            return result;
        }

        public DataServiceResult<ModelDetailInteriorDto> GetModelDetailInterior(int carId)
        {
            var result = new DataServiceResult<ModelDetailInteriorDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    CarInterior interiorAlias = null;
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailInteriorDto modelDto = null;

                    var modelDetailInterior = session.QueryOver<CarInterior>(() => interiorAlias)
                        .Left.JoinAlias(() => interiorAlias.Car, () => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Air_Conditioning_Front).WithAlias(() => modelDto.FrontAirConditioning)
                            .Select(x => x.Air_Conditioning_Rear).WithAlias(() => modelDto.BackAirConditioning)
                            .Select(x => x.Air_Filter).WithAlias(() => modelDto.AirFilter)
                            .Select(x => x.Seating).WithAlias(() => modelDto.Seating)
                            .Select(x => x.Front_Seat_Type).WithAlias(() => modelDto.FrontSeatType)
                            .Select(x => x.Front_Head_Restraints).WithAlias(() => modelDto.Shampoo)
                            .Select(x => x.Compass).WithAlias(() => modelDto.Compass)
                            .Select(x => x.Clock).WithAlias(() => modelDto.Clocks)
                            .Select(x => x.DC_Power_outlet).WithAlias(() => modelDto.DCPowerOutlet)
                            .Select(x => x.Tachometer).WithAlias(() => modelDto.Tachometer)
                            .Select(x => x.Water_Temp_Gauge).WithAlias(() => modelDto.WaterTempGauges)
                            .Select(x => x.Low_Fuel_Warning).WithAlias(() => modelDto.LowFuelWarning)
                            .Select(x => x.Exterior_Temperature).WithAlias(() => modelDto.DisplayOutsideTemp)
                            .Select(() => carAlias.Html_Interior).WithAlias(() => modelDto.FlashInterior)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailInteriorDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailInteriorDto>();

                    result.Target = modelDetailInterior;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex);
            }
            return result;
        }

        public DataServiceResult<ModelDetailExteriorDto> GetModelDetailExterior(int carId)
        {
            var result = new DataServiceResult<ModelDetailExteriorDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    CarExterior exteriorAlias = null;
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailExteriorDto modelDto = null;

                    var modelDetailExterior = session.QueryOver<CarExterior>(() => exteriorAlias)
                        .Left.JoinAlias(() => exteriorAlias.Car, () => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Front_Tires).WithAlias(() => modelDto.FrontTires)
                            .Select(x => x.Front_rear).WithAlias(() => modelDto.BackTires)
                            .Select(x => x.Wheels).WithAlias(() => modelDto.Wheels)
                            .Select(x => x.Sunroof).WithAlias(() => modelDto.SunRoof)
                            .Select(x => x.Rear_Window_Type).WithAlias(() => modelDto.BackWindowType)
                            .Select(x => x.DoorCount).WithAlias(() => modelDto.NumberOfDoors)
                            .Select(x => x.Mirrows).WithAlias(() => modelDto.RearviewMirror)
                            .Select(x => x.Heated_Door_Mirrows).WithAlias(() => modelDto.HeatedDoorMirror)
                            .Select(x => x.Windshied_Wipes).WithAlias(() => modelDto.WindshieldWipes)
                            .Select(x => x.System).WithAlias(() => modelDto.LiftingSystem)
                            .Select(() => carAlias.Html_Exterior).WithAlias(() => modelDto.FlashExterior)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailExteriorDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailExteriorDto>();

                    result.Target = modelDetailExterior;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex);
            }
            return result;
        }

        public DataServiceResult<ModelDetailColorDto> GetModelDetailColor(int carId)
        {
            var result = new DataServiceResult<ModelDetailColorDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailColorDto modelDto = null;

                    var modelDetailColor = session.QueryOver<Car>(() => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.Html_Color).WithAlias(() => modelDto.Color)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailColorDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailColorDto>();

                    result.Target = modelDetailColor;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex);
            }
            return result;
        }

        public DataServiceResult<ModelDetailImageDto> GetModelDetailImage(int carId)
        {
            var result = new DataServiceResult<ModelDetailImageDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    Car carAlias = null;
                    Year yearAlias = null;
                    Model modelAlias = null;
                    Style styleAlias = null;
                    Manufacturer manufactureAlias = null;
                    ModelDetailImageDto modelDto = null;

                    var modelDetailImage = session.QueryOver<Car>(() => carAlias)
                        .Left.JoinAlias(() => carAlias.Year, () => yearAlias)
                        .Left.JoinAlias(() => yearAlias.Model, () => modelAlias)
                        .Left.JoinAlias(() => modelAlias.Style, () => styleAlias)
                        .Left.JoinAlias(() => styleAlias.Manufacturer, () => manufactureAlias)
                        .Where(c => carAlias.Id == carId)
                        .SelectList(l => l
                            .Select(x => x.PathImages).WithAlias(() => modelDto.PathImages)
                            .Select(() => manufactureAlias.RewriteName).WithAlias(() => modelDto.ManufacturerName)
                            .Select(() => modelAlias.RewriteName).WithAlias(() => modelDto.ModelName)
                            .Select(() => yearAlias.Name).WithAlias(() => modelDto.YearName)
                        ).OrderBy(() => carAlias.Id).Desc
                        .TransformUsing(Transformers.AliasToBean<ModelDetailImageDto>())
                        .Take(1)
                        .SingleOrDefault<ModelDetailImageDto>();

                    result.Target = modelDetailImage;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex);
            }
            return result;
        }

        #region

        private string GetSqlQuerySelectModelReviewDetail()
        {
            return @"SELECT TOP 1
                    avatar.PathAvatar as [Image],
                    m.Name_Manufacturer as Brand,
                    mp.Name_Mode_Product as Model,
                    CONVERT(NVARCHAR(10), y.Name_Year) as Year,
                    CONCAT(m.Name_Manufacturer, ' ', CONVERT(varchar(10), y.Name_Year), 
                           ' ', mp.Name_Mode_Product) as ModelName,
                    sp.Fuel_Economy_City as FuelInCity, 
                    sp.Fuel_Economy_Highway as FuelInHighWay,
                    p.Price_Product as Price,
                    u.Name_Unit as Unit
                    FROM hdt.Manufacturer m
                    LEFT JOIN hdt.Type_Product t on m.Id_Manufacturer = t.Id_Manufacturer
                    LEFT JOIN hdt.Mode_Product mp on mp.Id_TypeProduct = t.Id_TypeProduct
                    LEFT JOIN hdt.Year y on y.Id_Mode_Product = mp.Id_Mode_Product
                    LEFT JOIN hdt.MName mn on mn.Id_Year = y.Id_Year
                    LEFT JOIN hdt.Avatar avatar on avatar.Id_MName = mn.Id_MName
                    LEFT JOIN hdt.Specifications sp on sp.Id_MName = mn.Id_MName
                    LEFT JOIN hdt.Product p on p.Id_MName = mn.Id_MName
                    LEFT JOIN hdt.Unit u on u.Id_Unit = p.Id_Unit
                    WHERE m.Id_Manufacturer = {0}
                    AND mp.Id_Mode_Product = {1}
                    AND y.Id_Year = {2}
                    ORDER BY mn.Id_MName DESC";
        }

        private string GetTotalRows(string selectQuery)
        {
            return string.Concat("SELECT COUNT(*) FROM (", selectQuery, ") AS TotalRow");
        }

        #endregion
    }
}
