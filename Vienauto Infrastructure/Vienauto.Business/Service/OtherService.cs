﻿using System;
using Vienauto.Interface.Dto;
using Vienauto.Entity.Enums;
using Vienauto.Interface.Result;
using Common.Core.Extension;
using System.Collections.Generic;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Services;

namespace Vienauto.Business.Service
{
    public class OtherService : BaseService, IOtherService
    {
        public DataServiceResult<IList<QuestionDto>> GetAllQuestions()
        {
            var result = new DataServiceResult<IList<QuestionDto>>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var questions = Session.QueryOver<Question>().OrderBy(q => q.Name_Question).Asc.List();
                    if (questions == null)
                        return new DataServiceResult<IList<QuestionDto>>
                        {
                            Errors = new List<ErrorDescriber> { new ErrorDescriber { Code = ErrorCode.FailToListAllQuestion } }
                        };

                    result.Target = questions.FromEntitiesToDtos();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.Exception, ex);
            }
            return result;
        }

        public DataServiceResult<IList<LocationDto>> GetAllLocations()
        {
            var result = new DataServiceResult<IList<LocationDto>>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var locations = Session.QueryOver<Location>().OrderBy(l => l.Name_Location).Asc.List();
                    if (locations == null)
                        return new DataServiceResult<IList<LocationDto>>
                        {
                            Errors = new List<ErrorDescriber> { new ErrorDescriber { Code = ErrorCode.FailToListLocation } }
                        };

                    result.Target = locations.FromEntitiesToDtos();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.Exception, ex);
            }
            return result;
        }

        public DataServiceResult<IList<ProvinceDto>> GetAllProvinces()
        {
            var result = new DataServiceResult<IList<ProvinceDto>>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var provinceHCMAndHN = session.QueryOver<Province>().Where(p => p.Name_TinhThanh == "TP.Hồ Chí Minh" || p.Name_TinhThanh == "TP. Hà Nội").OrderBy(p => p.Name_TinhThanh).Asc.List();
                    var provinces = session.QueryOver<Province>().Where(p => p.Name_TinhThanh != "TP.Hồ Chí Minh" && p.Name_TinhThanh != "TP. Hà Nội").OrderBy(p => p.Name_TinhThanh).Asc.List();
                    
                    if (provinces == null && provinceHCMAndHN == null)
                        return new DataServiceResult<IList<ProvinceDto>>
                        {
                            Errors = new List<ErrorDescriber> { new ErrorDescriber { Code = ErrorCode.FailToListProvince } }
                        };

                    provinceHCMAndHN.AddRange(provinces);
                    result.Target = provinceHCMAndHN.FromEntitiesToDtos();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.Exception, ex);
            }
            return result;
        }
    }
}
