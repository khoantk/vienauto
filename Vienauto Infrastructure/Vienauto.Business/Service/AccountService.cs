﻿using System;
using Vienauto.Interface.Dto;
using Vienauto.Entity.Enums;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Common.Core.Extension.Encode;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Services;

namespace Vienauto.Business.Service
{
    public class AccountService : BaseService, IAccountService
    {
        public DataServiceResult<UserDto> AuthenticateUser(string userName, string passWord)
        {
            var result = new DataServiceResult<UserDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var encodedMd5Password = EncodingExtensions.EncodeMD5(
                                                    EncodingExtensions.EncodeMD5(passWord));
                    var user = Session.QueryOver<User>()
                                      .Where(u => u.UserName == userName && u.PassWord == encodedMd5Password && u.Active == 1)
                                      .SingleOrDefault();
                    if (user == null)
                        return new DataServiceResult<UserDto>
                        {
                            Errors = new List<ErrorDescriber> { new ErrorDescriber { Code = ErrorCode.WrongUserNameOrPassword } }
                        };

                    //Update last login for user
                    user.LastLogin = DateTime.Now;
                    Session.SaveOrUpdate(user);
                    Session.Flush();

                    var userDto = user.FromEntityToDto();
                    result.Target = userDto;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.LogInFail, ex);
            }
            return result;
        }

        public DataServiceResult<RegisterDto> SignUpUser(RegisterDto registerDto)
        {
            var result = new DataServiceResult<RegisterDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var existedUser = Duplicate<User>(x => x.UserName == registerDto.UserName);
                    if (existedUser)
                        result.AddError(ErrorCode.DuplicateUser);

                    var user = new User();
                    user.UserName = registerDto.UserName;
                    user.PassWord = EncodingExtensions.EncodeMD5(registerDto.PassWord);
                    user.FullName = registerDto.FullName;
                    user.Phone = registerDto.Phone;
                    user.Mobile = registerDto.Mobile;
                    user.Active = registerDto.Active;
                    user.Avatar = registerDto.Avatar;
                    user.NgayGiaNhap = registerDto.JoinDate;
                    user.changesub = registerDto.ChangeSub;
                    user.TienChietKhau = registerDto.Discount;
                    user.Province = Get<Province>(registerDto.ProvinceId);
                    user.ToaDoMap = user.Province?.ToaDoMap2;
                    user.ZoomMap = user.Province?.ZoomMap2.ToString();
                    user.Level = Get<Level>(registerDto.LevelId);
                    user.Question = Get<Question>(registerDto.QuestionId);
                    registerDto.UserId = Create(user);
                    CommitChanges();                    
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.RegisterAgentUserFail, ex);
            }
            return result;
        }

        public DataServiceResult<RegisterDto> SignUpAgentUser(RegisterDto registerDto)
        {
            var result = new DataServiceResult<RegisterDto>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var existedUser = Duplicate<User>(x => x.UserName == registerDto.UserName);
                    if (existedUser)
                        result.AddError(ErrorCode.DuplicateUser);

                    var user = new User();
                    user.UserName = registerDto.UserName;
                    user.PassWord = EncodingExtensions.EncodeMD5(registerDto.PassWord);
                    user.FullName = registerDto.FullName;
                    user.Phone = registerDto.Phone;
                    user.Mobile = registerDto.Mobile;
                    user.Active = registerDto.Active;
                    user.Avatar = registerDto.Avatar;
                    user.NgayGiaNhap = registerDto.JoinDate;
                    user.changesub = registerDto.ChangeSub;
                    user.TienChietKhau = registerDto.Discount;
                    user.Province = Get<Province>(registerDto.ProvinceId);
                    user.ToaDoMap = user.Province?.ToaDoMap2;
                    user.ZoomMap = user.Province?.ZoomMap2.ToString();
                    user.Level = Get<Level>(registerDto.LevelId);
                    user.Question = Get<Question>(registerDto.QuestionId);
                    registerDto.UserId = Create(user);
                    CommitChanges();

                    if (registerDto.UserId <= 0)
                        result.AddError(ErrorCode.RegisterAgentFail);
                    else
                    {
                        var agent = new Agency();
                        agent.kichhoat = 1;
                        agent.filedinhkem = "";
                        agent.ngaydangki = DateTime.Now;
                        agent.Ten_CTY = registerDto.CompanyName;
                        agent.Diachi_giaodich = registerDto.TransactionAddress;
                        agent.MaSoThue = registerDto.TaxNumber;
                        agent.HoTen_Nguoidaidien = registerDto.DeputyFullName;
                        agent.Dien_thoai = registerDto.AgentPhone;
                        agent.Didong = registerDto.Mobile;
                        agent.Email_kichhoat = registerDto.EmailVerification;
                        agent.Vitri = registerDto.Location;
                        agent.Chinhanh = registerDto.TotalBranches;
                        agent.Soxe_giaodich = registerDto.NumberCarTransaction;
                        agent.Xe_phanphoi = registerDto.CarDistribution;
                        agent.CanTuVanthem = registerDto.NeedConsultMore ? "Có" : "Không";
                        agent.lamsao_cokh = registerDto.IntroduceCustomer;
                        agent.KH_cuaban = registerDto.YourCustomer;
                        agent.Banbietchungtoitudau = registerDto.HowToKnowUs;
                        agent.Banla_TV = registerDto.IsUser ? "rồi" : "chưa";
                        agent.denghi_cungcapdonhan = registerDto.CreateOrders ? "cần" : "không cần";
                        agent.User = Get<User>(registerDto.UserId);
                        CreateOrUpdate(agent);
                    }
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.RegisterAgentUserFail, ex);
            }
            return result;
        }
    }
}
