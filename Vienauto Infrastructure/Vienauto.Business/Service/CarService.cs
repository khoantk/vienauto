﻿using System;
using NHibernate;
using NHibernate.Transform;
using Vienauto.Interface.Dto;
using Vienauto.Interface.Result;
using Vienauto.Entity.Entities;
using System.Collections.Generic;
using NHibernate.SqlCommand;
using NHibernate.Criterion;
using Vienauto.Interface.Services;
using Vienauto.Entity.Enums;

namespace Vienauto.Business.Service
{
    public class CarService : BaseService, ICarService
    {
        public DataServiceResult<int> Booking(CarBookingDto carBookingDto)
        {
            var result = new DataServiceResult<int>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var carBooking = new CarBooking();
                    carBooking.Car = Get<Car>(carBookingDto.CarId);
                    carBooking.Xe = carBooking.Car?.Name;
                    carBooking.User = Get<User>(carBookingDto.UserId);
                    carBooking.Follower = Get<User>(carBookingDto.FollowerId);
                    carBooking.Location = Get<Location>(carBookingDto.LocationId);
                    carBooking.Manufacturer = Get<Manufacturer>(carBookingDto.ManufacturerId);
                    var carBookingEntity = carBookingDto.FromDtoToEntity(carBooking);
                    result.Target = Create(carBookingEntity);
                    CommitChanges();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToBookCar, ex);
            }
            return result;
        }

        public DataServiceResult<Car> GetCarById(int carId)
        {
            var result = new DataServiceResult<Car>();
            try
            {
                using (Session)
                {
                    result.Target = Get<Car>(carId);
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToGetCar, ex);
            }
            return result;
        }

        public DataServiceResult<IList<CarDto>> GetCarByYear(int yearId)
        {
            var result = new DataServiceResult<IList<CarDto>>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    CarDto carDto = null;
                    result.Target = Session.QueryOver<Car>().Where(c => c.Year.Id == yearId)
                                                            .SelectList(list => list
                                                                .SelectGroup(c => c.Id).WithAlias(() => carDto.Id)
                                                                .SelectGroup(c => c.Name).WithAlias(() => carDto.Name))
                                                            .TransformUsing(Transformers.AliasToBean<CarDto>())
                                                            .List<CarDto>();
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToListCarByYear, ex);
            }
            return result;
        }

        public DataServiceResult<string> GetAvatarByCarId(int carId)
        {
            var result = new DataServiceResult<string>();
            try
            {
                using (var session = OpenDefaultSession())
                {
                    var avatarResult = Session.CreateCriteria<Avatar>("avatar")
                                        .CreateAlias("avatar.Car", "car", JoinType.LeftOuterJoin).SetFetchMode("car", FetchMode.Join)
                                        .Add(Restrictions.Eq("car.Id", carId))
                                        .UniqueResult();

                    result.Target = (avatarResult != null ? (Avatar)avatarResult : null)?.PathAvatar;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ErrorCode.FailToGetAvatarByCar, ex);
            }
            return result;
        }
    }
}
