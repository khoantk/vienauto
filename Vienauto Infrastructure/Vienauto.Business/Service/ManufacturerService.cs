﻿using System;
using Vienauto.Entity.Enums;
using Vienauto.Interface.Result;
using System.Collections.Generic;
using Vienauto.Entity.Entities;
using Vienauto.Interface.Services;

namespace Vienauto.Business.Service
{
    public class ManufacturerService : BaseService, IManufacturerService
    {
        public DataServiceResult<IList<Manufacturer>> GetAllManufacturer()
        {
            var result = new DataServiceResult<IList<Manufacturer>>();
            try
            {
                result.Target = ListAll<Manufacturer>("asc", "Name");
            }
            catch(Exception ex)
            {
                result.AddError(ErrorCode.FailToListAll, ex);
            }
            return result;
        }
    }
}
