﻿using Castle.Windsor;
using System.Web.Mvc;
using Common.Core.Dependency;
using Common.Core.Dependency.Web;
using Castle.MicroKernel.Registration;

namespace Vienauto.DependencyInjection
{
    public class DependencyConfig
    {
        public static void Setup(IWindsorInstaller installer)
        {
            IDependencyResolver<IWindsorContainer> resolver = new WindsorDependencyResolver();
            var controllerFactory = new WindsorControllerFactory(resolver);
            resolver.Container.Install(installer);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }
    }
}
