﻿using System;
using System.Configuration;

namespace Common.Core.Extension
{
    public class ConfigExtensions<T>
    {
        private static T ConvertType(object value)
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }

        public static T GetValue(string key)
        {
            var keyType = typeof(T);
            var valueType = default(T);
            var value = ConfigurationManager.AppSettings[key];
            try
            {
                if (keyType == typeof(int))
                {
                    valueType = !string.IsNullOrEmpty(value) ? ConvertType(int.Parse(value)) : valueType;
                }
                else if (keyType == typeof(double))
                {
                    valueType = !string.IsNullOrEmpty(value) ? ConvertType(double.Parse(value)) : valueType;
                }
                else if (keyType == typeof(float))
                {
                    valueType = !string.IsNullOrEmpty(value) ? ConvertType(float.Parse(value)) : valueType;
                }
                else if (keyType == typeof(decimal))
                {
                    valueType = !string.IsNullOrEmpty(value) ? ConvertType(decimal.Parse(value)) : valueType;
                }
                else if (keyType == typeof(bool) || keyType == typeof(string))
                {
                    valueType = !string.IsNullOrEmpty(value) ? ConvertType(value) : valueType;
                }
                else if (keyType == typeof(string[]))
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        var arrayValues = value.Split(new char[','], StringSplitOptions.RemoveEmptyEntries);
                        if (arrayValues != null && arrayValues.Length > 0)
                            valueType = ConvertType(arrayValues);
                    }
                }
                return valueType;
            }
            catch (ArgumentException ex)
            {
                return default(T);
            }
        }
    }
}
