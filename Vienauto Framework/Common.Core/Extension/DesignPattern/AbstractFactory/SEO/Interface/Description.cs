﻿namespace Common.Core.Extension.DesignPattern.AbstractFactory.SEO.Interface
{
    public interface Description
    {
        string DisplayContent(string content);
    }
}
