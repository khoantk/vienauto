﻿namespace Common.Core.Extension.DesignPattern.AbstractFactory.SEO.Interface
{
    public interface Title
    {
        string DisplayHeader(string content);
    }
}
