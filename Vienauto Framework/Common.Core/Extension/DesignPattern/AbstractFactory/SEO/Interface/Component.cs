﻿namespace Common.Core.Extension.DesignPattern.AbstractFactory.SEO.Interface
{
    public interface Component
    {
        Title GetTitle();
        Description GetDescription();
    }
}
