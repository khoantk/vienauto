﻿using Microsoft.AspNet.SignalR;

namespace Common.Core.SignalR.Hubs
{
    /// <summary>
    ///  The Hub object contains method to send messages 
    ///  to a single client or broadcast to all connected clients.
    /// </summary>
    public class StationHub : Hub { }
}
